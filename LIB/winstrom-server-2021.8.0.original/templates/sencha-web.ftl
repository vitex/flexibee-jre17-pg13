<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

<#include "/i-sencha-header.ftl"/>
<script>

Ext.require([
    'Ext.ux.PreviewPlugin',
    'Ext.ux.grid.FiltersFeature',
    'Ext.ux.grid.filter.Filter',
    'Ext.ux.grid.filter.StringFilter',
    'Ext.ux.grid.filter.NumericFilter',
    'Ext.ux.grid.filter.BooleanFilter',
    'Ext.ux.grid.filter.DateFilter',
    'Ext.ux.grid.filter.ListFilter',
    'Ext.ux.grid.menu.ListMenu',
    'Ext.ux.grid.menu.RangeMenu',
    'Ext.ux.form.field.BoxSelect',
    'Ext.ux.TabReorderer',
    'Ext.ux.BoxReorderer',
    'Ext.ux.statusbar.StatusBar',
    'Ext.ux.TabCloseMenu',
    'Ext.toolbar.Paging',
    'Ext.ModelManager',
    'Ext.tip.QuickTipManager',
    <#if isProduction>
    'FlexiBee.main.All',
    'FlexiBee.adresar.All',
    </#if>
    'FlexiBee.root.MessageBus',
    'FlexiBee.root.Plugins'
],
    function() {
        Ext.require('FlexiBee.main.Application');
    }
);

Ext.onReady(function() {
    Ext.create('FlexiBee.main.Application');
});

</script>

<#include "/i-sencha-footer.ftl"/>
</#escape>
