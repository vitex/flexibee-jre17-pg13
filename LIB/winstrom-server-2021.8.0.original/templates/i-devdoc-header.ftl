<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign headerAddons="
<link type=\"text/css\" rel=\"stylesheet\" href=\"${staticPrefix}/css/shCore.css?v=${serverVersion}\" />
<link type=\"text/css\" rel=\"stylesheet\" href=\"${staticPrefix}/css/shThemeDefault.css?v=${serverVersion}\" />
<script type=\"text/javascript\" src=\"${staticPrefix}/js/sh/shCore.js?v=${serverVersion}\"></script>
<script type=\"text/javascript\" src=\"${staticPrefix}/js/sh/shBrushXml.js?v=${serverVersion}\"></script>
<script type=\"text/javascript\" src=\"${staticPrefix}/js/sh/shBrushPhp.js?v=${serverVersion}\"></script>
<script type=\"text/javascript\" src=\"${staticPrefix}/js/sh/shBrushJava.js?v=${serverVersion}\"></script>
<script type=\"text/javascript\" src=\"${staticPrefix}/js/sh/shBrushJScript.js?v=${serverVersion}\"></script>
<script type=\"text/javascript\" src=\"${staticPrefix}/js/sh/shBrushRuby.js?v=${serverVersion}\"></script>

<style type=\"text/css\">
table.parameters th code {
    white-space: nowrap;
}

table.parameters th.group {
    text-align: center;
    font-style: italic;
    font-size: small;
}

pre.shell {
    overflow-x: auto;
    background: white;
    border: 1px solid lightgray;
    border-left: 3px solid #6CE26C;
    padding: .5em;
}

a.noteref {
    vertical-align: super;
    font-size: 80%;
}
ol.notes > li:first-child {
    border-top: 1px solid black;
    padding-top: 1ex;
}
ol.notes > li {
    counter-increment: list;
    list-style: none;
    position: relative;
    font-size: 80%;
}
ol.notes > li:before {
    font-weight: bold;
    content: counter(list) \")\";
    position: absolute;
    left: -1.55em;
}
</style>
"/>

<#include "/header.ftl" />

<div class="flexibee-article-view">

</#escape>