<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#-- === ZAÚČTOVÁNÍ ============================= -->

<div class="flexibee-blok flexibee-evidence-zauctovani flexibee-blok-left flexibee-more">
<#-- ignorované položky -->
${marker.mark('rada')}
${marker.mark('polozkaRady')}
${marker.mark('ucetni')}
${marker.mark('zuctovano')}
<#-- KONEC: ignorované položky -->

<table>
    <col width="40%"/>
    <tr class="flexibee-small"><th><@tool.label items=items name='typDokl' />:</th>
    <td><@tool.place marker=marker object=object name='typDokl' items=items /></td>
    </tr>

    <#if items['stredisko']?exists && object['stredisko']?exists>
    <tr class="flexibee-small"><th><@tool.label items=items name='stredisko' />:</th>
    <td><@tool.place marker=marker object=object name='stredisko' items=items /></td>
    </tr>
    </#if>


    <#if items['zakazka']?exists && object['zakazka']?exists>
    <tr class="flexibee-small"><th><@tool.label items=items name='zakazka' />:</th>
    <td><@tool.place marker=marker object=object name='zakazka' items=items /></td>
    </tr>
    </#if>

    <#if items['typUcOp']?exists && object['typUcOp']?exists>
    <tr class="flexibee-small"><th><@tool.label items=items name='typUcOp' />:</th>
    <td><@tool.place marker=marker object=object name='typUcOp' items=items /></td>
    </tr>
    </#if>
</table>

<div class="flexibee-hidden">
<table>
    <col width="40%"/>
    <#if object['duzpPuv']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='duzpPuv' />:</th>
        <td><@tool.place marker=marker object=object name='duzpPuv' items=items /></td>
    </tr>
    </#if>
    ${marker.mark('duzpUcto')}
    <#if object['duzpUcto']?exists && asDate(object['duzpUcto'])?date != asDate(object['duzpPuv'])?date >
    <tr class="flexibee-small">
    <th><@tool.label items=items name='duzpUcto' />:</th>
        <td><@tool.place marker=marker object=object name='duzpUcto' items=items /></td>
    </tr>
    </#if>
    <#if object['datUcto']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='datUcto' />:</th>
        <td><@tool.place marker=marker object=object name='datUcto' items=items /></td>
    </tr>
    </#if>
    <#if object['danEvidK']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='danEvidK' />:</th>
        <td><@tool.place marker=marker object=object name='danEvidK' items=items /></td>
    </tr>
    </#if>
    <#if object['primUcet']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='primUcet' />:</th>
        <td><@tool.place marker=marker object=object name='primUcet' items=items /></td>
    </tr>
    </#if>
    <#if object['protiUcet']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='protiUcet' />:</th>
        <td><@tool.place marker=marker object=object name='protiUcet' items=items /></td>
    </tr>
    </#if>
    <#if object['dphSnizUcet']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='dphSnizUcet' />:</th>
        <td><@tool.place marker=marker object=object name='dphSnizUcet' items=items /></td>
    </tr>
    </#if>
    <#if object['dphZaklUcet']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='dphZaklUcet' />:</th>
        <td><@tool.place marker=marker object=object name='dphZaklUcet' items=items /></td>
    </tr>
    </#if>
    <#if object['clenDph']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='clenDph' />:</th>
        <td><@tool.place marker=marker object=object name='clenDph' items=items /></td>
    </tr>
    </#if>
    <#if object['statDph']?exists>
    <tr class="flexibee-small">
    <th><@tool.label items=items name='statDph' />:</th>
        <td><@tool.place marker=marker object=object name='statDph' items=items /></td>
    </tr>
    </#if>
</table>
</div>

</div>

</#escape>