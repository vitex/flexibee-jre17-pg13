<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#-- === ČÁSTKY ============================= -->
<div class="flexibee-blok flexibee-evidence-castky flexibee-blok-right flexibee-more">

<#-- ignorované položky -->
${marker.mark('sazbaDphOsv')}
${marker.mark('sazbaDphSniz')}
${marker.mark('sazbaDphZakl')}
${marker.mark('sumOsvMen')}
${marker.mark('dphSnizKoef')}
${marker.mark('dphZaklKoef')}
${marker.mark('zaokrJakSumK')}
${marker.mark('zaokrNaSumK')}
${marker.mark('zaokrJakDphK')}
${marker.mark('zaokrNaDphK')}
<#-- KONEC: ignorované položky -->

${marker.mark('mena')}
<#if tool.hasValue('sumCelkemMen', object, items, marker) && object['sumCelkemMen'] != 0>
    <big><big><big><strong><@tool.place marker=marker object=object name='sumCelkemMen' items=items /></strong>&nbsp;${object['mena'].symbol}</big> s&nbsp;DPH</big></big><br/>
    <strong><@tool.place marker=marker object=object name='sumZklCelkemMen' items=items /></strong>&nbsp;${object['mena'].symbol} bez&nbsp;DPH
<#else>
    <big><big><big><strong><@tool.place marker=marker object=object name='sumCelkem' items=items /></strong>&nbsp;${object['mena'].symbol}</big> s&nbsp;DPH</big></big><br/>
    <strong><@tool.place marker=marker object=object name='sumZklCelkem' items=items /></strong>&nbsp;${object['mena'].symbol} bez&nbsp;DPH
</#if>

<#if tool.hasValue('slevaDokl', object, items, marker) && object['slevaDokl'] != 0>
    <br/><br/><big><@tool.label items=items name='slevaDokl' />: <strong><@tool.place marker=marker object=object name='slevaDokl' items=items /></strong>&nbsp;%</big>
</#if>

<div class="flexibee-hidden">
<table class="flexibee-small flexibee-rekapitulace-dph">
    <col width="15%"/>
    <col width="25%"/>
    <col width="25%"/>
    <col width="25%"/>
    <tr>
    <td class="flexibee-r flexibee-sepr">0%</td>
    <td class="flexibee-r flexibee-sepr"><@tool.place marker=marker object=object name='sumOsv' items=items /></td>
    <td class="flexibee-r flexibee-sepr2"></td>
    <td class="flexibee-r"></td>
    </tr>
    <tr class="flexibee-sep">
    <td class="flexibee-r flexibee-sepr"><@tool.place marker=marker object=object name='szbDphSniz' items=items />%</td>
    <td class="flexibee-r flexibee-sepr"><@tool.place marker=marker object=object name='sumZklSniz' items=items /></td>
    <td class="flexibee-r flexibee-sepr2"><@tool.place marker=marker object=object name='sumDphSniz' items=items /></td>
    <td class="flexibee-r" ><@tool.place marker=marker object=object name='sumCelkSniz' items=items /></td>
    </tr>
    <tr class="flexibee-sep">
    <td class="flexibee-r flexibee-sepr"><@tool.place marker=marker object=object name='szbDphZakl' items=items />%</td>
    <td class="flexibee-r flexibee-sepr"><@tool.place marker=marker object=object name='sumZklZakl' items=items /></td>
    <td class="flexibee-r flexibee-sepr2"><@tool.place marker=marker object=object name='sumDphZakl' items=items /></td>
    <td class="flexibee-r"><@tool.place marker=marker object=object name='sumCelkZakl' items=items /></td>
    </tr>
    <tr class="flexibee-sep2">
    <td class="flexibee-r flexibee-sepr">Celkem</td>
    <td class="flexibee-r flexibee-sepr"><strong><@tool.place marker=marker object=object name='sumZklCelkem' items=items /></strong></td>
    <td class="flexibee-r flexibee-sepr2"><strong><@tool.place marker=marker object=object name='sumDphCelkem' items=items /></strong></td>
    <td class="flexibee-r"><strong><@tool.place marker=marker object=object name='sumCelkem' items=items /></strong></td>
    </tr>
</table>

<table class="flexibee-small">
    <col width="40%"/>
    <#if object['kurz']?exists && object['kurz'] != 0>
    <tr>
    <th><@tool.label items=items name='kurz' />:</th>
    <td><@tool.place marker=marker object=object name='kurz' items=items /> = <@tool.place marker=marker object=object name='kurzMnozstvi' items=items /> ${object['mena'].symbol}</td>
    </tr>
    </#if>

</table>
</div>
</div>

</#escape>