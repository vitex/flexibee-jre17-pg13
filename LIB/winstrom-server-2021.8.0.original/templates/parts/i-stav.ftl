<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#-- === STAV ============================= -->
<div class="flexibee-blok flexibee-evidence-stavu flexibee-blok-left flexibee-more">
    <table>
    <col width="40%"/>
    ${marker.mark('storno')}
    <#if object['storno'] == true>
    <tr><th colspan="2" class="verybig red">${lang('messages', 'doklFak.cancelled', 'STORNOVÁNO')}</th><tr>
    </#if>

    ${marker.mark('popis')}
    <#if object['popis']?exists>
        <tr><th><@tool.label items=items name='popis' />:</th><td><strong><@tool.place marker=marker object=object name='popis' items=items /></strong></td></tr>
    </#if>

    ${marker.mark('cisDosle')}
    <#if object['cisDosle']?exists>
        <tr><th><@tool.label items=items name='cisDosle' />:</th><td><@tool.place marker=marker object=object name='cisDosle' items=items /></td></tr>
    </#if>


    ${marker.mark('stavUhrK')}
    ${marker.mark('datUhr')}
    ${marker.mark('datSplat')}
    ${marker.mark('juhSum')}
    <#if !object['stavUhrK']?exists || object['stavUhrK'] != 'stavUhr.uhrazeno'>
    <#if object['datSplat']?exists>
        <#if asDate(object['datSplat'])?date &lt; now?date>
            <tr class="red big"><th><strong>${lang('messages', 'filterStavUhr.neuhrazPoSplatnosti', 'Po splatnosti')}:</strong></th><td><strong><@tool.place marker=marker object=object name='datSplat' items=items /></strong></td></tr>
        <#else>
                <tr><th><@tool.label items=items name='datSplat' /></th><td><@tool.place marker=marker object=object name='datSplat' items=items /></td></tr>
        </#if>
    </#if>

        <#if object['juhSum']?exists && object['juhSum'] != 0>
    <tr class="flexibee-green">
        <th><@tool.label items=items name='juhSum' />:</th>
            <td><@tool.place marker=marker object=object name='juhSum' items=items /></td>
    </tr>
    </#if>

    <#else>
        <tr class="flexibee-green"><th><strong><@tool.label items=items name='datUhr' />:</strong></th><td><@tool.place marker=marker object=object name='datUhr' items=items /></td></tr>
    </#if>


    <#if tool.hasValue('datVyst', object, items, marker)>
    <tr class="flexibee-small"><th><@tool.label items=items name='datVyst' />:</th><td><@tool.place marker=marker object=object name='datVyst' items=items /></td></tr>
    </#if>

    <#if tool.hasValue('stitky', object, items, marker)>
    <tr class="flexibee-small"><th><@tool.label items=items name='stitky' />:</th><td><@tool.place marker=marker object=object name='stitky' items=items /></td></tr>
    </#if>

    <#if tool.hasValue('stavUzivK', object, items, marker)>
    <tr class="flexibee-small"><th><@tool.label items=items name='stavUzivK' />:</th><td><@tool.place marker=marker object=object name='stavUzivK' items=items /></td></tr>
    </#if>

    <#if tool.hasValue('datTermin', object, items, marker)>
    <tr class="flexibee-small"><th><@tool.label items=items name='datTermin' />:</th><td><@tool.place marker=marker object=object name='datTermin' items=items /></td></tr>
    </#if>


    <#if tool.hasValue('datReal', object, items, marker)>
    <tr class="flexibee-small"><th><@tool.label items=items name='datReal' />:</th><td><@tool.place marker=marker object=object name='datReal' items=items /></td></tr>
    </#if>


    <#if tool.hasValue('uvodTxt', object, items, marker)>
        <tr class="flexibee-small"><th><@tool.label items=items name='uvodTxt' />:</th><td><strong><@tool.place marker=marker object=object name='uvodTxt' items=items /></strong></td></tr>
    </#if>

    <#if tool.hasValue('zavTxt', object, items, marker)>
        <tr class="flexibee-small"><th><@tool.label items=items name='zavTxt' />:</th><td><strong><@tool.place marker=marker object=object name='zavTxt' items=items /></strong></td></tr>
    </#if>


    <#if tool.hasValue('poznam', object, items, marker)>
        <tr class="flexibee-small flexibee-grey"><th><@tool.label items=items name='poznam' />:</th><td><strong><@tool.place marker=marker object=object name='poznam' items=items /></strong></td></tr>
    </#if>

    </table>


    <div class="flexibee-hidden">
    <table>
        <#if object['cisDodak']?exists>
        <col width="40%"/>
        <tr class="flexibee-small"><th><@tool.label items=items name='cisDodak' />:</th><td><@tool.place marker=marker object=object name='cisDodak' items=items /></td></tr>
        </#if>
        <col width="40%"/>
        <tr class="flexibee-small"><th><@tool.label items=items name='uzivatel' />:</th><td><@tool.place marker=marker object=object name='uzivatel' items=items /></td></tr>
    </table>
    </div>
</div>

</#escape>