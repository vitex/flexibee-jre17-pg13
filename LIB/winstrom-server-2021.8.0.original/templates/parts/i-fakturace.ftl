<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#-- === FAKTURACE ============================= -->
<div class="flexibee-blok flexibee-evidence-fakturace flexibee-blok-left">
<#-- ignorované položky -->
${marker.mark('podpisPrik')}
<#-- KONEC: ignorované položky -->

<table>
    <col width="40%"/>

    <#if tool.hasValue('formaUhrK', object, items, marker)>
    <tr>
    <th><@tool.label items=items name='formaUhrK' />:</th>
    <td><@tool.place marker=marker object=object name='formaUhrK' items=items /></td>
    </tr>
    </#if>
    <#if tool.hasValue('typDoklBan', object, items, marker)>
    <tr>
    <th><@tool.label items=items name='typDoklBan' />:</th>
    <td><@tool.place marker=marker object=object name='typDoklBan' items=items /></td>
    </tr>
    </#if>
    <#if tool.hasValue('varSym', object, items, marker)>
    <tr>
    <th><@tool.label items=items name='varSym' />:</th>
    <td><@tool.place marker=marker object=object name='varSym' items=items /></td>
    </tr>
    </#if>
    <#if tool.hasValue('konSym', object, items, marker)>
    <tr class="small">
    <th><@tool.label items=items name='konSym' />:</th>
    <td><@tool.place marker=marker object=object name='konSym' items=items /></td>
    </tr>
    </#if>

</table>

</div>

</#escape>