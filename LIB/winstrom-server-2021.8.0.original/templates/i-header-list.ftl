<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-common.ftl" />

<#assign title=lang('dialogs', it.name, it.name)/>

<#include "/header-html-start.ftl"/>
<@tool.showTitle title=title finish=false>
</@tool.showTitle>
<@tool.showTitleFinish title=title/>
<#include "/i-toolbar.ftl"/>

<#import "/l-tools.ftl" as tool />


</#escape>
