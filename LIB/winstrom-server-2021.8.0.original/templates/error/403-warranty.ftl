<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Přístup odepřen"/>
<#include "/error/header.ftl" />

<p>Pokoušíte se aktualizovat na novější verzi systému ABRA Flexi než Vás opravňuje zaplacená služba Roční podpora. Vraťte původní verzi a nebo kontaktujte dodavatele systému s žádostí o prodloužení uvedené služby.</p>

<#include "/error/footer.ftl" />

</#escape>
