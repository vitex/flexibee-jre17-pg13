<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=dlgText("error.503-service_unavailable", "503 - Service unavailable")/>
<#include "/error/header.ftl" />

    <div class="flexibee-note-error">${it.origException.localizedMessage}</div>

    <#include "/error/footer.ftl" />
</#escape>