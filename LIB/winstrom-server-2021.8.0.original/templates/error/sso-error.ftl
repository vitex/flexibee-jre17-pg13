<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="SSO chyba"/>
<#include "/error/header.ftl" />

<p>Chyba Single Sign-On služby: <strong>${message}</strong>.</p>

<#include "/error/footer.ftl" />
</#escape>