<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign title=dlgText("error.could_not_complete_operation.title")/>
<#include "/error/header.ftl" />

    <div class="flexibee-note-error">${it.message}</div>

<#include "/error/footer.ftl" />
</#escape>