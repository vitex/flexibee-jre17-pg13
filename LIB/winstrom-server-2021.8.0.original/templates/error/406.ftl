<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Nepodporovaný formát"/>
<#include "/error/header.ftl" />

<p>Chyba 406</p>

    <div class="flexibee-note-error">${it.message}</div>
</div>

<#include "/error/footer.ftl" />
</#escape>