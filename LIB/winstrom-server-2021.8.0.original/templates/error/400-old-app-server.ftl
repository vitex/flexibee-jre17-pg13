<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign title="Příliš stará verze ABRA Flexi"/>
<#include "/error/header.ftl" />

    <div class="flexibee-note-error">Verze databáze firmy ${it.origException.companyId} je novější než verze ABRA Flexi Serveru.</div>
</div>

<#include "/error/footer.ftl" />
</#escape>
