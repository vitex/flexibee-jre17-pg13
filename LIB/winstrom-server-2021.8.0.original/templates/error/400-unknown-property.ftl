<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign title="Neznámá vlastnost"/>
<#include "/error/header.ftl" />

<div class="flexibee-note-error">Vlastnost '${it.origException.propertyName}' neexistuje.</div>

<#include "/error/footer.ftl" />
</#escape>
