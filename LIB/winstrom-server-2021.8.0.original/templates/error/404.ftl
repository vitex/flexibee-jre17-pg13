<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Stránka nenalezena (404)"/>
<#include "/error/header.ftl" />


    <div class="flexibee-note-error">${it.message}</div>

<#include "/error/footer.ftl" />
</#escape>