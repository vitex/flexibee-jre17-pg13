<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#include "/header-html-start.ftl" />

<@tool.showTitle title=title showPath=false>
</@tool.showTitle>
<@tool.showTitleFinish />

<div class="flexibee-error-page">

    <div class="flexibee-error-page-box">

    <h3><#if title2?exists>${title2}<#else>${title}</#if></h3>

</#escape>
