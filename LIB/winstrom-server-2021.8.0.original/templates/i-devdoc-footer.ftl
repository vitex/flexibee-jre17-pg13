<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<ul>
    <li><a href="/devdoc">${lang('dialogs', 'devdoc.developers', 'Vývojářská dokumentace')}</a></li>
</ul>

</div>

<script type="text/javascript">SyntaxHighlighter.all();</script>

<#include "/footer.ftl" />

</#escape>