<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.Evidences', {
    singleton: true,
    alternateClassName: 'FlexiBee.Evidences',

    items: [
    <#list it.getEvidenceList().evidenceDescriptors as e>
        {
            id: '${e.senchaName}',
            name: '${e.evidenceName}',
            tag: '${e.tagName}'
        }<#if e_has_next>,</#if>
    </#list>
    ],

    itemsById: {},

    constructor: function() {
        var me = this;
        Ext.iterate(me.items, function(item) {
            me.itemsById[item.id] = item;
        });
    }
});

</#escape>
