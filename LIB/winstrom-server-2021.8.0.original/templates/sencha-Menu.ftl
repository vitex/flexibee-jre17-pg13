<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.Menu', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.fbMainMenu',

    requires: [
        'FlexiBee.main.MenuStructure'
    ],

    bodyBorder: false,
    border: 0,

    layout: 'accordion',
    margin: '0',

    initComponent: function() {
        var menuStructure = Ext.create('FlexiBee.main.MenuStructure');
        FlexiBee.Plugins.invoke(this, 'modifyMainMenu', menuStructure.items);
        this.items = menuStructure.asMenuPanelItems();

        this.callParent(arguments);
    }
});

<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="main.Menu"/>

</#escape>
