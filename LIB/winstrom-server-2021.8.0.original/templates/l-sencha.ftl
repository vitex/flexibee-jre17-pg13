<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#macro includePlugins path>
<#list override(path?replace(".", "/"), '*.js') as o>
<#noescape>${o.content}</#noescape>

FlexiBee.Plugins.register('FlexiBee.${path}', Ext.create('${o.className}'));
</#list>
</#macro>

</#escape>
