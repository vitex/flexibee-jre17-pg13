<#ftl encoding="utf-8" />
<#-- @ftlvariable name="it" type="cz.winstrom.net.server.action.RestActionHelper" -->
<#escape x as x?default("")?html>
<#if !isPlain && hideToolbar == false>
<#if isMobile == false || it.evidenceResource?? && it.evidenceResource.allowToolbar>

<#macro showToolbarItem item>
    <#if item.canShow(it.object, it.isCreate, it.isEdit, isMobile)>
        <#assign targetBlank = item.actionId?starts_with("pdf") || (item.actionId?starts_with("cstBtn") && !item.url?starts_with("javascript:")) />

            <#if item.url?has_content>
                <a href="<#if item.actionId != 'pdf'>${item.url}</#if>" data-actionId="${item.actionId}" <#if (item.actionId == "new" || item.actionId == "edit" || item.actionId?starts_with("pdf")) && isMobile == true>rel="external"</#if> <#if targetBlank>target="_blank"</#if> class="flexibee-toolbar-link
            <#else>
                <span class="flexibee-toolbar-link
            </#if>

            btn btn-default flexibee-toolbar-item-${item.actionId} <#if item.download>flexibee-toolbar-item-download</#if> flexibee-clickable <#if item.extraCssClass??>${item.extraCssClass}</#if> <#if item.javascriptOnly>flexibee-toolbar-item-javascript-only</#if>"
            <#if item.actionId == "pdf">data-toggle="modal" data-target="#flexibee-reports-dialog"</#if>
            >

            <i class="icon icon-${item.actionId}"></i>
            ${item.actionName}
            <#if item.url?has_content>
                </a>
            <#else>
                </span>
            </#if>

        <#if item.actionId == "pdf"><#assign pdfAction = item /></#if>
    </#if>
</#macro>

<#if it.evidenceResource?? && it.evidenceResource.allowToolbar>
<div class="btn-toolbar hidden-print" role="toolbar">
<!--FLEXIBEE:TOOLBAR:START-->
    <#assign toolbars = it.evidenceResource.getToolBars(it.object) />
    <#assign separateServices = toolbars.shouldShowServicesSeparate(it.object, it.isCreate, it.isEdit, isMobile) />
    <#if separateServices>
        <#assign list = toolbars.getBasic(isMobile) />
    <#else>
        <#assign list = toolbars.list />
    </#if>

    <div class="btn-group">
    <#list list as a>
        <@showToolbarItem item=a />
    </#list>

    <#if separateServices?? && separateServices>
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" id="dropdownService1">
            <span class="glyphicon glyphicon-cog"></span> ${lang('actions', 'sluzby', 'Služby')}
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownService1">
            <#list toolbars.getServices(isMobile) as a>
                <li role="presentation"><@showToolbarItem item=a /></li>
            </#list>
        </ul>
    </#if>

    </div>

<#if pdfAction??>
    <div class="modal fade" id="flexibee-reports-dialog" tabindex="-1" role="dialog" aria-labelledby="myPDFLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
            <ul>
            <#list it.evidenceResource.reports.list as report>
                <li><a href="${it.evidenceResource.getUrlForReport(report, pdfAction)}" <#if isMobile == true>rel="external"</#if> class="flexibee-report" target="_blank">${report.reportName}</a></li>
            </#list>
            </ul>
          </div>
        </div>
      </div>
    </div>
</#if>

<script>
$(function() {
    $('.flexibee-toolbar-link').click(function(e) {
        var self = this;

        if ($(self).hasClass('flexibee-toolbar-item-javascript-only') ||
            $(self).hasClass('flexibee-toolbar-item-download') ||
            $(self).attr('data-actionId') == "edit" ||
            $(self).attr('data-actionId') == "copy" ||
            $(self).attr('data-actionId') == "new"  ||
            $(self).attr('data-actionId').startsWith("cstBtn")
            ) {
            return;
        }

        var url = self.href;
        if (!url || url == '') {
            return;
        }

        $.showWait();

        var str = '<div class="modal fade flexibee-main-modal" tabindex="-1" role="dialog" aria-hidden="true" data-show="true">';
           str += '  <div class="modal-dialog">';
           str += '    <div class="modal-content">';
           str += '    </div><!-- /.modal-content -->';
           str += '  </div><!-- /.modal-dialog -->';
           str += '</div>';

           if (url.indexOf('?') !== -1) {
               url += "&isModal=true";
           } else {
               url += "?isModal=true";
           }

           var modal = $(str);
           modal.appendTo($('body'));
           modal.modal({
               remote: url,
               show: true
           });
           modal.on('hidden.bs.modal', function (e) {
              $(this).remove();
           });
           modal.bind('shown.bs.modal', function (e) {
               $.hideWait();
           });

        e.preventDefault();
    });
 });
 </script>

    <!--FLEXIBEE:TOOLBAR:CENTER-->

    <!--FLEXIBEE:TOOLBAR:END-->
    </div> <!-- btn-toolbar -->
    </#if>
</#if>
</#if>

</#escape>
