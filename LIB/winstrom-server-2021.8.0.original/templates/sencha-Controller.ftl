<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.Controller', {
    extend: 'Ext.app.Controller',

    requires: [
        'Ext.util.Cookies'
    ],

    init: function() {
        this.control({
            'fbMainMenu menuitem': {
                click: this.onMainMenuItemSelected
            },
            'fbMainToolbar button[itemId="openCurrentUser"]': {
                click: this.onCurrentUserClick
            }
        });

        FlexiBee.MessageBus.on('status', this.onStatusChange);
    },

    onStatusChange: function(event) {
        var statusbar = Ext.getCmp('mainstatusbar');
        if (event.busy === true) {
            statusbar.showBusy();
        } else if (event.busy === false) {
            statusbar.clearStatus();
        } else {
            statusbar.setStatus({
                iconCls: event.icon || '',
                text: event.text || ''
            });
        }
    },

    onCurrentUserClick: function(item) {
        var me = this;

        FlexiBee.MessageBus.publish('status', { busy: true });

        var id = 'code:${loggedUser.name}';
        Ext.require('FlexiBee.uzivatel.<#if isProduction>All<#else>Model</#if>', function() {
            FlexiBee.uzivatel.Model.load(id, {
                success: function(record) {
                    me.application.newController('FlexiBee.uzivatel.DetailController', {
                        title: '${loggedUser.name}',
                        model: record
                    });

                    FlexiBee.MessageBus.publish('status', { busy: false });
                }
            });
        });
    },

    onMainMenuItemSelected: function(item) {
        var me = this;

        if (!item.handler) {
            return;
        }

        var tab, tabs;
        switch (item.handler.type) {
            case 'evidence':
                FlexiBee.MessageBus.publish('status', { busy: true });
                Ext.require('FlexiBee.' + item.handler.id + '.<#if isProduction>All<#else>ListController</#if>', function() {
                    me.application.newController('FlexiBee.' + item.handler.id + '.ListController', {
                        title: item.handler.name
                    });

                    FlexiBee.MessageBus.publish('status', { busy: false });
                });
                break;
            case 'iframe':
                tab = Ext.create('Ext.panel.Panel', {
                    closable: true,
                    layout: 'fit',
                    title: item.handler.name,
                    items: [
                        {
                            xtype: 'component',
                            mode: 'iframe',
                            autoEl: {
                                tag: "iframe",
                                src: item.handler.link
                            }
                        }
                    ]
                });

                tabs = Ext.getCmp('content');
                tabs.add(tab);
                tabs.setActiveTab(tab);
                break;
            case 'backward':
                tab = Ext.create('FlexiBee.main.BackwardWindow', {
                    closable: true,
                    title: item.handler.name,
                    url: item.handler.link
                });

                tabs = Ext.getCmp('content');
                tabs.add(tab);
                tabs.setActiveTab(tab);
                break;
            case 'mvc':
                me.application.newController(item.handler.controller, {
                    title: item.handler.name
                });
                break;
            case 'custom':
                item.handler.action.call(this);
                break;
            default:
                Ext.log("Unknown handler type '" + item.handler.type + "' in menu");
        }
    }
});

</#escape>
