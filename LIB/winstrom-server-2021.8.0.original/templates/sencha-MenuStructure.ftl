<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

<#macro putMenu root>
<#list root.subMenus as item>
    <#if item.getMenu(menuTool)?? && item.getMenu(menuTool).isEnabled>
    {
        id: '${item.getMenu(menuTool).id}',
        name: '${item.getMenu(menuTool).name}',
        <#if !item.getMenu(menuTool).holder?? || !item.getMenu(menuTool).holder.evidenceType??>
        items: [
            <@putMenu root=item.rootMenu />
        ]
        <#else>
        <#if item.getMenu(menuTool).holder?? && item.getMenu(menuTool).holder.isStandardEvidence?? && item.getMenu(menuTool).holder.evidenceType??>
        handler: {
            type: 'evidence',
            id: '${item.getMenu(menuTool).holder.evidenceType.path?replace("-", "_")}',
            name: '${item.getMenu(menuTool).name}'
        }
        <#else>
        handler: {
            type: 'backward',
            name: '${item.getMenu(menuTool).name}',
            link: '/c/${it.companyId}${item.getMenu(menuTool).link}'
        }
        </#if>
        </#if>
    },
    </#if>
</#list>
</#macro>

<#-- TODO singleton!-->
Ext.define('FlexiBee.main.MenuStructure', {
    items: [
        <@putMenu root=menuTool().root/>
    ],

    convertToPanelItems: function(items, level) {
        var me = this;

        var panelItems = [];
        Ext.iterate(items, function(item) {
            var panelItem;
            if (level === 0) {
                panelItem = {
                    xtype: 'panel',
                    title: item.name,
                    items: [
                        {
                            xtype: 'menu',
                            border: 0,
                            stretch: true,
                            width: '100%',
                            floating: false
                        }
                    ]
                };

                if (item.items) {
                    panelItem.items[0].items = me.convertToPanelItems(item.items, level + 1);
                }
            } else {
                panelItem = {
                    xtype: 'menuitem',
                    text: item.name,
                    handler: item.handler
                };

                if (item.items) {
                    panelItem.menu = {
                        items: me.convertToPanelItems(item.items, level + 1)
                    };
                }

                if (level === 1) {
                    Ext.apply(panelItem, {
                        border: 0,
                        stretch: true,
                        floating: false
                    });
                }
            }

            panelItems.push(panelItem);
        });

        return panelItems;
    },

    asMenuPanelItems: function() {
        if (this.convertedToMenuPanelItems) {
            return this.convertedToMenuPanelItems;
        }

        var menuPanelItems = this.convertToPanelItems(this.items, 0);

        this.convertedToMenuPanelItems = menuPanelItems;
        return menuPanelItems;
    }
});

</#escape>
