<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign m = menuTool().root/>
<!--FLEXIBEE:MENU:START-->
<#if companyId??>
    <div class="flexibee-aplication-menu column sidebar-offcanvas <#if useSmallMenu == false>hidden-xs</#if>" id="sidebar">
        <div class="<#if useSmallMenu == true>hide</#if>" id="lg-menu">
        <ul class="nav">
        <#list m.subMenus as e><#if e.getMenu(menuTool).isEnabled>
                <li>
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-${e.getMenu(menuTool).id}">
                        <span class="${e.getMenu(menuTool).image}"></span>
                            ${e.getMenu(menuTool).name}
                    </a>
                    <ul id="collapse-${e.getMenu(menuTool).id}" class="nav panel-collapse collapse">
                            <#list e.rootMenu.subMenus as sub>
                                <#if sub.getMenu(menuTool)?? && sub.getMenu(menuTool).isEnabled>
                                    <li><a href="/c/${it.companyId}${sub.getMenu(menuTool).link}">${sub.getMenu(menuTool).name}</a></li>
                                </#if>
                            </#list>
                    </ul>
            </li>
        </#if></#list>
        <#if deviceType == 'web'>
            <li class="flexibee-application-menu-noseparate"><a href="/c/${it.companyId}?menu=nastroje"><span class="icon icon-nastaveni-large"></span> ${lang('formsInfo', 'nastav000', 'Nástroje')}</a></li>
        </#if>
        </ul>
        </div>

        <!-- tiny only nav-->
      <ul class="nav <#if useSmallMenu == false>hide</#if>" id="xs-menu">
          <#list m.subMenus as e><#if e.getMenu(menuTool).isEnabled>
                <li><a href="/c/${it.companyId}${e.getMenu(menuTool).link}">
                      <span class="${e.getMenu(menuTool).image}"></span></a></li>
            </#if>
        </#list>
        <#if deviceType == 'web'>
            <li class="flexibee-application-menu-noseparate"><a href="/c/${it.companyId}?menu=nastroje"><i class="glyphicon glyphicon-cog"></i></a></li>
        </#if>
        </ul>

    </div>
</#if>

</#escape>
