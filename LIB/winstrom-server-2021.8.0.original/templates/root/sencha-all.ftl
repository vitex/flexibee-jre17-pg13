<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>


<#list [
"root/sencha-MessageBus",
"root/sencha-Plugins",
"root/sencha-StatusBar",
"root/sencha-Toolbar",
"root/sencha-BaseForm",
"root/sencha-ReloadButton",
"root/sencha-GroupsModel",
"root/sencha-GroupsStore",
"root/sencha-GroupsGrid",
"root/sencha-CompaniesModel",
"root/sencha-CompaniesStore",
"root/sencha-CompaniesGrid",
"root/sencha-BrotherModel",
"root/sencha-BrotherStore",
"root/sencha-BrotherGrid",
"root/sencha-UsersModel",
"root/sencha-UsersStore",
"root/sencha-UsersGrid",
"root/sencha-SessionsModel",
"root/sencha-SessionsStore",
"root/sencha-SessionsGrid",
"root/sencha-AccessModel",
"root/sencha-UsersAccessModel",
"root/sencha-UsersAccessStore",
"root/sencha-UsersAccessGrid",
"root/sencha-AuthPanel",
"root/sencha-MailPanel",
"root/sencha-CertPanel",
"root/sencha-LicensePanel",
"root/sencha-ServerParams",
"root/sencha-SettingsPanel",
"root/sencha-CompaniesPanel",
"root/data/sencha-ComboSuggestStore",
"root/grid/sencha-BoxSelectColumn"
] as i>
<#include resolveTemplate("/" + i) />
</#list>

Ext.define('FlexiBee.root.All', {
});

</#escape>
