<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-sencha-header.ftl"/>

<script>

Ext.require([
    'Ext.tip.QuickTipManager',
    <#if isProduction>
    'FlexiBee.root.All'
    <#else>
    'FlexiBee.root.AdminWindow'
    </#if>
]);

Ext.onReady(function() {

    Ext.QuickTips.init();

    var win = Ext.create('FlexiBee.root.AdminWindow', {
    });
    win.show();

});

</script>

<#include "/i-sencha-footer.ftl"/>
</#escape>
