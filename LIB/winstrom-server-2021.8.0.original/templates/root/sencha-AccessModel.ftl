<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.AccessModel', {
    extend: 'Ext.data.Model',

    idProperty: 'companyId',

    fields: [
        <#--
        {
            name: 'admin',
            type: 'boolean'
        },
        {
            name: 'connectionType',
            type: 'string'
        },
        {
            name: 'dbNazev',
            type: 'string'
        }
        -->
        {
            name: 'companyId',
            type: 'string'
        }
    ],

    associations: [
        {
            type: 'belongsTo',
            model: 'FlexiBee.root.UsersAccessModel'
        }
    ]
});

</#escape>