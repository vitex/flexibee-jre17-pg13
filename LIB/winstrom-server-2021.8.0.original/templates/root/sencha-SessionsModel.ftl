<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.SessionsModel', {
    extend: 'Ext.data.Model',

    idProperty: 'shortSessionId',

    fields: [
        {
            name: 'shortSessionId',
            type: 'string'
        },
        {
            name: 'username',
            type: 'string'
        },
        {
            name: 'createDate',
            type: 'date'
        },
        {
            name: 'createDateAsString',
            type: 'string'
        },
        {
            name: 'keepAlive',
            type: 'date'
        },
        {
            name: 'keepAliveAsString',
            type: 'string'
        },
        {
            name: 'ignoreForLicense',
            type: 'boolean'
        },
        {
            name: 'consumesLicense',
            type: 'boolean'
        },
        {
            name: 'clientType',
            type: 'string'
        }
    ]
});


</#escape>