<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.Plugins', {
    singleton: true,
    alternateClassName: 'FlexiBee.Plugins',

    plugins: {},

    register: function(clazz, plugin) {
        if (!this.plugins[clazz]) {
            this.plugins[clazz] = [];
        }
        this.plugins[clazz].push(plugin);
        Ext.log("Registered plugin '" + Ext.getClassName(plugin) + "' for class '" + clazz + "'");
    },

    invoke: function(invoker, method) {
        var me = this;
        var pluginArgs = Array.prototype.slice.call(arguments, 2);

        var results = [];
        var clazz = Ext.getClass(invoker);
        while (clazz) {
            var className = clazz.$className;
            if (me.plugins[className]) {
                Ext.iterate(me.plugins[className], function(plugin) {
                    var pluginMethod = plugin[method];
                    if (pluginMethod) {
                        var result = pluginMethod.apply(plugin, pluginArgs);
                        results.push(result);
                    } else {
                        Ext.log("Plugin '" + Ext.getClassName(plugin) + "' registered for '" + className
                                + "' doesn't contain '" + method + "' method");
                    }
                });
            }
            clazz = clazz.superclass;
        }
        return results;
    },

    invokeSingle: function(invoker, method) {
        var me = this;
        var pluginArgs = Array.prototype.slice.call(arguments, 2);

        var availablePlugins = [];
        var clazz = Ext.getClass(invoker);
        while (clazz) {
            var className = clazz.$className;
            if (me.plugins[className]) {
                Ext.iterate(me.plugins[className], function(plugin) {
                    var pluginMethod = plugin[method];
                    if (pluginMethod) {
                        availablePlugins.push(plugin);
                    } else {
                        Ext.log("Plugin '" + Ext.getClassName(plugin) + "' registered for '" + className
                                + "' doesn't contain '" + method + "' method");
                    }
                });
            }
            clazz = clazz.superclass;
        }

        if (availablePlugins.length === 1) {
            var plugin = availablePlugins[0];
            return plugin[method].apply(plugin, pluginArgs);
        } else if (availablePlugins.length > 1) {
            var pluginNames = [];
            Ext.iterate(availablePlugins, function(plugin) {
                pluginNames.push(Ext.getClassName(plugin));
            });
            Ext.log("Method '" + method + "' can only be registered by single plugin, but it was registered by "
                    + pluginNames.length + ": " + pluginNames);
        }
        return false;
    }
});

</#escape>
