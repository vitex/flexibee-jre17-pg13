<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.BrotherModel', {
    extend: 'Ext.data.Model',

    idProperty: 'shortSessionId',

    fields: [
        {
            name: 'host',
            type: 'string'
        },
        {
            name: 'uuid',
            type: 'string',
            mapping: 'state.extendedStatus.uuid'
        },
        {
            name: 'version',
            type: 'string',
            mapping: 'state.extendedStatus.version'
        },
        {
            name: 'lastCheck',
            type: 'date',
            mapping: 'state.lastCheck'
        },
        {
            name: 'lastCheckAsString',
            type: 'string',
            mapping: 'state.lastCheckAsString'
        },
        {
            name: 'lastSuccess',
            type: 'date',
            mapping: 'state.lastSuccess'
        },
        {
            name: 'lastSuccessAsString',
            type: 'string',
            mapping: 'state.lastSuccessAsString'
        },
        {
            name: 'state',
            type: 'string',
            mapping: 'state.state'
        },
        {
            name: 'loggedUser',
            type: 'integer',
            mapping: 'state.extendedStatus.loggedUser'
        },
        {
            name: 'failureCount',
            type: 'integer',
            mapping: 'state.failureCount'
        },
        {
            name: 'isOk',
            type: 'boolean',
            mapping: 'state.isOk'
        },
        {
            name: 'bytesRead',
            type: 'numeric',
            mapping: 'state.extendedStatus.bytesRead'
        },
        {
            name: 'bytesWritten',
            type: 'numeric',
            mapping: 'state.extendedStatus.bytesWritten'
        },
        {
            name: 'memoryHeap',
            type: 'numeric',
            mapping: 'state.extendedStatus.memoryHeap'
        },
        {
            name: 'memoryUsed',
            type: 'numeric',
            mapping: 'state.extendedStatus.memoryUsed'
        },
        {
            name: 'startupTime',
            type: 'date',
            mapping: 'state.extendedStatus.startupTime'
        },
        {
            name: 'startupTimeAsString',
            type: 'string',
            mapping: 'state.extendedStatus.startupTimeAsString'
        },
        {
            name: 'systemLoad',
            type: 'numeric',
            mapping: 'state.extendedStatus.systemLoad'
        }
    ]
});


</#escape>