<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.AdminWindow', {
    extend: 'Ext.Viewport',

    requires: [
        'FlexiBee.root.Toolbar',
        'FlexiBee.root.Plugins',
        'FlexiBee.root.StatusBar',
        'FlexiBee.root.SettingsPanel'
    ],

    layout: 'border',
    cls: 'flexibee-background-dark',

    items: [
<#if isDesktop == false>
        {
            xtype: 'container',
            region: 'north',
            layout: {
                type: 'hbox'
            },
            cls: 'flexibee-background-dark',
            items: [
                {
                    xtype: 'container',
                    border: 0,
                    cls: 'flexibee-background-dark',
                    height: 55,
                    width: 210,
                    html: '<a href="/"><img style="text-align: center; padding: 15px 30px" src="${staticPrefix}/img/skin/${skin}/logo.png"></a>'
                },
                {
                    xtype: 'fbRootToolbar',
                    id: 'maintoolbar',
                    margin: '0px 18px 0 0',
                    flex: 1,
                    height: 50,
                },
            ]
        },
</#if>
        {
            xtype: 'fbSettingsPanel',
<#if isDesktop == false>
            margin: '0 12 0 12',
<#else>
            margin: '0 5 0 0',
</#if>
            region: 'center'
        }
<#if isDesktop == false>
        ,
        {
            xtype: 'container',
            layout: 'hbox',
            region: 'south',
            cls: 'flexibee-background-dark',
            margin: '0 18px 3px 15px',
            border: 1,
            height: 20,
            items: [
                {
                    xtype: 'component',
                    padding: '0 0 0 0',
                    html: ''
                },
                {
                    xtype: 'fbRootStatusBar',
                    id: 'mainstatusbar',
                    padding: '0 0 0 2em',
                    flex: 1
                }
            ]
        }
</#if>
    ]
});

</#escape>
