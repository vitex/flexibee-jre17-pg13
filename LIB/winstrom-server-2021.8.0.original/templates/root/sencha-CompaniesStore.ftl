<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.CompaniesStore', {
    extend: 'Ext.data.Store',

    requires: [
        'FlexiBee.root.CompaniesModel'
    ],

    autoLoad: true,
    autoSync: true,
//    buffered: true,
    pageSize: 100,
    model: 'FlexiBee.root.CompaniesModel',
    proxy: {
       type: 'rest',
       url: '/c.json',
       reader: {
           type: 'json',
           root: 'companies.company'
       },
       writer: {
           type: 'json'
       }
    }
});

</#escape>