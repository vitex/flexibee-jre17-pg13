<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>



Ext.define('FlexiBee.root.SettingsPanel', {
    extend: 'Ext.ux.GroupTabPanel',
    alias: 'widget.fbSettingsPanel',

    requires: [
        'Ext.tip.QuickTipManager',
        'Ext.window.Window',
        'FlexiBee.root.Plugins',
        'FlexiBee.root.GroupsGrid',
        'FlexiBee.root.CompaniesGrid',
        'FlexiBee.root.BrotherGrid',
        'FlexiBee.root.UsersGrid',
        'FlexiBee.root.SessionsGrid',
        'FlexiBee.root.AuthPanel',
        'FlexiBee.root.ServerParams',
        'FlexiBee.root.CertPanel',
        'FlexiBee.root.LicensePanel',
        'FlexiBee.root.MailPanel',
        'FlexiBee.root.UsersAccessGrid',
        'Ext.form.Panel',
        'Ext.tab.Panel',
        'Ext.ux.GroupTabPanel',
        'Ext.JSON'
    ],

    activeGroup: 0,
    items: [
        {
            mainItem: 0,
            items: [
                {
                    title: '${dlgText('admin.server.title')}',
                    margin: '10',
                    layout: 'fit',
                    items: [
                        {
                            xtype: 'container',
                            border: 0,
                            cls: 'flexibee-background-light',
                            items: [
                                {
                                    border: 0,
                                    margin: '30',
                                    height: '',
                                    xtype: 'container',
                                    items: [
                                        {
                                            xtype: 'container',
                                            html: '<center><img src="${staticPrefix}/img/logo-title.png" alt="FlexiBee"/></center>'
                                        }

                                    ]

                                },
                                {
                                    border: 0,
                                    margin: '20',
                                    xtype: 'container',
                                    html: '${lblText('root.version', 'Verze')}: <strong>${serverVersion}</strong>'
                                },
                                {
                                    border: 0,
                                    margin: '20',
                                    xtype: 'container',
                                    html: '<small>${lblText('root.explanation', 'FlexiBee je moderní účetní a ekonomický systém schopný zajistit kvalitní zpracování účetnictví a navazujících agend v širokém spektru společností. Za jednoduše ovladatelným uživatelským prostředím je skryta robustní a výkonná aplikace typu client/server, kterou je možné provozovat na platformách Windows, Linux a Mac OS X. Ekonomické systémy řady WinStrom působí na trhu od roku 1991.')}</small>'
                                }
                            ]
                        }
                    ]
                },
                // {
                //     title: 'Stav serveru',
                //     margin: '10',
                //     height: null
                // },
                {
                    title: '${dlgText('admin.users_logged_in.title')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbSessionsGrid'
                },
                <#if it.clusteringEnabled || true>
                {
                    title: '${dlgText('admin.run_in_cluster.title')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbBrotherGrid'
                },
                </#if>
            ]
        }, {
            mainItem: 0,
            items: [
                {
                    title: '${dlgText('admin.title')}',
                    noSwitch: true,
                    margin: '10',
                    border: 0
                },
                {
                    title: '${dlgText('admin.users.title')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbUsersGrid'
                },
                {
                    title: '${dlgText('admin.license_groups.title')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbGroupsGrid'
                },
                {
                    title: '${dlgText('admin.accounting_units.title')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbCompaniesGrid'
                },
                <#--
                {
                    title: '${dlgText('admin.default_licence.title')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbLicensePanel'
                },
                -->
                {
                    title: '${dlgText('admin.accesses.title')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbUsersAccessGrid'
                },
            ]
        },
        {
            mainItem: 0,
            items: [
                {
                    title: '${dlgText('admin.parameters.title')}',
                    noSwitch: true,
                    margin: '10',
                    border: 0
                }
                ,
                <#--
                {
                    title: 'SMTP',
                    margin: '10',
                    height: null,
                    xtype: 'fbMailPanel'
                }
                ,
                -->
                {
                    title: '${dlgText('admin.authorization.title')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbAuthPanel'
                }
                ,
                {
                    title: '${dlgText('admin.server_params.title.short')}',
                    margin: '10',
                    height: null,
                    xtype: 'fbServerParams'
                }
                <#--
                ,
                {
                    title: '${dlgText('admin.ssl_certificate.title')}',
                    margin: '10',
                    xtype: 'fbCertPanel',
                    height: null
                }
                -->
            ]
        }
    ],
    listeners: {
        'beforetabchange': function(me, newTab, oldTab) {
            if (newTab.noSwitch && newTab.noSwitch == true) {
                return false;
            }

            return true;
        }
    }
});

</#escape>
