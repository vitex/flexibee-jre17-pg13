<#ftl encoding="utf-8" />
<#include "/i-localization.ftl"/>
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.UsersAccessGrid', {
    extend: 'Ext.grid.Panel',
    alias: ['widget.fbUsersAccessGrid'],

    requires: [
        'FlexiBee.root.UsersAccessStore',
        'FlexiBee.root.UsersAccessModel',
        'FlexiBee.root.grid.BoxSelectColumn'
    ],

    usernameColumn: undefined,
    accessColumn: undefined,


    store: Ext.create('FlexiBee.root.UsersAccessStore'),

    onSuccess: function(form, action) {
    },
    onFailure: function(form, action) {
        obj = Ext.JSON.decode(action.response.responseText);
        Ext.Msg.alert('Save failed!', obj.errors.reason);
    },

    plugins: [
        Ext.create('Ext.grid.plugin.RowEditing', {
            pluginId: 'rowediting',
            clicksToEdit: 2,
            clicksToMoveEditor: 1,

            listeners: {
                beforeedit: function(editor, e) {
                    var savedRecord = e.record.isSaved();
                    console.log('beforeedit: rowIdx=' + e.rowIdx + ", isSaved=" + savedRecord);
                    this.grid.enableColumnEditor(this.grid.usernameColumn, e.record, !savedRecord);
                    this.grid.enableColumnEditor(this.grid.accessColumn, e.record, !e.record.get('manageAll'));
                },
                edit: function(editor, e) {
                    console.log('edit');
                }
            }
        })
    ],

    columns: [
        {
            text: 'Jméno',
            dataIndex: 'fullname',
            width: '12em',
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        },
        {
            text: 'E-mail',
            dataIndex: 'email',
            width: '20em',
            editor: {
                xtype: 'textfield',
                allowBlank: false
            }
        },
        {
            text: 'Telefon',
            dataIndex: 'phone',
            width: '8em',
            editor: {
                xtype: 'textfield'
            }
        },
        {
            id: 'usernameColumn',
            text: 'Uživatelské jméno',
            dataIndex: 'username',
            width: '12em',
            editor: {
                xtype: 'textfield',
                disabled: true,
                allowBlank: false
            }
        },
        {
            text: 'VF',
            tooltip: 'Všechny firmy',
            xtype: 'booleancolumn',
            dataIndex: 'manageAll',
            trueText: 'Ano',
            falseText: 'Ne',
            width: '4em',
            editor: {
                xtype: 'checkbox',
                listeners: {
                    change: function(cb, newVal, oldVal) {
                        console.log('manageAll checkbox changed: ' + newVal);
                        var accessField = this.up().down('#accessField');
                        if (newVal) {
                            accessField.disable();
                        } else {
                            accessField.enable();
                        }
                    }
                }
            }
        },
        {
            id: 'accessColumn',
            xtype: 'boxselectcolumn',
            text: 'Firmy',
            flex: 1,
            dataIndex: 'companies',

            proxyUrl: '/c.json',
            dataRoot: 'companies.company',
            valueProperty: 'dbNazev',
            displayProperty: 'nazev',

            editor: {
                id: 'accessField'
            },

            renderer: function(value, metaData, record, rowIndex, colIndex, store, view) {
                if (record.get('manageAll')) {
                    return '&nbsp;';
                }

                //TODO use "renderer" function from parent: this.callParent(arguments) is not possible
                var values = '';

                if (value) {
                    var me = this.columns[colIndex];
                    var dataMap = me.optionsStore.data.map;
                    Ext.Array.each(Array.isArray(value) ? value : [value], function(item, index) {
                        values += (index > 0 ? me.valueSeparator : '') + (dataMap[item] ? dataMap[item].get(me.displayProperty) : item);
                    });
                }
                return values;
            }
        },
        {
            id: 'groupColumn',
            xtype: 'boxselectcolumn',
            text: 'Licenční skupina',
            dataIndex: 'groupId',
            width: '12em',

            proxyUrl: '/g.json',
            dataRoot: 'groups.group',
            valueProperty: 'id',
            displayProperty: 'nazev',

            multiSelect: false
        }
    ],

    tbar: [
        {
            xtype: 'fbReloadButton',
            handler: function() {
                this.up().up().reloadStore();
            }
        }, '-',
        {
            xtype: 'button',
            text: 'Přidat',
            action: 'add',
            handler: function() {
                this.up().up().addNewUserAccess();
            }
        }, '-',
        {
            id: 'changeBtn',
            text: 'Změnit',
            disabled: true,
            handler: function() {
                this.up().up().changeUserAccess();
            }
        }, '-',
        {
            id: 'deleteBtn',
            text: 'Smazat',
            disabled: true,
            handler: function() {
                this.up().up().removeUserAccess();
            }
        }
    ],

    buttons: [{
        text: '<@lbl "ulozit" "Uložit"/>',
        formBind: true,

        // Function that fires when user clicks the button
        handler: function() {
            this.up().up().saveChanges();
        }
    }],

    initComponent: function() {

        var grid = this;

        this.callParent(arguments);

        this.usernameColumn = this.down('#usernameColumn');
        this.accessColumn = this.down('#accessColumn');

        this.getSelectionModel().on('selectionchange', function(selModel, selections) {
           grid.down('#changeBtn').setDisabled(selections.length === 0);
           grid.down('#deleteBtn').setDisabled(selections.length === 0);
        });
    },

    enableColumnEditor: function(column, record, enable) {
        var selRecord = record;
        if (!selRecord) {
            selRecord = this.getSelectionModel().getSelection()[0];
        }
        var editor = column.getEditor(selRecord);
        if (enable) {
            editor.enable();
        } else {
            editor.disable();
        }
    },

    reloadStore: function() {
        console.log('reloadStore()');
        this.store.load();
    },

    saveChanges: function() {
        console.log('Saving changes...');
        //this.store.commitChanges();
        this.store.sync();
    },

    addNewUserAccess: function() {
        var newAccess = Ext.create('FlexiBee.root.UsersAccessModel');
        this.store.insert(0, newAccess);
        this.getView().refresh();
        this.getSelectionModel().select(0);
        this.getPlugin('rowediting').startEdit(0, 0);
    },

    changeUserAccess: function() {
        var selUserAccess = this.getSelectionModel().getSelection()[0];
        if (selUserAccess) {
            this.getPlugin('rowediting').startEdit(selUserAccess, 0);
        }
    },

    removeUserAccess: function() {
        var selection = this.getSelectionModel().getSelection()[0];

        if (selection) {
            if (selection.isSaved()) {
                Ext.MessageBox.alert('Nelze vymazat', 'Existující uživatel už asi nepůjde vymazat, co Petře? :-)');
                return;
            }

            this.store.remove(selection);
            this.getView().refresh();
        }
    }
});

</#escape>