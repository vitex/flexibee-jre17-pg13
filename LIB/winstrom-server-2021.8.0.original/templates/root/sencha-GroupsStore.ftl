<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.root.GroupsStore', {
    extend: 'Ext.data.Store',

    requires: [
        'FlexiBee.root.GroupsModel'
    ],

    autoLoad: true,
    autoSync: true,
    model: 'FlexiBee.root.GroupsModel',
    proxy: {
       type: 'rest',
       url: '/g.json',
       reader: {
           type: 'json',
           root: 'groups.group'
       },
       writer: {
           type: 'json'
       }
    }
});

</#escape>