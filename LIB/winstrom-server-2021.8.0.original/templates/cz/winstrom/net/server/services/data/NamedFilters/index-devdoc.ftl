<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'namedFilters.title', 'Přehled pojmenovaných filtrů') />
<#assign titleId="root"/>
<#include "/header.ftl"/>

<p>Tento přehled slouží k&nbsp;výpisu pojmenovaných filtrů.</p>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl"/>
</#escape>
