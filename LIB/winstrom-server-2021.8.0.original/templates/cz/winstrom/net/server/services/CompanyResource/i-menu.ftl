<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

    <@tool.showTitle titleId="flexibee-menu" menu=false title=lang('formsInfo', it.name, it.name)>
        <#if isMobile == true>
        <a href="/c" class="flexibee-clickable flexibee-show-wait ui-btn-left" title="${lang('labels', 'vyberFirmySeznamFiremPN', 'Seznam firem')}" accesskey="a" data-transition="slidedown" data-icon="grid" <#if deviceType != "tablet">data-iconpos="notext"</#if>>${lang('labels', 'vyberFirmySeznamFiremPN', 'Seznam firem')}</a>
        <a href="/c/${it.companyId}?menu=nastroje" class="flexibee-menu-grp-nastroje flexibee-clickable ui-btn-right" title="${lang('formsInfo', 'nastav000', 'Nástroje')}" accesskey="a" data-transition="slidedown" data-icon="gear" <#if deviceType != "tablet">data-iconpos="notext"</#if>>${lang('formsInfo', 'nastav000', 'Nástroje')}</a>
        </#if>
    </@tool.showTitle>
    <@tool.showTitleFinish />

    <#-- základní rozcestník menu -->
        <#assign m = menuTool().root/>
        <#include "i-menu-items.ftl"/>

        <#if cloudOverride.isTemplate("company", "menu")>
        <#include cloudOverride.include("company", "menu") />
        </#if>

    <@tool.showFooter/>

</#escape>