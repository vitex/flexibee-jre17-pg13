<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', it.name, 'Přehled položek')/>
<#assign titleId="root"/>
<#include "/header.ftl"/>

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>Popis</th>
        <th>Strojový název</th>
        <th>Databázový název</th>
        <th><a href="/devdoc/variable-types">Typ proměnné</a></th>
        <th>Informace</th>
        <th>*</th>
        <th>rw</th>
        <th>1</th>
        <th>2</th>
        <th>3</th>
        <th>S</th>
    </tr>
    </thead>
    <tbody>
    <#list it.devDocItems as item>
        <tr>
        <th>${item.name}</th>
        <td><code>${item.propertyName}</code></td>
        <td>
        <#if item.dbName?exists>
            <code>${item.dbName}</code>
        </#if>
        </td>
        <td><code>${item.type}</code>
<#if item.type == 'relation'> (<#assign url=formListUrl(it.companyResource, item.fkEvidenceType)?default("") /><#if url != ""><a href="${url}/properties">${item.fkName}</a><#else><#if !item.fkName?? || item.fkName == ''><#list item.evidenceTypeVariants as et><#assign url=formListUrl(it.companyResource, et)?default("") /><#if url != ""><a href="${url}/properties">${et}</a><#else>${et}</#if><#if et_has_next>, </#if
    ></#list><#else>${item.fkName}</#if></#if>)</#if>
</td>
        <td>
<#if item.possibleValues?exists && item.possibleValues?size &gt; 0>
    Možné hodnoty:<br/><ul>
    <#list item.possibleValues as val>
        <li>${val.name} (<code>${val.key}</code>)</li>
    </#list>
    </ul>
</#if>
<#if item.minLength?exists>Min. délka: <code>${item.minLength}</code><br/></#if>
<#if item.maxLength?exists>Max. délka: <code>${item.maxLength}</code><br/></#if>
<#if item.digits?exists>Max. poč. číslic: <code>${item.digits}</code><br/></#if>
<#if item.decimal?exists>Počet deset. míst: <code>${item.decimal}</code><br/></#if>
<#if item.minValue?exists>Min. hodnota: <code>${item.minValue}</code><br/></#if>
<#if item.maxValue?exists>Max. hodnota: <code>${item.maxValue}</code><br/></#if>
<#if item.mask?exists>Maska: <code>${item.mask}</code><br/></#if>
</td>
<td><#if item.mandatory>&times;</#if></td>
<td><small><#if item.isWritable>rw<#else>ro</#if></small></td>
<td><#if item.inId>&times;</#if></td>
<td><#if item.inSummary>&times;</#if></td>
<td><#if item.inDetail>&times;</#if></td>
<td><#if item.isSortable>&times;</#if></td>
        </tr>
<#if !it.noComments && devDocMsg(it.tagName, item.propertyName)?exists>
        <tr>
        <td colspan="11"><#noescape>${devDocMsg(it.tagName, item.propertyName)}</#noescape></td>
        </tr>
</#if>
    </#list>
    </tbody>
</table>


<h3>Vysvětlivky</h3>
<table>
    <tr>
        <th>*</th><td>Požadovaná položka (<a href="/devdoc/internal-dependencies">Vnitřní vazby</a> mohou způsobit, že není nutné povinnou položku vyplňovat.)</td>
    </tr>
    <tr>
    <th>rw</th><td>Je položka zapisovatelná</td>
    </tr>
    <tr>
    <th>1</th><td>Je položka součástí seznamu 'id'</td>
    </tr>
    <tr>
    <th>2</th><td>Je položka součástí seznamu 'summary'</td>
    </tr>
    <tr>
    <th>3</th><td>Je položka součástí seznamu 'full'</td>
    </tr>
    <tr>
    <th>S</th><td>Je možné dle položky řadit či filtrovat</td>
    </tr>
</table>

<ul>
    <li><a href="relations">${lang('dialogs', 'relations.title', 'Přehled relací')}</a></li>
    <li><a href="named-filters">${lang('dialogs', 'namedFilters.title', 'Přehled pojmenovaných filtrů')}</a></li>
    <li><a href="reports">${lang('dialogs', 'reports.title', 'Přehled tiskových sestav')}</a></li>
    <li><a href="actions">${lang('dialogs', 'actions.title', 'Přehled akcí')}</a></li>
    <#if it.evidenceResource.isWorkflowEnabled>
    <li><a href="workflows">${lang('dialogs', 'workflows.title', 'Přehled definovaných workflow')}</a></li>
    </#if>

</ul>


<#include "/footer.ftl"/>
</#escape>
