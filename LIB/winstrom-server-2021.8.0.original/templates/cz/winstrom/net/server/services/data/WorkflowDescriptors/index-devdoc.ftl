<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'workflows.title', 'Přehled definovaných workflow') />
<#assign titleId="root"/>
<#include "/header.ftl"/>

<p>Tento přehled slouží k&nbsp;výpisu definovaných workflow k&nbsp;tomuto objektu.</p>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl"/>
</#escape>
