<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Stav pracovních vláken"/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<#--
<#if it.requestLoggerAvailable>
<form action="/status/thread-pool/request-logger-control" method="post" class="form-inline" role="form">
    <input type="hidden" name="token" value="${csrfToken}">
    <div class="form-group">
        <label class="" for="requestLoggerSwitch">Request logger</label>
        <input id="requestLoggerSwitch" type="submit" class="btn btn-md btn-${it.requestLoggerEnabled?string("success", "danger")}" title="Click to ${it.requestLoggerEnabled?string("disable", "enable")} it." value="${it.requestLoggerEnabled?string("enabled", "disabled")}">
    </div>
</form>
</#if>
-->
<div class="row">
    <strong>Filtrace:</strong>
    <a href="/status/thread-pool?counters=${(!it.showCounters)?string}&threads=${(it.showThreads)?string}&jdbc=${(it.showAll)?string}" class="btn btn-md btn-${it.showCounters?string("success", "danger")}">counters</a>
    <div class="btn-group">
    <a href="/status/thread-pool?counters=${(it.showCounters)?string}&threads=${(!it.showThreads)?string}" class="btn btn-md btn-${it.showThreads?string("success", "danger")}">threads</a>
    <a href="/status/thread-pool?counters=${(it.showCounters)?string}&threads=${(it.showThreads)?string}&all=${(!it.showAll)?string}" class="btn btn-md btn-${it.showAll?string("success", "danger")}<#if !it.showThreads> disabled</#if>">all</a>
    </div>
</div>

<br/>

<#list it.list as s>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
<thead>
    <tr>
        <th colspan="6"><h2>${s.server}</h2></th>
    </tr>
</thead>
<#if s.serverPoolCounters??>
<#assign c=s.serverPoolCounters>
<tbody>
    <tr>
        <td colspan="6">
            <table class="table table-condensed">
            <tbody>
                <tr>
                    <th>Jetty pracovní vlákna</th><th>aktuálně</th><th>maximum</th><th>konfigurace</th>
                </tr>
                <tr>
                    <td>Počet vláken</td>
                    <td title="Počet aktuálně využitých + volných vláken"><strong>${c.threads}</strong> (${c.idleThreads})</td>
                    <td title="Maximální dosažený počet vláken">${c.threadsMax}</td>
                    <td title="Konfigurovaný počet pracovních vláken serveru">min: ${c.minThreads} / max: ${c.maxThreads}</td>
                </tr>
                <tr>
                    <td>Délka fronty požadavků</td>
                    <td title="Aktuální délka fronty"><strong>${c.queueSize}</strong></td>
                    <td title="Maximální dosažená délka fronty">${c.maxQueued}</td>
                    <td title="Maximální kapacita fronty">${c.queueMaxSize}</td>
                </tr>
            </tbody>
            <#if c.jdbcThreads + c.jdbcThreadsMax &gt; 0>
            <tbody>
                <tr>
                    <th>JDBC vlákna</th><th>aktuálně</th><th colspan=2>maximum</th>
                </tr>
                <tr>
                    <td>JDBC spojení (vláken)</td>
                    <td><strong>${(c.jdbcThreads / 2)?ceiling}</strong> (${c.jdbcThreads})</td>
                    <td colspan=2>${(c.jdbcThreadsMax / 2)?ceiling} (${c.jdbcThreadsMax})</td>
                </tr>
            </tbody>
            </#if>
            <#if s.couchDbPoolCounters??>
            <#assign c=s.couchDbPoolCounters>
            <tbody>
                <tr>
                    <th>CouchDB spojení</th><th>aktuálně</th><th colspan=2>maximum</th>
                </tr>
                    <td>Aktivních HTTP spojení</td>
                    <td title="Aktuálně využitých konekcí v poolu"><strong>${c.connections}</strong></td>
                    <td title="Maximální počet konekcí v poolu" colspan=2>${c.maxConnections}</td>
                <tr>
                </tr>
            </tbody>
            </#if>
            <tbody>
                <tr>
                    <th>MainEventLoop</th><th colspan=3>aktuálně</th>
                </tr>
                    <td>Aktivních eventů ve frontě</td>
                    <td title="Aktivních eventů ve frontě" colspan=3><strong>${s.eventLoopQueueSize}</strong></td>
            </tbody>
            </table>
        </td>
    </tr>
</tbody>
</#if>
<#if s.threadListSize &gt; 0>
<thead>
    <tr>
        <th rowspan="2">
                <a class="${s.server}_more btn btn-md btn-success" title="Zobrazit acceptory" onClick="showConnectors(true, '${s.server}');">+</a>
                <a class="${s.server}_less btn btn-md btn-danger" title="Skrýt acceptory" style="display: none;" onClick="showConnectors(false, '${s.server}');">-</a>
                Vlákno / adresa
        </th>
        <th rowspan="2" title="Stav vlákna">St</th>
        <th rowspan="2" title="Typ vlákna">Typ</th>
        <th rowspan="2">Požadavek</th>
        <th rowspan="2">Uživatel</th>
        <th colspan="2" style="text-align:center;">Čas běhu</th>
    </tr>
    <tr>
        <th style="text-align:right;">požadavek</th>
        <th style="text-align:right;">celkem</th>
    </tr>
</thead>
<tbody>
<#list s.threadList as t>
    <tr class="${s.server}_THREAD_${t.type}" <#if t.type == "ACCEPTOR">style="display: none;"</#if>>
        <td style="white-space:nowrap;">${t.thread}<div style="text-align:right;"/><code style="text-align:right;"><strong>${t.remoteAddr}</strong></code></div></td>
        <td title="${t.state}">${t.state[0]}</td>
        <td title="${t.type}">${t.type}</td>
        <td><#if t.state == "RUNNING">
            <strong>${t.request}</strong>
        <#elseif t.state == "TERMINATING">
            <del>${t.request}</del>
        <#else>
            ${t.request}
        </#if></td>
        <td><code>${t.user}</code></td>
        <td title="${t.requestDuration} ms" style="white-space:nowrap; text-align:right;">${t.requestDurationAsString}</td>
        <td title="${t.duration} ms" style="white-space:nowrap; text-align:right;"><strong>${t.durationAsString}</strong></td>
    </tr>
</#list>
</tbody>
<tfoot>
    <tr>
        <th>#${s.threadListSize}</th>
        <th colspan="5">&nbsp;</th>
    </tr>
</tfoot>
</#if>
</table>
<#if s_has_next><hr/></#if>
</#list>

<#include "/footer.ftl" />
</#escape>

<script>
function toggle_by_class(cls, on) {
    var lst = document.getElementsByClassName(cls);
    for(var i = 0; i < lst.length; ++i) {
        lst[i].style.display = on ? '' : 'none';
    }
}

function showConnectors(toggle, server) {
    toggle_by_class(server + "_THREAD_ACCEPTOR", toggle);
    toggle_by_class(server + "_less", toggle);
    toggle_by_class(server + "_more", !toggle);
}

</script>
