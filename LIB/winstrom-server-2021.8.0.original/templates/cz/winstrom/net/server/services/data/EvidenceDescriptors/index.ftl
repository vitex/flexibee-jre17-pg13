<#ftl encoding="utf-8" />
<#-- @ftlvariable name="it" type="cz.winstrom.net.server.services.data.EvidenceDescriptors" -->
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', it.name, 'Přehled evidencí')/>
<#assign titleId="root"/>
<#include "/header.ftl"/>

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>Název</th>
        <th>Strojový název</th>
        <th>Databázový název</th>
        <th>Podpora importu</th>
        <th>Podpora ext. ID</th>
    </tr>
    </thead>
    <tbody>
    <#list it.evidenceDescriptors as item>
        <tr>
        <th>${item.evidenceName}</th>
        <td><code><a href="/c/${it.companyId}/${item.tagName}/properties">${item.tagName}</a></code></td>
        <td>
        <#if item.dbName?exists>
            <code>${item.dbName}</code>
        </#if>
        </td>
        <td><code>${item.importStatus}</code></td>
        <td><#if item.extIdSupported>&times;</#if></td>
        </tr>
        <#if item.evidenceFilter?exists>
            <tr>
            <td colspan="5"><i>${lang('labels', 'devdoc.evidenceFilter', 'Databázový filtr:')}${item.evidenceFilter}</i></td>
            </tr>
        </#if>
    </#list>
    </tbody>
</table>

<h3>Vysvětlivky</h3>
<table>
    <tr>
        <th><code>SUPPORTED</code></th><td>import je plně podporovaný</td>
    </tr>
    <tr>
        <th><code>NOT_DIRECT</code></th><td>import je podporovaný, ale nepřímo (např. položka faktury jen jako součást faktury)</td>
    </tr>
    <tr>
        <th><code>NOT_DOCUMENTED</code></th><td>import podporujeme, ale není dokumentovaný a nedoporučujeme jej používat</td>
    </tr>
    <tr>
        <th><code>DISALLOWED</code></th><td>import není vůbec podporovaný (je zakázaný)</td>
    </tr>
</table>

<#include "/footer.ftl"/>
</#escape>
