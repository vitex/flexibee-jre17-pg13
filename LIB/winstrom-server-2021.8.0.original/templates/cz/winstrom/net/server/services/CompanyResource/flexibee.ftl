<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=""/>
<#assign titleId="root"/>
<#assign useNaviPath=false />
<#assign showMenuAtEnd=false />
<#include "/header.ftl" />

<link rel="stylesheet" type="text/css" href="${staticPrefix}/css/spinner.css?v=${serverVersion}" />

<style type="text/css">
    body, html, #flexibee {
        height: 100%;
    }
</style>

<div id="flexibee" <#if inDesktopApp>class="inDesktopApp"</#if>>
        <div class="spinner-wrapper">
                <div class="spinner">
                        <div class="bounce1"></div>
                        <div class="bounce2"></div>
                        <div class="bounce3"></div>
                </div>
        </div>
</div>

<script type="text/javascript">
    window.csrfToken = "${csrfToken}";
    <#-- získání parametrů aplikace z aktuálně platného nastavení firmy -->
    <#assign s = settings()>
    window.appSettings = { "podvojUcto": ${s.podvojUcto?string}, "domaciMenaId": ${s.mena.id}, "domaciMenaKod": "${s.mena.kod}" };

    // for BLs
    var fb = fb || {};

    // why 'console.xyz.call(...)'? see http://stackoverflow.com/a/9678166
    fb.Console = {
        info: function() { console.info.call(console, arguments); },
        warn: function() { console.warn.call(console, arguments); },
        error: function() { console.error.call(console, arguments); },
        debug: function() { console.log.call(console, arguments); }
    };

    window.resourcePublicPath = "${staticPrefix}/js/flexibee-app/";
</script>
<script type="text/javascript" charset="utf-8" src="${staticPrefix}/js/flexibee-app/flexibee.js?v=${serverVersion}"></script>

<#if inDesktopApp>
    <#-- zadna paticka -->

    <#if isDevel>
        <a style="margin: 0; padding: 0 1em; position: fixed; bottom: 0; left: 0; z-index: 1030; background-color: lightgray;" href="javascript:(function(F,i,r,e,b,u,g,L,I,T,E){if(F.getElementById(b))return;E=F[i+'NS']&&F.documentElement.namespaceURI;E=E?F[i+'NS'](E,'script'):F[i]('script');E[r]('id',b);E[r]('src',I+g+T);E[r](b,u);(F[e]('head')[0]||F[e]('body')[0]).appendChild(E);E=new Image;E[r]('src',I+L);})(document,'createElement','setAttribute','getElementsByTagName','FirebugLite','4','firebug-lite.js','releases/lite/latest/skin/xp/sprite.png','https://getfirebug.com/','#startOpened');">Firebug Lite</a></td>
    </#if>
<#else>
    <#include "/footer.ftl" />
</#if>
</#escape>
