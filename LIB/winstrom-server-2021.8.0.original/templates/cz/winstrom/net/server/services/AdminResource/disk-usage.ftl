<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=dlgText("admin.disk_usage.title")/>
<#assign titleId="root"/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<link rel="stylesheet" type="text/css" href="${staticPrefix}/css/spinner.css?v=${serverVersion}" />

<style>
.color-box {
    display: inline-block;
    width: 1.5em;
    background-color: #ccc;
    border-radius: 5px;
    margin-right: 1ex;
}
</style>

<#macro companyName di>
<#compress>
    <#if di.show>
        ${di.nazev}
    <#else>
        <del title="&quot;${di.nazev}&quot; je odpojená firma.">${di.nazev}</del>
    </#if>
</#compress>
</#macro>

<div class="row">

    <div class="col-xs-8 col-xs-offset-2 col-sm-5 col-sm-offset-0 col-md-4 col-md-offset-1">
        <h4 class="text-center">
            Celková velikost firem: <strong class="total-size">zjišťuje se&hellip;</strong>
        </h4>
        <div id="error" class="flexibee-error-head" style="display:none; background-position:top left;"></div>
        <div class="spinner-wrapper" style="margin-top:5em;">
            <div class="spinner">
                <div class="bounce1"></div>
                <div class="bounce2"></div>
                <div class="bounce3"></div>
            </div>
        </div>
        <canvas id="usage-chart" style="width:200px; height:200px; background-color:white;">
        </canvas>
    </div>

    <div class="col-xs-12 col-sm-7 col-sm-offset-0 col-md-6 col-md-offset-1">
        <table class="table table-striped table-hover table-condensed">
            <thead>
                <tr><th>Firma</th><th class="text-right">Velikost</th></tr>
            <tbody>
            <#assign c = companiesAll()>
            <#list c as di>
                <tr>
                    <td><div id="color-${di.dbNazev}" class="color-box">&nbsp;</div><@companyName di/></td>
                    <td id="size-${di.dbNazev}" class="text-right text-nowrap">?</td>
                </tr>
            </#list>
            </tbody>
            <tfoot>
                <tr><th>#${c?size}</th><th class="total-size text-right text-nowrap">?</th></tr>
            </tfoot>
        </table>
    </div>

</div>


<script src="${staticPrefix}/js/jq/chart.js"></script>

<script>
function bytesToSize(bytes, precision) {
    var kilobyte = 1024;
    var megabyte = kilobyte * 1024;
    var gigabyte = megabyte * 1024;

    if (bytes < kilobyte) {
        return bytes + ' B';

    } else if (bytes < megabyte) {
        return (bytes / kilobyte).toFixed(precision) + ' KB';

    } else if (bytes < gigabyte) {
        return (bytes / megabyte).toFixed(precision) + ' MB';

    } else {
        return (bytes / gigabyte).toFixed(precision) + ' GB';
    }
}

var colors = [<#list c as di><#if di_index != 0>,</#if>"${hsv2rgb(360.0 * di_index / c?size, 0.666, 0.8)}"</#list>];
var highls = [<#list c as di><#if di_index != 0>,</#if>"${hsv2rgb(360.0 * di_index / c?size, 1.000, 0.8)}"</#list>];

$.get("/c-all.json?extendedInfo=true<#if isDesktop>&authSessionId=${authSessionId}</#if>", function( result ) {

    var totalSize = 0;
    var pieData = [];

    if (!result.companies || !result.companies.company) {
        return;
    }

    var companies = result.companies.company;
    if (!Array.isArray(companies)) {
        companies = [ companies ];
    }

    companies.forEach(function(info, index) {

        var companySize = parseInt(info.size);
        totalSize += companySize;

        var color = (index < colors.length) ? colors[index] : "#ccc";
        var highl = (index < highls.length) ? highls[index] : "#bbb";

        pieData[index] = {
            label: info.nazev,
            value: Math.round(companySize / 1048576), <#-- MB -->
            color: color,
            highlight: highl
        };

        $("#size-" + info.dbNazev).html(bytesToSize(companySize, 1));
        $("#color-" + info.dbNazev).css({"background-color": color});
    });

    $(".total-size").html(bytesToSize(totalSize, 1));

    pieData.sort(function(a,b) {
        return b.value - a.value;
    });

    $("div.spinner-wrapper").hide();

    var ctx = document.getElementById("usage-chart").getContext("2d");
    new Chart(ctx).Pie(pieData, {
        animation: false,
        segmentStrokeWidth: 1.5,
        responsive: true,
        tooltipTemplate: "<%if (label){%><%=label%>: <%}%><%= value %> MB"
    });

}).always(function() {
    $("div.spinner-wrapper").hide();

}).fail(function( xhr, statusText ) {
    var msg = "Chyba <b>" + xhr.status + " " + xhr.statusText + "</b>";
    var result = xhr.responseJSON || {};
    if (result.winstrom && result.winstrom.message) {
        msg += ": " + result.winstrom.message;
    }
    $("#error").html(msg).show();
});
</script>

<#include "/footer.ftl" />
</#escape>