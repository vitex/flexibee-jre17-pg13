<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=it.key/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<#if isMenu==true>
    <h2>${it.key}</h2>
</#if>

${it.value}

<#include "/footer.ftl" />
</#escape>
