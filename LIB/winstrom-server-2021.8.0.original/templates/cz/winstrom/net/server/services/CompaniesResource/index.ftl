<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=it.bundleAccessor.getMessage('companiesList', 'Seznam firem')/>

<#include "/header-html-start.ftl" />
<@tool.showTitle titleId="root" menu=false title=title back=false showPath=false>
</@tool.showTitle>
<@tool.showTitleFinish />
<#include "/i-toolbar.ftl"/>


<div class="maxwidth">
<#if it.userPrincipal.userInfo.isCreateCompany()>
    <p>
            <a href="/admin/zalozeni-firmy" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> <span>${it.bundleAccessor.getMessage('createCompany.novaFirma', 'Založení nové firmy')}</span></a>
    </p>
</#if>

<#if cloudOverride.isTemplate("company-list", "content")>
<#include cloudOverride.include("company-list", "content") />
</#if>

<#if it.list?size gt 0>
    <div class="list-group">
        <#list it.list as l>
            <a<#if l.isEstablished()> href="/c/${l.dbNazev}"</#if> class="list-group-item flexibee-keynav flexibee-show-wait <#if !l.isEstablished()> disabled</#if>">
                    <span<#if !l.isEstablished()> title="${l.stav}"</#if> class="list-group-item-heading">
                    <!-- <img src="/c/${l.dbNazev}/logo/thumbnail" title="Logo"/> --> ${l.nazev}</span>
            </a>
        </#list>
    </div>
</#if>
</div>

<script>
    $(document).ready(function() {
        $(".body").flexiBeeWallpaper();
    });
</script>

<#include "/footer.ftl" />
</#escape>
