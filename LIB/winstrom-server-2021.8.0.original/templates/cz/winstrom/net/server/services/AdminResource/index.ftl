<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'admin.title', 'Administrace')/>
<#assign titleId="root"/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

    <div class="list-group">
    <#if it.userPrincipal.userInfo.isCreateCompany()>
    <a href="/admin/zalozeni-firmy" class="flexibee-clickable list-group-item"><span>${lang('formsInfo', 'novaFirmaWiz', 'Založení nové firmy')}</span></a>
    </#if>
    <a href="/c" class="flexibee-clickable list-group-item"><span>${lang('labels', 'vyberFirmySeznamFiremPN', 'Seznam firem')}</span></a>
    <a href="/status/session" class="flexibee-clickable list-group-item"><span>${lang('dialogs', 'connections.list', 'Přehled přihlášených uživatelů')}</span></a>
    <a href="/a" class="flexibee-clickable list-group-item"><span>Přístupy</span></a>
<#--    <li class="flexibee-keynav"><a href="/u" class="flexibee-clickable"><span>${lang('formsInfo', 'cisOsoby', 'Osoby a uživatelé')}</span></a></li> -->
    </div>

<#include "/footer.ftl" />
</#escape>