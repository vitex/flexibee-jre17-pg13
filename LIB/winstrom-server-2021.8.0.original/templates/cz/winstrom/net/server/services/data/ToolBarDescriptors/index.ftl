<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'toolbars.title', 'Toolbar') />
<#assign titleId="root"/>
<#include "/header.ftl"/>

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>${lang('labels', 'identifier', 'Identifikátor')}</th>
        <th>${lang('labels', 'name', 'Název')}</th>
        <th></th>
        <th></th>
        <th>${lang('labels', 'url', 'URL')}</th>
    </tr>
    </thead>
    <tbody>
    <#list it.list as item>
        <tr>
        <th>${item.actionId}</th>
        <td>${item.actionName}</td>
        <td>${item.groupId}</td>
        <td>${item.actionMakesSense}</td>
        <td>${item.url}</td>
        </tr>
    </#list>
    </tbody>
</table>



<#include "/footer.ftl"/>
</#escape>
