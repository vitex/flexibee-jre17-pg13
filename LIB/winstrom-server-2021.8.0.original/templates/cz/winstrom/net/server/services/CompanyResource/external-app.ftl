<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=externalAppId />
<#assign titleId="root"/>
<#assign useNaviPath=false />
<#assign showMenuAtEnd=false />
<#include "/header.ftl" />

<#if externalAppInvocation.ok>
    <div id="flexibee-external-app-${externalAppId}" class="flexibee-external-app">
        ${externalAppInvocation.writeToFreeMarker()}
    </div>
<#elseif externalAppInvocation.timeout>
    <div class="flexibee-dialog">
        <p>Externí aplikace neodpověděla v časovém limitu. Zkuste opakovat požadavek později.</p>
    </div>
<#else>
    <div class="flexibee-dialog">
        <p>Při volání externí aplikace došlo k chybě. Zkuste opakovat požadavek později.</p>
        <p>${externalAppInvocation.error}</p>
    </div>
</#if>

<#include "/footer.ftl" />
</#escape>
