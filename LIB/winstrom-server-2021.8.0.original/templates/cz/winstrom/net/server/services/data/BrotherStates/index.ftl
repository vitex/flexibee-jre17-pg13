<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'cluster.list', 'Clusterový provoz') />


<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<#if it.totalCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>Server</th>
        <th>Stav</th>
        <th></th>
        <th>Přečteno / Zapsáno</th>
        <th>Poslední kontrola</th>
        <th>Poslední úspěšné</th>
        <th>Počet selhání</th>
    </tr>
    </thead>
    <tbody>
    <#list it.list as l>
    <tr <#if l.itsMyself>style="font-weight:bold"</#if>>
        <td>${l.hostAndPort}</td>
        <td>${l.state.state}</td>
        <#if l.state.es??>
            <td><small><strong>V:</strong>${l.state.es.version} <strong>L:</strong>${l.state.es.systemLoad}<br/>
            <strong>U:</strong>${l.state.es.uptime}<br/>
            <strong>M:</strong><#noescape>${l.state.es.memoryUsedAsString}/${l.state.es.memoryHeapAsString}</#noescape></small></td>
            <td><#noescape>${l.state.es.bytesReadAsString} / ${l.state.es.bytesWrittenAsString}</#noescape></td>
        <#else>
            <td>-</td>
            <td>-</td>
        </#if>
        <td>${l.state.lastCheckAsString}</td>
        <td>${l.state.lastSuccessAsString}</td>
        <td>${l.state.failureCount}</td>
    </tr>
        </#list>
    </tbody>
</table>
<#else>
<p>Není přihlášen žádný uživatel.</p>
</#if>

<#include "/footer.ftl" />
</#escape>