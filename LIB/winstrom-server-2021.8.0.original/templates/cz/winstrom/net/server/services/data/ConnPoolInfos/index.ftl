<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Stav databázových spojení"/>


<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<#list it.list as s>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
<thead>
    <tr>
        <th colspan="5"><h2>${s.server}</h2></th>
    </tr>
</thead>
<#if s.connPools?size &gt; 0>
<thead>
    <tr>
        <th>Database URL</th>
        <th></th>
        <th>Busy</th>
        <th>Connections</th>
        <th>Counters</th>
    </tr>
</thead>
<tbody>
<#list s.connPools as p>
    <tr<#if p_index % 2 == 0> class="flexibee-even"</#if>>
        <td>${p.key}</td>
        <td>
        <form action="/status/conn-pool/${p.companyId?url}/close" method="post" style="display:inline-block">
            <input type="hidden" name="token" value="${csrfToken}">
            <input type="hidden" name="instance" value="${p.instanceId}">
            <input type="submit" value="Zavřít" class="btn btn-danger">
        </form>
        <form action="/status/conn-pool/${p.companyId?url}/debug" method="post" style="display:inline-block">
            <input type="hidden" name="token" value="${csrfToken}">
            <input type="hidden" name="instance" value="${p.instanceId}">
            <input type="submit" value="<#if p.debugUnreturnedConnectionStackTraces>Neladit<#else>Ladit</#if>" class="btn btn-info">
        </form>
        </td>
        <td><#if p.numBusyConnectionsAllUsers &gt; 0>${p.numBusyConnectionsAllUsers} busy</#if></td>
        <td>${p.numConnectionsAllUsers} of ${p.maxPoolSize}</td>
        <td style="font-size: smaller">
            <#if p.numUnclosedOrphanedConnectionsAllUsers &gt; 0>${p.numUnclosedOrphanedConnectionsAllUsers} orphaned</#if>
            <#if p.numFailedCheckinsDefaultUser &gt; 0>${p.numFailedCheckinsDefaultUser},<br/></#if>
            <#if p.numFailedCheckoutsDefaultUser &gt; 0>${p.numFailedCheckoutsDefaultUser},<br/></#if>
            maxIdleTime: ${p.maxIdleTime},<br/>
            maxConnectionAge: ${p.maxConnectionAge}
        </td>
    </tr>
    <#assign connections = p.connections>
    <#if connections?size &gt; 0>
    <tr>
        <td colspan="5" style="font-size:smaller;">
        <table border="1" class="table flexibee-tbl-list table-striped table-hover table-condensed">
        <tr><th>Request</th><th>User</th><th>Age</th></tr>
        <#list connections as c>
        <tr>
            <td><pre style="margin:0;">${c.url}</pre></td>
            <td>${c.user}</td>
            <td style="white-space:nowrap; text-align:right;">${c.age} ms</td>
        </tr>
        </#list>
        </table>
        </td>
    </tr>
    </#if>
</#list>
</tbody>
<tfoot>
    <tr>
        <th>#${s.connPools?size}</th>
        <th colspan="4">&nbsp;</th>
    </tr>
</tfoot>
</#if>
</table>
<#if s_has_next><hr/></#if>
</#list>

<#include "/footer.ftl" />
</#escape>