<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=it.group.name/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<#if isMenu==true>
    <h2>${it.group.name}</h2>
</#if>

<p>${it.group.name}</p>

<#include "/footer.ftl" />
</#escape>
