<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if m.groupName == "obchPartneri">
  <div class="flexibee-search-cont">
       <#include "/evidence/adresar/i-search.ftl"/>
        </div>
</#if>

<#if m.groupName == "zbozi">
    <div class="flexibee-search-cont">
       <#include "/evidence/cenik/i-search.ftl"/>
  </div>
</#if>


<#if asRoot>
    <div class="row flexibee-root-menu">
<#else>
    <div class="row flexibee-root-menu">
</#if>

<#if !asRoot>
<#if cloudOverride.isTemplate("company", "menu-sub-" + m.groupName)>
<#include cloudOverride.include("company", "menu-sub-" + m.groupName) />
</#if>
</#if>

<#list m.subMenus as e>
  <#if e.getMenu(menuTool)?exists>
    <#if e.getMenu(menuTool).isEnabled>
        <#if !asRoot && e.rootMenu?has_content>
        </div>
        <hr/>
        <h3>${e.getMenu(menuTool).name}</h3>
        <!--FLEXIBEE:MENU:MAIN:START-->
        <div class="row flexibee-submenu">
            <#list e.rootMenu.subMenus as sub>
                <#if sub.getMenu(menuTool).isEnabled>
                        <a href="/c/${it.companyId}${sub.getMenu(menuTool).link}" class="flexibee-menu-grp-${sub.getMenu(menuTool).id} flexibee-show-wait flexibee-keynav flexibee-submenu-title-${sub.getMenu(menuTool).id?replace(".", "-")}">
                           <#if sub.getMenu(menuTool).image??><span class="${sub.getMenu(menuTool).image}"></span><#else><span class="icon icon-folder"></span></#if> <span>${sub.getMenu(menuTool).name}</span>
                        </a>
                </#if>
            </#list>
        <!--FLEXIBEE:MENU:MAIN:END-->
        <#else>
                <a href="/c/${it.companyId}${e.getMenu(menuTool).link}" class="col-md-2 col-xs-4 col-sm-3 flexibee-show-wait flexibee-menu-grp-${e.getMenu(menuTool).id} flexibee-keynav flexibee-clickable flexibee-menu-title-${e.getMenu(menuTool).id?replace(".", "-")}  <#if e_index == 0>flexibee-focus</#if>">
                   <#if e.getMenu(menuTool).image??><span class="${e.getMenu(menuTool).image}"></span><#else><span class="icon icon-folder"></span></#if> <span>${e.getMenu(menuTool).name}</span>
                </a>
        </#if>
    </#if>
  </#if>
</#list>

<#if cloudOverride.isTemplate("company", "menu-detail-" + m.groupName)>
<#include cloudOverride.include("company", "menu-detail-" + m.groupName) />
</#if>


    </div> <!-- flexibee-root-menu | flexibee-menu-title -->

    <#if cloudOverride.isTemplate("company", "menu-detail-after")>
    <#include cloudOverride.include("company", "menu-detail-after") />
    </#if>

</#escape>