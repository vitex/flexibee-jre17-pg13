<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=it.user.username/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />


<#if isMenu>
    <h2>${it.user.username}</h2>
</#if>

<div id="alert-placeholder"></div>

<form class="form-horizontal" role="form">
    <div class="form-group">
        <label class="col-sm-2 control-label" for="username">Přihlašovací jméno</label>
        <div class="col-sm-8 col-md-6 col-lg-5">
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user fa-fw"></i></div>
                <input type="text" id="username" name="username" class="form-control" value="${it.user.username}" readonly>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="givenName">Jméno</label>
        <div class="col-sm-4 col-md-3">
            <input type="text" id="givenName" name="givenName" class="form-control" value="${it.user.givenName}">
        </div>
        <label class="col-sm-1 control-label" for="familyName">Příjmení</label>
        <div class="col-sm-5 col-lg-4">
            <input type="text" id="familyName" name="familyName" class="form-control" value="${it.user.familyName}">
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="email">E-mail</label>
        <div class="col-sm-8 col-md-6 col-lg-5">
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></div>
                <input type="email" id="email" name="email" class="form-control" value="${it.user.email}">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="mobile">Mobil</label>
        <div class="col-sm-4 col-md-3">
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-mobile fa-fw"></i></div>
                <input type="tel" id="mobile" name="mobile" class="form-control" value="${it.user.mobile}">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="userType">${lblText("centServerUserType")}</label>
        <div class="col-sm-4 col-md-3">
            <select id="userType" name="userType" class="form-control" ${it.hasCreateUserPermission()?string("", "disabled")}>
                <option value="NORMAL" ${(it.user.userType == "NORMAL")?string("selected", "")}>${lblText("namedUserType.NORMAL")}</option>
                <option value="READ_ONLY" ${(it.user.userType == "READ_ONLY")?string("selected", "")}>${lblText("namedUserType.READ_ONLY")}</option>
                <option value="REST_API" ${(it.user.userType == "REST_API")?string("selected", "")}>${lblText("namedUserType.REST_API")}</option>
                <#if it.user.userType == "AUTOMATIC">
                <option value="AUTOMATIC" ${(it.user.userType == "AUTOMATIC")?string("selected", "")}>${lblText("namedUserType.AUTOMATIC")}</option>
                <#elseif it.user.userType == "REST_API_INACTIVE">
                <option value="REST_API_INACTIVE" ${(it.user.userType == "REST_API_INACTIVE")?string("selected", "")}>${lblText("namedUserType.REST_API_INACTIVE")}</option>
                </#if>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-2 control-label" for="blocked">Zablokování</label>
        <div class="col-sm-10 col-md-9 col-lg-8">
            <div class="input-group">
                <span class="input-group-addon">
                    <input type="checkbox" id="blocked" name="blocked" value="true" ${it.user.blocked?string("checked","")} ${it.hasCreateUserPermission()?string("", "disabled")}>
                </span>
                <input type="text" id="blockedText" name="blockedText" class="form-control" value="${it.user.blockedText}" placeholder="Napište důvod zablokování účtu..." ${(it.user.blocked && it.hasCreateUserPermission())?string("","disabled")}>
            </div>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary">${btnText("ulozit")}</button>
        </div>
    </div>
</form>

<script>
$("form").submit(function(event) {
    event.preventDefault();
    $.showWait();

    $("#alert-placeholder").hide().empty();

    function showMessage(style, message) {
        var icon_style = (style == 'success') ? 'fa-info-circle' : 'fa-times-circle';
        var el = '<div class="alert alert-' + style + '" role="alert"><i class="fa ' + icon_style + ' fa-fw fa-mr"></i>' + message + '</div>';
        $("#alert-placeholder").append(el).show('fade', 200);
    }

    $.ajax({
        type: "POST",
        url: "/u/${it.user.username}?token=${csrfToken}",
        async: false,
        data: $(this).serialize(),
        dataType: "json",
        success: function(result) {
            if (result.success) {
                showMessage("success", "Změny uloženy.");
            } else {
                showMessage("danger", result.errors.reason);
            }
        }
    });

    $.hideWait();
});

$("#blocked").on("change", function() {
    if ($(this).prop("checked")) {
        $("#blockedText").prop("disabled", false).focus().select();
    } else {
        $("#blockedText").prop("disabled", true);
    }
});
</script>

<#include "/footer.ftl" />
</#escape>
