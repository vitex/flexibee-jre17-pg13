<#-- @ftlvariable name="propsList" type="java.util.List<cz.winstrom.net.server.services.data.PropertyDescriptorsTree>" -->
<#-- @ftlvariable name="idPattern" type="java.lang.String" -->
<#-- @ftlvariable name="extIdPattern" type="java.lang.String" -->
<#-- @ftlvariable name="moreIdsPattern" type="java.lang.String" -->
<#-- @ftlvariable name="isForImport" type="java.lang.Boolean" -->
<#import "schemas-library.ftl" as my/>
<#escape x as (x!"")?xml>
<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
    <#--
      - Postup:
      - 1. vzdy se rovnou definuji typy fbId, fbBoolean (logicka hodnota) a pripadne fbImportMode (pokud jde o importni
      -    schema), protoze definovat je pokazde znovu anonymne by bylo pekelne neprehledne
      - 2. pro vsechny polozky i podpolozky se (rekurzivne) definuji typy s nazvy ve tvaru <polozka><podpolozka>Type;
      -    definovat typy anonymne az v miste pouziti nelze, protoze obcas je potreba definice na dva kroky: nejdriv
      -    restrikce zakladniho typu s nazvem <polozka><podpolozka>TypeInner a pak teprve vysledny komplexni typ
      -    <polozka><podpolozka>Type, ktery extenduje <polozka><podpolozka>TypeInner o potrebne atributy
      - 3. definuje se element winstrom, jako jeho obsah v danem poradi vsechny polozky a podpolozky (rekurzivne)
      -    s prislusnymi typy (a atributy, v pripade importu), a nakonec samozrejme atribut version
      -->

    <@my.defineBasicTypes isForImport=isForImport/>

    <#list propsList as props>
        <@my.defineTypes typeNamePrefix="${props.identifier}_" props=props isForImport=isForImport/>
    </#list>

    <xs:element name="winstrom">
        <xs:complexType>
            <xs:choice minOccurs="0" maxOccurs="unbounded">
                <#list propsList as props>
                    <@my.buildSchema typeNamePrefix="${props.identifier}_" props=props isForImport=isForImport/>
                </#list>
            </xs:choice>
            <xs:attribute name="version">
                <xs:simpleType>
                    <xs:restriction base="xs:string">
                        <xs:enumeration value="1.0"/>
                    </xs:restriction>
                </xs:simpleType>
            </xs:attribute>
            <#if !isForImport>
                <xs:attribute name="rowCount" type="xs:integer"/>
            </#if>
        </xs:complexType>
    </xs:element>
</xs:schema>
</#escape>
