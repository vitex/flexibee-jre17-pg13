<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'toolbars.title', 'Toolbar')/>
<#assign titleId="root"/>
<#include "/header.ftl"/>

<p>Tento přehled slouží k&nbsp;výpisu obsahu do toolbaru.</p>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl"/>
</#escape>
