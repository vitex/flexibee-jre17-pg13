<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=it.bundleAccessor.getMessage('setting', 'Nastavení')/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<#if isMenu==true>
    <h2>${it.bundleAccessor.getMessage('setting', 'Nastavení')}</h2>
</#if>

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
        <tr>
            <th>Variable</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
    <#list it.list as l>
        <tr><th>${l.key}</a></th>
        <td><#if l.value??><#if l.value?is_boolean><#if l.value>true<#else>false</#if><#else>${l.value}</#if><#else><i>-</i></#if></td>
        </tr>
    </#list>
    </tbody>
</table>


<#include "/footer.ftl" />
</#escape>
