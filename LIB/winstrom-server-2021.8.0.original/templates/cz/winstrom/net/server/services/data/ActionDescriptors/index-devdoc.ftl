<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'actions.title', 'Přehled akcí')/>
<#assign titleId="root"/>
<#include "/header.ftl"/>

<p><#noescape>${lang('labels', 'actions.description', 'Tento přehled slouží k&nbsp;výpisu podporovaných akcí.')}</#noescape></p>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl"/>
</#escape>
