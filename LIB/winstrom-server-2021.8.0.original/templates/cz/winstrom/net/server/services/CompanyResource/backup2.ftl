<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('formsInfo', 'zalohaDB', 'Záloha firmy')+" "+lang('formsInfo', it.name, it.name) />
<#assign titleId="root"/>
<#assign useNaviPath=false />
<#assign showMenuAtEnd=false />
<#assign headerAddons>
<meta http-equiv="Refresh" content="1; URL=/c/${it.companyId}/backup">
</#assign>

<#include "/header.ftl" />

<div class="flexibee-dialog">
    <br/>
    <div class="flexibee-wait">${lang('labels', 'zalohaProgressTitle', 'Zálohuji firmu...')}</div>
    <br/>
    <div class="flexibee-wait">${lang('labels', 'zalohaProgressDownloadTitle', 'Záloha bude po chvíli nabídnuta ke stažení.')}</div>
    <br/>
</div>

<#include "/footer.ftl" />
</#escape>