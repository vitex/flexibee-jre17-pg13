<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('formsInfo', it.name, it.name) />
<#assign useNaviPath=false />
<#assign showMenuAtEnd=false />
<#include "/header-html-start.ftl" />

<#assign asRoot=false/>
<#-- podpora již vybraného menu -->
<#if menuTool().selectedMenu?exists>
    <#assign m = menuTool().selectedMenu/>
    <#include "i-menu-detail.ftl"/>
<#else>
    <#assign m = menuTool().root/>
    <#assign asRoot=true/>
  <#include "i-menu.ftl"/>
  <#assign asRoot=false/>

    <#if useTouch == true && isMobile == true && false>
        <#list menuTool().allMenus as m>
            <#if m.groupName != 'root'>
                <#include "i-menu-detail.ftl"/>
            </#if>
        </#list>
    </#if>

</#if>

<#include "/footer-html-end.ftl" />
</#escape>