<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'xxx', 'Přehled otevřených firem') />
<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<#if it.totalCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>Uzel</th>
        <th>Instance</th>
        <th colspan="2">${lang('labels', 'loginDialogUzivatel', 'Uživatel')}</th>
        <th colspan="3">Firma (projekt) &mdash; licence</th>
    </tr>
    </thead>
    <tbody style="white-space: nowrap;">
    <#assign prevInstanceId = "" prevUsername = "">
    <#list it.list as l>
    <tr<#if l_index % 2 == 0> class="flexibee-even"</#if>>
        <td>${l.node}</td>
        <td>${l.instanceId}</td>
        <td>${l.username}</td>
        <#if prevInstanceId == l.instanceId && prevUsername == l.username>
        <td>&emsp;&emsp;&and;_&and;</td>
        <#else>
        <#assign prevInstanceId = l.instanceId prevUsername = l.username>
        <td>
            <form action="/status/user/${l.username}/logout" method="post">
                <input type="hidden" name="token" value="${csrfToken}">
                <input type="hidden" name="instance" value="${l.instanceId}">
                <input type="submit" value="Odhlásit" class="btn btn-danger">
            </form>
        </td>
        </#if>
        <#--
        <td><ul><#list l.authentication as a><li><a href="/status/session/${a.authSessionId}"><tt>${a.authSessionId}</tt></a> &ndash; ${a.clientType}</li></#list></ul></td>
        -->
        <td><tt>${l.companyId}</tt></td>
        <td>${l.licenseDesc}</td>
        <td>
            <form action="/status/user/${l.username}/logout?company=${l.companyId}" method="post">
                <input type="hidden" name="token" value="${csrfToken}">
                <input type="hidden" name="instance" value="${l.instanceId}">
                <input type="submit" value="Uvolnit" class="btn btn-info">
            </form>
        </td>
    </tr>
    </#list>
    </tbody>
    <tbody>
    <tr>
        <th colspan="7"># ${it.totalCount}</th>
    </tr>
    </tbody>
</table>
<#else>
<p>Na webu není otevřena žádná firma.</p>
</#if>


<#include "/footer.ftl" />
</#escape>