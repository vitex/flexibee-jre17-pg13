<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'connections.list', 'Přehled přihlášených uživatelů') />

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <tr>
        <th>Client ID</th>
        <td>${it.authSessionId}</td>
    </tr>
    <tr>
        <th>Instance ID</th>
        <td>${it.instanceId}</td>
    </tr>
    <tr>
        <th>${lang('labels', 'loginDialogUzivatel', 'Uživatel')}</th>
        <td>${it.username}</td>
    </tr>
    <tr>
        <th>Typ klienta</th>
        <td>${it.clientType}</td>
    </tr>
    <tr>
        <th>Čas přihlášení</th>
        <td>${it.createDate?datetime}  (${it.createDateAsString})</td>
    </tr>
    <tr>
        <th>Verze</th>
        <td>${it.version}</td>
    </tr>
    <tr>
        <th>Poslední změna</th>
        <td>${it.lastUpdate?datetime} (${it.lastUpdateAsString})</td>
    </tr>
    <tr>
        <th>Poslední obnovení</th>
        <td>${it.keepAlive?datetime} (${it.keepAliveAsString})</td>
    </tr>
    <tr>
        <th>Počet využití</th>
        <td>${it.usageCount}</td>
    </tr>
    <tr>
        <th>Instance admin</th>
        <td>${it.instanceAdmin?string("Ano", "Ne")}</td>
    </tr>
    <tr>
        <th>Použita licence</th>
        <td>${it.ignoreForLicense?string("Vyloučeno" , it.consumesLicense?string("Ano", "Ne"))}${it.obsolete?string(" (odloženo)", "")}${it.loggedOut?string(" (odhlášeno)", "")}</td>
    </tr>
    <tr>
        <th>Adresy</th>
        <td><#list it.addresses as addr>${addr}<#if addr_has_next>, </#if></#list></td>
    </tr>
</table>

<p><form action="/status/session/${it.authSessionId}/logout" method="POST">
    <input type="hidden" name="token" value="${csrfToken}">
    <input type="submit" class="btn btn-danger" value="Odhlásit" name="">
</form></p>

<#include "/footer.ftl" />
</#escape>