<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=it.bundleAccessor.getMessage('users', 'Osoby a uživatelé')/>

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
<thead>
    <tr>
        <th>Přihlašovací jméno</th>
        <th>Zablokování</th>
        <th>Jméno</th>
        <th>Příjmení</th>
        <th>Email</th>
        <th>Mobil</th>
        <th>${lblText("centServerUserType")}</th>
        <th>Datum vytvoření</th>
        <th>Licenční skupina</th>
    </tr>
</thead>
<tbody>
    <#list it.list as l>
        <tr <#if l.blocked>class="blocked-user"</#if>>
            <td><#if l.blocked><del></#if><a href="/u/${l.username}">${l.username}</a><#if l.blocked></del></#if></td>
            <td title="${l.blockedText}">${l.blocked?string(btnText('ano'), btnText('ne'))}</td>
            <td>${l.givenName}</td>
            <td>${l.familyName}</td>
            <td>${l.email}</td>
            <td>${l.mobile}</td>
            <td>${lblText("namedUserType." + l.userType)}</td>
            <td>${l.createDt?date}</td>
            <td>${l.licenseGroup}</td>
        </tr>
    </#list>
</tbody>
</table>

<#include "/footer.ftl" />
</#escape>
