<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'relations.title', 'Přehled relací')/>
<#assign titleId="root"/>
<#include "/header.ftl"/>

<p>Tento přehled slouží k&nbsp;přehledu vazeb mezi objekty.</p>
<p>URL se tvoří tak, že vezmete URL záznamu (např. faktury) a k&nbsp;němu přidáte jméno relace</p>

<p><code>/c/demo/adresar/1/kontakty</code></p>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl"/>
</#escape>
