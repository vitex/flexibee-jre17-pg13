<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lang('dialogs', 'connections.list', 'Přehled připojených uživatelů') />

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title leftMenu="/i-left-menu-admin.ftl">
</@tool.showTitle>
<@tool.showTitleFinish />

<#if it.totalCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>${lang('labels', 'loginDialogUzivatel', 'Uživatel')}</th>
        <th>Client ID</th>
        <th>Company ID</th>
        <th>Session ID</th>
        <th>Počet</th>
        <th>Typ</th>
    </tr>
    </thead>
    <tbody>
    <#list it.list as l>
    <tr>
        <td>${l.user}</td>
        <td><a href="/status/session/${l.authSessionId}">${l.authSessionId}</a></td>
        <td>${l.companySessionId.companyId}</td>
        <td>${l.companySessionId.sessionId}</td>
        <td>${l.count}</td>
        <td>${l.connectionType}</td>
    </tr>
    </#list>
    </tbody>
    <tbody>
    <tr>
        <th># ${it.totalCount}</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>&nbsp;</th>
        <th>${it.totalConnectionCount}</th>
        <th></th>
    </tr>
    </tbody>
</table>
<#else>
<p>Není přihlášen žádný uživatel.</p>
</#if>

<#include "/footer.ftl" />
</#escape>