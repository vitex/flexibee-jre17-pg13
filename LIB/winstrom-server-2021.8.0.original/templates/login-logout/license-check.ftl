<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="problém s licencí"/>
<#include "/error/header.ftl" />

<div class="flexibee-note-error">
        <p><#noescape>${errorMessage}</#noescape></p>
        <a class="btn btn-primary flexibee-clickable" href="${origUrl}">${lang('buttons', 'goBack', 'Zpět')}</a>
</div>

<#include "/error/footer.ftl" />

</#escape>