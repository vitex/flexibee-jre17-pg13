<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title=lblText('prvniSpusteniNadpis') />

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title menu=false showLogoInTitle=true showPath=false>
</@tool.showTitle>
<@tool.showTitleFinish />


<#if cloudOverride.isTemplate("root", "content")>
<#include cloudOverride.include("root", "content") />
</#if>
<div class="vertical-center-parent maxwidth2">
<div class="vertical-center">
<div class="desktop-login desktop-first-setup desktop-content">
    <form action="/login-logout/first-user" method="post" id="first-setup-form" data-parsley-validate >
        <div class="alice">
        <h4>${lblText('prvniSpusteniNadpis')}</h4>
        <p>${lblText('prvniSpusteniPopis')}</p>

        <div id="error-placeholder">
        </div>

        <div class="form-group">
            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                    <input type="text" name="username" value="${it.defaultUsername}" class="form-control input-lg focusFirst" data-parsley-required="true" placeholder="${lblText('prvniSpusteniLogin')}" id="user" required />
                </div>
            </div>
            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input type="password" name="password" value="" class="form-control input-lg" autocomplete="off"  placeholder="${lblText('prvniSpusteniHeslo')}" id="password" required />
                </div>
            </div>
            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input type="password" name="password2" value="" class="form-control input-lg" autocomplete="off"  placeholder="${lblText('prvniSpusteniHesloZnovu')}" id="password2" required />
                </div>
            </div>

        </div>
        </div>

        <div class="">
                <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="${btnText('prvniSpusteniVytvorit')}" id="submit" />
        </div>
    </form>


        <p class="links nav text-center"><a href="/login-logout/howto"><#if isCloud>${lblText('loginWithDesktopAppToCloud')}<#else>${lblText('loginWithDesktopApp')}</#if></a></p>

        <div class="footer hidden-xs">
            <p class="links text-center">
                <a href="http://www.flexibee.eu">${lblText('oProgramu')}</a> |
                <a href="http://www.flexibee.eu/eula" class="link">${lblText('eula')}</a>
            </p>
        </div>
</div>
</div>
</div>

<script>
$(document).ready(function() {
    $("#first-setup-form").submit(function() {
        $.showWait();

        $.ajax({
             type: "POST",
             dataType: 'json',
             url: "/login-logout/first-user.json?token=${csrfToken}",
             data: $(this).serialize(),
             success: function(data) {
                 if (data.success == true) {
                     window.location = '/';
                 } else {
                     var el = '<div class="alert alert-danger" role="alert"><i class="fa fa-exclamation-triangle"></i> ' + data.errors.reason + '</div>';
                     $('#error-placeholder').empty();
                     $(el).appendTo('#error-placeholder');
                 }
                 $.hideWait();
             },
             failure: function(data) {
                 $.hideWait();
             }
           });

        return false;
    });
    $(".body").flexiBeeWallpaper();
});
</script>

<#include "/footer.ftl" />
</#escape>
