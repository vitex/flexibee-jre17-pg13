<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="ABRA Flexi" />

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title menu=false showLogoInTitle=true showPath=false>
</@tool.showTitle>
<@tool.showTitleFinish />


<#if cloudOverride.isTemplate("root", "content")>
<#include cloudOverride.include("root", "content") />
</#if>
<div class="vertical-center-parent maxwidth">
<div class="vertical-center">
<div class="desktop-login">
<div class="desktop-content">
    <h4>${licenseHelper.getApplicationLicense().name}</h4>

    <form action="/login-logout/login" method="post" id="login-form" data-parsley-validate >

        <div id="error-placeholder"></div>

        <div class="form-group">
            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                    <input type="text" name="username" value="${it.defaultUsername}" class="form-control input-lg focusFirst" data-parsley-required="true" placeholder="${lang('labels', 'prvniSpusteniLogin', 'Jméno')}" id="user" required />
                </div>
            </div>
            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                    <input type="password" name="password" value="" class="form-control input-lg" autocomplete="off"  placeholder="${lang('labels', 'prvniSpusteniHeslo', 'Heslo')}" id="password" required />
                </div>
            </div>

            <#if it.authGlobalInformation.useOTP>
            <div class="control-group">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-ticket fa-fw"></i></span>
                    <input type="text" name="otp" value="" class="form-control input-lg" autocomplete="off"  placeholder="${lang('labels', 'OTP', 'OTP')}" required />
                </div>
            </div>
            </#if>
        </div>

        <#if it.showVersionWarning>
            <div id="snapshotWarning" class="alert alert-danger" role="alert">
                ${msgText('versionWarning', it.defaultVersionWarning)}
           </div>
        </#if>

        <div id="normalSubmit">
                <input type="submit" name="submit" class="btn btn-primary btn-lg btn-block" value="${lang('buttons', 'prihlasit', 'Přihlásit')}" />
        </div>

        <#if it.showVersionWarning>
            <div id="warningSubmit" class="flexibee-dialog-buttons">
                <input type="submit" name="warningSubmitBT" class="btn" value="${lang('buttons', 'poskoditData')}" />
                <input type="button" name="warningQuitBT" class="btn btn-primary" value="${lang('buttons', 'ukoncit')}" />
            </div>
        </#if>
      <input type="hidden" id="licenseChecked" name="licenseChecked" value="false">
    </form>

    <p class="links nav text-center"><a href="/login-logout/howto"><#if isCloud>${lblText('loginWithDesktopAppToCloud')}<#else>${lblText('loginWithDesktopApp')}</#if></a></p>

        <div class="footer hidden-xs">
            <p class="links text-center">
                <a href="http://www.flexibee.eu">${lblText('oProgramu')}</a> |
                <a href="http://www.flexibee.eu/eula" class="link">${lblText('eula')}</a>
            </p>
        </div>
</div>
</div>
</div>
</div>

<script>

var loginData = {};

// zajistuje zobrazeni login dialogu v urcitem stavu
var show = {

    // ma se warning zobrazit?
    shouldShowVersionWarning: ${it.showVersionWarning?string},

    // je ted warning zobrazeny?
    isVersionWarningOn: false,

    // cache pro elementy hledane pomoci jQery
    snapshotWarning: null,
    warningSubmit: null,
    warningSubmitBT: null,
    warningQuitBT: null,
    normalSubmit: null,
    normalSubmitBT: null,
    nameInput: null,
    passwordInput: null,

    focus: null,

    init: function() {

        this.snapshotWarning = $("#snapshotWarning");

        this.warningSubmit = $("#warningSubmit");
        this.warningSubmitBT = $("input[name=warningSubmitBT]");
        this.warningQuitBT = $("input[name=warningQuitBT]");

        this.normalSubmit = $("#normalSubmit");
        this.normalSubmitBT = $("input[name=submit]");

        this.nameInput = $("input[name=username]");
        this.passwordInput  = $("input[name=password]");

        this.focus = this.nameInput;
    },

    newLogin: function() {

        if(this.shouldShowVersionWarning) {
            this.isVersionWarningOn = false;

            this.snapshotWarning.hide();
            this.warningSubmit.hide();
            this.normalSubmit.show();
        }

        var nameFilled = this.nameInput.val() != null && this.nameInput.val().length != 0;
        var pswdFilled = this.passwordInput.val() != null && this.passwordInput.val().length != 0;

        if(nameFilled && pswdFilled) {
            this.focus = this.normalSubmitBT;
        } else if(nameFilled) {
            this.focus = this.passwordInput;
        } else if(pswdFilled) {
            this.focus = this.nameInput;
        } else {
            this.focus = this.nameInput;
        }

        this.setFocus();
    },

    versionWarning: function() {

        if(this.shouldShowVersionWarning) {

            this.isVersionWarningOn = true;

            this.snapshotWarning.show();
            this.warningSubmit.show();
            this.normalSubmit.hide();
            this.normalSubmit.focus();

            this.warningQuitBT.focus();
        }
    },

    loginFailed: function() {
        this.newLogin();

        this.focus = this.passwordInput;
        this.setFocus();

    },

    canSubmit: function() {
        if(this.shouldShowVersionWarning && !this.isVersionWarningOn) {
            return false;
        }

        return true;
    },

    setFocus: function(element) {
        var _this = this;

        // http://stackoverflow.com/a/7996630
        window.setTimeout(function(){
            _this.focus.focus();
        }, 300);
    }
};

$(document).ready(function() {

    show.init();
    show.newLogin();

    $("input[name=warningQuitBT]").click(function() {
        window.location.reload();
    });

    $("#login-form").submit(function() {

        loginData = $(this).serialize();

        if(!show.canSubmit()) {
            show.versionWarning();
            return false;
        }

        $.showWait();

        $.ajax({
            type: "POST",
             dataType: 'json',
             url: "/login-logout/login.json?token=${csrfToken}",
             data: loginData,
             success: function(data) {
                 if (data.success == true) {
                     <#noescape>
                     window.location = "${it.returnUrl}" + window.location.hash;
                     </#noescape>
                 } else {
                    show.loginFailed();
                     $.hideWait();
                     if(data.errors) {
                        var el = '<div class="alert alert-danger" role="alert"><i class="fa fa-exclamation-triangle"></i> ' + data.errors.reason + '</div>';
                        $('#error-placeholder').empty().append(el);
                     }else if(data.warnings){
                        var el = '<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-triangle"></i> ' + data.warnings.reason + '</div>';
                        $('#error-placeholder').empty().append(el);

                        //nastavení pomocného příznaku, že už jsme jednou získali v rámci kontroly licence warning
                        $('#licenseChecked').val("true");
                     }
                     $('div.desktop-login').effect('shake', { distance: 10, times: 2 }, 300);
                 }
             }
           });

        return false;
    });

    $(".body").flexiBeeWallpaper();
});
</script>

<#include "/footer.ftl" />
</#escape>
