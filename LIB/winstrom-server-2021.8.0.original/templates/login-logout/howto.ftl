<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="ABRA Flexi" />

<#include "/header-html-start.ftl" />

<@tool.showTitle title=title menu=false showLogoInTitle=true showPath=false>
</@tool.showTitle>
<@tool.showTitleFinish />


<#if cloudOverride.isTemplate("root", "content")>
<#include cloudOverride.include("root", "content") />
</#if>

<div class="row">
<div class="col-md-3 hidden-xs"></div>
<div class="col-md-6 col-xs-12">
<div class="desktop-content">
    <h1>${licenseName}</h1>

    <p>Klientská aplikace <#if isCloud>pro&nbsp;cloud</#if> umožňuje plnohodnotný přístup k&nbsp;systému ABRA Flexi. Na rozdíl od zjednodušené webové verze obsahuje všechny funkce a je preferovaná pro účetní a náročné uživatele.</p>

        <img src="https://www.flexibee.eu/static/img/muj-desktopova.png" />

     <p>
        Pro provoz klientské aplikace <#if isCloud>v&nbsp;cloudu</#if> je nutné provést instalaci aplikace.
         Aplikaci nainstalujte v&nbsp;režimu <code>Síťová instalace</code> a <code>Klient</code>.
    </p>

         <div style="padding-bottom:1em">
         <script type="text/javascript" src="https://www.flexibee.eu/static/js/download.js.php?client=true" ></script>
         </div>

      <p>Případně postupujte dle <a href="https://www.flexibee.eu/podpora/dokumentace/instalacni-prirucka/">instalační příručky</a> nebo kontaktujte <a href="https://www.flexibee.eu/podpora/reakce/">technickou podporu</a>.</p>

      <ul class="nav nav-tabs" data-tabs="tabs" role="tablist">
        <li class="active"><a href="#auto" data-toggle="tab"><span class="glyphicon glyphicon-ok"></span> Automatické nastavení</a></li>
        <li><a href="#manual" data-toggle="tab"><span class="glyphicon glyphicon-wrench"></span> Ruční nastavení</a></li>
      </ul>

      <div id="quick-setup-tab-content" class="tab-content">
         <div class="tab-pane active" id="auto">

            <a href="/login-logout/connection-file.flexibee-connection" class="btn btn-default"><i class="fa fa-download"></i> Uložit konfigurační soubor</a>



            <#if false>
            <br/>
            <br/>
            <br/>
             <ul>
                 <li><a href="flexibee://${it.defaultUsername}@${serverName}:${serverPort?string("0")}/">flexibee://${it.defaultUsername}@${serverName}:${serverPort?string("0")}</a></li>
                 <li><a href="/login-logout/connection-file.flexibee-connection">server.flexibee-connection</a></li>
             </ul>
             </#if>

        </div>
        <div class="tab-pane" id="manual">
          <ol>
            <li>Pustťe aplikaci ABRA Flexi.</li>
            <li>Při zobrazení přihlašovacího formuláře, zvolte ikonku <i class="glyphicon glyphicon-cog"></i> a otevřete <code>Nastavení datových zdrojů</code>.</li>
            <li>Přidejte nový datový zdroj na server <span class="nabidka"><code>${serverName}</code></span> a s&nbsp;portem <span class="nabidka"><code>${serverPort?string("0")}</code></span> (více o <a href="https://www.flexibee.eu/produkty/flexibee/dokumentace/reseni/konfigurace-datovych-zdroju/">správě datových zdrojů</a>).</li>
        </ol>

        </div>
      </div>
      <div id="measure-time" class="pull-right" style="color:grey"></div>

</div>
</div>
</div>

<script>
$(document).ready(function() {
    $(".body").flexiBeeWallpaper();

    function measureTime() {
        var start = new Date().getTime();
        $.ajax({
          url: "/status/measure.json",
          dataType: "json"
        }).done(function() {
            var end = new Date().getTime();
            var time = end - start;
            $("#measure-time").html(time+' ms');

        }).always(function(a, e) {
            setTimeout(function() {
                measureTime();
            }, 5000);
        });
    }

    setTimeout(function() {
        measureTime();
    }, 5000);
});
</script>

<#include "/footer.ftl" />
</#escape>
