<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<script type="text/javascript">

    $('#flexibee-inp-osobUpravaDph').change(
        function(){
            var label = $('label[for=flexibee-inp-osobUpravaDphDodavatel]');
            var input = $('#flexibee-inp-osobUpravaDphDodavatel');

            if (!$(this).is(':checked')) {
                input.prop('checked', false);
                label.hide();
                input.hide();
            } else {
                label.show();
                input.show();
            }
    });

</script>

</#escape>
