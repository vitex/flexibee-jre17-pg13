<#ftl strip_text=true encoding="utf-8"  strip_whitespace=true>
<#escape x as x?default('')?html>
<#--

Nástroje pro zjednoduššení editace

-->

<#macro beginForm multipart=false queryParams={}>
</#macro>

<#macro endForm>
</#macro>

<#macro beginForm2 multipart=false query="">
<#assign formAction = "${baseUrl}?token=${csrfToken}"/>
<#if query != "">
    <#assign formAction = "${formAction}&${query}"/>
</#if>
<form action="${formAction}" method="post" enctype="<#if multipart>multipart/form-data<#else>application/x-www-form-urlencoded</#if>" class="flexibee-form ">
<div id="flexibee-fb"></div>
</#macro>

<#macro endForm2>
</form>
</#macro>

<#macro placeErrorBegin myItem marker>
    <#local name = myItem.propertyName>
    ${marker.markEdit(name)}
    <#if it.editHandler?? && it.editHandler.getErrorsFor(name)??>
    <div class="alert alert-danger">
    </#if>
</#macro>

<#macro placeErrorEnd myItem>
    <#local name = myItem.propertyName>
    <#if it.editHandler?? && it.editHandler.getErrorsFor(name)??>
    <ul class="flexibee-errors">
    <#list it.editHandler.getErrorsFor(name) as e>
        <li>${e.message}</li>
    </#list>
    </ul>
    </div>
    </#if>
</#macro>

<#macro place object name items marker prefix="" level="1" onlyShow=false type="">
<#compress>
    <#if !items[name]??>
        <#return>
    </#if>

    <#assign myItem = items[name] />

    <#if !myItem.showToUser> <#-- TODO writable/overwritable -->
        <#return>
    </#if>

    <#assign addonClass = "flexibee-focus"/>

    <#if marker.getMarkCount(name) &gt; 0>
        <#assign idCount = "-dup" + marker.getMarkCount(name) />
        <#assign addonClass = addonClass + " flexibee-dup"/>
    <#else>
        <#assign idCount = ""/>
    </#if>

    <#if level != "1">
        <#assign addonClass = addonClass + " flexibee-level-"+level />
    </#if>

    <#if myItem.isUpperCase && myItem.type != "relation"> <#-- v inputech pro vazby nechci vynucovat uppercase -->
        <#assign addonClass = addonClass + " flexibee-inp-uppercase"/>
    </#if>

    <#assign disabled = ""/>

    <#local pname = myItem.propertyName>
    <#local valpname = myItem.originalPropertyName!myItem.propertyName>
    <#if object[valpname]??>
        <#local val = object[valpname]>
    <#elseif object.viewProperties??>
        <#local val = object.getValue(valpname)>
    </#if>

    <#assign namePrefix = prefix/>
    <#assign idPrefix = prefix/>
    <#if !myItem.isWritable
            || (myItem.type = 'select' && it.editHandler.hasValueAndItIsUnknown(pname, val, myItem.possibleValuesMap))>
        <#assign disabled = "disabled"/>
        <#assign namePrefix = namePrefix + "disabled-"/>
    </#if>

    <#if onlyShow>
        <#-- TODO tohle je momentálně jen náznak, bude potřeba vypisovat i hodnotu (pozor na různé datové typy) a asi podporovat totéž v makru textarea -->
        <span class="${addonClass}" id="flexibee-inp-${idPrefix}${pname}${idCount}"></span>
        <#return>
    </#if>
    <#assign addonClass = addonClass + " flexibee-inp"/>

<#if it.editHandler.getValue(pname)?? || val??>
    <input type="hidden" name="${namePrefix}${pname}-hadValue" value="1">
</#if>

    <@placeErrorBegin myItem marker/>
<#if myItem.type = 'date'>
    <div class="input-group">
        <input type="text" class="${addonClass} form-control flexibee-edit-date" format="dd.mm.yyyy" name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}" value="<#if it.editHandler.getValue(pname)??>${it.editHandler.getValue(pname)}<#else><#if val??>${val.time?date}</#if></#if>" ${disabled} />
        <div class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></div>
    </div>
<#elseif myItem.type = 'datetime'>
    <div class="input-group">
        <input type="text" class="${addonClass} form-control flexibee-edit-datetime" name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}"  value="<#if it.editHandler.getValue(pname)??>${it.editHandler.getValue(pname)}<#else><#if val??>${val.time?datetime}<#else></#if></#if>" ${disabled} />
        <div class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></div>
    </div>
<#elseif myItem.type = 'integer' || myItem.type = 'numeric'>
    <#-- originál: input type="number" step="any" <#if myItem.minValue??>min="${myItem.minValue}"</#if> <#if myItem.maxValue??>max="${myItem.maxValue}"</#if> ... -->
    <#-- bohužel type="number" je zoufale nepoužitelné -->
    <input type="text" <#if myItem.digits??>maxlength="${myItem.digits}"</#if> class="${addonClass} form-control " name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}" value="<#if it.editHandler.getValue(pname)??>${it.editHandler.getValue(pname)}<#elseif val??>${val?c}</#if>" ${disabled} />
<#elseif myItem.type = 'logic'>
    <input type="hidden" name="${namePrefix}${pname}-present" value="present" />
    <input type="checkbox" class="${addonClass} flexibee-form-check-input" name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}" value="true" <#if it.editHandler.getValue(pname)?? && it.editHandler.getValue(pname) = "true">checked="checked"<#else><#if val?? && val>checked="checked"</#if></#if> ${disabled} />
<#elseif myItem.type = 'select'>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-list-alt fa-fw"></i></div>
        <select class="${addonClass} form-control" name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}" ${disabled}>
        <#if !it.editHandler.isLokCisMandatory(pname)>
            <option value="" <#if !it.editHandler.getValue(pname)?? && !val??>selected="selected"</#if>>---</option>
        </#if>
        <#assign possibleValues = it.editHandler.getLokCisPossibleValues(pname)! />
        <#if possibleValues??>
        <#list possibleValues as pv>
            <#local isSelected = it.editHandler.getValue(pname)?? && it.editHandler.getValue(pname) = pv.key || val?? && val = pv.key>
            <option value="${pv.key}"<#if isSelected> selected="selected"</#if>>${pv.name}</option>
        </#list>
        </#if>
        </select>
    </div>
<#elseif myItem.type = 'relation'>
    <div class="input-group">
        <div class="input-group-addon"><i class="fa fa-list-alt fa-fw "></i></div>
        <#if !myItem.isWritable>
        <input type="text" class="${addonClass} form-control" name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}" value="<#if it.editHandler.getValue(pname)??>${it.editHandler.getValue(pname)}<#else>${val}</#if>" disabled />
        <#else>

        <#if it.editHandler.getUseInlineList(pname)>
        <select class="${addonClass} form-control" name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}" ${disabled}>
        <#-- rnovacek: I pro povinné vazby bych ponechal "neurčenou" položku, protože jinak se hodnota přednastaví první položkou ze seznamu
               a takové chování je dle mého podivné. Zatím změněno pouze pro vazby na CENIK. -->
        <#if !items[pname].mandatory || items[pname].fkEvidenceType == "CENIK">
            <option value="" <#if !it.editHandler.getValue(pname)?? && !val??>selected="selected"</#if>>---</option>
        </#if>
        <#local valOptionIsPresent = false>
        <#list it.editHandler.getInlineList(pname) as pv>
            <#if pv??>
                <#local itemDesc>
                <#compress>
                    <#if pv.viewProperties??>
                        <#if pv.getValue("kod")?? && pv.getValue("nazev")??>
                           ${pv.getValue("kod")}: ${pv.getValue("nazev")}
                        <#elseif pv.getValue("jmeno")?? && pv.getValue("prijmeni")??>
                           ${pv.getValue("titul")!} ${pv.getValue("jmeno")} ${pv.getValue("prijmeni")}
                        <#else>
                           ${pv?string}
                        </#if>
                    <#else>
                        ${pv?string}
                    </#if>
                </#compress>
                </#local>
                <#local isSelected = it.editHandler.getValue(pname)?? && it.editHandler.getValue(pname) == pv.id?c || val?? && val.id?c == pv.id?c>
                <#if isSelected><#local valOptionIsPresent = true></#if>
                <option value="${pv.id?c}"<#if isSelected> selected="selected"</#if>>${itemDesc}</option>
            <#else>
                <option disabled="disabled">&ndash;</option>
            </#if>
        </#list>
        <#if val?? && !valOptionIsPresent>
           <option value="$val.id?c" disabled="disabled" selected="selected">${val}</option>
        </#if>
        </select>

        <div class="input-group-addon relation-button" style="display:none;" >
            <a href="#" data-toggle="dropdown">
                <span class="fa fa-th-list fa-fw"></span>
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" style="float:right;right:0;left:auto"><!-- akce doplni --></ul>
        </div>
    </div>
    <#if myItem.fkEvidenceAccessible>
    <@tool.showRelating myItem.fkEvidenceType "flexibee-inp-${namePrefix}${pname}${idCount}" />
    </#if>
    <#else>
            <#assign initialValue><#if it.editHandler.getValue(pname)??>${it.editHandler.getObject(pname, it.editHandler.getValue(pname)).id?c}<#elseif val??>${val.id?c}</#if></#assign>
            <input type="text" name="${namePrefix}${pname}${idCount}" data-placeholder="${myItem.name}" id="flexibee-inp-${idPrefix}${pname}${idCount}" class="flexibee-flexbox ${addonClass}" value="${initialValue}">
        </div>
    <script type="text/javascript">
        <#assign fkSearchUrl = formListUrl(it.companyResource, myItem.fkEvidenceType)!"" />
        <#assign initialText><#if it.editHandler.getValue(pname)??>${it.editHandler.getObject(pname, it.editHandler.getValue(pname))}<#elseif val??>${val}</#if></#assign>
        <#assign initialHiddenValue><#if it.editHandler.getValue(pname)??>${it.editHandler.getValue(pname)}<#elseif val??>${val.id?c}</#if></#assign>
        $(function() {
                initFlexBox('${namePrefix}${pname}${idCount}', '${myItem.name}${idCount}', '${fkSearchUrl}.json?mode=suggest', '${initialValue}', '${initialText}', '<#if level != "1">flexibee-level-${level}</#if> form-control');
        });
    </script>
    <#if myItem.fkEvidenceType?? && myItem.fkEvidenceAccessible>
        <@tool.showRelating myItem.fkEvidenceType "flexibee-inp-${namePrefix}${pname}${idCount}" />
    </#if>


    </#if>
    </#if>

<#else>
    <#assign inputType = "text">
    <#local needDiv = false/>
    <#if type == "email" || type == "url" || type == "tel" || type == "fax" || type == "ean">
        <#assign needDiv = true/>
        <div class="input-group">
        <#if type == "email">
        <#assign inputType = "email">
        <span class="input-group-addon"><i class="fa fa-fw">@</i></span>
        <#elseif type == "url">
        <#assign inputType = "url">
        <span class="input-group-addon"><i class="fa fa-globe fa-fw"></i></span>
        <#elseif type == "tel">
        <#assign inputType = "tel">
        <span class="input-group-addon"><i class="fa fa-phone fa-fw"></i></span>
        <#elseif type == "fax">
        <span class="input-group-addon"><i class="fa fa-fax fa-fw"></i></span>
        <#elseif type == "ean">
        <span class="input-group-addon"><i class="fa fa-barcode fa-fw"></i></span>
        </#if>
   </#if>
    <input type="${inputType}" <#if myItem.maxLength??>maxlength="${myItem.maxLength}"</#if> class="${addonClass} form-control" name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}" value="<#if it.editHandler.getValue(pname)??>${it.editHandler.getValue(pname)}<#else>${val}</#if>" ${disabled} />
    <#if needDiv>
    </div>
    </#if>
</#if>
    <@placeErrorEnd myItem/>
</#compress>
</#macro>

<#macro textarea object name items marker rows=0 prefix="" level="1">
    <#if items[name]??>
    <#assign myItem = items[name] />

    <#if !myItem.showToUser>
        <#return>
    </#if>

    <#assign addonClass = "flexibee-inp flexibee-focus form-control"/>

    <#if marker.getMarkCount(name) &gt; 0>
    <#assign idCount = "-dup" + marker.getMarkCount(name) />
    <#assign addonClass = addonClass + " flexibee-dup"/>
    <#else>
    <#assign idCount = ""/>
    </#if>

    <#if level != "1">
    <#assign addonClass = addonClass + " flexibee-level-"+level />
    </#if>

    <#local pname = myItem.propertyName/>
    <#if object[pname]??>
        <#local val = object[pname]>
    <#elseif object.viewProperties??>
        <#local val = object.getValue(pname)>
    </#if>

    <@placeErrorBegin myItem marker/>

    <#assign disabled = ""/>
    <#assign namePrefix = prefix/>
    <#assign idPrefix = prefix/>
    <#if !myItem.isWritable>
        <#assign disabled = "disabled"/>
        <#assign namePrefix = namePrefix + "disabled-"/>
    </#if>

    <textarea class="${addonClass}" <#if rows&gt; 0>rows="${rows}"</#if> name="${namePrefix}${pname}${idCount}" id="flexibee-inp-${idPrefix}${pname}${idCount}" ${disabled}><#if it.editHandler.getValue(pname)??>${it.editHandler.getValue(pname)}<#else>${val}</#if></textarea>

    <@placeErrorEnd myItem/>
    </#if>
</#macro>

<#-- TODO pro FlexBoxy (a disablované inputy) se generuje špatně atribut 'for' -->
<#macro label name items prefix="" level="1">
<#if items[name]?? && items[name].showToUser><#assign myItem = items[name] /><label for="flexibee-inp-${prefix}${myItem.propertyName}"><#if level != '1'><span class="flexibee-level-${level}-label"></#if>${myItem.title}:<#if level != '1'></span></#if></label></#if>
</#macro>


<#macro listItems items object marker>
<#if isDevel == true && isModal == false>
<table class="flexibee-tbl-edit">
    <tbody>
    <#list items?values as item>
        <#if (marker.isMarked(item.propertyName) == false && item.showToUser)>
        <tr>
            <th><@edit.label name=item.propertyName items=items /></th>
            <td>
            <@edit.place object=object name=item.propertyName items=items marker=marker />
            </td>
        </tr></#if>
    </#list>
    </tbody>
</table>
</#if>
</#macro>

<#macro buttonsBegin>
<#if isModal == true>
</div>
</div>
</div> <!-- modal-body -->
<div class="modal-footer">
</#if>
</#macro>


<#macro buttonsEnd>
<#if isModal == true>
</div>
</#if>
</#macro>

<#macro saveButton showSubmitAndNew=true>
<@edit.buttonsBegin/>

    <div class="flexibee-buttons" <#if isMobile>data-role="controlgroup"  data-type="horizontal"</#if>>
        <input type="button" name="cancel" data-dismiss="modal" <#if isMobile>data-role="none"</#if> value="${lang('actions', 'cancel', 'Zrušit')}" class="btn btn-link negative flexibee-cancel-button"/>
        <input type="submit" name="submit-and-new" <#if isMobile>data-role="none"</#if> value="${lang('actions', 'saveAndNew', 'Uložit a nový')}" class="btn btn-default flexibee-submit-and-new-button flexibee-show-wait" style="display:none;"/>
        <input type="submit" name="submit" value="${lang('actions', 'save', 'Uložit')}" class="btn btn-primary flexibee-show-wait"/>
    </div>
    <#if isModal == false>
    <script>
        $(function() {
            $(".flexibee-cancel-button").show().click(function(e) {
                <#if isMobile>history.back(); return false;<#else>window.location = "${it.goBackURI}";</#if>
            });
         });
    </script>
    </#if>
<@edit.buttonsEnd/>
</#macro>

<#macro backButton>
<@edit.buttonsBegin/>
    <div class="flexibee-buttons" <#if isMobile>data-role="controlgroup"  data-type="horizontal"</#if>>
        <input type="button" name="cancel" data-dismiss="modal" <#if isMobile>data-role="none"</#if> value="${lang('actions', 'back', 'Zpět')?cap_first}" class="btn btn-link negative flexibee-cancel-button"/>
    </div>
    <#if isModal == false>
    <script>
        $(function() {
            $(".flexibee-cancel-button").show().click(function(e) {
                <#if isMobile>history.back(); return false;<#else>window.location = "${it.goBackURI}";</#if>
            });
         });
    </script>
    </#if>
<@edit.buttonsEnd/>
</#macro>

<#macro okCancelButton showCancel=true yesNo=false>
<@edit.buttonsBegin/>
    <#assign ok = 'ok' cancel = 'cancel'>
    <#if yesNo><#assign ok = 'yes' cancel='no'></#if>
    <div class="flexibee-buttons" <#if isMobile>data-role="controlgroup"  data-type="horizontal"</#if>>
        <#if showCancel>
            <input type="button" name="cancel" data-dismiss="modal" <#if isMobile>data-role="none"</#if> value="${lang('actions', cancel, 'Zrušit')?cap_first}" class="btn btn-link negative flexibee-cancel-button"/>
        </#if>
        <input type="submit" name="submit" <#if isMobile>data-role="none"</#if> value="${lang('actions', ok, 'OK')?cap_first}" class="btn btn-primary flexibee-show-wait" />
    </div>
    <#if isModal == false>
    <script>
        $(function() {
            $(".flexibee-cancel-button").show().click(function(e) {
                <#if isMobile>history.back(); return false;<#else>window.location = "${it.goBackURI}";</#if>
            });
         });
    </script>
    </#if>
<@edit.buttonsEnd/>
</#macro>

<#macro deleteButton>
<@edit.buttonsBegin/>
    <div class="flexibee-buttons" <#if isMobile>data-role="controlgroup"  data-type="horizontal"</#if>>
        <input type="button" name="cancel" data-dismiss="modal" <#if isMobile>data-role="none"</#if> value="${lang('buttons', 'cancel', 'Zrušit')}" class="btn btn-link negative flexibee-cancel-button" />
        <input type="submit" name="submit" <#if isMobile>data-role="none"</#if> value="${actText('deleteVyrCisla')}" class="btn btn-danger negative flexibee-show-wait"/>
    </div>
    <#if isModal == false>
    <script>
        $(function() {
            $(".flexibee-cancel-button").show().click(function(e) {
                <#if isMobile>history.back(); return false;<#else>window.location = "${it.goBackURI}";</#if>
            });
         });
    </script>
    </#if>
<@edit.buttonsEnd/>
</#macro>

<#-- FormHelper a spol. -->

<#macro propsTypes restActionHelper>
{
<#list restActionHelper.devDocItems as prop>
    ${prop.propertyName}: "${prop.type}"<#if prop_has_next>,</#if>
</#list>
}
</#macro>

<#macro filledProps object restActionHelper>
{
<#list restActionHelper.devDocItems as prop>
    <#local valpname = prop.originalPropertyName!prop.propertyName>
    ${prop.propertyName}: ${(object[valpname]??)?string}<#if prop_has_next>,</#if>
</#list>
}
</#macro>

<#macro generateNaiveJavascript object it>
    <#if isSlowMobile == false>
    <script type="text/javascript">
    $(function() {

        var defaults = {
            <#if object['id']?? && object['id'] != -1>
            id: "${object['id']?c}"
            </#if>
        };

        var form = new flexibee.FormHelper({
            evidence: {
                name: "${it.evidenceResource.evidenceName}",
                url: "${it.evidenceResource.listURI}",
                props: <@propsTypes restActionHelper=it/>,
                filledProps: <@filledProps object=object restActionHelper=it/>,
                defaults: defaults
            },
            page: {
                form: $(".flexibee-form") <#-- TODO chtělo by to lepší selektor -->
            },
            csrfToken: "${csrfToken}"
        });
        form.start();
    });
    </script>
    </#if> <#-- isSlowMobile == false -->
</#macro>

</#escape>
