<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.Application', {
    extend: 'Ext.app.Application',

    requires: [
        'Ext.History',
        'Ext.state.CookieProvider',
        'Ext.state.Manager',
        'FlexiBee.main.Window'
    ],

    name: 'FlexiBee',
    enableQuickTips: true,

    controllers: [
        'FlexiBee.main.Controller'
    ],

    launch: function() {
        var cp = Ext.create('Ext.state.CookieProvider', {
            path: "/c/${it.companyId}/",
            expires: new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 60)) // 60 days
        });
        Ext.state.Manager.setProvider(cp);

        var configs = FlexiBee.Plugins.invoke(this, 'createPathsConfig', '/c/${it.companyId}');
        Ext.iterate(configs, function(config) {
            if (config) {
                Ext.Loader.setPath(config);
            }
        });

        // udržování session každých 60 vteřin
        setInterval(function() {
          Ext.Ajax.request({
              url: '/c/${companyId}/session-keep-alive.js',
              timeout: 10000,
              method: 'GET',
              success: function(xhr) {
              }
          });
        }, 60000);

        Ext.create("FlexiBee.main.Window");
        Ext.History.init();

        FlexiBee.Plugins.invoke(this, 'applicationLaunched');
    },

    controllerId: 0,
    newController: function(name, config) {
        var realConfig = {
            application: this,
            id: "FlexiBeeController" + this.controllerId++
        };
        Ext.apply(realConfig, config);

        var controller = Ext.create(this.getModuleClassName(name, 'controller'), realConfig);
        controller.init(this);
        return controller;
    }
});

<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="main.Application"/>

</#escape>
