<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#compress>
<#--
    Máme tyto zařízení:
    - mobile - normální iPhone nebo android
    - mobile-wide - to samé co mobile, ale ve vyšším rozlišení
    - slow-mobile - starý mobil, který neumí buď vůbec a nebo pomalu JavaScript. Vizuálně by to mělo být podobné mobile, ale bez efektů. Aktivně vypínáme javascript
    - tablet
    - web
-->

    <#if isSlowMobile || isNotSupportedBrowser>
        <#assign useTouch = false/>
        <#assign isSlowMobile = true/>
        <#assign useJQuery = false/>
        <#assign useInfScroll = false/>
    <#else>
        <#assign useTouch = isMobile/> <#-- zapneme podporu pro dotykové displeje -->
        <#assign useJQuery = true/> <#-- zapneme načítání JQuery - pro některé zařízení je to příliš veliké a pomalé -->
        <#assign useInfScroll = false/> <#-- zapneme automatické načítání -->
    </#if> <#-- isSlowMobile || isNotSupportedBrowser -->

<#--
    Co dalšího máme:
    - isMobile - je na mobilu
    - isSlowMobile - je na pomalém mobilu - musíme používat bez jQuery a touch
    - isPlain - normální obrazovka bez HTML začátku a konce - jen obsah body bez hlavičky
    - isPanel - kompletní HTML, ale bez hlavičky a patičky a bez menu. Obsahuje "header" v html
-->
</#compress>
<#import "/l-tools.ftl" as tool /><#include "/i-localization.ftl"/><#if !isPlain><!DOCTYPE html>

<html lang="cs">
<head>
<meta charset="UTF-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<#if isMobile>
<meta name="viewport" content="width=device-width,user-scalable=no,initial-scale=1.0,maximum-scale=1.0"/>
<meta name="HandheldFriendly" content="true" />
<meta name="apple-mobile-web-app-capable" content="yes" />
<link rel="apple-touch-icon-precomposed" href="${staticPrefix}/img/flexibee-icon-iphone.png" />
<link rel="apple-touch-startup-image" href="${staticPrefix}/img/iphone-startup.gif" />
</#if> <#-- isMobile -->
<meta name="robots" content="index,nofollow" />
<#if !isMobile>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
</#if> <#-- !isMobile -->
<#if headerAddons??><#noescape>${headerAddons}</#noescape></#if>

<#if !isMenu && !isModal> <#-- kvůli optimalizacím s JQuery. Jinak znovu přenačítá všechny JS souboru || modální okna nepotřebují znovu načítat JS -->
<!--FLEXIBEE:JS:START-->
<script src="${staticPrefix}/js/jq/jquery.js"  type="text/javascript"></script>

<script src="${staticPrefix}/js/jq/jquery-ui.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/jquery-ui-timepicker-addon.js"  type="text/javascript"></script>
<#if languageCode != 'en'>
<script src="${staticPrefix}/js/jq/jquery.ui.datepicker-${languageCode}.js"  type="text/javascript"></script>
</#if> <#-- languageCode != 'en' -->
<#if useInfScroll>
<script src="${staticPrefix}/js/jq/jquery.infinitescroll.js"  type="text/javascript"></script>
</#if> <#-- useInfScroll -->

<#if true>
<script src="${staticPrefix}/js/jq/select2.js"  type="text/javascript"></script>
<#if languageCode != 'en'>
<script src="${staticPrefix}/js/jq/select2_locale_${languageCode}.js"  type="text/javascript"></script>
</#if> <#-- languageCode != 'en' -->
</#if>

<!--[if gte IE 8]>
<script src="${staticPrefix}/js/jq/jquery.xdr.js"  type="text/javascript"></script>
<![endif]-->
<script src="${staticPrefix}/js/jq/jquery.flexbox.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/jquery.autocomplete.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/jquery.ajaxmanager.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/jquery.textarearesizer.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/jquery.keynav.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/jquery.upload.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/jquery.cookie.js"  type="text/javascript"></script>

<script src="${staticPrefix}/js/jq/flexibee-jquery-web.js"  type="text/javascript"></script>
<#-- flexibee-jquery je _úmyslně_ až po fb-jquery-web/fb-jquery-mobile, zejm. kvůli proměnné "isMobile" a unifikaci události click -->
<script src="${staticPrefix}/js/jq/flexibee-jquery.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/flexibee-accessibility.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/flexibee-formandjson.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/flexibee-formhelper.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/jq/flexibee-itemshelper.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/bootstrap/bootstrap.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/bootstrap/flexibee-wallpaper.js"  type="text/javascript"></script>
<script src="${staticPrefix}/js/bootstrap/parsley.js"  type="text/javascript"></script>


<script type="text/javascript">
    <#-- code taken from http://digitalize.ca/2010/04/javascript-tip-save-me-from-console-log-errors/ -->
    if (typeof(console) === 'undefined') {
        var console = {};
        console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = function() {};
    }

    <#-- viz cz.winstrom.net.server.services.EvidenceResource.list() -->
    function initFlexBox(propertyName, name, url, initialKey, initialText, inputClass) {
    $('#flexibee-inp-' + propertyName).select2({
        ajax: {
            url: url,
            dataType: 'json',
            quietMillis: 50,
            minimumInputLength: 1,
            data: function (term, page) {
                return {
                    q: term,
                    p: (page - 1) * 20 + 1,
                    mode: 'suggest'
                };
            },
            results: function (data, page) { // parse the results into the format expected by Select2.
                var data2 = [];
                var item;

                if (initialKey !== "") {
                    data2.push({
                        id: initialKey,
                        text: initialText
                    });
                }

                for (item in data.results) {
                    if (initialKey == "" || data.results[item].id != initialKey) {
                        data2.push({
                            id: data.results[item].id,
                            text: data.results[item].name
                        });
                    }
                }
                var more = (page * 20) < data.total;

                return {results: data2, more: more};
            },
        },
        allowClear: true,
        initSelection: function(element, callback) {
            var id=$(element).val();
            if (id!=="") {
                var data = {
                    id: initialKey,
                    text: initialText
                };
                callback(data);
            }
        },
        selectOnBlur: false,
        inputClass: 'form-control input-sm <#if it.fullTextQuery?? && it.fullTextQuery != ''>btn-primary</#if>',
    });


    <#--
        $('#flexibee-inp-' + propertyName + '-flexbox').flexbox(url, {
            width: 300,
            showArrow: true,
            autoCompleteFirstMatch: false,
            noResultsText: '${lang('messages', 'suggest.nothingFound', 'Nic nenalezeno')}',
            paging: {
                style: 'links',
                summaryTemplate: '${lang('messages', 'suggest.summary', '{start} až {end} z celkem {total}')}'
            },
            watermark: '${lang('messages', 'suggest.watermark', '{name} (pište název)')}'.replace('{name}', name),
            initialValue: initialValue,
            inputClass: 'ffb-input flexibee-inp '+inputClass
        });
        <!--
          - FlexBox vyrobí skrytý input, kterému bude nastavovat správné hodnoty, takže:
          - * si nepotřebuju dělat žádný vlastní a nastavovat jeho hodnotu v onSelect;
          - * mu musím dát správné formulářové jméno a případně i počáteční hodnotu (ta ovšem musí být ID, nikoliv text)
          --
        $('#flexibee-inp-' + propertyName + '-flexbox_hidden').attr('name', propertyName).val(initialHiddenValue); -->
    }
</script>
<#if cloudOverride.isTemplate("header", "js")>
<#include cloudOverride.include("header", "js") />
</#if>
<!--FLEXIBEE:JS:END-->

<link rel="shortcut icon" href="${staticPrefix}/img/flexibee.ico" />

<!--FLEXIBEE:CSS:START-->
<link rel="stylesheet" type="text/css" href="${staticPrefix}/css/flexibee.css" media="all" />
<style>
    .body {
        padding-top: 10em;
    }
</style>

<#if cloudOverride.isTemplate("header", "css")>
<#include cloudOverride.include("header", "css") />
</#if>
<!--FLEXIBEE:CSS:END-->

</#if> <#-- !isMenu -->
</#if> <#-- !isPlain -->

<#if isMobile>
    <!--FLEXIBEE:IS:MOBILE-->
</#if>
<#if isMobile>
    <!--FLEXIBEE:IS:SLOWMOBILE-->
</#if>
<#if isPanel>
    <!--FLEXIBEE:IS:PANEL-->
</#if>

<#if !isPlain>
<!--FLEXIBEE:HEADER-->
<title><#if title != ''>${title} | </#if>ABRA Flexi</title>
</head>

<body>
</#if> <#-- !isPlain -->

<#if isNotSupportedBrowser>
    <#assign isShowTitleFirst = false />
    <div class="flexibee-root-page">
          <p class="flexibee-note-warn"><strong>Nepodporovaný prohlížeč:</strong> prohlížeč, který používáte, nepatří mezi podporované. Aktualizujte, prosím, na novější verzi <a href="http://www.microsoft.com/cze/windows/internet-explorer/">Internet Exploreru</a> a nebo nainstalujte rozšíření Google Chrome Frame.</p>

  <!--[if IE]>
    <script type="text/javascript"
        src="https://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js"></script>

    <style>
     .chromeFrameInstallDefaultStyle {
       width: 850px;
       border: 5px solid blue;
     }
    </style>

    <div id="prompt">
     <!-- if IE without GCF, prompt goes here -->
    </div>

    <script type="text/javascript">
     // The conditional ensures that this code will only execute in IE,
     // Therefore we can use the IE-specific attachEvent without worry
     window.attachEvent("onload", function() {
       CFInstall.check({
         mode: "inline", // the default
         node: "prompt"
       });
     });
    </script>
    <![endif]-->
    </div>
</#if> <#-- isNotSupportedBrowser -->


<#if isMenu || isPlain || isPanel>
    <div id="flexibee-panel"> <#-- kvůli kopírování celého obsahu stránky pro vývojáře -->
</#if> <#-- isMenu || isPlain || isPanel -->

<#if cloudOverride.isTemplate("header", "content")>
<#include cloudOverride.include("header", "content") />
</#if>

</#escape>
