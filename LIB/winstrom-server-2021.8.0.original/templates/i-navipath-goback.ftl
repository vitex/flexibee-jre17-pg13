<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#list paths as path>
    <#if path_index = paths?size - 2>
        <div class="flexibee-goback">
            <a href="${path.url}" class="flexibee-menu-company flexibee-clickable">${lang('labels', path.shortName, path.shortName)}</a>
        </div>
    </#if>
</#list>
</#escape>