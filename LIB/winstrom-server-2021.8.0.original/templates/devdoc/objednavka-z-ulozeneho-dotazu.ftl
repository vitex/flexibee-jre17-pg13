<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Objednávka z uloženého dotazu"/>
<#include "/i-devdoc-header.ftl" />

<p>V některých situacích nemusí vyhovovat standardní funkce ABRA Flexi pro objednávání zboží. ABRA Flexi podporuje vytvoření objednávky vydané z&nbsp;minimálního množství na skladě nebo z&nbsp;existujících objednávek přijatých.</p>

<p>Další metodou je použít uložený dotaz, který připraví data k&nbsp;objednání. Ta pak lze použít v&nbsp;aplikaci jako podklad pro objednávací formulář. Funkci najdete v&nbsp;<i>Objednávky vydané &gt; Služby &gt; Objednat podle výsledku dotazu</i>.</p>

<p>Dotaz musí mít jako primární formulář nastaven Objednávky vydané a jeho první čtyři vlastnosti (sloupce) musí být následujících typů:</p>
<ul>
    <li><code>integer</code> - id ceníku, který se má objednat</li>
    <li><code>integer</code> - id skladu, na který se má objednat</li>
    <li><code>double</code>  - množství, které se zobrazí v&nbsp;poli s&nbsp;množstvím k&nbsp;objednání</li>
    <li><code>double</code>  - množství, které se zobrazí v&nbsp;poli s&nbsp;již objednaným množstvím</li>
</ul>

<p>Množství, které se nabídne k&nbsp;výslednému objednání je rozdílem třetího a čtvrtého pole.</p>


<ul>
    <li>

<#include "/i-devdoc-footer.ftl" />
</#escape>
