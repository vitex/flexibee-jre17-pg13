<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Referenční příručka REST API"/>
<#assign dir="/devdoc/"/>
<#include "/header.ftl" />

<div class="flexibee-article-view">
<!-- start -->

Využívání API nad instancí v Cloudu musí být v souladu s <a href="https://www.flexibee.eu/eula/">licenčními podmínkami</a>.

<h3>Dokumentace</h3>
<ul>
    <li><a href="${dir}login">Autentizace</a></li>
    <li><a href="${dir}urls">Způsob tvorby URL</a></li>
    <li><a href="${dir}format-types">Podporované formáty</a></li>
    <li><a href="${dir}pdf">Export do PDF</a></li>
    <li><a href="${dir}permissions">Přístupová práva</a></li>
    <li><a href="${dir}errors">Obsluha chyb</a></li>
    <li><a href="${dir}performance">Výkonnostní optimalizace</a></li>
    <li><a href="${dir}http-operations">Podporované HTTP operace</a></li>
    <li><a href="${dir}validations">Validace při modifikaci záznamů</a></li>
    <li><a href="${dir}dry-run">Testovací uložení (dry-run)</a></li>    
</ul>

<h4>Výpis záznamů</h4>
<ul>
    <li><a href="${dir}list">Výpis záznamů</a></li>
    <li><a href="${dir}detail-levels">Úrovně detailu</a></li>
    <li><a href="${dir}paging">Stránkování</a></li>
    <li><a href="${dir}ordering">Řazení záznamů</a></li>
    <li><a href="${dir}filters">Filtrování záznamů</a></li>
    <li><a href="${dir}sumace">Sumace záznamů</a></li>
    <li><a href="${dir}attachments">Přílohy</a></li>
</ul>

<h4>Práce s firmami</h4>
<ul>
    <li><a href="${dir}create-company">Založení společnosti</a></li>
    <li><a href="${dir}restore-backup">Obnovení společnosti ze zálohy</a></li>
    <li><a href="${dir}company-identifier">Identifikátor společnosti</a></li>
</ul>

<h4>Podpora ABRA Flexi XML</h4>

<p><a href="${dir}xml">ABRA Flexi XML</a> je technologie sloužící pro výměnu dat s&nbsp;ekonomickým systémem.</p>

<ul>
    <li><a href="${dir}variable-types">Typy proměnných</a></li>
    <li><a href="${dir}identifiers">Identifikátory záznamů</a>
        <ul>
            <li><a href="${dir}article-individual-codes">Režim individuálních kódů ceníkových položek</a></li>
        </ul>
    </li>
    <li><a href="${dir}partial-updates">Inkrementální aktualizace</a></li>
    <li><a href="${dir}required-fields">Povinnost atributů</a></li>
    <li><a href="${dir}internal-dependencies">Vnitřní vazby při ukládání</a></li>
    <li><a href="${dir}create-update-mode">Režim pro založení/změnu</a></li>    
    <li><a href="${dir}copy">Kopie záznamu</a></li>    
    <li><a href="${dir}actions">Provádění akcí</a></li>    
    <li><a href="${dir}last-update">Datum poslední změny</a></li>
<!--    <li><a href="${dir}xpath">Na výstupní XML lze aplikovat XPath</a></li>     -->
    <li><a href="${dir}dph">Výpočet Daně z přidané hodnoty (DPH)</a></li>
    <li><a href="${dir}users">Práce s uživateli</a></li>
    <li><a href="${dir}previous-value">Předchozí hodnoty &ndash; způsob reakce na změnu</a></li>
    <li><a href="${dir}batch-operations">Dávkové operace</a></li>
    <li><a href="${dir}tx">Transakční zpracování</a></li>
    <li><a href="${dir}workflow">Workflow a procesy</a></li>
    <li><a href="${dir}xsd">XML Schéma (XSD)</a></li>
    <li><a href="${dir}kurz">Kurzy na dokladech</a></li>
    <li><a href="${dir}qrcode">QR kódy pro úhrady dokladů</a></li>
    <li><a href="${dir}stitky">Práce se štítky</a></li>
</ul>

<h4>Pokročilé příkazy</h4>
<ul>
    <li><a href="${dir}parovani-plateb">Párování bankovních plateb</a>
        <ul>
            <li><a href="${dir}hotovostni-uhrada">Hotovostní úhrada faktur</a></li>
            <li><a href="${dir}uhrada-preplatku">Úhrada faktury z&nbsp;přeplatků v&nbsp;pokladně a bance</a></li>
            <li><a href="${dir}uhrada-zapoctem">Úhrada faktury vzájemným zápočtem závazků</a></li>
            <li><a href="${dir}odpocet-zaloh">Odpočet záloh</a></li>
            <li><a href="${dir}nacitani-vypisu">Načítání bankovních výpisů</a></li>
            <li><a href="${dir}vzajemny-zapocet">Vzájemné zápočty</a></li>
            <li><a href="${dir}vazby-zdd">Vazby ZDD</a></li>
        </ul>
    </li>
    <li><a href="${dir}prikaz-k-uhrade">Příkaz k úhradě</a></li>
    <li><a href="${dir}prikaz-k-inkasu">Příkaz k inkasu</a></li>
    <li><a href="${dir}odesilani-mailem">Odesílání dokladů e-mailem</a></li>
    <li><a href="${dir}individualni-cenik">Individuální ceník</a></li>
    <li><a href="${dir}generovani-faktur">Generování faktur ze smluv</a></li>
    <li><a href="${dir}uzivatelske-dotazy">Uživatelské dotazy</a>
        <ul>
            <li><a href="${dir}objednavka-z-ulozeneho-dotazu">Objednávka z uloženého dotazu</a></li>    
        </ul>
    </li>
    <li><a href="${dir}changes-api">Changes API (Sledování změn)</a></li>
    <li><a href="${dir}web-hooks">Web Hooks</a></li>
    <li><a href="${dir}stav-skladu-k-datu">Stav skladu k datu</a></li>
    <li><a href="${dir}aktualizace-pozadavku">Aktualizace požadavků na výdej</a></li>
    <li><a href="${dir}analyza-nakupu-prodeje">Analýzy nákupu / prodeje</a></li>
    <li><a href="${dir}nabidni-objednej">Tvorba nabídky/objednávky z poptávky/nabídky</a></li>
    <li><a href="${dir}realizace-objednavky">Realizace objednávky</a></li>
    <li><a href="${dir}atributy">Atributy ceníku</a></li>
    <li><a href="${dir}uzivatelske-vazby">Uživatelské vazby</a></li>
    <li><a href="${dir}zamykani-odemykani">Zamykání a odemykání záznamů</a></li>
    <li><a href="${dir}dobropisy">Dobropisy</a></li>            
    <li><a href="${dir}stavy-uctu">Stavy účtů</a></li>
    <li><a href="${dir}ucely">Vytvoření a zrušení účelu GDPR</a></li>
    <li><a href="${dir}import-eet">Import EET certifikátů</a></li>
    <li><a href="${dir}zauctovat-prodejky">Zaúčtování prodejek</a></li>
    <li><a href="${dir}prepocet-skladu">Přepočet skladu</a></li>
    <li><a href="${dir}prepocet-uctu">Přepočet stavů účtů</a></li>
    <li><a href="${dir}inicializace-noveho-obdobi">Inicializace nového období</a></li>
</ul>

<h4>Účetní výstupy</h4>
<ul>
    <li><a href="${dir}rozvaha-zisku-a-ztraty">Rozvaha a výkaz zisku a ztráty v XML</a></li>
    <li><a href="${dir}vykaz-zisku-a-ztraty">Výkaz zisku a ztráty</a></li>
    <li><a href="${dir}obratova-predvaha">Obratová předvaha</a></li>
    <li><a href="${dir}prehled-o-peneznich-tocich">Přehled o peněžních tocích</a></li>
    <li><a href="${dir}rozvaha">Rozvaha</a></li>
</ul>

<h4>Přizpůsobení aplikace</h4>
<ul>
    <li><a href="${dir}uzivatelske-tlacitko">Uživatelské tlačítko</a></li>
    <li><a href="${dir}uzivatelske-mailove-sablony">Uživatelské mailové šablony</a></li>
</ul>

<h4>Podpora pro konkrétní systémy</h4>
<ul>
<!--    <li><a href="${dir}extjs">Podpora pro ExtJS</a></li> -->
    <li><a href="${dir}ruby">Podpora pro Ruby On Rails</a></li>
</ul>

<h4>ABRA Flexi embeddable</h4>
<p>Technologie, která umožňuje vložení ABRA Flexi serveru do cizí aplikace pomocí HTML rozhraní.</p>
<ul>
    <li><a href="${dir}externi-autorizace">Externí autorizace</a></li>
    <li><a href="${dir}autentizace-kontaktu">Autentizace kontaktu</a></li>
    <li><a href="${dir}batch-api">Batch API - serverová administrace</a></li>
    <li><a href="${dir}logout">Odhlašování uživatelů</a></li>
    <li><a href="${dir}automatic-startup">Automatické startování jádra</a></li>
    <li><a href="${dir}ssl">Vyměna SSL certifikátu</a></li>
    <li><a href="${dir}security">Bezpečnost</a></li>
</ul>

<!-- end -->

</div>
<#include "/footer.ftl" />
</#escape>
