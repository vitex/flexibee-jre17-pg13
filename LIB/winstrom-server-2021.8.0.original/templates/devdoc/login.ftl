<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Autentizace"/>
<#include "/i-devdoc-header.ftl" />

<p>ABRA Flexi podporuje několik způsobů autentizace:</p>
<ul>
    <li>HTTP autentizace</li>
    <li>JSON autentizace</li>
    <li>SAMLv2<sup>1)</sup></li>
    <li>OpenID<sup>1)</sup></li>
</ul>
<ol class="small">
<hr/>
<li>Podporováno pouze na lokální instalaci. Dostupnost omezena ABRA Flexi licencí. Konfiguruje se ve webovém rozhraní serveru na adrese <code>/admin/auth-settings</code>.</li>
</ol>

<h2 id="http">HTTP autentizace</h2>

<p><a href="https://en.wikipedia.org/wiki/Basic_access_authentication">Jednoduchá HTTP autentizace</a> je původní způsob ověřování uživatelů při přihlášování do webového rozhraní ABRA Flexi <abbr title="Web User Interface (webové rozhraní)">WUI</abbr>. Současně je to také jedna z možností autentizace uživatelů REST API. Stačí s každým HTTP požadavkem posílat autentizační hlavičku. Pokud hlavičku neuvedete, dojde k přesměrování na přihlašovací formulář, případně bude navrácen status <code>401 Authorization required</code> (požadavek na HTTP autentizaci).</p>
<p>Volání REST API se snažíme detekovat a nadále nabízet HTTP autentizaci. Pokud by detekce selhala, či bude nutné řídit způsob autentizace, je možné využít parametr <code>?auth=http</code> (viz <a href="urls#parameters">parametry URL</a>).</p>

Ukázka komunikace s jednoduchou HTTP autentizací pomocí příkazu <code>curl</code>:
<pre class="shell">
curl -L -v <strong>-u winstrom:winstrom</strong> 'https://demo.flexibee.eu:5434/c/demo/adresar.xml?detail=custom:kod&amp;limit=1'
</pre>
<pre class="brush: xml">
&vellip;
* Server auth using Basic with user 'winstrom'
&gt; GET /c/demo/adresar.xml?detail=custom:kod&amp;limit=1 HTTP/1.1
&gt; Host: demo.flexibee.eu:5434
&gt; Authorization: Basic d2luc3Ryb206d2luc3Ryb20=
&gt; User-Agent: curl/7.54.1
&gt; Accept: */*
&gt;
&lt; HTTP/1.1 200 OK
&lt; Cache-Control: private, max-age=0, no-cache
&lt; Set-Cookie: lang=cs;Version=1
&lt; Vary: Accept
&lt; Content-Type: application/xml
&lt; Content-Length: 150
&lt; Via: 1.1.loadbal-fra-1.c.flexibee.eu
&lt;
&lt;?xml version="1.0" encoding="utf-8"?&gt;

&lt;winstrom version="1.0"&gt;
  &lt;!-- Adresář --&gt;
  &lt;adresar&gt;
    &lt;!-- ID (celé číslo) - --&gt;
    &lt;id&gt;755&lt;/id&gt;
    &lt;!-- Zkratka (řetězec) - max. délka: 20 --&gt;
    &lt;kod&gt;AAA&lt;/kod&gt;
  &lt;/adresar&gt;
&lt;/winstrom&gt;
</pre>

Pro ukázku autentizace v <code>PHP</code> viz reálné příklady <a href="https://www.flexibee.eu/api/#apiGuide">seriálu API</a>.

<h2 id="json">JSON autentizace (autentizační sezení)</h2>

<p>Pokud chcete ověřit ABRA Flexi a využívat vytvořenou autentizaci i pro další volání, použijte API pro získání <em>autentizačního tokenu</em>:</p>

<pre><strong>POST</strong> /login-logout/login<font style="color:gray">.json</font></pre>
<p>Pozn.: Metoda poskytuje výsledek pouze ve formátu <code>JSON</code>, proto v dotazu specifikujte správně <a href="format-types">požadovaný formát</a>.</p>

<p>Volání musí obsahovat <strong>formulářová data</strong>:</p>
<table class="table table-striped table-condensed">
    <tr><td><code>username</code></td><td>Uživatelské jméno</td></tr>
    <tr><td><code>password</code></td><td>Uživatelské heslo</td></tr>
    <tr><td><code>otp</code></td><td>Jednorázové heslo (pokud je vyžadováno)</td></tr>
</table>

<h3>Výsledek volání</h3>

<h4>Úspěšná autentizace</h4>

<p>Výsledkem je příznak úspěchu <code>success=true</code> a <em>autentizační token</em> <code>authSessionId</code>:</p>
<pre class="brush:js">
{
  "success": true,
  "authSessionId": "00112233445566778899aabbccddeeff00112233445566778899aabbccddeeff"
}
</pre>

<h4>Neúspěšná autentizace</h4>

<p>Neúspěšnou autentizaci signalizuje příznak <code>success=false</code> s upřesňujícím popisem v řetězci <code>errors</code>.<code>reason</code>:</p>
<pre class="brush:js">
{
  "success": false,
  "errors": {
    "reason": "Bylo zadáno chybné uživatelské jméno či heslo."
  }
}
</pre>

<h3>Možnosti využití <em>autentizačního tokenu</em></h3>

<p>Získanou autentizaci lze předávat v dotazech třemi způsoby:</p>
<ul>
    <li>Cookie: <code>authSessionId: 00112233445566778899aabbccddeeff00112233445566778899aabbccddeeff</code></li>
    <li>HTTP hlavička: <code>X-authSessionId: 00112233445566778899aabbccddeeff00112233445566778899aabbccddeeff</code></li>
    <li>URL query <code>?authSessionId=00112233445566778899aabbccddeeff00112233445566778899aabbccddeeff</code></li>
</ul>

<p>Pozor: U varianty v&nbsp;URL query budou autentizační údaje logovány na ABRA Flexi serveru.</p>
<p>Abyste udrželi token platný, je potřeba udržovat spojení pomocí občasného zavolání GET <code>/login-logout/session-keep-alive.js</code>. My ji voláme každých 60 vteřin, ale mělo by stačit i jednou za 30 minut.</p>

<p>Pomocí API lze také provádět <a href="logout">odhlášování uživatelů</a>.</p>

<h2>Umístění přihlašovacího formuláře na své stránky</h2>
<p>Pokud potřebujete umístit přihlašování na své stránky, inspirujte se tímto HTML:
<pre class="brush: html">
&lt;form action="https&gt;://demo.flexibee.eu:5434/login-logout/login.html" method="POST"&gt;
    Jméno: &lt;input type="text" name="username" value=""/&gt;&lt;br/&gt;
    Heslo: &lt;input type="password" name="password"/&gt;&lt;br/&gt;

    &lt;input type="submit" name="submit" value="Přihlásit"/&gt;
&lt;/form&gt;
</pre>

<p>Je také možné přidat parametru <code>returnUrl</code> a určit adresu na kterou má být uživatel přesměrován. Pokud používáte OTP, je nutné přidat ještě parametr <code>otp</code>. Při použití SSO (OpenID nebo SAMLv2) nelze tuto metodu použít.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
