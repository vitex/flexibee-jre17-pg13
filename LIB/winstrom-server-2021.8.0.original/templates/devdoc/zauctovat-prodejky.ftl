<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Zaúčtování prodejek"/>
<#include "/i-devdoc-header.ftl" />

<p>REST API dovoluje vykonat službu <strong><em>Zaúčtovat prodejky</em></strong> voláním PUT nebo POST na adresu <code>/c/{firma}/pokladni-pohyb<strong>/zauctovat-prodejky</strong>.[json|xml]?datum=2019-12-31&amp;kasa=35&amp;pokladna=2&amp;typPokDokl=5'</code>.</p>

<h3>Parametry</h3>
<ul>
<li><b>datum</b> - v GUI pole <em>&bdquo;Zaúčtování prodejek z data&ldquo;</em> (povinné) - formát <code>yyyy-mm-dd</code></li>
<li><b>kasa</b> - v GUI pole <em>&bdquo;Prodejky z kasy&ldquo;</em> (povinné) - ID z evidence typ-prodejky</li>
<li><b>pokladna</b> - v GUI pole <em>&bdquo;Pokladna&ldquo;</em> (povinné) - ID z evidence pokladna</li>
<li><b>typPokDokl</b> - v GUI pole <em>&bdquo;Typ pokladního dokladu&ldquo;</em> (povinné) - ID z evidence typ-pokladni-pohyb</li>
<li><b>cisRada</b> - není povinný parametr, je vyžadován, pokud není nalezena číselná řada odpovídající pokladně a typu pokladního dokladu - ID z evidence <code>/rada-pokladni-pohyb</code></li>
</ul>

<p>Pro všechny parametry identifikující záznam, lze kromě číselného ID použít i ostatní podporované <a href="identifiers">typy identifikátorů</a> (např. <code>&amp;kasa=code:KASA</code>).</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
