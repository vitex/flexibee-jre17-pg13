<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
    <#assign title="Zápočty"/>
    <#include "/i-devdoc-header.ftl" />

<p>Vzájemný zápočet je fiktivní bankovní výpis, kterým jsou prováděny zejména úhrady pohledávek a závazků na principu vzájemných zápočtů.</p>

<pre class="brush: xml">
&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt;
    &lt;id&gt;code:FAV1&lt;/id&gt;
    &lt;typDokl&gt;code:FAKTURA&lt;/typDokl&gt;
    &lt;bezPolozek&gt;true&lt;/bezPolozek&gt;
    &lt;sumOsv&gt;100.0&lt;/sumOsv&gt;
  &lt;/faktura-vydana&gt;
  &lt;faktura-prijata&gt;
    &lt;id&gt;code:FAP1&lt;/id&gt;
    &lt;typDokl&gt;code:FAKTURA&lt;/typDokl&gt;
    &lt;cisDosle&gt;F123&lt;/cisDosle&gt;
    &lt;datSplat&gt;2017-12-31+01:00&lt;/datSplat&gt;
    &lt;bezPolozek&gt;true&lt;/bezPolozek&gt;
    &lt;sumOsv&gt;100.0&lt;/sumOsv&gt;
  &lt;/faktura-prijata&gt;
&lt;/winstrom&gt;
</pre>

<p>Pro připravené faktury lze provést zápočet následujícím způsobem:</p>

<pre class="brush: xml">
&lt;winstrom version="1.0"&gt;
  &lt;vzajemny-zapocet&gt;
    &lt;id&gt;code:ZAP+0016/2017&lt;/id&gt;
    &lt;typDokl&gt;code:ZAPOCET&lt;/typDokl&gt;
    &lt;typPohybuK&gt;typPohybu.prijem&lt;/typPohybuK&gt;
    &lt;!-- částka úhrady --&gt;
    &lt;sumOsv&gt;1.0&lt;/sumOsv&gt;
    &lt;bezPolozek&gt;true&lt;/bezPolozek&gt;
    &lt;sparovani&gt;
      &lt;uhrazovanaFak type="faktura-vydana"&gt;code:FAV1&lt;/uhrazovanaFak&gt;
      &lt;zbytek&gt;castecnaUhrada&lt;/zbytek&gt;
    &lt;/sparovani&gt;
    &lt;!-- číslo zápočtu --&gt;
    &lt;cisSouhrnne&gt;ZAP-123&lt;/cisSouhrnne&gt;
  &lt;/vzajemny-zapocet&gt;
  &lt;vzajemny-zapocet&gt;
    &lt;id&gt;code:ZAP+0017/2017&lt;/id&gt;
    &lt;typDokl&gt;code:ZAPOCET&lt;/typDokl&gt;
    &lt;typPohybuK&gt;typPohybu.vydej&lt;/typPohybuK&gt;
    &lt;sumOsv&gt;1.0&lt;/sumOsv&gt;
    &lt;bezPolozek&gt;true&lt;/bezPolozek&gt;
    &lt;sparovani&gt;
      &lt;uhrazovanaFak type="faktura-prijata"&gt;code:FAP1&lt;/uhrazovanaFak&gt;
      &lt;zbytek&gt;castecnaUhrada&lt;/zbytek&gt;
    &lt;/sparovani&gt;
    &lt;cisSouhrnne&gt;ZAP-123&lt;/cisSouhrnne&gt;
  &lt;/vzajemny-zapocet&gt;
&lt;/winstrom&gt;
</pre>

<p>Může se stát, že částky na uhrazujícím a uhrazovaném dokladu nesouhlasí (zbývá část doplatit), v takovém případě se import řídí hodnotou v tagu <code>zbytek</code>. Lze zvolit tyto hodnoty:</p>

<ul>
    <li><code>ne</code>: zbytek nesmí nastat; pokud k němu dojde, jedná se o chybu</li>
    <li><code>castecnaUhrada</code>: pokud je částka na uhrazujícím dokladu <em>menší</em> než na uhrazovaném, jedná se o částečnou úhradu</li>
</ul>

<p>Analogicky lze provádět i odpárování:</p>

<pre class="brush: xml">
&lt;winstrom version="1.0"&gt;
  &lt;vzajemny-zapocet&gt;
    &lt;id&gt;code:ZAP+0016/2017&lt;/id&gt;
    &lt;odparovani&gt;
      &lt;uhrazovanaFak type="faktura-vydana"&gt;code:FAV1&lt;/uhrazovanaFak&gt;
    &lt;/odparovani&gt;
  &lt;/vzajemny-zapocet&gt;
&lt;/winstrom&gt;
</pre>


    <#include "/i-devdoc-footer.ftl" />
</#escape>