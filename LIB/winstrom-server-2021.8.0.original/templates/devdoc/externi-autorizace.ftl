<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Externí autorizace"/>
<#include "/i-devdoc-header.ftl" />

<p>Ekonomický systém ABRA Flexi normálně ověřuje uživatele vůči interní databázi uživatelů. Je samozřejmě možné ověřovat i proti externím systémům (např. LDAP). Pro tyto účely jsme připravili rozhraní v&nbsp;Jazyce Java, které Vám toto umožní. Takto napsaný kód pak poběží jako součást serveru a umožní ověření proti externím systémům.</p>

<p>Pozor: toto rozhraní je experimentální a není jej možné používat v&nbsp;cloudu. Pokud budete chtít tuto funkci použít, kontaktujte nás.</p>

<p>Základem všeho je rozhraní <code>cz.winstrom.auth.Auth</code>:</p>

<pre class="brush: java">package cz.winstrom.auth;

import cz.winstrom.vo.UserInfo;
import java.sql.Connection;

/**
 *
 * Základní rozhraní pro ověřování uživatelů proti jiným službám. Její nastavení se provede pomocí nastavení flexibee-server.xml:
 *
 * &lt;entry key="authClass"&gt;cz.winstrom.auth.AuthSimple&lt;/entry&gt;
 *
 * Jako parametr je zde název třídy, kterou je nutné dostat do CLASSPATH serveru.
 *
 * V tuto chvíli umí aplikace pouze ověření plain heslem.
 *
 * Kvůli změnám v tomto rozhraní doporučujeme, aby implementátoři dědily od abstraktní třídy "AuthBase".
 *
 * @author fers
 */
public interface Auth {
    /**
     * Autorizuj uživatele dle jména a hesla.
     *
     * @param connection napojení do databáze centralServer
     * @param userInfo informace o uživateli z centralServer.csuzivatel. Pokud je null, žádný takový uživatel neexistuje v hlavní databázi uživatelů ABRA Flexi. Při úspěšné autorizaci bude vytvořen.
     * @param username jméno uživatele
     * @param password heslo uživatele
     * @return true pokud bylo heslo zadáno správně.
     */
    boolean authenticate(Connection connection, UserInfo userInfo, String username, String password);

    /**
     * Pokud se povede autorizace, ale uživatel neexistuje v databází centralServer.csuzivatel, bude založen. Autorizační systém může ovlivnit informace o založeném uživateli tím,
     * že vyplní třídu UserInfo.
     *
     * ID se nesmí měnit (má hodnotu -1) i heslo doporučujeme nevyplňovat, protože ověření proběhne vždy vúči autorizačnímu systému.
     *
     * Obvykle se vyplňují pouze přístupová práva (založení firmy, smazání firmy, ...)
     *
     * @param connection napojení do databáze centralServer
     * @param userInfo informace o uživateli, které by měli být vráceny (může být vrácena i jiná instance, ale musí být správně vyplněna.
     * @return informace o uživateli, které budou uloženy do databáze. Obvykle se vrací upravený parametr userInfo.
     */
    public UserInfo getUserInfo(Connection connection, UserInfo userInfo);
}
</pre>

<p>Nicméně pro přímou implementaci používejte vždy dědění od třídy <code>cz.winstrom.auth.AuthBase</code>, která nám umožní měnit toto rozhraní a přitom zachovat zpětnou kompatibilitu, případně předimplementovat některé metody.</p>

<p>Samotná implementace autorizační třídy může vypadat takto:</p>
<pre class="brush: java">public class AuthSimple extends AuthBase {

    public boolean authenticate(Connection connection, UserInfo userInfo, String username, String password) {
    // jednoduché ověření: jméno a heslo se rovnají
        return username.equalsIgnoreCase(password);
    }

    @Override
    public UserInfo getUserInfo(Connection connection, UserInfo userInfo) {
        userInfo.setCreateCompany(true); // novému uživateli povolíme založení firmy

        return userInfo;
    }
}
</pre>

<p>A pro kompletnost už jen výchozí implementace ověřování z&nbsp;ABRA Flexi serveru:</p>
<pre class="brush: java">/**
 * Základní ověřovací mechanizmus, který používá obvykle ABRA Flexi. Ověřuje jméno a heslo oproti tabulce centralServer.csuzivatel.
 */
public class AuthDefault extends AuthBase {

    /**
     * Ověří uživatele podle hesla v centralServer.csuzivatel.
     * Pokud uživatel neexistuje, je userInfo null.
     *
     * @param connection
     * @param userInfo
     * @param username
     * @param password
     * @return
     */
    public boolean authenticate(Connection connection, UserInfo userInfo, String username, String password) {
        if (userInfo == null) {
            return false;
        }

        String md5password = DigestUtils.md5Hex(password);
        if (!md5password.equalsIgnoreCase(userInfo.getHeslo())) {
            return false;
        }

        return true;
    }
}</pre>

<p>To, jaká třída se má použít pro ověřování serveru se určuje pomocí proměnné <code>&lt;entry key="authClass"&gt;cz.winstrom.auth.AuthSimple&lt;/entry&gt;</code> v&nbsp;souboru <code>flexibee-server.xml</code>.</p>

<p>Doplňující soubory je nutné přidat do classpath. To např. na Linuxu můžete provést tak, že upravíte soubor <code>/etc/default/flexibee</code> a přidáte proměnnou <code>CLASSPATH</code>:</p>
<pre>
export CLASSPATH="cesta/ke/knihovne-1.0.jar:cesta/k/auth.jar"
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
