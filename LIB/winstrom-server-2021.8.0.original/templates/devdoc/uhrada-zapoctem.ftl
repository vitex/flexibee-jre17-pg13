<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Úhrada faktury vzájemným zápočtem závazků"/>
<#include "/i-devdoc-header.ftl" />

<p>ABRA Flexi umožňuje uhradit fakturu vzájemným zápočtem závazků. Pro danou firmu se zkouší k úhradě použít všechny dostupné závazky, jejichž typ má povoleno použití pro automatizovaný vzájemný zápočet. Fakturu lze takto uhradit i pouze částečně.</p>

<p>K tomu slouží akce <code>uhrad-zapoctem</code>:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana id="100" action="uhrad-zapoctem" /&gt;
&lt;/winstrom&gt;
</pre>

<p>Akci lze vyvolat i dávkově nad skupinou dokladů pomocí <a href="filters">filtru</a>:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana filter="not stavUhrK begins 'stavUhr.uhrazeno' and typDokl = 'code:INTERNET'" action="uhrad-preplatky" /&gt;
&lt;/winstrom&gt;</pre>



<#include "/i-devdoc-footer.ftl" />
</#escape>
