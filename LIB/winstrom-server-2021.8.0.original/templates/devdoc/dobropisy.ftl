<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Dobropisy"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST-API je možné provázat dobropis s fakturou.</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt; &lt;!-- dobropis; může být i "faktura-prijata" --&gt;
    &lt;id&gt;code:DOBROPIS1&lt;/id&gt;
    &lt;!-- lze normálně uvést další vlastnosti dokladu jako při běžném importu --&gt;

    &lt;vytvor-vazbu-dobropis&gt;
      &lt;dobropisovanyDokl&gt;code:FAKTURA1&lt;/dobropisovanyDokl&gt; &lt;!-- identifikátor dobropisovaného dokladu--&gt;
    &lt;/vytvor-vazbu-dobropis&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<p>Lze uvést ID dobropisované faktury (tag <code>&lt;dobropisovanyDokl&gt;</code>) podle <a href="identifiers">obvyklých pravidel</a>. Pokud tato faktura není nalezena, import skončí s chybou.</p>

<p>Jeden dobropis je možné navázat jen na jednu fakturu. Na jednu fakturu ale může být navázáno více dobropisů. V případě, že je již dobropis navázán na nějakou fakturu a mělo by dojít k navázání na nějakou jinou, import skončí s chybou.</p>

<p>Přes REST-API je také možné vazbu dobropisu zrušit.</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt; &lt;!-- dobropis; může být i "faktura-prijata" --&gt;
    &lt;id&gt;code:DOBROPIS1&lt;/id&gt;

    &lt;zrus-vazbu-dobropis&gt;&lt;/zrus-vazbu-dobropis&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<h3>Důležitá upozornění</h3>
<ul>
    <li>Služba vytváří <b>pouze vazbu</b>, dobropisující doklad musí mít správně vytvořené položky.</li>
    <li>Vytvořená vazba přidává pouze <b>propojení hlaviček</b> dokladů, neřeší propojení položek.</li>
    <li><b>Nákupní cena</b> uvedená na položkách by měla odpovídat <b>skladové ceně</b> zboží z dobropisovaného dokladu, jinak nebude sedět stav skladu.</li>
    <li>Pokud není uvedena nákupní cena, použije se aktuální <b>skladová cena</b>!</li>
    <li>Pokud není zboží skladem, použije se <b>nákupní cena z ceníku</b>!</li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>