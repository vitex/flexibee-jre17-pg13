<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Validace dat"/>
<#include "/i-devdoc-header.ftl" />

<p>Při ukládání dat je prováděna validace. Chyby mohou být tří typu:</p>
<ul>
    <li><strong>chyba:</strong> záznam kvůli této chybě nejde uložit a operace byla zrušena</li>
    <li><strong>varování:</strong> při ukládání se objevil problém, ale záznam byl uložen. </li>
    <li><strong>informace:</strong> doplňující informace pro uživatele. Záznam byl uložen.</li>
</ul>

<p>Ukázka chyby při ukládání:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;success&gt;true&lt;/success&gt;
  &lt;result&gt;
    &lt;id&gt;105&lt;/id&gt;
    &lt;warnings&gt;
      &lt;warning for="radekDph"&gt;Záznam nemá vyplněn řádek DPH a proto nebude doklad zaúčtován.&lt;/warning&gt;
    &lt;/warnings&gt;
    &lt;infos&gt;
      &lt;info&gt;Došlo k automatickému výběru výrobního čísla.&lt;/info&gt;
    &lt;/infos&gt;
  &lt;/result&gt;
  &lt;result&gt;
    &lt;id&gt;103&lt;/id&gt;
  &lt;/result&gt;
&lt;/winstrom&gt;
</pre>


<p>Pokud aplikace narazí na chybu, je zpracování okamžitě ukončeno. U varování a informací je proveden kompletní import a následně jsou vráceny všechny chybové stavy. Pokud chcete, aby i v&nbsp;případě varování nedošlo k&nbsp;uložení dat, přidejte do URL parametr: "<code>?fail-on-warning=true</code>".</p>
<p>Pokud chcete zvalidovat záznam a nechcete jej při tom ukládat (tzv. <a href="dry-run">dry-run</a>), přidejte do URL parametr "<code>?dry-run=true</code>".</p>

<ul>
        <li><a href="required-fields">Povinnost atribitů</a></li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>