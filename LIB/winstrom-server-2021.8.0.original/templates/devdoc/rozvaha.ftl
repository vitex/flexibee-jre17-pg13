<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Rozvaha"/>
<#include "/i-devdoc-header.ftl" />

<h3>Způsob volání</h3>

<p>Dostupná <a href="http-operations">HTTP metoda</a>: <code>GET</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/sestava.pdf</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Výsledkem je požadovaný PDF dokument.</p>


<h3>Parametry</h3>

Služba vyžaduje povinný parametr <code>report-name</code>, kterým zvolíte požadovanou tiskovou sestavu.

Možné varianty Rozvahy:
<ul>
  <li>rozvaha$$SUM_ZAKL - Rozvaha v základním rozsahu</li>
  <li>rozvaha$$SUM - Rozvaha ve zjednodušeném rozsahu</li>
  <li>rozvaha$$NES - Rozvaha v plném rozsahu</li>
</ul>

<h3>Ukázky volání</h3>

<ul>
  <li><code>GET https://demo.flexibee.eu/c/demo/sestava.pdf?report-name=rozvaha$$SUM_ZAKL</code></li>
  <li><code>GET https://demo.flexibee.eu/c/demo/sestava.pdf?report-name=rozvaha$$SUM</code></li>
  <li><code>GET https://demo.flexibee.eu/c/demo/sestava.pdf?report-name=rozvaha$$NES</code></li>
</ul>

Ve všech případech je odpovědí HTTP 200 a požadovaný dokument ve formátu PDF.

<#include "/i-devdoc-footer.ftl" />
</#escape>