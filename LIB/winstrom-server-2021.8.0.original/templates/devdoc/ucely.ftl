<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Vytvoření a zrušení účelu GDPR"/>
<#include "/i-devdoc-header.ftl" />

<p>Účel GDPR pro záznam lze přes XML vytvořit následujícím způsobem:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt; &lt;!-- záznam, může být z kterékoliv podporované evidence" --&gt;
    &lt;id&gt;code:FAKTURA1&lt;/id&gt;
    &lt;!-- lze normálně uvést další vlastnosti záznamu jako při běžném importu --&gt;

    &lt;vytvor-ucel&gt;
      &lt;definiceUcelu&gt;code:DEFINICE&lt;/definiceUcelu&gt;
      &lt;poznam&gt;Uživatelská poznámka&lt;/poznam&gt; &lt;!-- nepovinné --&gt;
    &lt;/vytvor-ucel&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<p>Účel GDPR lze také odvázat a smazat:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt; &lt;!-- záznam, může být z kterékoliv podporované evidence" --&gt;
    &lt;id&gt;code:FAKTURA1&lt;/id&gt;
    &lt;!-- lze normálně uvést další vlastnosti záznamu jako při běžném importu --&gt;

    &lt;zrus-ucel&gt;
      &lt;id&gt;1&lt;/id&gt;
    &lt;/zrus-ucel&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<p>Identifikátor účelu se získá dotazem na konkrétní evidenci. Ty, které podporují GDPR, obsahují relaci "ucely".</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>