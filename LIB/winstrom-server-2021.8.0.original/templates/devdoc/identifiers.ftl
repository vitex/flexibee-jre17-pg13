<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Identifikátory záznamů"/>
<#include "/i-devdoc-header.ftl" />

<p>Záznamy v ABRA Flexi je možné identifikovat několika způsoby:</p>

<table class="table table-striped table-condensed" summary="Přehled typů identifikátorů">
    <thead>
    <tr>
        <th>Název</th>
        <th>Poznámka</th>
        <th>Ukázka</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>Identifikátor ABRA Flexi</th>
        <td>Identifikátor přidělený aplikací. Přiděluje ABRA Flexi a nelze jej měnit.</td>
        <td><code>123</code></td>
    </tr>
    <tr>
        <th>Kód/Zkratka</th>
        <td>Uživatelské označení. Přiděleno uživatelem a je možné jej měnit v aplikaci.</td>
        <td><code>code:CZK</code></td>
    </tr>
<!-- Deprecated by Key
    <tr>
        <th><a href="http://en.wikipedia.org/wiki/UUID" title="Universally Unique Identifier">UUID</a></th>
        <td>Globálně unikátní náhodný identifikátor. Délka není nijak stanovena či omezena. V aplikaci jej nelze měnit. Lze měnit z externích systémů.</td>
        <td><code>uuid:550e8400-e29b-41d4-a716</code></td>
    </tr>
-->
    <tr>
        <th>Key</th>
        <td>Unikátní náhodný identifikátor, který přiděluje ABRA Flexi dokladům (atribut UUID). Nelze jej měnit.</td>
        <td><code>key:550e8400e29b41d4a716</code></td>
    </tr>
    <tr>
        <th><a href="http://en.wikipedia.org/wiki/Price_Look-Up_code" title="Price Look-Up code">PLU</a></th>
        <td>PLU je identifikační kód, který se používá při prodeji. Obvykle se jedná o 4- či 5místný číselný kód.</td>
        <td><code>plu:4020</code></td>
    </tr>
    <tr>
        <th><a href="http://cs.wikipedia.org/wiki/%C4%8C%C3%A1rov%C3%BD_k%C3%B3d#K.C3.B3dy_typu_EAN" title="European Article Number">EAN</a></th>
        <td>Záznam je identifikován podle kódu EAN (čárový kód). Položku ceníku je možné dohledat jak podle jejího EAN tak podle EAN jejího balení. Počet importovaných položek ceníku se v tomto případě nemění.</td>
        <td><code>ean:4710937332698</code></td>
    </tr>
    <tr>
        <th>Externí identifikátor</th>
        <td>Identifikátor z&nbsp;externí aplikace. V&nbsp;aplikaci jej nelze měnit. Lze měnit z&nbsp;externích systémů. Skládá se z&nbsp;identifikátoru externího systému a identifikátoru řádku v&nbsp;tomto systému. <strong>Externí identifikátor musí být unikátní pro celou evidenci (např. <code>faktura-vydana</code>)!</strong></td>
        <td><code>ext:SHOP:123</code></td>
    </tr>
    <tr>
        <th>VAT ID</th>
        <td>Identifikátor dle daňového čísla. V&nbsp;ČR odpovídá DIČ, v&nbsp;SR odpovídá IČO&nbsp;DPH.</td>
        <td><code>vatid:CZ28019920</code></td>
    </tr>
    <tr>
        <th>IČO</th>
        <td>Identifikátor dle IČO.</td>
        <td><code>in:28019920</code></td>
    </tr>
    <tr>
        <th>IBAN</th>
        <td>Identifikátor dle kódu IBAN.</td>
        <td><code>iban:CZ1201000002801992</code></td>
    </tr>
    </tbody>
</table>

<p>Díky <a href="partial-updates">inkrementální aktualizaci</a> je možné libovolně připojovat další externí identifikátory k&nbsp;již existujícím záznamům:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
    &lt;cenik id="123"&gt;
        &lt;id&gt;ext:SHOP:abc&lt;/id&gt;
        &lt;id&gt;ext:SYSTEM3:xyz&lt;/id&gt;
    &lt;/cenik&gt;
&lt;/winstrom&gt;
</pre>

<p>Tento zápis je ekvivalentní s tímto:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
    &lt;cenik&gt;
        &lt;id&gt;123&lt;/id&gt;
        &lt;id&gt;ext:SHOP:abc&lt;/id&gt;
        &lt;id&gt;ext:SYSTEM3:xyz&lt;/id&gt;
    &lt;/cenik&gt;
&lt;/winstrom&gt;
</pre>

<h3 id="internal_id">Interní identifikátory</h3>
<p>Interní identifikátor (samostatné číslo) jsou identifikátory, které přiděluje systém ABRA Flexi. Pokud se na něj odkážete a záznam neexistuje, operace selže (na rozdíl od ostatních identifikátorů).</p>
<p>Pro přidělování identifikátorů je používána databázová <code>sequence</code>. Ta zajišťuje, že stejný identifikátor nebude nikdy přidělen dvakrát (ani když záznam smažete). Současně negarantuje číselnou návaznost (např. při rollbacku se číslo zahodí).</p>


<h3>Vytvoření / aktualizace záznamu</h3>

<p>Pokud použijete pro identifikaci záznamu jiný typ identifikátoru než <a href="#internal_id">interní identifikátor</a> (např. kód) a odkazovaný záznam nebude existovat, tak dojde k vytvoření nového záznamu. V opačném případě se provede aktualizace existujícího záznamu.</p>
<p>Následující ukázka XML / JSON zajistí aktualizaci, případně vytvoření, záznamu v ceníku se zkratkou <b>T100</b> (hodnota identifikátoru zde nahrazuje uvedení atributu <code>kod</code>):</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
    &lt;cenik&gt;
        &lt;id&gt;code:T100&lt;/id&gt;
        &lt;nazev&gt;Téčko 100 mm&lt;/name&gt;
    &lt;/cenik&gt;
&lt;/winstrom&gt;
</pre>

<pre class="brush: js">{
    "winstrom": {
        "@version": "1.0",
        "cenik": [{
            "id": "code:T100",
            "name": "Téčko 100 mm"
        }]
    }
}
</pre>


<h3 id="multi-id">Vícenásobné identifikátory</h3>

<p>Záznam je možné identifikovat také použitím několika identifikátorů zároveň:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
    &lt;cenik&gt;
        &lt;id&gt;123&lt;/id&gt;
        &lt;id&gt;code:KRABICE&lt;/id&gt;
    &lt;/cenik&gt;
&lt;/winstrom&gt;
</pre>

<p>V takovém případě musí všechny identifikátory označovat tentýž záznam, jinak jde o chybu. Identifikátory, které v ABRA Flexi neexistují, budou ignorovány, takže z externího systému lze poslat do ABRA Flexi všechny známé identifikátory a záznam bude nalezen podle těch existujících.</p>

<p>Více elementů <code>id</code> lze použít pouze v importním XML. Na jiných místech (URL v REST API, ostatní identifikátory záznamů v importním XML, ale u elementů <code>id</code> lze také) je potřeba používat specializovanou syntaxi: <code>[123][code:CZK][ext:SHOP:abc]</code>.</p>

<p>Pokud identifikátor obsahuje znaky <code>[</code>, <code>]</code> nebo <code>\</code>, je potřeba je escapovat: <code>\[</code>, <code>\]</code>, <code>\\</code>. Při použití v URL je třeba nezapomenout také na správné zakódování URL jako takového.</p>


<h3>Mazání externích identifikátorů</h3>

<p>Provádí se atributem evidence "<code>removeExternalIds</code>", kde jeho hodnota představuje prefix externích identifikátorů, které se mají odstranit.</p>

<p>Např. mějme objekt ceníku s <code>ID=123</code> a externími identifikátory: "<code>SHOP:abc</code>", "<code>SYSTEM-1</code>" a "<code>SYSTEM-2</code>". Pak následující příklad odstraní oba identifikátory začínající řetězcem "<code>SYSTEM</code>" a přidá nový identifikátor "<code>SHOP:123</code>":

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
    &lt;cenik removeExternalIds="ext:SYSTEM"&gt;
        &lt;id&gt;123&lt;/id&gt;
        &lt;id&gt;ext:SHOP:123&lt;/id&gt;
    &lt;/cenik&gt;
&lt;/winstrom&gt;
</pre>

<p>Ekvivalentní zápis ve formátu JSON:</p>

<pre class="brush: js">{
    "winstrom": {
        "@version": "1.0",
        "cenik": [{
            "id": [ "123", "ext:SHOP:123" ],
            "@removeExternalIds": "ext:SYSTEM"
        }]
    }
}
</pre>

<p>Prefix externího identifikátoru "<code>ext:</code>" se v hodnotě atributu nemusí uvádět. Prázdný řetězec znamená, že chceme odstranit všechny externí identifikátory.</p>

<h4>Mazání externích identifikátorů z položek</h4>

<p>Provádí se podobně jako u hlavní evidence s tím, že atribut "<code>removeExternalIds</code>" je možné uvést společný pro všechny přítomné položky <b>a nebo</b> přímo na konkrétní položce (má přednost):</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
    &lt;faktura-vydana&gt;
        &lt;id&gt;123&lt;/id&gt;
        &lt;polozkyFaktury removeExternalIds="ext:P"&gt;
            &lt;faktura-vydana-polozka&gt;
                &lt;id&gt;10&lt;/id&gt;
            &lt;/faktura-vydana-polozka&gt;
            &lt;faktura-vydana-polozka removeExternalIds="ext:X"&gt;
                &lt;id&gt;20&lt;/id&gt;
            &lt;/faktura-vydana-polozka&gt;
        &lt;/polozkyFaktury&gt;
    &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>

<p>Výše uvedený příklad odebere z položky <code>10</code> externí identifikátory začínající znakem "P", zatímco z položky <code>20</code> externí identifikátory začínající znakem "X". Externí identifikátory případných dalších položek na faktuře zůstanou nezměněny.</p>

<p>Zápis předchozího příkladu ve formátu <code>JSON</code> by vypadal takto:</p>

<pre class="brush: js">{
    "winstrom": {
        "@version": "1.0",
        "faktura-vydana": [{
            "id": "123",
            "polozkyFaktury": [
                { "id": "10" },
                { "id": "20", "@removeExternalIds": "ext:X" }
            ],
            "polozkyFaktury@removeExternalIds": "ext:P"
        }]
    }
}
</pre>


<#include "/i-devdoc-footer.ftl" />
</#escape>
