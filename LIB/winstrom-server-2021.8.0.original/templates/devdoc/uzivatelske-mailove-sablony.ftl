<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Uživatelské mailové šablony"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API (i webové rozhraní) lze vytvořit a získat mailové šablony pro <a href="odesilani-mailem">posílání dokladů</a> a Aktualizaci mezd. Tato evidence je dostupná pouze pro licenci Premium.</p>

<p>Chceme-li získat šablonu, použijeme standardně URL <code>/c/{firma}/sablona-mail/{id}</code>. Lze samozřejmě použít i <a href="detail-levels">úroveň detailu</a>.</p>

<p>Pro vytvoření šablony pošleme XML/JSON na URL <code>/c/{firma}/sablona-mail</code>.</p>

<p>Chceme-li šablonu připojit k typu dokladu (zde konkrétně typ faktury vydané), na výše zmíněné URL pošleme například takovéto XML:</p>

<pre class="brush: xml">
&lt;winstrom version="1.0"&gt;
  &lt;typ-faktury-vydane&gt;
    &lt;kod&gt;{kod}&lt;/kod&gt;
    &lt;nazev&gt;FAV se šablonou&lt;/nazev&gt;
    &lt;modul&gt;FAV&lt;/modul&gt;
    &lt;radaPrijem&gt;code:FAKTURA-STANDARD&lt;/radaPrijem&gt;
    &lt;typDoklK&gt;typDokladu.faktura&lt;/typDoklK&gt;
    &lt;sablonaMail&gt;
        {id šablony, kterou chceme přiřadit}
    &lt;/sablonaMail&gt;
    &lt;poznam&gt;Šablona pro faktury vydané&lt;/poznam&gt;
  &lt;/typ-faktury-vydane&gt;
&lt;/winstrom&gt;
</pre>

<h2>Příklady šablony a Freemarker proměnné</h2>

<p>Využívá se šablonovací systém <a href="https://freemarker.apache.org/docs/index.html">FreeMarker</a>.</p>

<p>Před uložením šablony program zkontroluje, zda šablona neobsahuje nepovolené výrazy.</p>

<#noparse>
<p>V šablonách je možné použít následující proměnné:</p>
<ul>
    <li><b>${application}</b> - Název aplikace, tedy "ABRA Flexibee"</li>
    <li><b>${user}</b> - Objekt uživatele, se kterým můžeme dále pracovat</li>
    <li><b>${company}</b> - Nastavení firmy</li>
    <li><b>${uzivatelJmeno}</b> - Vaše křestní jméno</li>
    <li><b>${uzivatelPrijmeni}</b> - Vaše příjmení</li>
    <li><b>${titulJmenoPrijmeni}</b> - Vaše celé jméno, včetně dosažených titulů</li>
    <li><b>${nazevFirmy}</b> - Název firmy</li>
    <li><b>${object}</b> - Obecný přístup na předávaný objekt</li>
    <li><b>${doklad}</b> - Doklad určený k odeslání</li>
</ul>
Zde naleznete <a href="https://demo.flexibee.eu/c/demo/evidence-list">seznam vlastností</a> pro příslušné evidence - vlastnosti dokladů
</#noparse>

<h3>Příklady použití těchto proměnných v šabloně</h3>

Pro dodržení odstavců použijte HTML element <a href="https://developer.mozilla.org/en-US/docs/Web/HTML/Element/p">paragraph</a>.
Bez tohoto elementu dojde ke sloučení textu do jednoho odstavce.

<#noparse>
Jak si můžete všimnout, při přistupování na jednotlivé vlastnosti objektů (např. <b>${doklad.firma!}</b>) se používá na konci vykřičník jakožto kontrola NULL hodnot.
</#noparse>

<pre>
<#noparse>
Dobrý den, zasílám Vám doklad <b>${doklad}</b>, jehož interní číslo je <b>${doklad.kod}</b>. Jmenuji se <b>${uzivatelJmeno} ${uzivatelPrijmeni}</b>, včetně mého titulu <b>${titulJmenoPrijmeni}</b>, pracuji pro <b>${nazevFirmy}</b>. Mé telefonní číslo je <b>${user.mobil}</b>, DIČ firmy je <b>${company.dic}</b>.
</#noparse>
</pre>

<pre>
<#noparse>
Dobrý den, uživatel <b>${user}</b> Vám zasílá <b>${doklad.nazev}</b>. S pozdravem <b>${titulJmenoPrijmeni}</b>, zasláno z aplikace <b>${application}</b>.
</#noparse>
</pre>

<pre>
<#noparse>
Dobrý den, společnost <b>${nazevFirmy}</b> Vám fakturuje <b>${doklad.sumCelkem}</b> za své služby. S pozdravem <b>${titulJmenoPrijmeni}</b>, zasláno z aplikace <b>${application}</b>.
</#noparse>
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
