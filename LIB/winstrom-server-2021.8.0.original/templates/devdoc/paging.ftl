<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Stránkování"/>
<#include "/i-devdoc-header.ftl" />

<p>Kvůli zvýšení výkonu webových aplikací se běžně používá systém "stránkování". Seznam je rozdělen na stránky, které jsou uživateli zobrazeny a on mezi nimi může přepínat.</p>
<p>Stránkování se řídí parametry URL:</p>

<table class="table table-striped table-condensed">
    <thead>
    <tr>
        <th>Parameter</th>
        <th>Název</th>
        <th>Ukázka</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><code>limit</code></th>
        <td>Maximální počet záznamů na jedné stránce. Implicitně je <code>20</code>. Hodnota <code>0</code> vrací všechny záznamy bez omezení.</td>
        <td><code>/c/firma/adresar?limit=10</code></td>
    </tr>
    <tr>
        <th><code>start</code></th>
        <td>Kolik záznamů přeskočit. Není závislé na parametru <code>limit</code>.</td>
        <td><code>/c/firma/adresar?limit=10&amp;start=4</code></td>
    </tr>
    <tr>
        <th><code>add-row-count=true</code></th>
        <td>Pokud uvedete tento parameter, bude do XML/JSON exportu přidán počet záznamů v&nbsp;dané evidenci (zohledňuje aplikované filtry).</td>
        <td><code>/c/firma/adresar.xml?add-row-count=true</code></td>
    </tr>
    </tbody>
</table>


<#include "/i-devdoc-footer.ftl" />
</#escape>