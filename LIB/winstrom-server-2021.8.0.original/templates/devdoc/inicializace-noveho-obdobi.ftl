<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Inicializace nového období"/>
<#include "/i-devdoc-header.ftl" />

<h3>Způsob volání</h3>

<p>Dostupná <a href="http-operations">HTTP metoda</a>: <code>GET</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/ucetni-obdobi/inicializace-noveho-obdobi.json</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Jsou podporovány <a href="format-types">výstupní formáty</a>: <code>XML</code> nebo <code>JSON</code>.</p>

Jedná se o ekvivalent stejné funkcionality v GUI.

<h3>Parametry</h3>

Je nutné uvést následující povinné parametry:
<ul>
  <li><code>ucetOtv</code> - Účet otevření účetní knihy (druhUctuK="druhUctu.otevknih")</li>
  <li><code>ucetZav</code> - Účet uzavření účetní knihy (druhUctuK="druhUctu.uzavknih")</li>
  <li><code>ucetPre</code> - Účet převodu hospodářského výsledku (druhUctuK="druhUctu.prhosvys")</li>
  <li><code>ucetVys</code> - Účet výsledku hospodaření ve schvalovacím řízení (druhUctuK="druhUctu.pasivhvy")</li>
</ul>

Ukázka volání: <code>https://demo.flexibee.eu/c/demo/ucetni-obdobi/inicializace-noveho-obdobi.json?ucetOtv=code:701000&ucetZav=code:702000&ucetPre=code:710000&ucetVys=code:431001</code>

Volitelně je možné nastavit tyto parametry:
<ul>
  <li><code>preceneni</code> - Provést přecenění neuhrazených dokladů (true/false)</li>
  <li><code>prevodSkladu</code> - Provést převod skladu (true/false)</li>
  <li><code>vynechatNulove</code> - Vynechat kartu s nulovým zůstatkem (true/false)</li>
  <li><code>dnyBezPohybu</code> - Počet dnů bez pohybu pro vynechání (celé číslo)</li>
  <li><code>zrusitStare</code> - V novém účetním období zrušit nepoužíváné staré karty (true/false)</li>
  <li><code>typDokl</code> - Typ dokladu pro generování závazků leasingových splátek</li>
</ul>

<b>Výchozí</b> hodnoty všech booleanových parametrů jsou false.


Pokud některý z povinných parametrů chybí, vrátí se např. tato chyba:
<pre class="brush: javascript">
{
  "winstrom": {
    "@version": 1,
    "success": false,
    "message": "K provedení operace je vyžadován parametr 'ucetOtv'"
  }
}
</pre>

Pokud byl vybrán nesprávný účet (např. ucetZav=701000), vrátí se tato chyba:
<pre class="brush: javascript">
{
  "winstrom": {
    "@version": 1,
    "success": false,
    "message": "Parametr 'ucetZav' má nepodporovanou hodnotu! Zvolte jednu z následujících možností: [Zvolený účet musí mít druhUctuK 'druhUctu.uzavknih']"
  }
}
</pre>

<h3>Volání</h3>

Pokud neexistuje následující účetní období, vrací se chyba 400 a dokument:

<pre class="brush: javascript">
{
  "winstrom": {
    "@version": 1,
    "success": false,
    "message": "Neexistuje následující účetní období. Prosím založte ho."
  }
}
</pre>

Po založení účetního období např. odtud <code>/flexi/demo/settings/ucetni-obdobi</code> lze opakovaně volat inicializaci.

Pokud mají některé typy dokladů nestandardně nastavené zaokrouhlení, následuje další chyba:
<pre class="brush: javascript">
{
  "winstrom": {
    "@version": 1,
    "success": false,
    "message": "Následující typy dokladů mají nestandardně nastavené zaokrouhlení DPH (očekávané je zaokrouhlení na setiny nebo jednotky, viz § 37 ZDPH):\nFAKTURA: nastaveno \"0.1\"\nOBP: nastaveno \"0.1\"\nNásledující typy dokladů mají nestandardně nastavený způsob zaokrouhlení DPH (očekávané je zaokrouhlení matematicky, viz § 37 ZDPH):\nFAKTURA: nastaveno \"nahoru\"\nOBP: nastaveno \"nahoru\"\nZÁLOHA: nastaveno \"nahoru\""
  }
}
</pre>

Tuto chybu lze potlačit parametrem <code>kontrolaZaokrouhleni=false</code> (obdoba tlačítka Ano), nebo opravit zaokrouhlení na typech dokladů.

Volbu “Potvrzovat vynechání karty” (GUI) nebude API podporovat.

Pokud existují závazky pro následující účetní období, je parametr typDokl povinný. Není-li v tomto případě uveden, vrátí se odpovídající chyba:
<pre class="brush: javascript">
{
  "winstrom": {
    "@version": 1,
    "success": false,
    "message": "K provedení operace je vyžadován parametr 'typDokl'"
  }
}
</pre>

Vybraný typ dokladu musí mít uloženou řadu dokladu, jinak se vrátí chyba:
<pre class="brush: javascript">
{
  "winstrom": {
    "@version": 1,
    "success": false,
    "message": "Vyplněný typ dokladu nemá zadanou řadu dokladu a žádná není specifikovaná."
  }
}
</pre>

Řada typu dokladu musí mít uloženou roční položku číselné řady k následujícímu účetnímu období, jinak se vrátí chyba:
<pre class="brush: javascript">
{
  "winstrom": {
    "@version": 1,
    "success": false,
    "message": "Vybraná řada typu dokladu nemá zadanou roční položku číselné řady k následujícímu účetnímu období."
  }
}
</pre>

Následuje kontrola kurzů pro přecenění, před voláním inicializace je možné zavolat subresource:
<code>GET https://demo.flexibee.eu/c/demo/ucetni-obdobi/meny-bez-kurzu.json</code>
, který vrátí např.
<pre class="brush: javascript">
{
  "meny-bez-kurzu": {
    "datumPreceneni": "2020-12-31T00:00:00+01:00",
    "meny": {
      "mena": "EUR"
    }
  }
}
</pre>

Tyto hodnoty mají být uloženy do nového kurzu v resource <code>https://demo.flexibee.eu/c/demo/kurz/ (platiOdData, mena).</code>

Pokud při inicializaci některé kurzy chybí, vrátí se:
<pre class="brush: javascript">
{
  "winstrom": {
    "@version": 1,
    "success": false,
    "message": "Nebyly zadány všechny potřebné kurzy platné k poslednímu dni účetního období,\nkteré jsou nutné pro přecenění neuhrazených pohledávek/závazků."
  }
}
</pre>

<h3>Výsledek</h3>

Pokud má inicializace všechna potřebná data, spustí proces na pozadí a vrátí status 202 Accepted.

Na resource <code>/c/{firma}/ucetni-obdobi</code> je možné kontrolovat, jestli už inicializace skončila (property <b>lastUpdate</b> aktuálního účetního období).

<#include "/i-devdoc-footer.ftl" />
</#escape>