<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Changes API"/>
<#include "/i-devdoc-header.ftl" />

<h2>Changes API (sledování změn)</h2>

<p>Je-li zapnuté, ABRA Flexi zaznamenává všechny změny provedené v databázi firmy do <i>changelogu</i> a umožňuje seznam změn zpětně získat. Změny jsou vzestupně číslované, takže firma má v každém okamžiku dobře definovanou <i>globální verzi</i>. (Čísla verzí nemusí následovat těsně po sobě, v řadě mohou být z technických důvodů mezery. Vždy je však číslo verze unikátní a rostoucí.) Toho lze využít k automatizované synchronizaci externích systémů s ABRA Flexi a také jde o základ pro funkci okamžitého upozorňování na změny (<a href="web-hooks">Web Hooks</a>).</p>

<p>Licence ABRA Flexi musí mít aktivní REST API minimálně pro čtení. Zjištění stavu a zapnutí / vypnutí lze provést nejsnáze ve webovém rozhraní na adrese <code>/c/{firma}/changes/control</code>. Případně lze zapnout <code>PUT</code> requestem na adresu <code>/c/{firma}/changes/enable.xml</code> a vypnout taktéž <code>PUT</code> requestem na adresu <code>/c/{firma}/changes/disable.xml</code>. Kromě <code>PUT</code> lze použít také <code>POST</code>. Pokud nemáte aktivní REST API pro čtení nebo pro zápis, odpověď je <code>403 Forbidden</code>.</p>

<p>Ukázka aktivace pomocí programu curl:</p>
<pre class="shell">curl -k -L -u jmeno:heslo -X PUT https://localhost:5434/c/{firma}/changes/enable.xml -H Content-Length:0</pre>

<h2 id="globalVersion">Získání aktuální globální verze</h2>

<p>Do jakéhokoliv XML (resp. JSON) exportu získaného přes REST API lze doplnit aktuální globání verzi přidáním parametru <code>?add-global-version=true</code>. Odpověď bude vypadat takto:</p>

<pre class="brush: xml">&lt;winstrom version="1.0" globalVersion="6"&gt;
  ...
&lt;/winstrom&gt;
</pre>

<h2>Získání záznamů o změnách</h2>

<p>Na adrese <code>/c/firma/changes.xml</code> se nachází seznam všech změn od začátku jejich sledování. Výpis vypadá takto:</p>

<pre class="brush: xml">&lt;winstrom version="1.0" globalVersion="6"&gt;
  &lt;faktura-vydana in-version="3" operation="create" timestamp="2019-01-01 00:00:00.0"&gt;
    &lt;id&gt;1&lt;/id&gt;
  &lt;/faktura-vydana&gt;
  &lt;faktura-vydana-polozka in-version="4" operation="create" timestamp="2019-06-07 12:34:56.7"&gt;
    &lt;id&gt;1&lt;/id&gt;
  &lt;/faktura-vydana-polozka&gt;
  &lt;faktura-vydana in-version="5" operation="update" timestamp="2019-06-07 12:34:56.7"&gt;
    &lt;id&gt;1&lt;/id&gt;
    &lt;id&gt;code:VF1-0001/2012&lt;/id&gt;
  &lt;/faktura-vydana&gt;
  &lt;next&gt;6&lt;/next&gt;
&lt;/winstrom&gt;
</pre>

<p>Uvedeno je vždy číselné ID objektu (<code>&lt;id&gt;1&lt;/id&gt;</code>) a kód (<code>&lt;id&gt;code:KÓD&lt;/id&gt;</code>); pokud měl objekt v době provádění operace i nějaká externí ID, pak jsou uvedena i ta (<code>&lt;id&gt;ext:...&lt;/id&gt;</code>).</p>

<p>V atributech každého elementu je uvedeno, v jaké verzi k operaci došlo (<code>in-version</code>) a o jakou operaci šlo (<code>operation</code>; možné hodnoty jsou <code>create</code>, <code>update</code> a <code>delete</code>).</p>

<p>Vždy je přítomen atribut <code>globalVersion</code>. Posledním elementem ve výpisu je vždy <code>next</code>, který udává číslo verze, kterou by tento výpis pokračoval, případně <code>none</code>, pokud žádné další změny nejsou.</p>

<p>Výpis lze upravit následujícími parametry:</p>

<table class="table table-striped table-condensed">
    <tr><th><code>?start=123</code></th><td>Od které verze se má vypisovat (včetně); defaultně od počátku sledování.</td></tr>
    <tr><th><code>?limit=500</code></th><td>Kolik záznamů se má vypsat; defaultně 100, maximálně 1000.</td></tr>
    <tr><th><code>?evidence=faktura-vydana</code></th><td>Pro které evidence se mají změny vypisovat; lze uvést vícekrát, není-li uvedeno, vypisují se všechny.</td></tr>
</table>

<p>Ve formátu JSON vypadají změny takto:</p>

<pre class="brush: js">
{
    "winstrom": {
        "@globalVersion": "8",
        "changes": [
            {
                "@evidence": "faktura-vydana",
                "@in-version": "3",
                "@operation": "create",
                "@timestamp": "2019-01-01 00:00:00.0",
                "id": "1",
                "external-ids": []
            },
            {
                "@evidence": "faktura-vydana-polozka",
                "@in-version": "4",
                "@operation": "create",
                "@timestamp": "2019-06-07 12:34:56.7",
                "id": "1",
                "external-ids": []
            },
            {
                "@evidence": "faktura-vydana",
                "@in-version": "5",
                "@operation": "update",
                "@timestamp": "2019-06-07 12:34:56.7",
                "id": "1",
                "external-ids": ["code:VF1-0001/2012"]
            }
        ],
        "next": "6"
    }
}
</pre>

<h2>Zjištění stavu zapnutí Changes API</h2>

<p>Uživatelsky lze sjistit stav zapnutí na adrese <code>/c/{firma}/changes/control</code>. Zde je také možné Changes API zapínat nebo vypínat.</p>

<p>Pokud potřebujete zjistit stav programově, tak použijte <code>GET /c/firma/changes/status.xml</code>. V případě odpovědi <code>&lt;winstrom&gt;&lt;success&gt;true&lt;/success&gt;&lt;/winstrom&gt;</code> je Changes API zapnuté. Pokud je odpovědí false nebo chyba (pokud není povolené REST API) je Changes API vypnuté.</p>

<h2>Synchronizace externích systémů s ABRA Flexi</h2>

<p>Verzované změny lze snadno využít k efektivní synchronizaci externích systémů s ABRA Flexi (na rozdíl od <a href="last-update">data poslední změny</a>). Postup je následující:</p>

<h3>Počáteční nahrání dat:</h3>
<ol>
    <li>Získat aktuální data včetně jejich verze (<code>?add-global-version=true</code>)</li>
    <li>Uložit data</li>
    <li>Zapamatovat si verzi (z atributu <code>globalVersion</code>)</li>
</ol>

<h3>Rozdílová synchronizace:</h3>

<ol>
    <li>Stáhnout změny od poslední zapamatované verze (<code>?start=&lt;verze&gt;</code>)</li>
    <li>Stáhnout změněná data a uložit je, případně smazat odstraněná data</li>
    <li>Zapamatovat si verzi (z elementu <code>next</code>, případně z atributu <code>globalVersion</code>)</li>
    <li>GOTO 1</li>
</ol>

<h3>ERROR: could not obtain lock on relation "????"</h3>

<p>Pokud se vám zobrazí chyba <code>ERROR: could not obtain lock on relation "????"</code>, nezoufejte. Kvůli výkonnosti nejsou v databázi funkce, které obsluhují Changes API vůbec přidané. V případě aktivace Changes API je do systému zaneseme. Proto je potřeba exkluzivně zamknout celou databázi.</p>

<p>Řešením tedy je se odhlásit z ABRA Flexi - jak z webového rozhraní tak klientské aplikace. Pak už to projde.</p>

<p>Ukázka chyby:</p>
<pre>
ERROR: could not obtain lock on relation "drady"
Kde: SQL statement "LOCK TABLE drady IN ACCESS EXCLUSIVE MODE NOWAIT"
</pre>


<#include "/i-devdoc-footer.ftl" />
</#escape>
