<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Autentizace kontaktu"/>
<#include "/i-devdoc-header.ftl" />

<p>Ekonomický systém ABRA Flexi umožňuje použít kontakty uložené v databázi k autentizaci.</p>

<p>Pomocí API je nejdříve nutné kontaktu nastavit jméno a heslo. Existují dva způsoby:</p>

<ul>
<li>
<p>Heslo je možné zaslat ve formě plain-text:</p>
<pre>POST /c/firma/kontakty/1.xml

&lt;kontakt&gt;
&lt;username&gt;jan&lt;/username&gt;
&lt;password hash=&quot;&quot; salt=&quot;&quot;&gt;heslo&lt;/password&gt;
&lt;/kontakt&gt;
</pre>

<p>Heslo, zaslané jako plain-text, je v ABRA Flexi uložené v bezpečné formě pomocí hash funkce.</p>
</li>
<li>
<p>Heslo je možné zaslat ve formě výsledku volání hash funkce. V tomto případě jsou povinné parametry salt a typ hash funkce:</p>
<pre>POST /c/firma/kontakty/1.xml

&lt;kontakt&gt;
&lt;username&gt;jan&lt;/username&gt;
&lt;password hash=&quot;sha256&quot; salt=&quot;abcd&quot;&gt;ed9bddc9f20f1aa3587979c9536c625d485&lt;/password&gt;
&lt;/kontakt&gt;
</pre>
<p>Hodnota elementu <code>&lt;password&gt;</code> je zde výsledek hash funkce aplikované na řetězec vzniklý spojením hesla a hodnoty salt. Původní heslo v tomto případě není nutné zasílat.</p>
<p>Podporované typy hash funckí:</p>
<ul>
<li>sha1</li>
<li>sha256</li>
<li>sha512</li>
<li>pbkdf2</li>
<li>md5</li>
</ul>

</li>
</ul>

<p>Poté je možné pomocí API autentizovat kontakt voláním:</p>

<pre>POST /c/firma/kontakty/1/authenticate

Accept: application/xml
Content-Type: application/x-www-form-urlencoded

username=jan&password=heslo
</pre>

<p>Autentizace funguje také na obecné URL kontaktů:</p>

<pre>POST /c/firma/kontakt/authenticate

Accept: application/xml
Content-Type: application/x-www-form-urlencoded

username=jan&password=heslo
</pre>

<p>Výsledkem je vždy odpověď s HTTP status kódem 200, výsledek je uvedený v těle odpovědi:</p>
<ul>
<li>
<p>Úspěšná autentizace:</p>
<pre>
&lt;?xml version=&quot;1.0&quot; ?&gt;

&lt;winstrom version=&quot;1.0&quot;&gt;
  &lt;success&gt;true&lt;/success&gt;
  &lt;message&gt;&lt;/message&gt;
&lt;/winstrom&gt;
</pre>
</li>
<li>
<p>Neúspěšná autentizace:</p>
<pre>
&lt;winstrom version=&quot;1.0&quot;&gt;
&lt;success&gt;false&lt;/success&gt;
&lt;message&gt;Bylo zad&#225;no chybn&#233; uživatelsk&#233; jm&#233;no či heslo.&lt;/message&gt;
&lt;/winstrom&gt;
</pre>
</li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
