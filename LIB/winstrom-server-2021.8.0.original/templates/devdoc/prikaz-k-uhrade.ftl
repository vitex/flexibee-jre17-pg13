<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Příkaz k úhradě"/>
<#include "/i-devdoc-header.ftl" />

<p>Příkaz k úhradě lze vytvářet přes REST API klasicky <a href="http-operations">HTTP operací</a> PUT nebo POST na evidenci <code>/c/{firma}<strong>/prikaz-k-uhrade</strong></code>.</p>

<p>Ukázka vytvoření příkazu ve formátu <code>XML</code>:
<pre class="brush: xml">
&lt;winstrom version="1.0"&gt;
    &lt;!-- Příkazy k úhradě --&gt;
    &lt;prikaz-k-uhrade&gt;
        &lt;banka&gt;code:BANKOVNÍ ÚČET&lt;/banka&gt;
        &lt;polozky&gt;
            &lt;!-- Položky --&gt;
            &lt;prikaz-k-uhrade-polozka&gt;
                &lt;buc&gt;123456&lt;/buc&gt;
                &lt;castka&gt;1200.0&lt;/castka&gt;
                &lt;smerKod&gt;code:0100&lt;/smerKod&gt;
            &lt;/prikaz-k-uhrade-polozka&gt;
            &lt;!-- Položka uhrazující zadaný doklad --&gt;
            &lt;prikaz-k-uhrade-polozka&gt;
                &lt;doklFak&gt;code:PF1478/2020&lt;/doklFak&gt;
            &lt;/prikaz-k-uhrade-polozka&gt;
        &lt;/polozky&gt;
    &lt;/prikaz-k-uhrade&gt;
&lt;/winstrom&gt;
</pre>

<h3>Pokročilejší operace</h3>

<table class="table table-striped table-condensed">
    <tr>
        <td>GET</td>
        <td>
            <code>/c/{firma}/prikaz-k-uhrade/{id}<strong>/stazeni</strong></code><br/>
            <code>?<strong>dat-splat-z-hlavicky</strong>=</code> možnosti: <code>true</code>, <code>false</code>
        </td>
        <td>
            Získá soubor s elektronickým popisem příkazu k úhradě vhodný k odeslání do banky.<br/>
            Volitelným parametrem <code>dat-splat-z-hlavicky</code> lze ovlivnit nastavení volby data splatnosti z hlavičce příkazu.
        </td>
    </tr>
    <tr>
        <td>GET</td>
        <td>
            <code>/c/{firma}/prikaz-k-uhrade/{id}<strong>/odeslani-fio</strong></code><br/>
            <code>?<strong>dat-splat-z-hlavicky</strong>=</code> možnosti: <code>true</code>, <code>false</code>
        </td>
        <td>
            Provede online odeslání příkazu k úhradě (pouze Fio banka s online napojením).<br/>
            Volitelným parametrem <code>dat-splat-z-hlavicky</code> lze ovlivnit nastavení volby data splatnosti z hlavičce příkazu.
        </td>
    </tr>
    <tr>
        <td>PUT/POST</td>
        <td><code>/c/{firma}/prikaz-k-uhrade/{id}<strong>/zrusit-odeslani</strong></code></td>
        <td>Zruší stav odeslání příkazu.</td>
    </tr>
</table>

<#include "/i-devdoc-footer.ftl" />
</#escape>