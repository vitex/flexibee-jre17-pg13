<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Stav skladu k datu"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API lze zjišťovat stav skladu na URL:</p>

<code>/c/{firma}/stav-skladu-k-datu</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.

<h3>Parametry</h3>

<table class="table table-striped table-condensed">
    <tr>
        <th>Parametr</th>
        <th>*</th>
        <th>Popis</th>
        <th>Výchozí hodnota</th>
    </tr>
    <tr>
        <th><code>sklad</code></th>
        <td>&times;</td>
        <td>Identifikátor skladu, který lze zadat jako:
            <ol type="a">
                <li>celočíselný identifikátor</li>
                <li>vyhledávací kód (<code>code:KOD_SKLADU</code>)</li>
            </ol>
        </td>
        <td />
    </tr>
    <tr>
        <th><code>datum</code></th>
        <td />
        <td>Datum k němuž se provádí zjištění stavu skladu. Do výpočtu jsou zahrnuty skladové pohyby s datem vystavení do zadaného data včetně.</td>
        <td>Aktuální den</td>
    </tr>
</table>

<p><small>* = povinný parametr</small></p>


<h3>Ukázky</h3>

<ul>
    <li><code>GET /c/demo/stav-skladu-k-datu?sklad=4</code></li>
    <li><code>GET /c/demo/stav-skladu-k-datu?sklad=code:SKLAD&amp;datum=2012-12-12</code></li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
