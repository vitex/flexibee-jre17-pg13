<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Import EET certifikátů"/>
<#include "/i-devdoc-header.ftl" />

<p>EET certifikáty je možné importovat v API rozhraní. Importovaný soubor musí obsahovat <b>certifikát</b> i <b>privátní klíč</b>.</p>

<h3>Způsob volání</h3>

<p>Lze využít <a href="http-operations">HTTP metodu</a>: <code>PUT</code> nebo <code>POST</code>.</p>

<p>Služba je dostupná na adrese: <code>/c/{firma}/certifikat-eet/import</code>, kde <i>{firma}</i> je databázový idenfitikátor firmy.</p>

<p>Jsou podporovány <a href="format-types">výstupní formáty</a>: <code>XML</code> nebo <code>JSON</code>.</p>

<p>Příklad volání:</p>

<code>curl -sk -u jmeno:heslo -T {jmeno_souboru} -X PUT(/POST)
"https://localhost:5434/c/{firma}/certifikat-eet/import?heslo={heslo}&provozovna={provozovna}"
 -H 'Content-Type: {content-type}' -H 'Accept: {application/xml | application/json}'</code>

<h3>Parametry</h3>

<p>Oba parametry (<i>heslo</i>, <i>provozovna</i>) jsou povinné, parametr <i>heslo</i> slouží k odemčení certifikátu a <i>provozovna</i> je označení Vaší EET provozovny.</p>
<p>Při špatném hesle a neplatné (či chybějící) provozovně dojde k chybě. Operace skončí kódem 400.</p>

<p>Dále je nutné uvést v hlavičce správný <code>Content-Type</code> odpovídající formátu importovaného souboru:</p>
<table class="table table-striped table-condensed" summary="Přehled podporovaných formátů">
<tr><th>MIME type</th><th>Formát souboru s certifikátem</th></tr>
<tr>
<td><code>application/x-pem-file</code></td>
<td>pro X.509 certifikáty v textovém PEM formátu (obvyklé přípony: .pem, .cer, .cert, .crt)</td>
</tr>
<tr>
<td><code>application/x-pkcs12</code></td>
<td>pro certifikáty ve formátu PKCS #12 (obvyklé přípony: .p12, .pfx)</td>
</tr>
</table>

<#include "/i-devdoc-footer.ftl" />
</#escape>