<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Generování faktur ze smluv"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API (i přes webové rozhraní) lze vygenerovat faktury ze smluv (buďto všech nebo jedné konkrétní). Jde o jednoduché volání přes <code>PUT</code> nebo <code>POST</code>:</p>

<table class="table table-striped table-condensed">
    <tr><th><code>/c/firma/smlouva/generovani-faktur.xml</code></th><td>Vygeneruje faktury pro všechny smlouvy</td></tr>
    <tr><th><code>/c/firma/smlouva/1/generovani-faktur.xml</code></th><td>Vygeneruje faktury pro smlouvu s ID 1</td></tr>
</table>

<p>Při volání přes REST API má odpověď (ve formátu XML) následující podobu:</p>

<pre>
&lt;winstrom version="1.0"&gt;
  &lt;operation&gt;Generování faktur&lt;/operation&gt;
  &lt;success&gt;ok&lt;/success&gt;
  &lt;messages&gt;
    &lt;message&gt;Počet úspěšně vygenerovaných faktur: 1&lt;/message&gt;
  &lt;/messages&gt;
  &lt;errors&gt;
    &lt;error&gt;...&lt;/error&gt;
  &lt;/errors&gt;
&lt;/winstrom&gt;
</pre>

<p>Element <code>success</code> může nabývat hodnot <code>ok</code>, <code>partial</code>, <code>failed</code> a <code>unknown</code>:</p>

<table class="table table-striped table-condensed">
    <tr><th><code>ok</code></th><td>Vygenerování faktur proběhlo v pořádku (ale nemusely být vygenerovány žádné faktury, pokud to nebylo potřeba)</td></tr>
    <tr><th><code>partial</code></th><td>Pro některé smlouvy proběhlo vygenerování faktur v pořádku, ale u jiných smluv došlo k chybě</td></tr>
    <tr><th><code>failed</code></th><td>Nebyly vygenerovány žádné faktury, u některých smluv došlo k chybě</td></tr>
    <tr><th><code>unknown</code></th><td>Nemělo by nikdy nastat</td></tr>
</table>

<p>Elementy <code>message</code> obsahují hlášení o úspěchu (nejvýše jedno), elementy <code>error</code> chybová hlášení (jedno pro každou smlouvu, u které došlo k chybě).</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>