<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Workflow a procesy"/>
<#include "/i-devdoc-header.ftl" />

<p>Základem workflow a procesů je nástroj <a href="http://www.activiti.org/">Activiti</a>. Jeho součástí je programátorské rozhraní <a href="http://www.activiti.org/userguide/index.html#N12C5C">Activiti REST API</a> Toto rozhraní je přístupné na této adrese:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/activiti/</pre>

<p>Existují tato omezení:</p>
<ul>
    <li>Není podporované rozhraní <code>/login</code>. Používá se autorizace ABRA Flexi.</li>
    <li>Není podporovaná modifikace uživatelů, skupin a členství. Vše se dělá přes rozhraní ABRA Flexi.</li>
    <li>Nahrání nového procesu musí být realizováno přes ABRA Flexi (kvůli definici platností workflow). Aktualizace procesů mohou být již přes Activiti API.</li>
</ul>

<h2>API pro ABRA Flexi</h2>

<p>Výpis definicí workflow k dané evidenci:</p>
<pre>GET /c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/workflows.xml</pre>

<p>Spuštění workflow</p>
<pre>PUT /c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:red">&lt;id záznamu&gt;</font>/workflows/<font style="color:red">&lt;processId&gt;</font>/start</pre>
<p>Při spuštění je možné také předat parametry pro workflow, které možné použít při rozhodování:</p>
<pre>PUT /c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:red">&lt;id záznamu&gt;</font>/workflows/<font style="color:red">&lt;processId&gt;</font>/start?parametr1=value1&amp;parametr2=value2</pre>

<p>Seznam událostí (poznámek a úkolů) u daného objektu:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>/udalost</pre>

<p>Vypsání událostí včetně dalších atributů workflow:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>/udalost.xml?includes=udalost/actRuTask</pre>

<p>Při práci s workflow chcete obvykle úkoly pouze určitého typu:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>/udalost/(typUdalosti = 'druhUdal.workflow')</pre>

<p>Výpis všech úkolů pro konkrétní workflow:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/udalost/(typUdalosti = 'druhUdal.workflow' and processDefinitionId = '<font style="color:red">&lt;processId&gt;</font>')</pre>

<p>Výpis všech úkolů s&nbsp;daným klíčem úkolu pro konkrétní workflow:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/udalost/(typUdalosti = 'druhUdal.workflow' and processDefinitionId = '<font style="color:red">&lt;processId&gt;</font>' and taskDefinitionKey = '<font style="color:red">&lt;taskKey&gt;</font>')</pre>

<p>Výpis všech úkolů, které může aktuálně přihlášený uživatel vyřešit:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/udalost@ukoly-k-realizaci</pre>

<p>Zaslání signálu všem běžícím workflow, které na něj může reagovat:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>/workflow-signal/<font style="color:blue">&lt;signalId&gt;</font>?param1=value</pre>

<p>Zaslání zprávy všem běžícím workflow, které na něj může reagovat:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>/workflow-message/<font style="color:blue">&lt;messageId&gt;</font>?param1=value</pre>


<p>Práce s úkolem:</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/udalost/<font style="color:blue">&lt;ID záznamu&gt;</font>/<font style="color:green">&lt;operace&gt;</font></pre>

<table class="table table-striped table-condensed">
    <tr>
        <th>claim.xml</th><td>přivlastnit si úkol</td>
    </tr>
    <tr>
        <th>unclaim.xml</th><td>odvlastnit si úkol</td>
    </tr>
    <tr>
        <th>complete.xml<br/>
            complete.xml?param1=value&hellip;
            <a href="#note1" class="noteref" title="Pozor na správný URL encoding">1)</a>
        </th>
        <td>vyřešit úkol</td>
    </tr>
    <tr>
        <th>assign.xml?username=user
            <a href="#note1" class="noteref" title="Pozor na správný URL encoding">1)</a>
        </th>
        <td>předat úkol uživateli user</td>
    </tr>
    <tr>
        <th>add-comment.xml?comment=text
            <a href="#note1" class="noteref" title="Pozor na správný URL encoding">1)</a>
            <a href="#note2" class="noteref" title="Kódování textů v UTF-8">2)</a>
        </th>
        <td>přidat komentář k úkolu</td>
    </tr>
</table>
<ol class="notes">
<li id="note1">Pro hodnoty parametrů obsahující jiné než <a href="http://en.wikipedia.org/wiki/Uniform_resource_locator#List_of_allowed_URL_characters">povolené znaky URL</a> (mezera, zavináč atd.), je zapotřebí provést <a href="http://en.wikipedia.org/wiki/Percent-encoding"><i>URL encoding</i></a>.</li>
<li id="note2">Kódování textů je očekáváno v <code>UTF-8</code>.</li>
</ol>

<p>Poznámka: události, které jsou typu úkol pro workflow se zruší po tom co jsou vyřešeny. Místo .xml lze také použit <code>.json</code> nebo hlavičku <code>Accept</code>.</p>

<p>Seznam úkolů a k nim související objekty</p>
<pre>/c/<font style="color:green">&lt;identifikátor firmy&gt;</font>/<font style="color:red">&lt;evidence&gt;</font>/<font style="color:blue">&lt;ID záznamu&gt;</font>/udalost.xml?includes=udalost/doklFak</pre>

<h2>Objekty použitelné při rozhodování ABRA Flexi</h2>

<table class="table table-striped table-condensed">
    <tr>
        <th><code>flexibee.user(username)</code></th><td>Vrací uživatele s uvedeným jménem</td>
    </tr>
    <tr>
        <th><code>flexibee.userQuery(username)</code></th><td>Vrátí dotazovací objekt uživatele s uvedeným jménem (výstup je shodný s metodou objectQuery).</td>
    </tr>
    <tr>
        <th><code>flexibee.object(evidenceType)</code></th><td>Vrátí objekt daného typu, který je připojen k úkolu workflow</td>
    </tr>
    <tr>
        <th><code>flexibee.objectQuery(evidenceType).relation(relationName).filter(filter).list()</code></th><td>Umožňuje filtrovat objekty v&nbsp;relaci.</td>
    </tr>
    <tr>
        <th><code>flexibee.objectQuery(evidenceType).relationFilter(relationName).filter(filter).one()</code></th><td>Umožňuje vrátit první objekt ve filtrované relaci.</td>
    </tr>
    <tr>
        <th><code>flexibee.objectQuery(evidenceType).relationFilter(relationName).filter(filter).sum()</code></th><td>Umožňuje sečíst objekty ve filtrované relaci.</td>
    </tr>
    <tr>
        <th><code>flexibee.objectQuery(evidenceType).relationFilter(relationName).filter(filter).firstQuery().relation(relationName2).one()</code></th><td>Umožňuje zřetězit zpracování dotazování.</td>
    </tr>
    <tr>
        <th><code>flexibee.objectQuery(evidenceType).relation(relationName).list()</code></th><td>Umožňuje filtrovat objekty v&nbsp;relaci.</td>
    </tr>
    <tr>
        <th><code>flexibee.objectQuery(evidenceType).relation(relationName).one()</code></th><td>Umožňuje vrátit první objekt v&nbsp;relaci.</td>
    </tr>
    <tr>
        <th><code>flexibee.objectQuery(evidenceType).relation(relationName).sum()</code></th><td>Umožňuje sečíst objekty v&nbsp;relaci.</td>
    </tr>
    <tr>
        <th><code>flexibee.wrap(object)</code></th><td>Obalí objekt pomocí query (stejný výsledek jako vrací např. <code>objectQuery</code>). Lze pak využít sadu funkcí, které nabází query.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).filter(filter).one()</code></th><td>Vrátí jeden objekt daného typu, který odpovídá uvedenému <a href="filters">filtru</a>.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).filter(filter).firstQuery().signal('signalName')</code></th><td>Vyvolá signál nad všemi workflow, které běží nad zvoleným záznamem.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).filter(filter).list()</code></th><td>Vrátí všechny objekty daného typu, který odpovídá uvedenému <a href="filters">filtru</a>.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).filter(filter).count()</code></th><td>Vrátí počet objektů daného typu, který odpovídá uvedenému <a href="filters">filtru</a>.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).filter(filter).sum(propertyName)</code></th><td>Vrátí sum daného sloupečku pro všechny objekty daného typu, který odpovídá uvedenému <a href="filters">filtru</a>.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).filter(filter).max(propertyName)</code></th><td>Vrátí max daného sloupečku pro všechny objekty daného typu, který odpovídá uvedenému <a href="filters">filtru</a>.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).filter(filter).min(propertyName)</code></th><td>Vrátí min daného sloupečku pro všechny objekty daného typu, který odpovídá uvedenému <a href="filters">filtru</a>.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).filter(filter).avg(propertyName)</code></th><td>Vrátí avg daného sloupečku pro všechny objekty daného typu, který odpovídá uvedenému <a href="filters">filtru</a>.</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).asc(propertyName).first()</code></th><td>Umožní řazení vzestupně (více na <a href="ordering">řazení v&nbsp;API</a>).</td>
    </tr>
    <tr>
        <th><code>flexibee.query(evidenceType).desc(propertyName).first()</code></th><td>Umožní řazení sestupně (více na <a href="ordering">řazení v&nbsp;API</a>).</td>
    </tr>
    <tr>
        <th><code>flexibee.settings()</code></th><td>Vrátí aktuální nastavení firmy (evidence <code>nastaveni</code>). Název firmy lze tedy získat např. <code><span>$</span>{flexibee.settings().nazFirmy}</code></td>
    </tr>
    <tr>
        <th><code>flexibee.settingsForDate(dt)</code></th><td>Vrátí nastavení firmy (evidence <code>nastaveni</code>) pro daný den (obvykle datum dokladu). Název firmy lze tedy získat např. <code><span>$</span>{flexibee.settingsForDate(dt).nazFirmy}</code></td>
    </tr>
    <tr>
        <th><code>flexibee.varName(evidenceType)</code></th><td>Vrátí interní název proměnné, kterou používá ABRA Flexi pro vazbu jednotlivých workflow na doklady a objekty v ABRA Flexi. Používá se obvykle při volání podprocesu.</td>
    </tr>
    <tr>
        <th><code>flexibee.isDesktop()</code></th><td>Vrátí true pokud workflow aktuálně běží v desktopové aplikaci</td>
    </tr>
    <tr>
        <th><code>flexibee.isServer()</code></th><td>Vrátí true pokud workflow aktuálně běží přes REST API nebo v&nbsp;prohlížeči</td>
    </tr>
    <tr>
        <th><code>flexibee.importXml(string)</code></th><td>Provede import XML řetězce dle ABRA Flexi REST API. Vrací kolekci identifikátorů objektů ze XML importu.</td>
    </tr>
    <tr>
        <th><code>now</code></th><td>Aktuální datum a čas.</td>
    </tr>
    <tr>
        <th><code>authenticatedUserId</code></th><td>Jméno aktuálně přihlášeného uživatele (pro více informací je nutné použít <code>flexibee.user(authenticatedUserId)</code> (více dokumentace <a href="http://www.activiti.org/userguide/index.html#apiExpressions">acitiviti</a>).</td>
    </tr>
    <tr>
        <th><code>task</code></th><td>Záznam activiti, který reprezentuje úkol (více dokumentace <a href="http://www.activiti.org/userguide/index.html#apiExpressions">acitiviti</a>).</td>
    </tr>
    <tr>
        <th><code>execution</code></th><td>Informace o aktuálně běžícím workflow (více dokumentace <a href="http://www.activiti.org/userguide/index.html#apiExpressions">acitiviti</a>).</td>
    </tr>
</table>

<h2>Volání podprocesu</h2>

<p>Pokud voláte podproces a chtete, aby výsledné úkoly byly provázány k&nbsp;dokladům, je nutné aktivovat propagaci vazební proměnné do workflow. To učiníte takto:</p>
<pre class="brush: xml">
&lt;callActivity id="callSubProcess" calledElement="checkCreditProcess"&gt;
  &lt;extensionElements&gt;
          &lt;activiti:in source="&#36;{flexibee.varName('faktura-prijata')}" target="&#36;{flexibee.varName('faktura-prijata')}" /&gt;
          &lt;activiti:in source="initiator" target="initiator" /&gt;
  &lt;/extensionElements&gt;
&lt;/callActivity&gt;
</pre>

<h2>Práce s uživateli ve workflow</h2>
<p>Pokud chcete pracovat s uživateli, je několik následujících možností. Dejte si pozor, abyste v rámci definice uživatelů nepoužili znak čárky (,). Activiti má chybu, která zabraňuje jeho použití.</p>

<h3>Původce workflow</h3>
<p>Při startu workflow nadefinujte proměnnou <code>initiator</code>. Tu pak můžete použít jako název uživatele pro přidělení úkolu.</p>
<pre class="brush: xml">
    &lt;startEvent id="theStart"  activiti:initiator="initiator"&gt;
</pre>

<h3>Aktuální uživatel</h3>
<p>Pokud chcete úkol přidělit stejnému uživateli, který provedl aktuální operaci, použijte proměnnou <code>authenticatedUserId</code>.</pre>

<h3>Konkrétní uživatel</h3>
<p>Při zpracování můžete použít konkrétního uživatele. Můžete také vyhledat uživatele podle určitých kritérií (např. štítek):</p>
<pre class="brush: xml">
&lt;formalExpression&gt;&#36;{flexibee.query('uzivatele').relation('stitek = "code:PRACOVNIK"').one().kod}&lt;/formalExpression&gt;
</pre>
<p>Nebo uživatelskou vazbu:</p>
<pre class="brush: xml">
&lt;formalExpression&gt;&#36;{flexibee.userQuery(initiator).relation('uzivatelske-vazby').filter('typVazby = "code:NADRIZENY"').one().kod}&lt;/formalExpression&gt;
</pre>

<h2>Modifikace dokladů</h2>
<p>Někdy je nutné při zpracování workflow modifikovat jeden nebo více dokladů. To lze provést použitím <code>flexibee-xml</code>. Aby metoda <code>flexibee.object()</code> fungovala, musí být workflow navázané na tento dokument.</p>

<pre class="brush: xml">
&lt;serviceTask id="storno" activiti:class="flexibee-xml"&gt;
    &lt;extensionElements&gt;
        &lt;activiti:field name="object" expression="&#36;{flexibee.object('faktura-vydana')}"/&gt;
        &lt;activiti:field name="xml"&gt;
            &lt;activiti:expression&gt;
                &lt;![CDATA[
                  &lt;winstrom&gt;
                      &lt;faktura-vydana action="storno"&gt;
                      &lt;/faktura-vydana&gt;
                  &lt;/winstrom&gt;
                ]]&gt;
            &lt;/activiti:expression&gt;
        &lt;/activiti:field&gt;
    &lt;/extensionElements&gt;
&lt;/serviceTask&gt;
</pre>

<p>Lze také modifikovat více objektů:</p>
<pre class="brush: xml">
&lt;serviceTask id="storno" activiti:class="flexibee-xml"&gt;
    &lt;extensionElements&gt;
        &lt;activiti:field name="xml"&gt;
            &lt;activiti:expression&gt;
                &lt;![CDATA[
                  &lt;winstrom&gt;
                      &lt;faktura-vydana action="storno"&gt;
                            &lt;id&gt;code:FAV0001/2013&lt;/id&gt;
                      &lt;/faktura-vydana&gt;
                      &lt;faktura-vydana action="storno"&gt;
                            &lt;id&gt;code:FAV0002/2013&lt;/id&gt;
                      &lt;/faktura-vydana&gt;
                  &lt;/winstrom&gt;
                ]]&gt;
            &lt;/activiti:expression&gt;
        &lt;/activiti:field&gt;
    &lt;/extensionElements&gt;
&lt;/serviceTask&gt;
</pre>

<h3>Získání identifikátorů modifikovaných objektů</h3>

<p>Pomocí proměnné <code>idsVar</code> lze získat kolekci identifikátorů, které byly modifikovány XML importem.</p>

<pre class="brush: xml">
&lt;serviceTask id="import" activiti:class="flexibee-xml"&gt;
    &hellip;
    &lt;extensionElements&gt;
        &lt;activiti:field name="idsVar" stringValue="seznamId" /&gt;
    &lt;/extensionElements&gt;
&lt;/serviceTask&gt;
</pre>

<p>Po úspěšně vykonaném XML importu pak bude proměnná <code>seznamId</code> obsahovat kolekci identifikátorů zmodifikovaných objektů.</p>

<h3>Zpracování chyb při importu XML</h3>

<p>Pokud nastane chyba při importu XML v tasku typu <code>flexibee-xml</code>, je zpracování workflow přerušeno výjimkou <code>WSBusinessRTException</code>. Výjimka obsahuje popis chyby a zobrazí se v GUI jako chybový dialog.</p>
<p>Pokud má workflow pokračovat i při chybě v importu, lze definici tasku rozšířit o parametry <code><strong>errorVar</strong></code> a <code><strong>errorMessageVar</strong></code>:</p>
<pre class="brush: xml">
&lt;serviceTask id="storno" activiti:class="flexibee-xml"&gt;
    &hellip;
    &lt;extensionElements&gt;
        &lt;activiti:field name="errorVar" stringValue="wasError" /&gt;
        &lt;activiti:field name="errorMessageVar" stringValue="errorMessage" /&gt;
    &lt;/extensionElements&gt;
&lt;/serviceTask&gt;
</pre>
<table class="table table-striped table-condensed">
    <tr><th>proměnná</th><th>datový typ</th><th>popis</th>
    <tr>
        <td><code><strong>errorVar</strong></code></td>
        <td><code>Boolean</code></td>
        <td>definuje název proměnné (např. <code>wasError</code>), kam se uloží příznak výskytu chyb při importu</td>
    </tr>
    <tr>
        <td><code><strong>errorMessageVar</strong></code></td>
        <td><code>String</code></td>
        <td>definuje název proměnné (např. <code>errorMessage</code>), kam se uloží text popisující chyby importu</td>
    </tr>
</table>

<h2>Podmíněné větvení na základě objektů</h2>
<p>Ve workflow lze pokládat dotazy ABRA Flexi a reagovat na základě odpovědi:</p>

<pre>&#36;{flexibee.object('objednavka-prijata').sumCelkem &gt; 1000}</pre>

<p>Lze se ptát i obecně, např. kolik neuhrazených faktur je u firmy, která je uvedena u aktuální faktury.:</p>
<pre>&#36;{flexibee.query('faktura-vydana').filter('stavUhrK != "stavUhr.uhrazeno" and stavUhrK != "stavUhr.uhrazenoRucne" and firma='.concat(flexibee.object('faktura-vydana').firma.id)).sum('sumCelkem') &gt; 1000}</pre>

<h2>Přenášení stavu workflow do dokladu</h2>
<p>Někdy je nutné na základě stavu workflow měnit i stav dokladu. To lze realizovat pomocí štítků, které jsou přiděleny do jedné skupiny u které je nastavena exkluzivita (pak při nastavení nového štítku ze stejné skupiny dojde ke zrušení ostatních). Realizace je pak opět pomocí <code>flexibee-xml</code>, které zavoláte při každé změně stavu workflow:</p>

<pre class="brush: xml">
&lt;serviceTask id="storno" activiti:class="flexibee-xml"&gt;
    &lt;extensionElements&gt;
        &lt;activiti:field name="object" expression="&#36;{flexibee.object('faktura-vydana')}"/&gt;
        &lt;activiti:field name="xml"&gt;
            &lt;activiti:expression&gt;
                &lt;![CDATA[
                  &lt;winstrom&gt;
                      &lt;faktura-vydana&gt;
                         &lt;stitky&gt;SCHVÁLENO&lt;/stitky&gt;
                      &lt;/faktura-vydana&gt;
                  &lt;/winstrom&gt;
                ]]&gt;
            &lt;/activiti:expression&gt;
        &lt;/activiti:field&gt;
    &lt;/extensionElements&gt;
&lt;/serviceTask&gt;
</pre>
<#include "/i-devdoc-footer.ftl" />
</#escape>
