<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Štítky"/>
<#include "/i-devdoc-header.ftl" />

<p>V celém systému ABRA Flexi je přidána podpora pro štítky. Ty umožňují přilepit stav k různým objektům (např. doklady, adresář, zakázka, ...) případně i k různým stavům (např. způsob úhrady, apod.). Můžete tak v&nbsp;propojeném systému reagovat na situaci, kdy je faktura uhrazena způsobem, který má štítek (např. použít metodu v&nbsp;eshopu).</p>

<h2 id="updateAction">Smazání / aktualizace štítků</h2>
<p>Práce se štítky je trochu odlišná, protože se jedná o relaci, která je emulovaná jako položka. Pro vymazání štítků nebo jejich aktualizaci je zapotřebí použít atribut <code>removeAll="true"</code>:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;adresar&gt;
    &lt;id&gt;14&lt;/id&gt;
    &lt;stitky removeAll="true"&gt;&lt;/stitky&gt;
  &lt;/adresar&gt;
&lt;/winstrom&gt;
</pre>

<p>Výše uvedené XML zajistí odstranění všech štítků z uvedeného záznamu v adresáři.</p>

<p>Pro aktualizaci štítků stačí uvést v elementu <code>&lt;stitky&gt;</code> nový seznam hodnot:</p>

<pre class="brush: xml">&lt;stitky removeAll="true"&gt;STITEK1,NOVY_STITEK&lt;/stitky&gt;
</pre>

resp. v JSON:

<pre class="brush: js">"stitky@removeAll": "true",
"stitky": "STITEK1,NOVY_STITEK"
</pre>


<p>Bez uvedení atributu <code>removeAll="true"</code> se provede pouze přidání neexistujících štítků.</p>

<h2>Export skupiny štítků</h2>
<p>Štítky můžete dávat do skupin štítků. Ty pak umožňují lepší zpracování při exportu (viz níže). Pokud nastavíte u skupiny, že doklad může obsahovat pouze jeden štítek, budou ostatní štítky ze stejné skupiny smazány. Lze tím snadno emulovat stav dokladu. Objekt bude vždy obsahovat pouze jeden štítek ze skupiny s pouze jedním štítkem.</p>

<code>/c/{firma}/{evidence}.xml?skupina-stitku=SKUPINA1,SKUPINA2</code>.
<p>Výstup pak může vypadat takto:</p>

<pre class="brush: xml">
&lt;winstrom&gt;
  ...
  &lt;faktura-vydana&gt;
    ...
    &lt;stitky SKUPINA1="STITEK1" SKUPINA2="STITEK2"&gt;STITEK1,STITEK2,STITEK3&lt;/stitky&gt;
  ...
&lt;/winstrom&gt;
</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
