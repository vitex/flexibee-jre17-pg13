<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Adresář pro vývojářské přizpůsobení"/>
<#include "/i-devdoc-header.ftl" />

<p>Pro některé účely mohou programátoři přizpůsobit nastavení systému. K tomu je nutné nastavit:</p>

<ol>
<li>Je potřeba upravit flexibee-server.xml (<a href="http://www.flexibee.eu/produkty/flexibee/dokumentace/otazky/umisteni-adresaru/">kde jej najít?</a>) a přidat tam
<pre>&lt;entry key="developerDirectory"&gt;/devel/&lt;/entry&gt;</pre><br/>
Místo /devel/ dejte adresář, kde budou data pro modifikaci (např. C:\Projekty\FlexiBee\).</li>

<li><a href="/produkty/flexibee/dokumentace/reseni/start-databaze/">Restartovat</a> ABRA Flexi</li>

<li>V daném adresáři je potřeba vytvořit adresář "default" (tzv. výchozí instance - jiná hodnota má smysl pouze v cloudovém provozu) a v něm adresář s&nbsp;identifikátorem firmy (ten je stejný jako přes webové rozhraní). Případně je možné použít speciální identifikátor "!all" (bez uvozovek).</li>
</ol>

<#include "/i-devdoc-footer.ftl" />
</#escape>
