<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Podporované typy proměnných"/>
<#include "/i-devdoc-header.ftl" />

<p>Typy proměnných jsou použivány při exportu a importu do XML či JSON a při <a href="filters">filtraci</a>.</p>

<table class="table table-striped table-condensed">
    <thead>
    <tr>
        <th>Strojový název</th>
        <th>Název</th>
        <th>Poznámka</th>
        <th>Ukázka</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>string</th>
        <td>Řetězec</td>
        <td>Kódování je unicode. Lze tedy použít libovolný znak.</td>
        <td>šílený koníček<br/>
        こちらは田中さんです
        </td>
    </tr>
    <tr>
        <th>integer</th>
        <td>Celé číslo</td>
        <td>Musí být bez mezer. Jde o znaménkový 4bajtový integer, ovšem rozsah může být omezený (viz přehled položek dané evidence)</td>
        <td><code>12</code></td>
    </tr>
    <tr>
        <th>numeric</th>
        <td>Desetinné číslo</td>
        <td>Musí být bez mezer, oddělovačem desetinných míst je tečka. Jde o 8bajtový double, ovšem rozsah může být omezený (viz přehled položek dané evidence)</td>
        <td><code>12.5</code></td>
    </tr>
    <tr>
        <th>date</th>
        <td>Datum</td>
        <td>Datum ve formátu <code>YYYY-MM-DD</code>; lze zadat i časovou zónu (<code>YYYY-MM-DDZZZ</code>), ale ta bude ignorována.
           <code>ZZZ</code> je označení časové zóny (<code>Z</code> nebo <code>+HH:MM</code> nebo <code>-HH:MM</code>).
           <p>
           <ul>
           <li>Podporované formáty vycházejí z <a href="http://www.w3.org/TR/NOTE-datetime">W3C specifikace</a>.</li>
           <li>Pro <a href="filters">filtraci</a> je možné použít pouze zápis bez časové zóny.</li>
           </ul>
           </p>
        </td>
        <td><code>1980‑05‑06</code><br/>
            <code>2015‑01‑30Z</code><br/>
            <code>2008‑09‑01+02:00</code>
        </td>
    </tr>
    <tr>
        <th>datetime</th>
        <td>Datum + čas</td>
        <td>Datum a čas ve formátu <code>YYYY-MM-DD'T'HH:MM:SS.SSS</code>; lze zadat i časovou zónu (<code>YYYY-MM-DD'T'HH:MM:SS.SSSZZZ</code>), ale ta bude ignorována.
           <code>ZZZ</code> je označení časové zóny (<code>Z</code> nebo <code>+HH:MM</code> nebo <code>-HH:MM</code>).
           <p>
           <ul>
           <li>Podporované formáty vycházejí z <a href="http://www.w3.org/TR/NOTE-datetime">W3C specifikace</a>.</li>
           <li>Pro <a href="filters">filtraci</a> je možné použít pouze zápis bez časové zóny.</li>
           </ul>
           </p>
        </td>
        <td><code>1980‑05‑06</code><br/>
            <code>1980‑05‑06T12:30:12</code><br/>
            <code>2015‑01‑30T22:55:33Z</code><br/>
            <code>2008‑09‑01T17:18:14+02:00</code><br/>
            <code>2008‑09‑01T17:18:14.075+02:00</code>
        </td>
    </tr>
    <tr>
        <th>logic</th>
        <td>Logická hodnota</td>
        <td>boolean</td>
        <td><code>true</code><br/>
            <code>false</code>
        </td>
    </tr>
    <tr>
        <th>select</th>
        <td>Výběr jedné z hodnot</td>
        <td>Výběr jedné z hodnot. Je reprezentován jako řetězec.</td>
        <td><code>typVztahu.odberDodav</code>
</td>
    </tr>
    <tr>
        <th>relation</th>
        <td>Vazba mezi daty</td>
        <td>Vstupem je záznam z jiné evidence (přehled <a href="identifiers">typů identifikátorů</a>)</td>
        <td><code>123</code><br/>
            <code>code:CZK</code>
        </td>
    </tr>
    </tbody>
</table>


<#include "/i-devdoc-footer.ftl" />
</#escape>
