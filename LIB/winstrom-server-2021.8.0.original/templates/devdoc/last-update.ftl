<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Datum poslední změny"/>
<#include "/i-devdoc-header.ftl" />

<p>Každý záznam v ABRA Flexi obsahuje atribut datum poslední změny (<code>lastUpdate</code>). Ten umožňuje detekovat, že byl záznam v ABRA Flexi modifikován. Tato hodnota je také používána u rozhraní ABRA Flexi Sync k&nbsp;rozpoznání změn.</p>

<p>Tento atribut lze používat i při <a href="filters">filtraci</a>.</p>

<p><b>Pozor:</b> ke sledování změn je lepší použít <a href="changes-api">samostatnou funkci</a>, kterou ABRA Flexi nabízí.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
