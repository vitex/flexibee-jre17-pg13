<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Odesílání dokladů e-mailem"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API (i přes webové rozhraní) lze odeslat doklad e-mailem. Aby to fungovalo, je třeba nastavit připojení k SMTP serveru. Do souboru <code>flexibee-server.xml</code> (na Linuxu v <code>/etc/flexibee/flexibee-server.xml</code>) doplňte následující hodnoty:</p>

<table class="table table-striped table-condensed">
    <tr><th><code>smtp.host</code></th><td>Adresa SMTP serveru, typicky <code>localhost</code> (SMTP server běží na stejném stroji jako ABRA Flexi Server)</td></tr>
    <tr><th><code>smtp.port</code></th><td>Port SMTP serveru, typicky <code>25</code>; <strong>nepovinné</strong></td></tr>
    <tr><th><code>smtp.defaultFrom</code></th><td>Výchozí e-mailová adresa odesílatele</td></tr>
    <tr><th><code>smtp.encryption</code></th><td>Režim zabezpečení SMTP komunikace:
        <dl>
            <dt>none</dt><dd>bez zabezpečení <strong>(výchozí)</strong></dd>
            <dt>starttls</dt><dd>zabezpečená komunikace, pokud ji server podporuje (příkaz <code>STARTTLS</code>)</dd>
            <dt>tls</dt><dd>vyžadované přepnutí na zabezpečenou komunikaci (<abbr title="Transport Layer Security">TLS</abbr>)</dd>
            <dt>ssl</dt><dd>plně zabezpečená komunikace (<abbr title="Secure Socket Layer">SSL</abbr>)</dd>
        </dl>
    </td></tr>
    <tr><th><code>smtp.auth.user</code></th><td>Přihlašovací jméno, pokud SMTP server vyžaduje autentizaci; <strong>nepovinné</strong></td></tr>
    <tr><th><code>smtp.auth.password</code></th><td>Heslo; <strong>nepovinné</strong></td></tr>
</table>

<p>Pozn.: pokud potřebujete nějaké další konfigurační hodnoty, např. pro SSL, dejte nám vědět.</p>

<p>Odeslání dokladu přes REST API na správně nakonfigurovaném serveru pak znamená <code>PUT</code> (resp. <code>POST</code>) na URL <code>/c/firma/faktura-vydana/1/odeslani-dokladu.xml</code> s následujícími parametry:</p>

<table class="table table-striped table-condensed">
    <tr><th><code>?to=email@example.com</code></th><td>Adresát; parametrů <code>to</code> lze uvést více</td></tr>
    <tr><th><code>?cc=email@example.com</code></th><td>Kopie; parametrů <code>cc</code> lze uvést více</td></tr>
    <tr><th><code>?subject=Doklad ABC</code></th><td>Předmět e-mailu</td></tr>
</table>

<p>Musí být zadán alespoň jeden adresát nebo adresát na kopii, předmět je také povinný. Jako odesílatel (hlavička <code>From</code>) bude uveden aktuální uživatel, pod kterým odeslání provádíte, případně výchozí hodnota z konfigurace.</p>

<p>V těle požadavku musí být tělo e-mailu v textové podobě kódované v UTF-8. E-mail bude odeslán v textové i HTML variantě podle šablony, která je momentálně součástí ABRA Flexi. Do budoucna plánujeme možnost uživatelských šablon.</p>

<p>Jako součást e-mailu bude v příloze odeslána PDF a případně i ISDOC podoba dokladu.</p>

<p>Kompletní příklad odeslání dokladu z příkazové řádky použitím nástroje <code>curl</code>:</p>

<pre>curl -k -L -u uzivatel:heslo -X PUT -d 'Dobrý den,

zasíláme Vám slíbený dokument.

S pozdravem

...' "https://localhost:5434/c/firma/faktura-vydana/1/odeslani-dokladu.xml?to=email@example.com&subject=Doklad%20ABC"</pre>

<p>Všimněte si zejm. předmětu &ndash; jako obvykle, parametry v URL musí být správně zakódovány.</p>

<p>Poznámka: je nutné uvést buď <code>odeslani-dokladu<strong>.xml</strong></code> nebo hlavičku <code>Accept: text/xml</code>.</p>

<h2>Přizpůsobování e-mailových zpráv</h2>
<p>V ABRA Flexi lze upravovat zasílané zprávy takto:</p>
<ul>
    <li>Přizpůsobení textu &ndash; lze upravit v&nbsp;typu dokladu výchozí text, který bude použit pro zasílání dokladů.</li>
    <li>Přizpůsobení šablony</li>
</ul>

<h3>Přizpůsobení šablony e-mailové zprávy</h3>
<p>Základem je zapnutí vývojářeského adresáře (developerDirectory). <a href="developers-directory">Kompletní popis</a> je delší, ale nás zajímá pouze část s&nbsp;nastavením a následně úpravou šablony:</p>

<ol>
<li>Je potřeba upravit flexibee-server.xml (<a href="http://www.flexibee.eu/produkty/flexibee/dokumentace/otazky/umisteni-adresaru/">kde jej najít?</a>) a přidat tam
<pre>&lt;entry key="developerDirectory"&gt;/devel/&lt;/entry&gt;</pre><br/>
Místo <code>/devel/</code> dejte adresář, kde budou data pro modifikaci (např. C:\Projekty\FlexiBee\).</li>

<li><a href="http://www.flexibee.eu/produkty/flexibee/dokumentace/reseni/start-databaze/">Restartovat</a> ABRA Flexi</li>

<li>V daném adresáři je potřeba vytvořit adresář "default" (tzv. výchozí instance - jiná hodnota má smysl pouze v&nbsp;cloudovém provozu) a v&nbsp;něm adresář s&nbsp;identifikátorem firmy (ten je stejný jako přes webové rozhraní). Případně je možné použít speciální identifikátor "!all" (bez uvozovek).</li>

<li>Nakopírovat <a href="http://download.flexibee.eu/download/devDirSamples.zip">ukázkové skripty</a> do $developerDirectory/$instance/!all a pro nás jsou důležité soubory z&nbsp;adresáře <code>mail-templates</code>.</li>
</ol>

<h2>Automatické odeslání dokladů</h2>
<p>ABRA Flexi podporuje automatické odeslání všech neodeslaných dokladů, které jsou označené k odeslání.</p>
<p>Odeslání lze vynutít příkazem:</p>

<pre>curl -H "Accept: application/xml" -u winstrom:winstrom -X PUT -L https://demo.flexibee.eu:5434/c/demo/faktura-vydana/automaticky-odeslat-neodeslane</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
