<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Odpočet záloh"/>
<#include "/i-devdoc-header.ftl" />

<p>Při vytváření dokladů (např. faktur) je potřeba umožnit odpočet jiným dokladem (např. zálohovou platbou).</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana&gt;

    &lt;!-- Typ faktury (objekt) - max. délka: 20 --&gt;
    &lt;typDokl&gt;code:FAKTURA&lt;/typDokl&gt;

    &lt;!-- Firma (objekt) - max. délka: 20 --&gt;
    &lt;firma&gt;code:PBENDA&lt;/firma&gt;

    &lt;!-- Bezpol. dokl. (logická hodnota) - --&gt;
    &lt;bezPolozek&gt;true&lt;/bezPolozek&gt;
    &lt;!-- Základ DPH zákl. [Kč] (destinné číslo) - počet číslic: 15 --&gt;
    &lt;sumZklZakl&gt;1000.0&lt;/sumZklZakl&gt;
    &lt;odpocty&gt;
        &lt;odpocet&gt;
            &lt;castkaMen&gt;1200.0&lt;/castkaMen&gt; &lt;!-- částka v měně určená pro odpočet --&gt;
            &lt;doklad&gt;code:ZDD0001/2010&lt;/doklad&gt; &lt;!-- identifikátor dokladu pro odpočet --&gt;
        &lt;/odpocet&gt;
    &lt;/odpocty&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<p>Pozor: nyní není možné udělat částečný odpočet zálohy. <code>&lt;castkaMen/&gt;</code> musí vždy odpovídat celé částce odpočítaného dokladu.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>