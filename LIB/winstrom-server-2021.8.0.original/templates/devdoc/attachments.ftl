<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Přílohy"/>
<#include "/i-devdoc-header.ftl" />


<p>U záznamů, které podporují přílohy, lze zobrazit seznam příloh takto:</p>
<pre>/c/firma/adresar/12/prilohy</pre>

<p>Metadata jedné konkrétní přílohy lze získat obvyklým způsobem:</p>
<pre>/c/firma/adresar/12/prilohy/75</pre>

<p>Binární data přílohy lze získat pomocí GET, součástí odpovědi je i správná hlavička Content-Type:</p>
<pre>/c/firma/adresar/12/prilohy/75/content</pre>

<p>Pokud je příloha obrázkem, lze získat obdobným způsobem i náhled (neexistuje-li, odpovědí bude chyba 404):</p>
<pre>/c/firma/adresar/12/prilohy/75/thumbnail</pre>

<p>Přílohu lze také přes PUT založit, musíte při tom zadat jméno souboru a v hlavičce Content-Type poslat jeho typ:</p>
<pre>PUT /c/firma/adresar/12/prilohy/new/&lt;jméno souboru&gt;
Content-Type: image/jpeg</pre>

<p>Binární data přílohy musí být v těle požadavku. Existující přílohu nelze měnit, v takovém případě je potřeba přílohu smazat a znovu založit. Detaily o importu příloh přes XML níže.</p>

<p>Obsah přílohy lze také exportovat přímo do XML, data se exportují zakódovaná v Base64 (<code>&lt;content encoding="base64"&gt;...&lt;/content&gt;</code>).</p>

<p>Přílohy lze při přístupu přes REST API exportovat i jako součást objektů, ke kterým patří, pak je potřeba v URL uvést parametr <code>relations=prilohy</code>.</p>

<p>Je podporován i import příloh přes XML (data musí být zakódována opět v Base64), má to ale jistá omezení:</p>
<ul>
    <li>novou přílohu lze založit pouze jako součást jiného objektu (nemůže to být kořenový tag)</li>
    <li>měnit lze pouze metadata, data přílohy už nikoliv</li>
</ul>

<h3>Podpora obrázků</h3>
<p>Pokud nahrajete do ABRA Flexi přílohu ve formátu obrázku, je přidána podpora pro generování náhledů. Obrázek musí být v jednom z těchto formátů:</p>
<ul>
    <li>image/jpeg</li>
    <li>image/gif</li>
    <li>image/png</li>
</ul>

<p>U objektů s přílohou je možné požádat o primární obrázek (neexistuje-li, odpovědí bude chyba 404):</p>
<pre>/c/firma/cenik/12/thumbnail.png</pre>

<p>Je možné určit velikost obrázku pomocí parametrů <code>w</code> a <code>h</code>.</p>




<h3>Přílohy k nastavení</h3>

<p>Nastavení firmy obsahuje dvě přílohy: logo a podpis a razítko. S nimi se pracuje speciálním způsobem.</p>

<p>Zjistit, zda je k nastavení připojeno logo, lze zjistit pomocí GET:</p>
<pre>GET /c/firma/nastaveni/1/logo</pre>

<p>Pokud je logo připojeno, je vráceno přesměrování na kanonické URL přílohy (ve tvaru <code>/c/firma/priloha/3</code>), tj. kód 303 a hlavička Location. Pokud logo připojeno není, je vrácen kód 404.</p>

<p>Pokud není logo připojeno, lze ho připojit voláním PUT nebo POST (se správnou hlavičkou Content-Type):</p>
<pre>PUT /c/firma/nastaveni/1/logo
Content-Type: image/jpeg</pre>

<p>Binární data loga musí být jako obvykle v těle požadavku. Stejně jako u příloh, nelze připojit logo k nastavení, pokud už nějaké logo připojeno je. V takové situaci je vrácen chybový kód 400. Úspěch je značen kódem 201 a v hlavičce Location je URL nově vzniklé přílohy.</p>

<p>Logo lze odstranit použitím DELETE:</p>
<pre>DELETE /c/firma/nastaveni/1/logo</pre>

<p>Pokud bylo logo v pořádku odstraněno, je vrácen kód 200. Pokud žádné logo neexistovalo, je vrácen chybový kód 404.</p>

<p>S podpisem a razítkem se pracuje úplně stejně, jen namísto slova <code>logo</code> se v URL použije <code>podpis-razitko</code> (tedy např. <code>/c/firma/nastaveni/1/podpis-razitko</code>).</p>
<#include "/i-devdoc-footer.ftl" />
</#escape>
