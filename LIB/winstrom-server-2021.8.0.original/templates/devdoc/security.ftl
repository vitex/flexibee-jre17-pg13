<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Bezpečnost"/>
<#include "/i-devdoc-header.ftl" />

<h2>Man in the middle attack</h2>
<p>Jedním z možných útoků na ABRA Flexi je Man In The Middle Attack, kdy se útočník vydává za servery ABRA Flexi a uživatel mu poskytne autorizační údaje, kterými se pak útočník může autorizovat proti skutečným serverům a tak analyzovat průběh komunikace.</p>
<p>Standardně si ABRA Flexi vygeneruje tzv. Self-Signed Certificate. <a href="ssl">SSL certifikát lze vyměnit</a>.

<p>Abychom tomu zabránili, má ABRA Flexi několik funkcí, které tento problém eliminují:</p>
<ul>
    <li>Při spuštění si ABRA Flexi stáhne certifikát a při práci se serverem jej pak ověřuje. Proto není možné vyměnit certifikát za běhu.</li>
    <li>DANE - uložení certifikátu do DNS</li>
</ul>

<pre>$ ./swede --insecure create --port 5434 --certificate certifikat.pem --selector 1 --output rfc demo.flexibee.eu
_5434._tcp.demo.flexibee.eu. IN TLSA 1 0 1 85b928d1cab396d8e632d15d57b3a97ebbc2769ab74a292040dc4fc340153973
</pre>

<p>Místo nástroje <a href="https://github.com/pieterlexis/swede">SWEDE</a> je také možné získat konfiguraci z&nbsp;běžícího serveru ABRA Flexi na adrese <code>/certificate/tlsa</code>.</p>

<p>do DNS pak přidáme tento záznam:</p>
<code>_5434._tcp.demo.flexibee.eu. IN TXT TLSA 1 0 1 85b928d1cab396d8e632d15d57b3a97ebbc2769ab74a292040dc4fc340153973</code>
<p>Tj. jako hodnota je uloženo celé "TLSA 1 0 1 85b928d1cab396d8e632d15d57b3a97ebbc2769ab74a292040dc4fc340153973"</p>

<p>Hledání probíhá v tomto pořáadí:
<pre>5434._tcp.demo.flexibee.eu.
_tcp.demo.flexibee.eu.
5434._tcp.flexibee.eu.
_tcp.flexibee.eu.</pre>

<p>Pozor: pokud nepoužíváte současně i DNSSec, není zaručena bezpečnost odpovědi DNS serveru.</p>
<p>Pozor: ABRA Flexi client také v tuto chvíli nepodporuje kompletní DANE, ale pouze tyto parametry: --selector 1, --mtype 1, --usage 0</p>

<h2>CSRF</h2>
<p>ABRA Flexi má integrovanou ochranu proti Cross Site Request Forgery a v tomto směru není nutné nic nastavovat.</p>

<h2>SQL Injection a XSS</h2>
<p>Při návrhu jsme věnovali mnoho úsilí, abychom zabránili SQL Injection Cross Site Scripting (XSS). Proto k těmto typům chyb nedochází.</p>

<!-- <h2>Kvalita hesel a OTP</h2> -->

<h2>Ukládání hesel</h2>
<p>Dříve ABRA Flexi používalo pro ukládání hesel šifru MD5. Nyní je použita SHA256 a je použito saltování (salt). Stará hesla jsou překódována až při změně.</p>

<!-- <h2>Útok hrubou silou</h2> -->

<#include "/i-devdoc-footer.ftl" />
</#escape>
