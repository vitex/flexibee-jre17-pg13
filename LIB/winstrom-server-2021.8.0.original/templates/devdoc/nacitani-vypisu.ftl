<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Načítání bankovních výpisů"/>
<#include "/i-devdoc-header.ftl" />

<p>Bankovní výpisy lze načíst jednoduchým požadavkem přes REST API: soubor s výpisem je třeba poslat metodou <code>PUT</code> nebo <code>POST</code> na URL <code>/c/{firma}/bankovni-ucet/{id}/nacteni-vypisu</code>.</p>

<p>U bankovního účtu musí být nastaveny příslušné vlastnosti týkající se elektronického bankovnictví (formát výpisu atd.).</p>

<p>Pokud se výpis načte správně, odpověď je <code>200</code>. Tělo odpovědi může obsahovat informaci, že některé doklady už byly načteny:</p>

<pre>   Výpis 223 pro účet 0000001000 - Načítání proběhlo úspěšně. Počet položek 6
      položka s popisem POPLATEK             - Tento doklad byl již načten.
      položka s popisem VYBER HOTOVOSTI      - Tento doklad byl již načten.
</pre>

<p>V případě chyby je odpověď <code>400</code> a tělo odpovědi obsahuje popis problému. Např.:</p>

<pre>   Výpis 223 pro účet 0000001000 - Pro tento výpis nebyl nalezen bankovní účet. Počet položek 6</pre>

<p>Nebo:</p>

<pre>U bankovního účtu není definován formát elektronického výpisu.</pre>

<p>Tělo odpovědi, pokud je uvedeno, je momentálně vždy textové, ačkoliv požadujete XML nebo JSON. Tento problém bude v budoucnosti opraven.</p>

<h3>Načítání výpisů online</h3>
<p>Pomocí REST-API je možné také spustit načítání online výpisů.</p>
<p>Pro spuštění je nutné odeslat požadavek metodou <code>PUT</code> nebo <code>POST</code> na URL <code>/c/{firma}/banka/nacteni-vypisu-online</code>.</p>
<p>Bankovní účet, pro který se mají výpisy stahovat, musí být správně nastaven a banka musí online výpisy podporovat.</p>


<#include "/i-devdoc-footer.ftl" />
</#escape>