<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Úhrada faktury z přeplatků v pokladně a bance"/>
<#include "/i-devdoc-header.ftl" />

<p>Jednou z funkcí ABRA Flexi je možnost uhradit fakturu z přeplatků v bance nebo pokladně. Při párování úhrady na fakturu je vyplněna firma a pokud je úhrada větší než faktura, zůstane přeplatek. Ten je pak možné automaticky pomocí této funkce napárovat na fakturu.</p>

<p>K tomu slouží akce <code>uhrad-preplatky</code> takto:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana action="uhrad-preplatky"&gt;
    &lt;id&gt;17&lt;/id&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>

<p>Akci lze vyvolat i dávkově nad skupinou dokladů pomocí <a href="filters">filtru</a>:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana action="uhrad-preplatky" filter="not stavUhrK begins 'stavUhr.uhrazeno' and typDokl = 'code:INTERNET'"&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;</pre>



<#include "/i-devdoc-footer.ftl" />
</#escape>
