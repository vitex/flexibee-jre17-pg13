<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Serverová autorizace"/>
<#include "/i-devdoc-header.ftl" />

<p>Pro některé případy je nutné umožnit, aby externí aplikace mohla ovlivnit, pod kterým uživatelem přistupuje. Pro tyto účely lze použít <code>server-auth.xml</code>.</p>

<p>Vytvořte soubor <code>/etc/flexibee/server-auth.xml</code>:</p>

<pre class="brush: xml">
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;!DOCTYPE properties SYSTEM "http://java.sun.com/dtd/properties.dtd"&gt;
&lt;properties&gt;
    &lt;comment&gt;WinStrom server configuration&lt;/comment&gt;
    &lt;entry key="username"&gt;server-admin&lt;/entry&gt;
    &lt;entry key="password"&gt;heslo&lt;/entry&gt;
&lt;/properties&gt;
</pre>

<p>Pak lze pomocí HTTP hlavičky <code>X-FlexiBee-Authorization</code> určit, pokd kterým uživatelem má být sezení přihlášeno.</p>

<p>Tato funkce bude v budoucnu nahrazena autorizací pomocí SSL klientského certifikátu.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>