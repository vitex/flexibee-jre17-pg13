<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Stavy účtů"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API (i webové rozhraní) lze získat Stavy účtů.</p>

<p>Chceme-li získat Stavy účtů pro účetní období platné k aktuálnímu datu, použijeme URL <code>.../c/{firma}/stav-uctu[.xml]</code>. Lze samozřejmě použít i <a href="detail-levels">úroveň detailu</a> a <a href="filters">filtraci</a>.</p>

<p>Pokud potřebujeme stavu pro jiné než aktuální účetní období je nutné do URL doplnit parametr <code>ucetniObdobi</code>. Hodnotou tohoto parametru je zkratka požadovaného účetního období. Výsledné URL tedy bude vypadat například <code>.../c/{firma}/stav-uctu[.xml]?ucetniObdobi=2016</code>.</p>
<p>Další možností, jak získat stavy účtů pro jiné než aktuální účetní období, je doplnit parametr <code>idUcetniObdobi</code>. Tento parametr umožňuje využití <a href="identifiers">identifikátorů</a> účetního období. Pokud je tento parametr použit, má přednost před parametrem <code>ucetniObdobi</code>.</p>

<table class="table table-striped table-condensed">
    <tr><th><code>/c/{firma}/stav-uctu[.xml]</code></th><td>Stavy účtů pro aktuální účetní obdobi</td></tr>
    <tr><th><code>/c/{firma}/stav-uctu[.xml]?idUcetniObdobi=3</code></th><td>Stavy účtů pro účetní období s ID 3</td></tr>
    <tr><th><code>/c/{firma}/stav-uctu[.xml]?idUcetniObdobi=code:2016</code></th><td>Stavy účtů pro účetní období se zkratkou 2016</td></tr>
    <tr><th><code>/c/{firma}/stav-uctu[.xml]?ucetniObdobi=2016</code></th><td>Stavy účtů pro účetní období se zkratkou 2016</td></tr>
</table>

<#include "/i-devdoc-footer.ftl" />
</#escape>