<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Kurzy na dokladech"/>
<#include "/i-devdoc-header.ftl" />

<p>Pokud při importu dokladu v cizí měně neuvedete kurz, ABRA Flexi si jej automaticky stáhne (např. z ČNB nebo ECB). Nicméně se může stát, že se kurz bude lišit o malé částky (setiny/tisíciny) proti oficiálnímu kurzu. To je záměrné chování.</p>

<p>Částka v cizí měně, částka v domácí měně a kurz musí být vždycky "v rovnováze" (vynásobením či vydělením dvou z nich musíte dostat to třetí). Problém je, že částky se zaokrouhlují na dvě desetinná místa, takže vzniká určitá nepřesnost.</p>
<p>Řekněme, že mám částku 1.4 EUR a kurz 25.265. Vynásobením dostaneme 35.371 CZK a po zaokrouhlení tedy 35.37. Jenže 35.37 / 25.265 už není 1.4, ale 1.39996041955. My se zachováme tak, aby částky vždy seděly, a upravíme ("obětujeme") proto kurz, který zaokrouhlujeme až na šest desetinných míst (a umíme ho tedy uložit "přesněji"). V tomto případě ho přepočteme na 35.37 / 1.4 = 25.264286.</p>
<p>Vzhledem k tomu, že pokaždé je nepřesnost jiná, vyjde taky vždycky trochu jiný kurz (např. při částce 5.95 EUR je částka v tuzemské měně po zaokrouhlení 150.33, a kurz tak, aby částky navzájem seděly, vyjde 25.265546).</p>
<p>Obvykle jde o malé rozdíly, kde by zaokrouhlení částek zase všechno srovnalo, ale už jsme se setkali i s případem, kdy byl rozdíl 1 Kč (např. částka v cizí měně byla 2599.0 a kurz 0.0038), což je nepřípustné. Proto to děláme. Možná bychom mohli být chytřejší a dělat to jen, když je to opravdu nutné, ale zatím to je tak, jak to je.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
