<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Uživatelské tlačítko"/>
<#include "/i-devdoc-header.ftl" />

<h2>Uživatelské tlačítko</h2>

<h3>Možnosti použití</h3>

<p>Uživatelské tlačítko slouží k přizpůsobení ABRA Flexi, když dává vývojářům a uživatelům možnost definovat vlastní akci ve formě tlačítka. Po aktivaci tlačítka dojde k zobrazení panelu v ABRA Flexi či otevření webového prohlížeče, v obou případech s libovolnou webovou stránkou. Pomocí uživatelského tlačítka může být zobrazena relevantní část intranetového informační systému, vyhledáno zboží ve srovnávači cen, otevřena příslušná část webového rozhraní ABRA Flexi či vyvolána akce prostřednictvím našeho REST-API. Do adresy webové stránky je možné dynamicky vkládat parametry jako např. IČO právě upravované firmy či EAN zobrazeného zboží.</p>

<h3>Způsob použití</h3>

<p>Parametry uživatelského tlačítka jako např. jeho text, cílové URL či umístění v ABRA Flexi se zapíší do definice tlačítka, souboru ve formátu XML. Vytvořená definice uživatelského tlačítka se načte do ABRA Flexi <a href="/help/import_z_xml">importem z XML</a> souboru a při opětovném připojení k firmě bude tlačítko součástí uživatelského rozhraní (ať již klientské aplikace či webového rozhraní).</p>

<h3>Definice uživatelského tlačítka</h3>

<p>Uvedení některých prvků je povinné (nepovinný prvek je <strong><em>browser</em></strong>), každý prvek může být v rámci definice jednoho tlačítka uveden nanejvýše jednou (výjimkou je prvek <strong><em>id</em></strong>). Soubor může obsahovat definici více uživatelských tlačítek, při vícenásobném uvedení definice tlačítka se jednotlivé definice považují za jeho aktualizaci a fakticky se projeví poslední aktualizace jeho vlastnosti. Nevyhovující definice uživatelského tlačítka bude odmítnuta již při importu do ABRA Flexi.</p>

<ul>
    <li><strong><em>id</em></strong>
        <ul>
            <li><a href="identifiers">Identifikátory záznamu</a> slouží pro přidělení kódu/zkratky při vytváření a pro přesné určení uživatelského tlačítka při jeho pozdější aktualizaci či mazání.</li>
            <li>Pro identifikaci uživatelského tlačítka je možné použít:
                <ul>
                    <li>Kód/zkratku – uživatelské označení (prefix <code>code:</code>)</li>
                    <li>Externí identifikátor – identifikátor z externí aplikace (prefix <code>ext:</code>)</li>
                    <li>Identifikátor ABRA Flexi – číselný neměnný identifikátor přidělovaný aplikací (bez prefixu)</li>
                </ul>
            </li>
            <li>Povinný prvek, při vytváření uživatelského tlačítka musí být prvek uveden s kódem (zkratkou).</li>
            <li>Podrobnější informace o použití prvku naleznete v části <a href="identifiers">Identifikátory záznamů</a>.</li>
        </ul>
    </li>

    <li><strong><em>url</em></strong>
        <ul>
            <li>Určuje URL webové stránky či síťového zdroje, které bude po stisku tlačítka otevíráno.</li>
            <li>URL musí být uvedeno v plném, absolutním tvaru, tj. musí obsahovat schéma a doménovou adresu serveru (např. https://www.flexibee.eu/).</li>
            <li>URL doporučujeme zadávat v <code>&lt;![CDATA[ ]]&gt;</code>, aby přítomnost znaku '&amp;' nezpůsobila nevalidní XML.</li>
            <li>V hodnotě URL není podporováno URI schéma <code>file</code> používané pro přístup k lokálně uloženým souborům. Definice uživatelských tlačítek obsahující URL s <code>file://</code> budou při importu odmítnuty jako nepovolené.</li>
            <li>Při konstrukci URL je možné uvést proměnné, které budou vyhodnoceny FreeMarkerem při běhu aplikace a zajistí předání hodnot z aplikace. Např. zápis <code><#noparse>${object.ic}</#noparse></code> slouží pro získání IČO partnera v adresáři. Řetězec <code>object</code> je v názvu proměnných povinný, pomocí něj je odkazováno na aktuální záznam zobrazené evidence. Seznam dostupných atributů jednotlivých evidencí lze zobrazit ve webovém rozhraní (adresy jako https://localhost:5434/c/mojefirma/adresar/<strong>properties</strong>).</li>
            <li>Podporované proměnné:
                <ul>
                    <li><code>object</code> – aktuální záznam (viz předchozí bod).</li>
                    <li><code>objectIds</code> – seznam <code>ID</code> vybraných záznamů oddělených čárkou.</li>
                    <li><code>user</code> – aktuálně přihlášený uživatel (viz https://localhost:5434/c/mojefirma/uzivatel/properties).</li>
                    <li><code>url</code> – úplná URL adresa objektu, na kterém bylo tlačítko vyvoláno (např. https://instance.flexibee.eu/c/demo/adresar/1).</li>
                    <li><code>flexiUrl</code> – adresa webového rozhraní firmy, ve které je tlačítko umístěno (např. https://instance.flexibee.eu/flexi/demo/).</li>
                    <li><code>companyUrl</code> – adresa API rozhraní firmy, ve které je tlačítko umístěno (např. https://instance.flexibee.eu/c/demo/).</li>
                    <li><code>evidence</code> – jméno evidence, na které je tlačítko umístěno.</li>
                    <li><code>authSessionId</code> – autentizační token k aktuálnímu sezení uživatele. Po dobu platnosti sezení lze využít k autentizace dotazů. Viz využití autentizačního tokenu v popisu <a href="login">Autentizace</a>.</li>
                    <li><code>customerNo</code> – číslo zákazníka odpovídající licenci.</li>
                    <li><code>licenseId</code> – identifikátor licence.</li>
                </ul>
            </li>
            <li><strong>Pozor</strong>: Proměnné <code>object</code> a <code>objectIds</code> se vzájemně vylučují!</li>
        </ul>
    </li>

    <li><strong><em>evidence</em></strong>
        <ul>
    <li>Určuje evidenci, popř. konkrétní vazbu (relaci) evidence ABRA Flexi, pro kterou má být tlačítko zobrazováno.</li>
    <li>Např. <code>adresar</code> pro evidenci obchodních partnerů, <code>faktura-vydana</code> pro evidenci faktur vydaných, atd. Ve variantě pro vazby určité evidence pak např. <code>faktura-vydana-polozka</code> pro položky faktury vydané či <code>majetek-zapujcka</code> pro zápůjčky v evidenci majetku atd.
    <li>Pro evidence obecně použijte řetězec zobrazovaný v URL webového rozhraní v sekci evidence (https://localhost:5434/c/mojefirma/<strong>cenik</strong>).</li>
    <li>Seznam všech evidencí je k dispozici na adrese https://localhost:5434/c/mojefirma/evidence-list</li>
    <li>Pro vazby evidencí obecně použijte řetězec evidence (viz výše) doplněný o spojovník (pomlčku) a řetězec vazby zjištěný na přehledu vazeb evidence (https://localhost:5434/c/mojefirma/<strong>cenik</strong>/relations).</li>
        </ul>
    </li>

    <li><strong><em>location</em></strong>
        <ul>
    <li>Určuje zobrazení tlačítka na přehledu záznamů nebo na kartě konkrétního záznamu.</li>
    <li>Přípustné hodnoty:
        <ul>
            <li><code>list</code> – pro přehled záznamů</li>
            <li><code>detail</code> – pro konkrétní záznam</li>
        </ul>
    </li>
    <li>Má-li být tlačítko dostupné v nástrojové liště na přehledu záznamů a zároveň na kartě konkrétního záznamu, je nezbytné připravit dvě definice uživatelského tlačítka, které se budou lišit hodnotou prvku <em>location</em>.</li>
        </ul>
    </li>

    <li><strong><em>title</em></strong>
        <ul>
            <li>Text zobrazený na tlačítku.</li>
        </ul>
    </li>

    <li><strong><em>description</em></strong>
        <ul>
            <li>Detailní popis tlačítka zobrazovaný v bublině.</li>
        </ul>
    </li>


    <li><strong><em>browser</em></strong>
        <ul>
    <li>Určuje prohlížeč, ve kterém se URL otevře. Interní prohlížeč se zobrazí rychleji, ale nemusí obsahovat uživatelské přizpůsobení a data (hesla, cookies, data formulářů, navštívené odkazy). Externí prohlížeč je prostředím, na které je uživatel zvyklý.</li>
    <li>Nepovinný prvek, výchozí hodnotou je <code>automatic</code>.</li>
    <li>Z podstaty webového rozhraní je v něm nastavení prvku browser ignorováno.</li>
    <li>Přípustné hodnoty:
        <ul>
            <li><code>desktop</code> – externí prohlížeč</li>
            <li><code>automatic</code> – interní prohlížeč; není-li dostupný, otevře se externí prohlížeč</li>
        </ul>
    </li>
        </ul>
    </li>
</ul>

<h3>Příklad vytvoření</h3>

<#noparse>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;custom-button&gt;
    &lt;id&gt;code:JUSTICECZ&lt;/id&gt;
    &lt;url&gt;
      &lt;![CDATA[https://or.justice.cz/ias/ui/rejstrik-$firma?ico=${object.ic}&amp;jenPlatne=VSECHNY]]&gt;
    &lt;/url&gt;
    &lt;title&gt;Obch. rejstřík&lt;/title&gt;
    &lt;description&gt;Zobraz záznam firmy v obchodním rejstříku justice.cz&lt;/description&gt;
    &lt;evidence&gt;adresar&lt;/evidence&gt;
    &lt;location&gt;detail&lt;/location&gt;
    &lt;browser&gt;desktop&lt;/browser&gt;
  &lt;/custom-button&gt;
&lt;/winstrom&gt;
</pre>
</#noparse>

<h3>Příklad aktualizace tlačítka</h3>

<p>Uvedete-li v definici jednoznačnou <a href="identifiers">identifikaci</a> existujícího tlačítka, můžete jej aktualizovat. ABRA Flexi umožňuje <a href="partial-updates">částečné aktualizace záznamů</a>, takže při změně adresy obchodního rejstříku ve výše uvedeném příkladu stačí uživatelské tlačítko identifikovat a uvést novou hodnotu URL:</p>

<#noparse>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;custom-button&gt;
    &lt;id&gt;code:JUSTICECZ&lt;/id&gt;
    &lt;url&gt;
      &lt;![CDATA[https://or.justice.cz/ias/ui/rejstrik-$firma?ico=${object.ic}&amp;jenPlatne=VSECHNY&amp;polozek=500]]&gt;
    &lt;/url&gt;
  &lt;/custom-button&gt;
&lt;/winstrom&gt;
</pre>
</#noparse>

<h3>Příklad smazání tlačítka</h3>

<p>Pro smazání existujícího tlačítka je potřeba použít atribut <code>action</code> (více o jeho použití naleznete v části <a href="actions">Provádění akcí</a>). Uživatelské tlačítko z výše uvedeného příkladu lze smazat pomocí:</p>

<#noparse>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;custom-button action="delete"&gt;
    &lt;id&gt;code:JUSTICECZ&lt;/id&gt;
  &lt;/custom-button&gt;
&lt;/winstrom&gt;
</pre>
</#noparse>


<#include "/i-devdoc-footer.ftl" />
</#escape>
