<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Filtrování záznamů"/>
<#include "/i-devdoc-header.ftl" />

<p>Záznamy v ABRA Flexi je možné filtrovat. Hodnoty ve filtrech mohou být buď přímo &ndash; <a href="variable-types">podporované typy proměnných</a>, <a href="identifiers">identifikátory záznamů</a>  nebo zástupné (např. <code>now()</code> či <code>me()</code>).</p>

<p>Seznam podporovaných atributů a jejich hodnoty (dle typu) lze získat u každé evidence v seznamu políček.</p>

Podporované operátory:
<table class="table table-striped table-condensed" summary="Přehled operátorů">
    <thead>
    <tr>
        <th>Operátor</th>
        <th>Název</th>
        <th>Popis</th>
        <th>Ukázka</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><code>=</code> nebo <code>==</code> nebo <code>eq</code></th>
        <td>Rovnost</td>
        <td>Operátor se rovná hodnotě</td>
        <td><code>a = 1</code></td>
    </tr>
    <tr>
        <th><code>&lt;&gt;</code> nebo <code>!=</code> nebo <code>ne</code> nebo <code>neq</code></th>
        <td>Nerovnost</td>
        <td>Operátor se nerovná hodnotě</td>
        <td><code>a != 1</code></td>
    </tr>
    <tr>
        <th><code>&lt;</code> nebo <code>lt</code></th>
        <td> Menší </td>
        <td> </td>
        <td><code>a &lt; 1</code></td>
    </tr>
    <tr>
        <th><code>&lt;=</code> nebo <code>lte</code></th>
        <td> Menší nebo rovno </td>
        <td> </td>
        <td><code>a &lt;= 1</code></td>
    </tr>
    <tr>
        <th><code>&gt;</code> nebo <code>gt</code></th>
        <td> Větší </td>
        <td> </td>
        <td><code>a &gt; 1</code></td>
    </tr>
    <tr>
        <th><code>&gt;=</code> nebo <code>gte</code></th>
        <td> Větší nebo rovno </td>
        <td> </td>
        <td><code>a &gt;= 1</code></td>
    </tr>
    <tr>
        <th><code>like</code></th>
        <td> Obsahuje </td>
        <td> Záznam obsahuje řetězec </td>
        <td><code>a like 'inStr'</code></td>
    </tr>
    <tr>
        <th><code>like similar</code> <a href="#note1" class="noteref" title="Vyžaduje PostgreSQL 9.0 nebo novější">1)</a></th>
        <td> Obsahuje bez ohledu na háčky a čárky</td>
        <td> Záznam obsahuje řetězec bez ohledu na háčky a čárky </td>
        <td><code>a like similar 'inStr'</code></td>
    </tr>
    <tr>
        <th><code>between</code></th>
        <td> Je v rozsahu </td>
        <td> </td>
        <td><code>vek between 18 100 </code></td>
    </tr>
    <tr>
        <th><code>begins</code></th>
        <td> Začíná na </td>
        <td> </td>
        <td><code>a begins 'Win'</code></td>
    </tr>
    <tr>
        <th><code>begins similar</code> <a href="#note1" class="noteref" title="Vyžaduje PostgreSQL 9.0 nebo novější">1)</a></th>
        <td> Začíná na bez ohledu na háčky a čárky</td>
        <td> </td>
        <td><code>a begins similar 'Win'</code></td>
    </tr>
    <tr>
        <th><code>ends</code></th>
        <td> Končí na </td>
        <td> </td>
        <td><code>a ends 'Strom'</code></td>
    </tr>
    <tr>
        <th><code>in</code></th>
        <td> Je prvkem výčtu </td>
        <td> </td>
        <td><code>a in (1, 2, 3)</code></td>
    </tr>
    <tr>
        <th><code>in subtree</code></th>
        <td> Patří do podstromu </td>
        <td> <a href="#subtree">(viz níže)</a> </td>
        <td><code>in subtree 1</code></td>
    </tr>
    <tr>
        <th><code>is true/false</code></th>
        <td> Porovnání logických hodnot </td>
        <td> </td>
        <td><code>a is true</code></td>
    </tr>
    <tr>
        <th><code>is [not] null</code></th>
        <td> Je (není) vyplněno </td>
        <td> </td>
        <td><code>a is null</code></td>
    </tr>
    <tr>
        <th><code>is [not] empty</code></th>
        <td> Je (není) prázdné </td>
        <td> není vyplněno nebo je nula/false/prázdný řetězec </td>
        <td><code>a is not empty</code></td>
    </tr>
    </tbody>
</table>

<ol class="notes">
<li id="note1">Operátory ze skupiny <code>similar</code> fungují správně pouze na ABRA Flexi s databází PostgreSQL 9.0 nebo novější. Se starší verzí databáze tyto operátory háčky a čárky <strong>rozlišují</strong>, či-li chovají se stejně jako operátory bez klíčového slova <code>similar</code>.</li>
</ol>
<br/>

<table class="table table-striped table-condensed" summary="Logické operátory">
    <thead>
    <tr>
        <th>Operátor</th>
        <th>Název</th>
        <th>Popis</th>
        <th>Ukázka</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><code>and</code></th>
        <td>Logický operátor and</td>
        <td>Umožňuje kombinovat podmínky</td>
        <td><code>a = 1 and b = 1</code></td>
    </tr>
    <tr>
        <th><code>or</code></th>
        <td>Logický operátor or</td>
        <td>Umožňuje kombinovat podmínky</td>
        <td><code>a = 1 or b = 1</code></td>
    </tr>
    <tr>
        <th><code>not</code></th>
        <td>Logický operátor not</td>
        <td>Negace podmínky</td>
        <td><code>not a = 1</code></td>
    </tr>
    <tr>
        <th><code>( )</code></th>
        <td>Závorky</td>
        <td></td>
        <td><code>(a = 1 or b = 1) and (c = 2)</code></td>
    </tr>
    </tbody>
</table>

<p>Operátory mají obvyklou prioritu: základní operátory v první tabulce nejvyšší, pak <code>not</code>, <code>and</code> a nejnižší prioritu má operátor <code>or</code>. Pokud si nejste jistí, použijte závorky.</p>

<table class="table table-striped table-condensed" summary="Podporované funkce">
    <thead>
    <tr>
        <th>Proměnná</th>
        <th>Název</th>
        <th>Popis</th>
        <th>Ukázka</th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th><code>now()</code></th>
        <td>Aktuální datum a čas</td>
        <td>Umožňuje zobrazovat záznamy s&nbsp;ohledem na aktuální datum<#-- a čas-->.</td>
        <td><code>datSplat &lt; now()</code></td>
    </tr>
    <tr>
        <th><code>currentYear()</code></th>
        <td>Aktuální rok</td>
        <td>Umožňuje zobrazovat záznamy s&nbsp;ohledem na aktuální rok.</td>
        <td><code>platiDo &lt;= currentYear()</code></td>
    </tr>
    <tr>
        <th><code>me()</code></th>
        <td>Jméno přihlášeného uživatele</td>
        <td>Umožňuje zobrazovat záznamy s&nbsp;ohledem na aktuálně přihlášeného uživatele.</td>
        <td><code>uzivatel = me()</code></td>
    </tr>
    </tbody>
</table>

<p>Filtry musí být v&nbsp;URL správně zakódovány. Při ruční tvorbě filtru jej stačí napsat nezakódovaný v&nbsp;prohlížeči Firefox. Když jej pak zkopírujete do schránky, Firefox jej překóduje.</p>

<h3>Zápis hodnot</h3>

<p>Ve výrazech jako <code>a = 1</code> lze zadat čísla, textové řetězce, logické hodnoty, datum a datum+čas. Kromě toho lze použít funkce uvedené v&nbsp;tabulce výše.</p>

<p>Čísla mohou být celá, <code>-1</code> nebo <code>10</code>, a desetinná, <code>5.8</code> nebo <code>-10.0</code>.</p>

<p>Textové řetězce mohou být uvedeny v uvozovkách, <code>"abc"</code>, nebo apostrofech, <code>'abc'</code>.</p>

<p>Logické hodnoty jsou pouze <code>true</code> a <code>false</code>.</p>

<p>Datum se zadává ve tvaru <code>YYYY-MM-DD</code>, např. <code>2011-11-01</code>, datum a čas ve tvaru <code>YYYY-MM-DD'T'HH:MM:SS[.sss]</code>, např. <code>2011-11-01T12:30:00</code>. Nejsou povoleny všechny způsoby zápisu podle ISO&nbsp;8601, pouze tyto dva (resp. tři) zde uvedené.

<p>Když se uvádí vazba na objekt, lze použít libovolný <a href="identifiers">identifikátor</a>. Interní ID se zapisují jako čísla (<code>firma = 1</code>), ostatní identifikátory jako řetězce (<code>firma = 'code:ABC'</code>).</p>

<h3>Filtrace podle vnořených hodnot</h3>

<p>Některé atributy, podle kterých se filtruje, jsou ve skutečnosti vazby. Např. vydané faktury lze filtrovat podle odběratele takto:</p>

<pre>firma = 'code:FIRMA'</pre>

<p>V případě těchto vazeb lze tečkovou notací filtrovat i podle <em>jejich</em> atributů; např. lze filtrovat vydané faktury podle skupiny odběratele:</p>

<pre>firma.skupFir = 'code:ODBĚRATEL-STANDARD'</pre>

<p>Hloubka zanoření není omezena.</p>

<p><strong>Pozor:</strong> z technických důvodů nelze používat negativní operátory (např. <code>&lt;&gt;</code>). V takovém případě dostanete chybové hlášení <code>OR logical subselect filter not supported</code>.</p>

<h3>Filtrace podle štítků</h3>

<p>Kromě atributů lze stejným způsobem filtrovat i&nbsp;podle štítků (pokud jsou štítky v&nbsp;dané evidenci podporovány). Např. pokud chcete vyhledat všechny ceníkové položky se štítkem VIP, bude filtr:</p>

<pre>stitky='code:VIP'</pre>

<p>a <a href="urls">celé URL</a> pak</p>

<pre>/c/firma/cenik/(stitky='code:VIP').xml</pre>

<p>Nebo pokud chcete všechny se štítky VIP nebo DULEZITE, použijete:</p>

<pre>stitky='code:VIP' or stitky='code:DULEZITE'</pre>

<p>resp.</p>

<pre>/c/firma/cenik/(stitky='code:VIP' or stitky='code:DULEZITE').xml</pre>

<p>Stejně lze použít i&nbsp;operátor <code>and</code>.</p>

<h3 id="subtree">Filtrace podle příslušnosti do podstromu</h3>

<p>Položky ceníku jsou řazeny do stromové struktury (je možné, že v budoucnu půjde do stromové struktury řadit i v jiných evidencích) a podle tohoto zařazení lze také filtrovat. Uvažujme tento strom ceníku:</p>

<ul>
    <li>
        Strom ceníku
        <ul>
            <li>
                Připojené položky <code>(id = 2)</code>
                <ul>
                    <li>
                        Kategorie 1 <code>(id = 3)</code>
                        <ul>
                            <li>Kategorie 1.1 <code>(id = 4)</code></li>
                            <li>
                                Kategorie 1.2 <code>(id = 5)</code>
                                <ul>
                                    <li>Kategorie 1.2.1 <code>(id = 6)</code></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>
                        Kategorie 2 <code>(id = 7)</code>
                        <ul>
                            <li>Kategorie 2.1 <code>(id = 8)</code></li>
                            <li>Kategorie 2.2 <code>(id = 9)</code></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li>Nepřipojené položky (virtuální uzel, pod kterým se v aplikaci zobrazují položky, které do stromu nejsou zařazeny; v něm filtrovat nelze)</li>
        </ul>
    </li>
</ul>

<p>Takto se strom zobrazuje v aplikaci, ovšem pro účely stromové kategorizace položek považujeme za kořen až <em>Připojené položky</em>. Uvedená ID jsou jen ilustrační; pokud pracujete se stromem přes REST API, je doporučeno použít <a href="identifiers">externí identifikátory</a>.</p>

<p>Nad ceníkem (<code>/c/firma/cenik</code>) lze zapsat filtr pro získání všech položek z <em>Kategorie 1</em> a všech podkategorií (tedy z uzlů <em>Kategorie 1</em>, <em>Kategorie 1.1</em>, <em>Kategorie 1.2</em> a <em>Kategorie 1.2.1</em>) takto:</p>

<pre>in subtree 3</pre>

<p>což je vlastně zkrácený zápis pro</p>

<pre>id in subtree 3</pre>

<p>Celé URL by vypadalo takto:</p>

<pre>/c/firma/cenik/(in subtree 3)</pre>

<p>Pokud je potřeba získat položky jen z daného uzlu a ne z celého podstromu, lze použít modifikátor <code>nonrecursive</code>; např. pro získání položek jen z <em>Kategorie 2</em>, ale ne už z <em>Kategorie 2.1</em> a <em>Kategorie 2.2</em>, lze psát</p>

<pre>in subtree 7 nonrecursive

/c/firma/cenik/(in subtree 7 nonrecursive)</pre>

<p>Výše bylo uvedeno, že zápis <code>in subtree 3</code> je zkratkou pro <code>id in subtree 3</code>, což je důležité ve chvíli, kdy je potřeba podle zařazení ceníku do podstromu filtrovat jiné evidence. Lze např. vyfiltrovat všechny skladové karty pro ceníkové položky z určitého podstromu, takto:</p>

<pre>cenik in subtree 3

/c/firma/skladova-karta/(cenik in subtree 3)</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>
