<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Individuální ceník"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API (i webové rozhraní) lze získat individuální ceník pro danou firmu či danou ceníkovou skupinu.</p>

<p>Chceme-li např. získat individuální ceník pro firmu, která má kód <code>FIRMA</code>, použijeme URL <code>.../c/{firma}/adresar/code:FIRMA/individualni-cenik[.xml]</code>. Jako obvykle dojde k přesměrování na číselné ID záznamu, výsledné URL je <code>.../c/{firma}/adresar/400/individualni-cenik</code> (pokud má firma <code>FIRMA</code> interní ID <code>400</code>). Lze samozřejmě použít i <a href="detail-levels">úroveň detailu</a>.</p>

<table class="table table-striped table-condensed">
    <tr><th><code>/c/{firma}/adresar/400/individualni-cenik</code></th><td>Individuální ceník pro firmu, která má ID <code>400</code></td></tr>
    <tr><th><code>/c/{firma}/cenikova-skupina/2/individualni-cenik</code></th><td>Individuální ceník pro ceníkovou skupinu, která má ID <code>2</code></td></tr>
</table>
<h3>Parametry</h3>

<p>Parametrem <code>date</code> v URL (formát <code>YYYY-MM-DD</code>) se nastaví, pro jaké datum se má individuální ceník vyexportovat. Není-li parametr uveden, použije se aktuální datum.</p>

<p>Parametrem <code>currency</code> v URL se nastaví, pro jakou měnu se má individuální ceník vyexportovat. Není-li parametr uveden, použije se tuzemská měna. Hodnotou tohoto parametru je zkratka požadované měny.</p>

<p>Parametrem <code>centre</code> v URL se nastaví, pro jaké středisko se má individuální ceník vyexportovat. Není-li parametr uveden, vyexportují se nějlepší ceny bez ohledu na středisko. Hodnotou tohoto parametru je zkratka požadovaného střediska.</p>

<table class="table table-striped table-condensed">
    <tr><th><code>?date=2011-12-01</code></th><td>Individuální ceník k datu 1. prosince 2011.</td></tr>
    <tr><th><code>?currency=EUR</code></th><td>Individuální ceník v jiné měně než v tuzemské. V tomto případě EUR.</td></tr>
    <tr><th><code>?centre=C</code></th><td>Individuální ceník pro konkrétí středisko. V tomto případě středisko se zkratkou C.</td></tr>
</table>

<h2>Hledání obecné ceny pro zákazníka</h2>
<p>Obdobně lze získat všechny varianty cen zboží pro všechny firmy a ceníkové skupiny:</p>

<table class="table table-striped table-condensed">
    <tr><th><code>/c/{firma}/individualni-cenik</code></th><td>Kompletní přehled cen</td></tr>
</table>

<p>Při hledání ceny v případě, že máte staženou celou tabulku "individualni-cenik" je nutné postupovat podle těchto kroků:</p>
<ol>
    <li>vyhledat cenu pro konkrétní firmu a zboží</li>
    <li>vyhledat cenu pro cenovou úroveň a zboží (cenovou úroveň lze nalézt u firmy v&nbsp;adresáři)</li>
    <li>použít standardní cenu zboží z&nbsp;ceníku</li>
    <li>na výslednou cenu je nutné aplikovat procentuální slevu firmy (je u firmy v&nbsp;adresáři)</li>
</ol>

<#include "/i-devdoc-footer.ftl" />
</#escape>