<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Transakční zpracování"/>
<#include "/i-devdoc-header.ftl" />

<p><strong>Tato stránka popisuje pokročilou variantu XML importu, která při nevhodném použití může vést k nekonsistenci dat!</strong></p>

<p>Ve výchozím nastavení se každý import provádí jako jedna databázová transakce &ndash; tedy buď se uloží všechno nebo nic.</p>

<p>To je obvykle požadované chování a většinou není důvod na něm něco měnit. Může ovšem nastat situace, kdy transakční chování není nutné, pak lze atributem <code>atomic</code> toto chování změnit.</p>

<pre class="brush: xml">&lt;winstrom version="1.0" atomic="false"&gt;
  &lt;faktura-vydana&gt;
    &lt;id&gt;code:123&lt;/id&gt;
    ...
    &lt;polozkyFaktury&gt;
      &lt;faktura-vydana-polozka&gt;...&lt;/faktura-vydana-polozka&gt;
      &lt;faktura-vydana-polozka&gt;...&lt;/faktura-vydana-polozka&gt;
      &lt;faktura-vydana-polozka&gt;...&lt;/faktura-vydana-polozka&gt;
    &lt;/polozkyFaktury&gt;
  &lt;/faktura-vydana&gt;
  &lt;faktura-vydana&gt;
    &lt;id&gt;code:456&lt;/id&gt;
    ...
    &lt;polozkyFaktury&gt;
      &lt;faktura-vydana-polozka&gt;...&lt;/faktura-vydana-polozka&gt;
      &lt;faktura-vydana-polozka&gt;...&lt;/faktura-vydana-polozka&gt;
      &lt;faktura-vydana-polozka&gt;...&lt;/faktura-vydana-polozka&gt;
    &lt;/polozkyFaktury&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>

<p>Pokud nastavíte atribut <code>atomic</code> na hodnotu <code>false</code>, importuje se každý záznam v samostatné transakci. Tedy v příkladu výše proběhnou dvě databázové transakce, jedna pro fakturu <code>123</code>, druhá pro fakturu <code>456</code>. Položky jsou součástí faktury a tedy se importují ve stejné transakci, jako faktura samotná.</p>

<p>Jaký to přináší užitek? Když importujete velká XML s mnoha záznamy, transakce trvá dlouho a mnoho informací se musí držet v paměti. Obojí má nepříznivý vliv na výkon. Pokud je ale každý záznam samostatný a nevadí, že uložení některého z nich selže (např. když import pravidelně opakujete a/nebo dokážete v případě problémů zasáhnout ručně), můžete podstatně snížit paměťovou náročnost importu.</p>

<p>V případě opravdu velkých importů jsou paměťové nároky pro uchování ještě neuložených dat tak velké, že podstatnou část procesorového času začne zabírat <a href="http://cs.wikipedia.org/wiki/Garbage_collector">garbage collector</a> (to lze sledovat např. nástrojem <code>jconsole</code>, který je standardní součástí vývojového prostředí JDK). V módu <code>atomic="false"</code> se v takovém případě může razantně snížit i náročnost časová.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>