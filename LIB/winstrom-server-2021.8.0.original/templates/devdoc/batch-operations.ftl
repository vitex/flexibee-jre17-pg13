<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Dávkové operace"/>
<#include "/i-devdoc-header.ftl" />

<p>Při importu lze jedním elementem aktualizovat více záznamů zároveň. Takto se například přidá všem položkám ceníku s&nbsp;dodavatelem <code>FIRMA</code> štítek <code>VIP</code>:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;cenik filter="dodavatel = 'code:FIRMA'"&gt;
     &lt;stitky&gt;VIP&lt;/stitky&gt;
  &lt;/cenik&gt;
&lt;/winstrom&gt;
</pre>

<p></p>

<p>Atribut <code>filter</code> určuje záznamy z&nbsp;dané evidence (zde <code>cenik</code>), na kterých se operace provede. Filtrační jazyk je <a href="filters">stejný jako v&nbsp;REST API</a>.</p>

<p>Dávkové zpracování funguje tak, jako by namísto jediného elementu <code>cenik</code> s&nbsp;atributem <code>filter</code> byly uveden elementy <code>cenik</code> pro všechny záznamy, které odpovídají dané podmínce. Jediným rozdílem je, že při dávkových operacích se úplně ignorují elementy <code>id</code>.</p>

<p>Pro zadání dávkové operace lze samozřejmě použít i formát JSON:</p>
<pre class="brush: js">
{
    "winstrom": {
        "@version": "1.0",
        "faktura-vydana": {
            "@filter": "stitky='code:OVERENO'",
            "@action": "lock"
        }
    }
}
</pre>

<p>Tímto příkladem by došlo k <a href="actions">vyvolání akce</a> <code>lock</code> nad všemi záznamy evidence <code>faktura-vydana</code>, které mají přiřazen <a href="stitky">štítek</a> <code>OVERENO</code>.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
