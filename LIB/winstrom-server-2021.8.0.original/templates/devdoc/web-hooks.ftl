<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Web Hooks"/>
<#include "/i-devdoc-header.ftl" />

<h2>Web Hooks</h2>

<p><a href="http://web.archive.org/web/20101111173315/http://wiki.webhooks.org/w/page/13385124/FrontPage">Web Hooks</a> jsou způsob, jak se ve Vaší aplikaci v reálném čase dozvědět o změně v ABRA Flexi. Princip je jednoduchý: když dojde v&nbsp;databázi ABRA Flexi ke změně, je (obvykle v&nbsp;řádu několika málo vteřin) odeslán <code>POST</code> HTTP request na všechna zaregistrovaná URL. Obsahem požadavku je <strong>výpis změn najednou od posledního zavolání hooku</strong>, a to ve stejném formátu, jaký získáte od ABRA Flexi přes rozhraní <a href="changes-api">Changes API</a>.</p>

<h3>Postup</h3>

<p>Musí být zapnuto <a href="changes-api">Changes API</a> (sledování změn). Také musí být hooky povoleny v konfiguračním souboru ABRA Flexi Serveru <code>flexibee-server.xml</code> (<a href="http://www.flexibee.eu/produkty/flexibee/dokumentace/otazky/umisteni-adresaru/">umístění adresářů s&nbsp;<code>flexibee-server.xml</code></a>):</p>

<pre class="brush: xml">
  ...
  &lt;entry key="enableHooks"&gt;true&lt;/entry&gt;
  ...
</pre>

<p>Důvodem je, že při startu serveru je potřeba ihned nastartovat jádro (vizte též <a href="automatic-startup">automatické startování jádra</a>), což je časově náročná operace. Pozn.: pokud máte nastaveno <code>enableHooks</code> na <code>true</code>, není už třeba nastavovat <code>startKernel</code>.</p>

<p>Hook se zaregistruje <code>PUT</code> (příp. <code>POST</code>) požadavkem na adresu <code>/c/{firma}/hooks</code> s následujícími parametry:</p>

<table class="table table-striped table-condensed">
    <tr><th colspan="2" class="group">povinné</th></tr>
    <tr><th><code>?url=http://muj.server.cz/hook.php</code></th><td>URL, které se má zavolat</td></tr>
    <tr><th><code>?format=XML</code></th><td>Formát dat (možné hodnoty jsou <code>XML</code> a <code>JSON</code>)</td></tr>
    <tr><th colspan="2" class="group">volitelné</th></tr>
    <tr><th><code>?lastVersion=123</code></th><td>Verze od které započne posílání následujích změn, tj. od nejbližší vyšší verze. Defaultní hodnota je rovna <a href="changes-api#globalVersion">aktuální globální verzi</a> (<code>globalVersion</code>) v momentě registrace hooku. Přípustné hodnoty jsou z intervalu: <code>[0, globalVersion]</code>.</td></tr>
    <tr><th><code>?secKey=MyHookSecretToken0687</code></th><td>Libovolný řetězec, který bude odesílán s každou notifikací změn v HTTP hlavičce. Slouží k&nbsp;jednoduchému ověření, zda patří příchozí notifikace Vámi registrovanému hooku. Název klíče v&nbsp;HTTP hlavičce je <code>X-FB-Hook-SecKey</code>.</td></tr>
    <tr><th><code>?skipUrlTest=true</code></th><td>Potlačení testu funkčnosti předaného URL.</td></tr>
</table>

<p>Např.:</p>

<pre class="shell">curl -u uzivatel:heslo -L -k -X PUT "https://localhost:5434/c/{firma}/hooks.xml?url=http://muj.server.cz/hook.php&format=XML&lastVersion=123&secKey=MyHookSecretToken0687"</pre>

<p>Registrace provádí test předaného URL odesláním prázdné notifikace. Při návratovém kódu jiném než <code>2xx</code> nebude hook zaregistrován. Provedení testu lze potlačit parametrem <i>skipUrlTest</i>.</p>

<p>ABRA Flexi od verze <a href="https://www.flexibee.eu/2017-1-1/">2017.1.1</a> podporuje <a href="https://en.wikipedia.org/wiki/Server_Name_Indication" title="Server Name Indication">SNI</a>. To znamená, že je možně registrovat hooky směřující na HTTPS virtuální host.</p>

<p>Prozatím není možné specifikovat, kterých evidencí se má hook týkat, vždy je upozorněn na všechny změny, které v ABRA Flexi nastanou.</p>

<p>Jako obvykle, úspěch je oznámen stavovým kódem <code>200</code> a neúspěch kódem <code>400</code> (v&nbsp;odpovědi je textový popis příčiny).</p>

<p>Výpis zaregistrovaných hooků je na adrese <code>/c/{firma}/hooks</code>, odregistrovat hook lze <code>DELETE</code> požadavkem na adresu <code>/c/{firma}/hooks/{id}</code>.</p>

<h3>Chování hooku při chybě</h3>

<p>Pokud nastává chyba při zpracování hooku, pokouší se server zasílat požadavky opakovaně. Pokud hook i nadále selhává, začne docházet ke zpožďování volání hooků. Typicky v případě, že je služba zcela nedostupná začne každé volání později. Pro tyto účely se používá <code>penalty</code>, která reprezentuje dobu mezi jednotlivými pokusy.</p>

<p>Aktuální penalizaci můžete získat pomocí GET:</p>
<pre class="shell">curl -u uzivatel:heslo -L -k "https://localhost:5434/c/{firma}/hooks/{id}.xml"</pre>

<p>Vynulování penalizace a okamžité zavolání hooku zajistí PUT požadavek:</p>
<pre class="shell">curl -u uzivatel:heslo -L -k -X PUT "https://localhost:5434/c/{firma}/hooks/{id}/retry"</pre>

<p>Registrované hooky jsou ukládány v databázi a tak dojde k odeslání hooku i po restartování serveru. Služba garantuje, že se žádná změna neztratí a všechny jsou předány registrovanému hooku.</p>

<h3>Doporučení pro implementaci hooku</h3>

<p>Celý mechanismus funguje na principu <a href="http://en.wikipedia.org/wiki/Best-effort_delivery">best effort</a>. To znamená, že i když se snažíme doručovat oznámení co nejdříve a vyhýbat se duplicitám, je potřeba počítat s&nbsp;tím, že zpoždění nebo duplicita mohou nastat (prostě stejný požadavek doručíme vícekrát). Pro eliminaci duplicit můžete zpracovávat <code>globalVersion</code>.</p>

<p>Zpracování hooku by mělo trvat co nejkratší dobu (&lt; 15 sekund) a rozhodně <strong>nesmí</strong> přesáhnout 30 sekund, jinak se považuje volání za neúspěšné. Odpověď musí mít status kód <code>200</code> (resp. <code>2xx</code>) a neměla by obsahovat žádné tělo. Pokud je některá z těchto podmínek porušena, daný hook je penalizován (tj. nějakou dobu nebude vůbec zavolán) a v krajním případě může dojít i k jeho úplnému vypnutí.</p>

<p>Ideální implementace hooku provádí pouze persistenci přijatých změn s případnou rychlou filtrací na relevatní změny a s přeskakovaním duplicit (již zpracovaných změn). Vlastní zpracování přijatých změn by mělo běžet asynchronně v nezávislém vlákně.</p>

<h3>Další podporované stavové kódy odpovědí</h3>

<p>Zpracování hooku podporuje v odpovědích, kromě klasického potvrzení statusem <code>200</code>, ještě následující možnosti:</p>
<dl>
<dt>301 (Moved Permanently) / 308 (Permanent Redirect)</dt>
<dd>V případě, že přesměrování vede na validní URL, tak proběhne aktualizace adresy registrovaného hooku a po krátké penalizaci již proběhne notifikace změn na nově evidovanou adresu.</dd>
<dt>410 (Gone)</dt>
<dd>Předpokládá se, že byl daný hook permanentně zrušen, a na straně ABRA Flexi proběhne jeho automatická odregistrace.</dd>
</dl>

<#include "/i-devdoc-footer.ftl" />
</#escape>
