<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="XSLT transformace"/>
<#include "/i-devdoc-header.ftl" />

<p>Aby ekonomický systém ABRA Flexi dokázal načítat a exportovat i další XML formáty, než jen nativní, je možné použít uživatelské a integrované <a href="http://cs.wikipedia.org/wiki/XSLT">XSLT</a> transformace.</p>

<p>XSLT lze použít pouze v&nbsp;kombinaci s&nbsp;XML. Při importu se na XML uvedené v&nbsp;body REST-API požadavku nejdříve aplikuje XSLT transformace a výsledné XML je importováno do&nbsp;ABRA Flexi. V&nbsp;případě exportu je nejdříve aplikován <a href="filters">filtr</a>, <a href="paging">stránkování</a> a <a href="detail-levels">úroveň detailu</a> a na výsledné XML je aplikována XSLT transformace. <!-- Na výsledný dokument je možné aplikovat také <a href="xpath">XPath</a>. --></p>

<p>XSLT lze aplikovat při importu / exportu parametrem <code>format</code>.</p>

<p>Ukázka použití uživatelské XSLT transformace v REST-API volání:</p>
<pre class="brush: xml">
/c/demo/faktura-vydana.xml?format=code:xyz
</pre>

<p>Ukázka použití integrované XSLT transformace v REST-API volání:</p>
<pre class="brush: xml">
/c/demo/faktura-vydana.xml?format=awis-items
</pre>

<p><a href="http://www.kosek.cz/xml/xslt/">Základy jazyka XSLT...</a></p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
