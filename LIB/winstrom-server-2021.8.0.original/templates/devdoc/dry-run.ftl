<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Testovací uložení (dry-run)"/>
<#include "/i-devdoc-header.ftl" />

<p>Když pracujete s&nbsp;REST API a potřebujete zkontrolovat, zda jsou všechna data v pořádku, pokud uživatel změnil nějakou hodnotu, která ovlivňuje jinou (např. typ dokladu a zaúčtování) a nebo potřebujete spočítat výslednou cenu při objednávce, můžete použít režim testovacího uložení.</p>

<p>Při uložení přidejte parameter <code>?dry-run=true</code>. Výsledkem bude, že se záznam neuloží, ale jen se provedou validace. Navíc získáte v&nbsp;tagu <code>&lt;content /&gt;</code> výslednou reprezentaci záznamu tak, jak by vypadal, kdyby jste jej nyní uložili.</p>

<p>Ukázka výsledného XML:</p>
<pre class="brush: xml">
&lt;?xml version="1.0" encoding="utf-8"?&gt;
&lt;winstrom version="1.0"&gt;
  &lt;success&gt;true&lt;/success&gt;
  &lt;warnings&gt;
    &lt;warning for="firma"&gt;Nebyl určen příjemce a tak nebude doklad zaúčtován.&lt;/warning&gt;
  &lt;warnings&gt;
  &lt;result&gt;
    &lt;content&gt;
      &lt;!-- Doklady faktur --&gt;
      &lt;faktura-vydana&gt;
        &lt;!-- ID (celé číslo) - --&gt;
        &lt;id&gt;-1&lt;/id&gt;
        &lt;!-- Interní číslo (řetězec) - max. délka: 20 --&gt;
        &lt;kod&gt;00000041/09&lt;/kod&gt;
        &lt;!-- Variabilní symbol (řetězec) - --&gt;
        &lt;varSym&gt;0000004109&lt;/varSym&gt;
        &lt;!-- Vystaveno (datum) - --&gt;
        &lt;datVyst&gt;2009-08-12+02:00&lt;/datVyst&gt;
        ...
      &lt;/faktura-vydana&gt;
    &lt;/content&gt;
  &lt;/result&gt;
&lt;/winstrom&gt;
</pre>

<p>V tomto případě došlo i k&nbsp;přidělení čísla dokladu. Nicméně číslo bylo hned uvolněno k&nbsp;dalšímu použití a tak je možné, že při skutečném uložení získá dokument jiné číslo. Je zde také ukázka <a href="validations">validačního upozornění</a>.</p>

<p>Výstup je možné zpracovávat také ve formátu <a href="format-types">JSON</a>.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>