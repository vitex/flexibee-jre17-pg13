<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Export do PDF"/>
<#include "/i-devdoc-header.ftl" />

<p>Tiskové sestavy dostupné v aplikaci je možné získat i přes REST API, a to obvykle jak pro konkrétní záznam, tak pro celý seznam. URL vypadají takto:</p>

<pre>/c/<span style="color:green">&lt;identifikátor firmy&gt;</span>/<span style="color:red">&lt;evidence&gt;</span>.pdf</pre>
<pre>/c/<span style="color:green">&lt;identifikátor firmy&gt;</span>/<span style="color:red">&lt;evidence&gt;</span>/<span style="color:blue">&lt;ID záznamu&gt;</span>.pdf</pre>

<p>Konkrétní sestavu lze vybrat použitím parametru <code>report-name</code>. Pokud je potřeba, lze vybrat i jazyk sestavy; k tomu slouží parametr <code>report-lang</code>. Podporované jazyky jsou <code>cs</code>, <code>sk</code>, <code>en</code> a <code>de</code>.</p>

<p>Lze vygenerovat i elektronicky podepsané PDF. Tato funkce má několik omezení: funguje pouze s certifikáty uloženými v ABRA Flexi, a uložen smí být právě jeden certifikát. Je třeba v URL uvést parametr <code>report-sign=true</code>. V budoucnu bude možné vybrat z uložených certifikátů, který se má použít.</p>

<p>Kompletní URL může vypadat např. takto:</p>

<pre>/c/<span style="color:green">firma</span>/<span style="color:red">faktura-vydana</span>/<span style="color:blue">1</span>.pdf?report-name=dodaciList</pre>
<pre>/c/<span style="color:green">firma</span>/<span style="color:red">faktura-vydana</span>/<span style="color:blue">1</span>.pdf?report-name=dodaciList&report-lang=en</pre>
<pre>/c/<span style="color:green">firma</span>/<span style="color:red">faktura-vydana</span>/<span style="color:blue">1</span>.pdf?report-name=dodaciList&report-sign=true</pre>

<p>Přehled podporovaných reportů pro danou evidenci najdete na <code>/c/<span style="color:green">&lt;identifikátor firmy&gt;</span>/<span style="color:red">&lt;evidence&gt;</span>/reports</code> (např. <a href="https://demo.flexibee.eu/c/demo/faktura-vydana/reports">pro fakturu</a>). Výstup lze opět exportovat ve formátech <a href="https://demo.flexibee.eu/c/demo/faktura-vydana/reports.xml">XML</a> či <a href="https://demo.flexibee.eu/c/demo/faktura-vydana/reports.json">JSON</a>.

<p>Výstup obsahuje tyto informace:</p>
<ul>
    <li><code>reportId</code>: identifikátor reportu. Uvádí se jako parametr <code>report-name</code> při volání</li>
    <li><code>reportName</code>: uživatelský název. Je zobrazen v&nbsp;aktuálním jazyce (dle prohlížeče/agenta).</li>
    <li><code>isDefault</code>: je sestava výchozí? Tj. volí se při stisku rychlého tlačítka.</li>
    <li><code>predvybranyPocet</code>: některé sestavy jsou přehledové, některé tisknou konkrétní záznam. Nabývá hodnot <code>1</code> a <code>N</code>.</li>
    <li><code>rozsiritelna</code>: existuje k tiskové sestavě její rozšířená verze?</li>
    <li><code>sumovana</code>: podporuje tisková sestava sumaci?</li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
