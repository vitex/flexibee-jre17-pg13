<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="ABRA Flexi XML"/>
<#include "/i-devdoc-header.ftl" />

<p>Základem komunikace s&nbsp;ABRA Flexi je ABRA Flexi XML. Ta může být ve formátu XML nebo JSON. V&nbsp;obou případech je pak struktura shodná. Rozdíl je jen ve způsobu uložení atributů do JSON. Jsou pak uloženy jako záznam s&nbsp;zavináčem: <code>@rowCount</code>.</p>

<p>Důležitou vlastností pro ABRA Flexi XML je <a href="partial-updates">inkrementální aktualizace</a>, <a href="required-fields">požadované atributy</a>, <a href="identifiers">identifikátory záznamů</a> a <a href="variable-types">typy proměnných</a>.</p>

<h3>Velikosti písmen</h3>
<p>Velikosti písmen v&nbsp;názvech tagů a atributů jsou ignorovány a tak je možné používat libovolnou kombinaci.</p>

<h3>Používané atributy</h3>
<p>Přehled atributů, které daná evidence podporuje se podívejte buď do ukázkového XML (první záznam je vždy okomentovaný) nebo do dokumentace u konkrétní evidence. Všechny atributy mají <a href="internal-dependencies">vnitřní vazby</a> a tak není nutné uvádět všechny atributy, protože ostatní se buď automaticky dopočtou, jsou převzaty z&nbsp;typu dokladu a nebo jsou určeny jinou vazbou (např. u faktury vybráním firmy dojde k&nbsp;vyplnění i IČO, adresy apod.).</p>



<ul>
    <li><a href="variable-types">Typy proměnných</a></li>
    <li><a href="identifiers">Identifikátory záznamů</a></li>
    <li><a href="partial-updates">Inkrementální aktualizace</a></li>
    <li><a href="required-fields">Povinnost atributů</a></li>
    <li><a href="internal-dependencies">Vnitřní vazby při ukládání</a></li>
    <li><a href="create-update-mode">Režim pro založení/změnu</a></li>
    <li><a href="actions">Provádění akcí</a></li>
    <li><a href="last-update">Datum poslední změny</a></li>
<!--    <li><a href="xpath">Na výstupní XML lze aplikovat XPath</a></li>     -->
    <li><a href="dph">Výpočet Daně z přidané hodnoty (DPH)</a></li>
    <li><a href="users">Práce s uživateli</a></li>
    <li><a href="previous-value">Předchozí hodnoty - způsob reakce na změnu</a></li>
    <li><a href="batch-operations">Dávkové operace</a></li>
    <li><a href="tx">Transakční zpracování</a></li>
    <li><a href="xsd">XML Schéma (XSD)</a></li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>
