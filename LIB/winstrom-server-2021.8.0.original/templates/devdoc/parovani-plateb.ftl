<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Párování plateb"/>
<#include "/i-devdoc-header.ftl" />

<p>Pokladnu nebo banku lze spárovat s jednou nebo více fakturami vydanými nebo přijatými následujícím způsobem:</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;banka&gt; &lt;!-- uhrazující doklad; může být i "pokladni-pohyb" --&gt;
    &lt;id&gt;code:BANKA1&lt;/id&gt;
    &lt;!-- lze normálně uvést další vlastnosti dokladu jako při běžném importu --&gt;

    &lt;sparovani&gt;
      &lt;!-- uhrazovaný doklad - pro uhrazení více faktur se element opakuje
       type - ve spárování lze použít pouze faktury stejného typu (vydané nebo přijaté)
       castka - (volitelný) určuje částku, která se má z faktury uhradit --&gt;
      &lt;uhrazovanaFak type="faktura-vydana" castka="1000"&gt;code:FV1&lt;/uhrazovanaFak&gt;
      &lt;zbytek&gt;ignorovat&lt;/zbytek&gt; &lt;!-- co dělat se zbytkem, pokud nastane --&gt;
    &lt;/sparovani&gt;
  &lt;/banka&gt;
&lt;/winstrom&gt;</pre>

<p>V jednom spárování lze uhrazovat více faktur najednou. Při spárování s více fakturami musí být všechny uvedené faktury stejného typu faktury (vydané nebo přijaté).</p>

<p>U každé uhrazované faktury lze uvést atribut <code>castka</code>, jehož hodnota omezuje celkovou částku k úhradě, která bude z faktury uhrazena.</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;banka&gt;
    &lt;id&gt;code:BANKA1&lt;/id&gt;
    &lt;sparovani&gt;
      &lt;!-- uhrazují se dvě faktury najednou --&gt;
      &lt;uhrazovanaFak type="faktura-vydana" castka="500"&gt;code:FV1&lt;/uhrazovanaFak&gt; &lt;!-- z FV1 se uhrazuje 500 --&gt;
      &lt;uhrazovanaFak type="faktura-vydana"&gt;code:FV2&lt;/uhrazovanaFak&gt; &lt;!-- z FV2 se uhrazuje celá zbývající částka --&gt;
      &lt;zbytek&gt;ignorovat&lt;/zbytek&gt;
    &lt;/sparovani&gt;
  &lt;/banka&gt;
&lt;/winstrom&gt;</pre>

<p>Hodnota atributu <code>castka</code> nesmí překročit zbývající částku k úhradě na uhrazované faktuře. Je-li hodnota  atributu <code>castka</code> menší než zbývající částka k úhradě, bude tato konkrétní faktura vždy uhrazena jako částečná úhrada. Je-li hodnota atributu <code>castka</code> rovna zbývající částce k úhradě, pak atribut ztrácí význam a spárování proběhne stejně, jako by nebyl uveden.</p>

<p>Může se stát, že uhrazující částka na uhrazujícím dokladu a součet částek na uhrazovaných fakturách nesouhlasí (např. při kurzovém rozdílu a nebo schází doplatit pár korun), v takovém případě se import řídí hodnotou v tagu <code>&lt;zbytek/&gt;</code>. Lze zvolit tyto hodnoty:</p>

<ul>
    <li><code>ne</code>: zbytek nesmí nastat; pokud k němu dojde, jedná se o chybu</li>
    <li><code>zauctovat</code>: zbytek se <em>zaúčtuje</em></li>
    <li><code>ignorovat</code>: zbytek se <em>ignoruje</em></li>
    <li><code>castecnaUhrada</code>: pokud je částka na uhrazujícím dokladu <em>menší</em> než na uhrazovaném, jedná se o částečnou úhradu</li>
    <li><code>castecnaUhradaNeboZauctovat</code>: pokud je částka na uhrazujícím dokladu <em>větší</em> než na uhrazovaném, zbytek se zaúčtuje; pokud je <em>menší</em>, jedná se o částečnou úhradu</li>
    <li><code>castecnaUhradaNeboIgnorovat</code>: pokud je částka na uhrazujícím dokladu <em>větší</em> než na uhrazovaném, zbytek se ignoruje; pokud je <em>menší</em>, jedná se o částečnou úhradu</li>
</ul>

<p>Výsledek importu v závislosti na zvolené hodnotě tagu <code>&lt;zbytek/&gt;</code> a velikosti rozdílu uhrazující a uhrazované částky (zbytku):</p>

<table class="table table-bordered">
  <tr class="active">
    <th>parametr <code>zbytek</code></th>
    <th>zbytek = 0</th>
    <th>zbytek > 0</th>
    <th>zbytek < 0</th>
  </tr>
  <tr>
    <th><code>ne</code></th>
    <td rowspan=6 style="max-width: 24em;"><p>Faktury budou <em>zcela uhrazeny</em>, nebo <em>částečně uhrazeny</em>, pokud došlo k omezení úhrady atributem <code>castka</code>.</p><p>Uhrazující doklad bude spárován.</td>
    <td colspan=2 class="text-danger"><p>CHYBA:</p><p>400 - Částky na uhrazovaném a uhrazujícím dokladu se neshodují</p></td>
  </tr>
  <tr>
    <th><code>zauctovat</code></th>
    <td colspan=2><p>Faktury budou <em>zcela uhrazeny</em>, nebo <em>částečně uhrazeny</em>, pokud došlo k omezení úhrady atributem <code>castka</code>.</p><p>Uhrazující doklad bude spárován.</p><p><strong>Pro zbytek vznikne interní doklad.</strong></p></td>
  </tr>
  <tr>
    <th><code>ignorovat</code></th>
    <td colspan=2><p>Faktury budou <em>zcela uhrazeny</em>, nebo <em>částečně uhrazeny</em>, pokud došlo k omezení úhrady atributem <code>castka</code>.</p><p><strong>Uhrazující doklad nebude spárován.</strong></p></td>
  </tr>
  <tr>
    <th><code>castecnaUhrada</code></th>
    <td class="text-danger"><p>CHYBA:</p><p>400 - Částečná úhrada nemá smysl, částka na uhrazujícím dokladu je větší než na uhrazovaném</p></td>
    <td rowspan=3><p>Částka uhrazujícího dokladu se postupně &bdquo;spotřebovává&ldquo; na uhrazení faktur nebo částek, které se z nich mají uhradit, v pořadí jejich uvedení v elementu <code>&lt;sparovani/&gt;</code>.</p><p>Faktura, na kterou z úhrady už nezbývá dostatečná částka na úplnou úhradu nebo úhradu celé uvedené částky, se <em>uhradí částečně</em> do výše zbývajících prostředků z uhrazující částky.</p><p>Faktury, na které z uhrazující částky nezbývají žádné prostředky, jsou z párování vyřazeny a zůstanou <em>neuhrazeny</em>.</p></td>
  </tr>
  <tr>
    <th><code>castecnaUhradaNeboZauctovat</code></th>
    <td><p>Faktury budou <em>zcela uhrazeny</em>, nebo <em>částečně uhrazeny</em>, pokud došlo k omezení úhrady atributem <code>castka</code>.</p><p>Uhrazující doklad bude spárován.</p><p><strong>Pro zbytek vznikne interní doklad.</strong></p></td>
  </tr>
  <tr>
    <th><code>castecnaUhradaNeboIgnorovat</code></th>
    <td><p>Faktury budou <em>zcela uhrazeny</em>, nebo <em>částečně uhrazeny</em>, pokud došlo k omezení úhrady atributem <code>castka</code>.</p><p><strong>Uhrazující doklad nebude spárován.</strong></p></td>
  </tr>
</table>

<p>V tagu <code>&lt;sparovani/&gt;</code> lze navíc uvést ještě i následující. Není to povinné a standardně se bere z nastavení firmy.</p>

<pre class="brush: xml">
  &lt;!-- kurzový rozdíl, defaultně z nastavení firmy --&gt;
  &lt;krTypDokl&gt;&lt;/krTypDokl&gt; &lt;!-- typ dokladu pro kurzový rozdíl --&gt;
  &lt;krTypDoklZisk&gt;&lt;/krTypDoklZisk&gt; &lt;!-- typ dokladu pro zisk kurzového rozdílu --&gt;
  &lt;krTypDoklZtrata&gt;&lt;/krTypDoklZtrata&gt; &lt;!-- typ dokladu pro ztrátu kurzového rozdílu --&gt;
  &lt;krRada&gt;&lt;/krRada&gt; &lt;!-- řada pro kurzový rozdíl --&gt;

  &lt;!-- zbytek, defaultně z nastavení firmy --&gt;
  &lt;zbTypDokl&gt;&lt;/zbTypDokl&gt; &lt;!-- typ dokladu pro zbytek --&gt;
  &lt;zbTypDoklZisk&gt;&lt;/zbTypDoklZisk&gt; &lt;!-- typ dokladu pro zisk zbytku --&gt;
  &lt;zbTypDoklZtrata&gt;&lt;/zbTypDoklZtrata&gt; &lt;!-- typ dokladu pro ztrátu zbytku --&gt;
  &lt;zbRada&gt;&lt;/zbRada&gt; &lt;!-- řada pro zbytek --&gt;
</pre>

<h3>Spárování úhrady v domácí měně s fakturou v cizí měně</h3>
<p>Vedle párování dokladů ve stejných měnách, lze také spárovat pokladnu nebo banku v domácí měně s fakturami v cizí měně. Cizí měna musí být pro všechny párované faktury stejná. V tomto případě se uhrazující doklad automaticky převede do cizí měny v kurzu rovnající se poměru uhrazující částky na bance v domácí měně ku celkové uhrazované částce na fakturách v cizí měně.</p>

<h3>Odpárování</h3>
<p>Analogicky lze provádět i odpárování:</p>

<pre class="brush: xml">
&lt;winstrom version="1.0"&gt;
  &lt;banka&gt;
    &lt;id&gt;code:BANKA1&lt;/id&gt;
    &lt;odparovani&gt;
      &lt;uhrazovanaFak type="faktura-vydana"&gt;code:FV1&lt;/uhrazovanaFak&gt; &lt;!-- nepovinné, lze vícekrát --&gt;
    &lt;/odparovani&gt;
  &lt;/banka&gt;
&lt;/winstrom&gt;
</pre>

<p>Pokud žádný uhrazovaný doklad není uveden, odpárují se všechny, které jsou s daným uhrazujícím dokladem spárovány.</p>

<p>Párování je idempotentní (tj. lze opakovat jeho volání).</p>

<h3>Automatické párování</h3>
<p>Přes API lze vyvolat i automatické párování plateb.</p>

<pre>curl -H "Accept: application/xml" -u winstrom:winstrom -X PUT -L https://demo.flexibee.eu:5434/c/demo/banka/automaticke-parovani</pre>

<br/>
<p><a href="filters">Filtrováním</a> lze omezit úhrady vstupující do párování.</p>

<pre>/c/{firma}/banka/{filtr}/automaticke-parovani</pre>

<br/>
<p>Následující příklad bude párovat jen úhrady zadané od 1.3.2020</p>

<pre>/c/{firma}/banka/(datVyst>='2020-03-01')/automaticke-parovani</pre>

<br/>
<p>Pomocí parametrů lze nastavit mód párování, omezit v jakých účetních obdobích se budou hledat doklady k úhradě a jak nakládat s rozdílem mezi úhradou a uhrazovaným dokladem.</p>

<pre>/c/{firma}/banka/automaticke-parovani?<strong>mod=</strong>jenVar&<strong>obdobi=</strong>aktualni&<strong>ignorovat-rozdil-castka=</strong>1.5&<strong>zauctovat-rozdil=</strong>true</pre>

<p><code>mod</code> - mód automatického párování. Možné hodnoty:</p>
<ul>
    <li><code>varCasUcet</code>: párovat dle variabilního sym. a částky a účtu</li>
    <li><code>varCas</code>: párovat dle variabilního sym. a částky <strong>(výchozí hodnota)</strong></li>
    <li><code>jenVar</code>: párovat dle variabilního sym.</li>
    <li><code>jenCastka</code>: připojit - párovat kdy souhlasí částka a nesouhlasí VS</li>
</ul>

<p><code>obdobi</code> - v kterých obdobích se budou hledat doklady k úhradě. Možné hodnoty:</p>
<ul>
    <li><code>aktualni</code>: aktuální účetní období</li>
    <li><code>aktualni-predchozi</code>: aktuální a předchozí účetní období</li>
    <li><code>vsechna</code>: všechna účetní období <strong>(výchozí hodnota)</strong></li>
</ul>

<p><code>ignorovat-rozdil-castka</code> - jak velký rozdíl mezi úhradou a uhrazovaným dokladem ignorovat (výchozí hodnota 0.0 - částky musí odpovídat, v módu <code>jenVar</code> se nastavení rozdílu ignoruje)</p>

<p><code>zauctovat-rozdil</code> - zda se zaúčtují doklady pokud dojde ke spojení úhrad, kdy nejsou částky dokladů shodné (výchozí hodnota true - vznikne interní doklad na rozdíl mezi doklady a doklady budou plně spárovány)</p>

<h3>Původní způsob párování pouze přes REST API</h3>

<p>Je podporován i zastaralý způsob, kterým šlo párovat pouze přes REST API (nikoliv XML importem) na URL <code>/c/{firma}/parovani-uhrad</code>.</p>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;sparovani&gt;
    &lt;uhrazovanaFak type="faktura-prijata"&gt;code:FP1&lt;/uhrazovanaFak&gt; &lt;!-- faktura --&gt;
    &lt;uhrazujiciDokl type="banka"&gt;code:BANKA1&lt;/uhrazujiciDokl&gt;  &lt;!-- bankovní doklad --&gt;
    &lt;zbytek&gt;ignorovat&lt;/zbytek&gt; &lt;!-- zbytek ignorovat --&gt;
  &lt;/sparovani&gt;
&lt;/winstrom&gt;</pre>

<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;odparovani&gt;
    &lt;uhrazujiciDokl&gt;code:foo&lt;/uhrazujiciDokl&gt; &lt;!-- povinné --&gt;
    &lt;uhrazovanaFak&gt;code:bar&lt;/uhrazovanaFak&gt; &lt;!-- nepovinné, lze vícekrát --&gt;
  &lt;/odparovani&gt;
&lt;/winstrom&gt;</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>