<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Inkrementální aktualizace"/>
<#include "/i-devdoc-header.ftl" />

<p>ABRA Flexi umožňuje částečné aktualizace záznamů (změny hodnot). Při aktualizaci záznamů uveďte jen takové atributy, které chcete změnit. Pokud uvedete prázdný atribut, dojde ke smazání hodnoty atributu.</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;cenik id="123"&gt;
     &lt;nazevA&gt;Nový název&lt;/nazevA&gt; &lt;!-- změněná hodnota --&gt;
     &lt;ean&gt;&lt;/ean&gt;                 &lt;!-- smazaná hodnota --&gt;
  &lt;/cenik&gt;
&lt;/winstrom&gt;
</pre>

<h2>Mazání položek</h2>
<p>Většina záznamů může obsahovat položky (např. faktura). Tyto položky je možné přidávat a mazat. Pokud u položky není uveden identifikátor, je určující její pořadí. Doporučujeme i u položek uvádět externí identifikátor, protože na položku mohou být navázány další informace (např. z jakého skladu bylo zboží vydáno). Pokud dojde k&nbsp;řízení položek jen podle pořadí a dojde k&nbsp;vložení záznamu na začátek, hodnoty budou přepsány, ale navázané informace zůstanou u původních záznamů. Tento postup tedy není vhodný při aktualizacích.</p>

<h3>Aktualizace všech položek</h3>
<p>Pokud při aktualizaci položek není uveden <a href="identifiers">identifikátor</a>, dojde vždy k&nbsp;přidání záznamů. Pokud chcete seznam záznamů nahradit a smazat tak všechny položky, které nejsou při aktualizaci uvedeny, použijte atribut <code>removeAll="true"</code>:</p>
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;faktura-vydana id="123"&gt;
    &lt;polozkyFaktury removeAll="true"&gt;
      &lt;faktura-vydana-polozka&gt;
        &lt;id&gt;14&lt;/id&gt;
        &vellip;
      &lt;/faktura-vydana-polozka&gt;
    &lt;/polozkyFaktury&gt;
  &lt;/faktura-vydana&gt;
&lt;/winstrom&gt;
</pre>

<p>V&nbsp;tomto případě platí, že všechny položky, které nejsou v&nbsp;seznamu uvedeny, budou smazány. Ty položky, které uvedeny jsou budou aktualizovány/založeny.</p>

<p>Ten samý požadavek přepsaný do JSON formátu by vypadal takto:<p>
<pre class="brush: js">
{
    "winstrom": {
        "@version": "1.0",
        "faktura-vydana": [{
            "id": "123",
            "polozkyFaktury@removeAll": "true",
            "polozkyFaktury": [{
                "id": "14",
                &vellip;
            }]
        }]
    }
}
</pre>

<p>Pro přímé mazání vybraných položek lze použít atribut <code>action="delete"</code> viz <a href="actions">Provádění akcí</a>.</p>

<p>Mezi položkami mohou existovat <a href="internal-dependencies">vnitřní závislosti</a> a tak při aktualizaci může dojít ke změně položky, která není explicitně modifikována.</p>

<p>Na podobném principu jako aktualizace položek funguje i <a href="stitky#updateAction">aktualizace štítků</a>.</p>

<#include "/i-devdoc-footer.ftl" />
</#escape>
