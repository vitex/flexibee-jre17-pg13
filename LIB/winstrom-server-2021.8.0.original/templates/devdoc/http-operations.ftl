<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Podporované HTTP Operace"/>
<#include "/i-devdoc-header.ftl" />

<h2>Přečtení záznamu</h2>
<p>Data lze přečíst pomocí metody GET. Je zohledňován <a href="format-types">výstupní formát</a>.</p>

<h2>Mazání záznamu</h2>
<p>Mazat lze pouze jednotlivé záznamy a to pomocí jejich detailového URL (tj. obsahují <a href="identifiers">identifikátor</a>). Více záznamů současně lze smazat pouze použitím <a href="actions">akcí</a> při běžné aktualizaci (viz níže).</p>
<p>Pokud záznam neexistuje, je vrácen kód 404. Pokud se záznam podařilo smazat, je vrácen kód 200.</p>

<h2>Vytvoření/aktualizace záznamu</h2>
<p>ABRA Flexi nerozlišuje operace POST a PUT. Význam tedy vždy závisí na cílové adrese (URL) a na obsahu, který je zaslán. Pokud ukládáme záznamy na adresu <a href="list">výpisu evidence</a>, jsou záznamy buď přidány nebo aktualizovány podle toho, zda byl nalezen <a href="identifiers">identifikátor</a>.</p>

<p>Pokud provedu operaci na detail záznamu, nemusí zpráva obsahovat již žádný identifikátor a je vzat identifikátor z&nbsp;URL.</p>

<p>Záznam, který modifikuji pomocí detailového URL, musí vždy existovat.</p>

<p>Pomocí výpisového URL mohu modifikovat současně více záznamů. Pokud mají záznamy uvedený identifikátor přidělovaný ABRA Flexi, musí existovat. Pokud obsahují jako identifikaci např. externí ID, budou záznamy, které nejsou v ABRA Flexi dostupné, vytvořeny.</p>

<p>Poznámka: v případě metody POST se očekávají data ve formátu XML nebo JSON a nikoliv jako formulářová data (multipart/form-data).</p>

<h3>Volba formátu při vytváření záznamu</h3>

<p><a href="format-types">Formát</a>, v&nbsp;jakém jsou očekávána data a v&nbsp;jakém je odpověď, jsou vždy shodné a nelze je kombinovat. Vstupní formát je určen buď podle hlavičky <code>Content-Type</code> nebo podle přípony v&nbsp;URL.</p>

<h3>Identifikátor nového záznamu</h3>
<p>Identifikátor vytvořeného dokladu je předán několika způsoby (více viz <a href="http-operations">podporované HTTP operace</a>):</p>
<ul>
    <li>HTTP Hlavičkou <code>Location: https://demo.flexibee.eu:5434/c/demo/faktura-vydana/105</code></li>
    <li>Součást odpovědi ve formátu ABRA Flexi XML:
<pre class="brush: xml">&lt;winstrom version="1.0"&gt;
  &lt;success&gt;true&lt;/success&gt;
  &lt;result&gt;
    &lt;id&gt;105&lt;/id&gt;
  &lt;/result&gt;
&lt;/winstrom&gt;
</pre>
    </li>
</ul>


<#include "/i-devdoc-footer.ftl" />
</#escape>
