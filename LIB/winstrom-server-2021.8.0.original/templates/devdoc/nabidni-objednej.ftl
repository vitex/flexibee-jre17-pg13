<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Tvorba nabídky/objednávky z poptávky/nabídky"/>
<#include "/i-devdoc-header.ftl" />

<p>Přes REST API lze z poptávky vytvořit nabídku a to takto:</p>

<pre class="brush:xml">&lt;poptavka-prijata&gt;
  &lt;id&gt;code:POP0001/2013&lt;/id&gt;
  &lt;nabidni&gt;
    &lt;id&gt;ext:NABIDKA&lt;/id&gt;            &lt;!-- nepovinný externí identifikátor vytvořené nabídky --&gt;
    &lt;typDokl&gt;code:NAV&lt;/typDokl&gt;     &lt;!-- typ dokladu vytvořené nabídky, pokud není zadaný bere se z nastavení --&gt;
  &lt;/nabidni&gt;
&lt;/poptavka-prijata&gt;</pre>

<p>Obdobně je možno z poptávky i nabídky vytvořit objednávku:</p>

<pre class="brush:xml">&lt;poptavka-prijata&gt;
  &lt;id&gt;code:POP0001/2013&lt;/id&gt;
  &lt;objednej&gt;
    &lt;id&gt;ext:OBJEDNAVKA&lt;/id&gt;         &lt;!-- nepovinný externí identifikátor vytvořené objednávky --&gt;
    &lt;typDokl&gt;code:OBP&lt;/typDokl&gt;     &lt;!-- typ dokladu vytvořené objednávky, pokud není zadaný bere se z nastavení --&gt;
  &lt;/objednej&gt;
&lt;/poptavka-prijata&gt;</pre>

<#include "/i-devdoc-footer.ftl" />
</#escape>