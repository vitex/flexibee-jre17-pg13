<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Výměna SSL certifikátu"/>
<#include "/i-devdoc-header.ftl" />

<p>V aplikačním serveru se po instalaci vygeneruje tzv. self-signed certifikát neboli <a href="https://cs.wikipedia.org/wiki/Certifik%C3%A1t_podepsan%C3%BD_s%C3%A1m_sebou">certifikát podepsaný sám sebou</a>. Na tento certifikát upozorňuje prohlížeč. Je možné, jej vyměnit za vlastní certifikát.<p>
<p>Certifikát musí splňovat několik požadavků:</p>
<ul>
    <li>Musí být ve formátu PEM</li>
    <li>V souboru musí být jak veřejná část (certifikát) tak i soukromá část (klíč).</li>
    <li>Klíč musí být ve formátu PKCS#1 (začíná BEGIN RSA PRIVATE KEY). Pokud máte klíč ve formátu PKCS#8 (začíná BEGIN PRIVATE KEY),
        převeďte si klíč do formátu PKCS#1 příkazem <code>openssl rsa -in klic.pem -out klic2.pem</code>.
    <li>V souboru musí být celý řetězec certifikátů (chain). Tj. i certifikát kořenové autority.</li>
    <li>Po nahrání je potřeba restartovat aplikační server.</li>
</ul>

<p>Nahrání proběhne na adresu <code>https://server:5434/certificate?password=abc</code>. Certifikát musí být buď bez hesla (pak lze vynechat parametr password) a nebo musí mít heslo uvedené v parametru.</p>
<p>V databázi je pak certifikát uložen bez hesla.</p>

<p>Pozor: v cloudu nelze měnit certifikát (i když příkaz uspěje). Časem nasadíme SNI, ale jeho podpora je stále problematická.</p>

<p>Ukázka příkazu pro CURL:</p>
<pre>curl -X PUT -u jmeno:heslo -k -L -T domena.eu.pem https://localhost:5434/certificate</pre>

<ul>
    <li><a href="security">Bezpečnostní nastavení</a></li>
</ul>

<#include "/i-devdoc-footer.ftl" />
</#escape>