<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl" />

<@edit.beginForm />
    <p>${lang('labels', 'actions.confirm.' + actionId, 'actions.confirm.' + actionId)}</p>

    <@edit.okCancelButton yesNo=true />
<@edit.endForm />


<#include "/i-footer-edit.ftl" />
</#escape>