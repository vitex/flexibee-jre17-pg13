<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#macro generateJavaScript it object>

<#if isSlowMobile == false && isMobile == false>

<#import "/l-prijemce-tools.ftl" as prijemce />
<@prijemce.placeCreditCheckJavaScript />

<script type="text/javascript">
    $(function() {
        <#if (object.ic?? && object.ic != '') || (object.dic?? && object.dic != '')>
            var ic = '${object.ic}';
            var dic = '${object.dic}';
            var stat = '<#if object.stat??>${object.stat.kod}<#else>CZ</#if>';

            handleCreditCheck(stat, ic, dic);
        </#if>

    <#if object['mena']?? && object['mena'].kod == settings(object['datVyst']).mena.kod>
                $("a[href='#fragment-18']").parent().hide();
    </#if>
    });
</script>

</#if>

</#macro>


<#macro showUhradaZbyva it object marker title>
    <#if object['zbyvaUhraditMen'] != 0>
    <#noescape>${title}</#noescape>&nbsp;<strong><@tool.placeList marker=marker object=object name="zbyvaUhraditMen" items=it.devDocItemsForHtmlMap /> ${object['mena'].symbol}</strong> <#if object['zbyvaUhraditMen'] &lt; object['sumCelkemMen']> z <@tool.placeList marker=marker object=object name="sumCelkemMen" items=it.devDocItemsForHtmlMap /> ${object['mena'].symbol}</#if>
    <#elseif object['zbyvaUhradit'] != 0>
    <#noescape>${title}</#noescape>&nbsp;<strong><@tool.placeList marker=marker object=object name="zbyvaUhradit" items=it.devDocItemsForHtmlMap /> ${object['mena'].symbol}</strong> <#if object['zbyvaUhradit'] &lt; object['sumCelkem']>z <@tool.placeList marker=marker object=object name="sumCelkem" items=it.devDocItemsForHtmlMap /> ${object['mena'].symbol}</#if>
    </#if>
</#macro>

<#macro showUhrada it object marker>
    <#if object['storno']?? && object['storno']>
        <div class="flexibee-note-warn">Doklad byl stornován.</div>
    </#if>
    <#if object['stavUhrK']??>
        <div class="alert alert-info" role="alert"">
            <@tool.placeList marker=marker object=object name="stavUhrK" items=it.devDocItemsForHtmlMap /> <@tool.placeList marker=marker object=object name="datUhr" items=it.devDocItemsForHtmlMap />
            <#if object['zbyvaUhradit'] &gt; 0>
                 <#if isTablet == false && isMobile == true><br/><#else>&middot;</#if> <@dokladView.showUhradaZbyva it=it object=object marker=marker title="Zbývá&nbsp;uhradit:" />
            </#if>

        <#if !object['stavUhrK']?? || (object['stavUhrK'] != 'stavUhr.uhrazeno' && object['stavUhrK'] != 'stavUhr.uhrazenoRucne')>
        <#if object['datSplat']??>
                    <#if object['datSplat'].time?date &gt; now?date>
                <br/>Splatnost&nbsp;<@tool.placeList marker=marker object=object name="datSplat" items=it.devDocItemsForHtmlMap />
                <#else>
                    <br/><span class="flexibee-red">Po&nbsp;splatnosti&nbsp;<@tool.placeList marker=marker object=object name="datSplat" items=it.devDocItemsForHtmlMap /></span>
                </#if>
            </#if>
        </#if>

        </div>
    <#else>

    <#if !object['stavUhrK']?? || (object['stavUhrK'] != 'stavUhr.uhrazeno' && object['stavUhrK'] != 'stavUhr.uhrazenoRucne')>
        <#if object['datSplat']??>
            <#if object['datSplat'].time?date &gt; now?date>
                <div class="alert alert-info" role="alert">Splatnost&nbsp;<@tool.placeList marker=marker object=object name="datSplat" items=it.devDocItemsForHtmlMap /> <#if isTablet == false && isMobile == true><br/><#else>&middot;</#if> <@dokladView.showUhradaZbyva it=it object=object marker=marker title="K&nbsp;úhradě:" /></div>
            <#else>
                <div class="alert alert-danger" role="alert">Po&nbsp;splatnosti&nbsp;<@tool.placeList marker=marker object=object name="datSplat" items=it.devDocItemsForHtmlMap /> <#if isTablet == false && isMobile == true><br/><#else>&middot;</#if> <@dokladView.showUhradaZbyva it=it object=object marker=marker title="K&nbsp;úhradě:" /></div>
            </#if>
        </#if>
    </#if>

    </#if>
</#macro>

</#escape>