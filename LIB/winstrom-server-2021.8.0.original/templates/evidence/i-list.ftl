<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/evidence/i-list-begin.ftl" />

<#if it.rowCount &gt; 0>
<table class="table table-striped table-hover table-condensed flexibee-tbl-list" summary="${lang('dialogs', it.name, it.name)}">
    <thead>
    <tr>
        <#list it.devDocItemsForHtml as item>
        <#if item.showToUser && item.isVisible>
            <th><@tool.listHeader name=item.propertyName items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection /></th>
        </#if>
        </#list>
    </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
    <#list it.list as object>
    <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
        <#list it.devDocItemsForHtml as item>
        <#if item.showToUser && item.isVisible>
            <#assign objectLink><#if object.id &gt; -1>/c/${it.companyResource.companyId}/${it.evidenceType.path}/${object.id?string('0')}</#if></#assign>
            <td <#if item.type=='numeric'>class="flexibee-r"</#if>><#if objectLink?has_content><a href="${objectLink}"></#if>
            <@tool.placeList marker=marker object=object name=item.propertyName items=it.devDocItemsForHtmlMap />
            <#if objectLink?has_content></a></#if></td>
        </#if>
        </#list>
    </tr>
        </#list>
    </tbody>
</table>
</#if>

<#include "/evidence/i-list-end.ftl" />

</#escape>