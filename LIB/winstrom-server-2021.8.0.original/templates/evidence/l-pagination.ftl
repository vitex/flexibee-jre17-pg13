<#ftl encoding="utf-8" strip_whitespace=true />
<#escape x as x?default("")?html>

<#macro showPagina pagina>
    &nbsp;
    <#if pagina.isCurrent><strong>${pagina.symbol}</strong>
    <#elseif pagina.isActive><a href="${pagina.url}">${pagina.symbol}</a>
    <#else><span class="flexibee-page-disabled">${pagina.symbol}</span>
    </#if>
</#macro>

<#macro paginate pagination>
<div class="flexibee-page">
    <@showPagina pagina=pagination.first/>
    <@showPagina pagina=pagination.previous/>
    <#list pagination.pagination as pagina>
        <@showPagina pagina=pagina/>
    </#list>
    <@showPagina pagina=pagination.next/>
    <@showPagina pagina=pagination.last/>
</div>
</#macro>

</#escape>
