<#ftl encoding="utf-8"  strip_whitespace=true />
<#escape x as x?default("")?html>

<#include "/i-header-list.ftl" />
<#include "/evidence/i-list-begin.ftl" />


<#import "l-doklad-list.ftl" as my/>

<#if it.rowCount &gt; 0>
<table class="table flexibee-list flexibee-tbl-list table-striped table-hover table-condensed" summary="${lang('dialogs', it.name, it.name)}">
    <thead>
    <tr>
        <th><@tool.listHeader name="kod"       items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
        <th><@tool.listHeader name="firma"     items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
        <th><@tool.listHeader name="datVyst"   items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
        <th><@tool.listHeader name="sumCelkem" items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
    </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
    <#list it.list as object>
        <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
            <td colspan="4">
                <@my.dokladInList object=object iteratorIndex=object_index marker=marker items=it.devDocItemsForHtmlMap />
            </td>
        </tr>
    </#list>
    </tbody>
</table>
</#if>

<#include "/evidence/i-list-end.ftl" />


<#include "/footer.ftl" />
</#escape>
