<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "i-page.ftl" />

<#if it.limit?exists && it.rowCount &gt; it.limit && useInfScroll == true>
<script type="text/javascript">
<#if isMobile>
$('div').live('pagecreate', function(event) {
    var _this = $(this);
<#else>
$(document).ready(function() {
    var _this = $(document);
</#if>

  $('.flexibee-tbl-list', _this).infinitescroll({
    path          : '<#noescape><#escape x as x?js_string>${url}</#escape></#noescape>',
    pathSuffix   : '<#noescape><#escape x as x?js_string>${order}${filterQueryParam}</#escape></#noescape>',
    navSelector  : "div.flexibee-page",
    itemSelector : ".flexibee-tbl-list-body",
    loadingText  : "${lang('messages', 'pageLoad.loading', 'Načítám další stránku ...')}",
    loadNextText : "${lang('messages', 'pageLoad.loadNext', 'Načíst další stránku')}",
    donetext     : "${lang('messages', 'pageLoad.end', 'Načteny všechny stránky.')}",
    localMode     : true,
    loadingImg   : "${staticPrefix}/css/images/ajax-loader.png",
    currPage     : ${it.page},
    bind         : $('.flexibee-ramecek-wrapper', _this)[0]
  }, function(parent, children) {
      $('.flexibee-keynav', children).keynav('flexibee-keynav-active', 'flexibee-keynav');
  });
});
</script>
</#if>

</#escape>