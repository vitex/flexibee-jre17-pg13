<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl" />

<@edit.beginForm />
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />



<#include "/parts/i-dalsi-informace.ftl" />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />



<#include "/i-footer-edit.ftl" />
</#escape>