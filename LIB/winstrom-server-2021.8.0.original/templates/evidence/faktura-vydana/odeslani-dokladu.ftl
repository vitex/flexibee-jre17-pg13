<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign myTitle = "Odeslání dokladu" />
<#include "/i-header-edit.ftl"/>

<#if odeslaniDokladuOk?? && odeslaniDokladuOk>

    <div class="alert alert-success">
        <strong>Doklad byl úspěšně odeslán e-mailem.</strong>
    </div>

    <@edit.buttonsBegin/>
        <a class="flexibee-focus btn btn-primary flexibee-clickable"  data-dismiss="modal"  href="${it.goBackURI}"><span>${lang('labels', 'backToDocument', 'Zpět na doklad')}</span></a>
    <@edit.buttonsEnd/>

<#else>

<#if odeslaniDokladuNeuspesne??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            <li>${odeslaniDokladuNeuspesne}</li>
        </ul>
    </div>
</#if>

<@edit.beginForm />
    <div class="flexibee-steps">
    <h2 class="flexibee-step1">Určení příjemců</h2>
    <div>

    <#assign hasSomething = false/>

    <div class="form-horizontal">

    <#if vybranyEmailFirmy??>
        <div class="form-group">
            <div class="col-sm-2">
                <input type="checkbox" name="firmaCheck" class="flexibee-inp" id="flexibee-inp-firmaCheck" value="true" <#if firmaChecked?? && firmaChecked>checked</#if>>
                <label for="flexibee-inp-firmaEmail" class="control-label">Firma:</label>
            </div>
            <div class="col-sm-10">
                <input type="text" name="firmaEmail" class="flexibee-inp form-control" id="flexibee-inp-firmaEmail" <#if vybranyEmailFirmy??>value="${vybranyEmailFirmy}"</#if>>
            </div>
        </div>
        <#assign hasSomething = true/>
    </#if>

    <#if kontakty??>
    <div class="form-group">
        <div class="col-sm-2">
            <input type="checkbox" name="kontaktniOsobaCheck" id="flexibee-inp-kontaktniOsobaCheck" class="flexibee-inp" value="true" <#if kontaktniOsobaChecked?? && kontaktniOsobaChecked>checked</#if>>
            <label for="flexibee-inp-kontaktniOsobaEmail" class="control-label">Kontaktní osoba:</label>
        </div>
        <div class="col-sm-10">
            <select name="kontaktniOsobaEmail" id="flexibee-inp-kontaktniOsobaEmail" class="flexibee-rounded-form-select flexibee-inp form-control">
            <#list kontakty as kontakt>
                <option value="${kontakt.id}" <#if vybranyKontakt?? && vybranyKontakt == kontakt.id>selected</#if>>${kontakt.name}</option>
            </#list>
            </select>
        </div>
    </div>
        <#assign hasSomething = true/>
    </#if>

    <div class="form-group">
        <div class="col-sm-2">
            <input type="checkbox" name="dalsiCheck" id="flexibee-inp-dalsiCheck" class="flexibee-inp" value="true" <#if dalsiChecked?? && dalsiChecked>checked</#if>>
            <label for="flexibee-inp-dalsiEmail" class="control-label"><#if hasSomething>Další:<#else>Příjemce:</#if></label>
        </div>
        <div class="col-sm-10">
            <input type="text" name="dalsiEmail" id="flexibee-inp-dalsiEmail" class="flexibee-inp form-control" <#if vybranyDalsiEmail??>value="${vybranyDalsiEmail}"</#if>>
        </div>
    </div>


    <#if vybranyEmailSobe??>
    <div class="form-group">
        <div class="col-sm-2">
            <input type="checkbox" name="sobeCheck" id="flexibee-inp-sobeCheck" class="flexibee-inp" value="true" <#if sobeChecked?? && sobeChecked>checked</#if>>
            <label for="flexibee-inp-sobeEmail">Sobě:</label>
        </div>
        <div class="col-sm-10">
            <input type="text" name="sobeEmail" id="flexibee-inp-sobeEmail" <#if vybranyEmailSobe??>value="${vybranyEmailSobe}"</#if> class="flexibee-inp form-control">
        </div>
    </div>
    </#if>

    </div>

    <h2 class="flexibee-step2">Zpráva</h2>
    <div class="form-horizontal">
        <div class="form-group">
            <label for="flexibee-inp-predmet" class="col-sm-2 control-label">Předmět:</label>
            <div class="col-sm-10">
                <input type="text" name="predmet" id="flexibee-inp-predmet" class="flexibee-inp form-control" <#if vybranyPredmet??>value="${vybranyPredmet}"</#if>>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-12">
                <textarea rows="10" cols="80" name="obsah" class="flexibee-inp form-control"><#if vybranyObsah??>${vybranyObsah}</#if></textarea>
            </div>
        </div>
    </div>

    </div>

    <h2 class="flexibee-step3">Přílohy</h2>
    <div>
    <p>Ke zprávě budou připojeny tyto přílohy:</p>
    <#if pdfFileName??>
        <div style="margin-left: 2em; float: left;text-align:center">
            <a href="/c/${it.companyId}/${it.evidenceType.path}/${object.id?c}.pdf">
                <i class="icon icon-pdf fa-3x"></i><br/><span>${pdfFileName}</span>
            </a>
        </div>
    </#if>
    <#if isdocxFileName??>
        <div style="margin-left: 2em; float: left;text-align:center">
            <a href="/c/${it.companyId}/${it.evidenceType.path}/${object.id?c}.isdocx">
                <i class="icon icon-isdoc fa-3x"></i><br/><span>${isdocxFileName}</span>
            </a>
        </div>
    </#if>

    <#if dalsiPrilohy??>
        <#list dalsiPrilohy as priloha>
        <div style="margin-left: 2em; float: left;text-align:center">
                <i class="fa fa-file-o fa-3x"></i><br/><span>${priloha}</span>
        </div>
        </#list>
    </#if>
    <br style="clear: both;">
    </div>

    </div> <#-- flexibee-steps -->

    <br>
    <@edit.okCancelButton />
<@edit.endForm />

<script type="text/javascript">
    $(function() {
        function enableDisableInput(name) {
            var checked = $("#flexibee-inp-" + name + "Check").is(":checked");
            var field = $("#flexibee-inp-" + name + "Email");
            if (checked) {
                field.removeAttr("disabled");
            } else {
                field.attr("disabled", "disabled");
            }
        }

        $.each(['firma', 'kontaktniOsoba', 'dalsi', 'sobe'], function(idx, name) {
            $("#flexibee-inp-" + name + "Check").click(function() {
                if ($(this).is(":checked")) {
                    $("#flexibee-inp-" + name + "Email").focus();
                }
                enableDisableInput(name);
            });
            enableDisableInput(name);
        });
    });
</script>

</#if>


<#include "/i-footer-edit.ftl" />
</#escape>
