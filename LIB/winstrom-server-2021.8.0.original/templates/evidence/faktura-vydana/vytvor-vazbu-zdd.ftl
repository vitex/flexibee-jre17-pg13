<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#if chyba?? || vygenerovanaVazbaUrl??>
    <#include "/i-header-edit.ftl"/>

    <#if chyba??>
        <#-- Obsluha chyby -->
        <div class="alert alert-danger">
            <ul class="flexibee-errors">
                ${chyba}
            </ul>
        </div>
    <#else>
        <#-- Vysledek - vygenerovana vazba -->
        <div class="alert alert-success">
            <p><strong>${lang('messages', 'zalohyZddSparovano', 'Doklady byli úspěšně spárovány.')} <a href="${vygenerovanaVazbaUrl}">${fakeKodVazby}</a>.</strong></p>
        </div>
    </#if>

    <@edit.buttonsBegin/>
        <a class="flexibee-focus btn btn-primary flexibee-clickable"  data-dismiss="modal"  href="${it.goBackURI}"><span>${lang('labels', 'backToDocument', 'Zpět na doklad')}</span></a>
    <@edit.buttonsEnd/>

    <#include "/i-footer-edit.ftl" />
</#if>

<#-- Vyber modulu -->
<#if selectModul?? && selectModul>
    <#include "/i-header-edit.ftl"/>
    <@edit.beginForm />
        <table>
        <tr>
            <td><input type="radio" name="modul" id="pokladna"  class="flexibee-inp" value="pokladna" checked /></td>
            <td><label for="pokladna">${lang('buttons', 'parovaniZalohPokladna', 'Pokladna')}</label></td>
        </tr>
        <tr>
            <td><input type="radio" name="modul" id="banka"  class="flexibee-inp" value="banka" /></td>
            <td><label for="banka">${lang('buttons', 'parovaniZalohBanka', 'Banka')}</label></td>
        </tr>
        </table>
        <@edit.okCancelButton showCancel=false />
    <@edit.endForm />
    <#include "/i-footer-edit.ftl" />
</#if>


<#-- seznam dokladu pro vyber -->
<#if uhrady??>

    <#include "/i-header-list.ftl"/>
    <#include "/evidence/i-list-begin.ftl" />
    <table class="flexibee-tbl-list" summary="${lang('dialogs', banPokIt.name, banPokIt.name)}">
    <thead>
        <tr>
            <th><@tool.listHeader name="kod"       it=banPokIt items=banPokIt.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
            <th><@tool.listHeader name="firma"     it=banPokIt items=banPokIt.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
            <th><@tool.listHeader name="datVyst"   it=banPokIt items=banPokIt.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
            <th><@tool.listHeader name="sumCelkem" it=banPokIt items=banPokIt.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
        </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
        <#list uhrady as object>
        <#assign dataUrl = "/c/" + it.companyResource.companyId + "/" + it.evidenceName + "/" + id + "/vytvor-vazbu-zdd/" + object.id?string('0') />
        <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
            <td colspan="4">
                <a href="${dataUrl}">
                    <small><@tool.placeList marker=marker object=object name="kod" it=banPokIt items=banPokIt.devDocItemsForHtmlMap /></small>
                    &nbsp;
                    <@tool.placeList marker=marker object=object name="firma" it=banPokIt items=banPokIt.devDocItemsForHtmlMap />

                    &nbsp;
                    <small>vystaveno <@tool.placeList marker=marker object=object name="datVyst" it=banPokIt items=banPokIt.devDocItemsForHtmlMap /></small>

                    <span style="float:right;text-align:right">

                    <span class="flexibee-big">
                    <#if object['sumCelkemMen'] != 0>
                        <@tool.placeList marker=marker object=object name="sumCelkemMen" it=banPokIt items=banPokIt.devDocItemsForHtmlMap />
                        <@tool.menaSymbol object.mena/>
                    <#else>
                        <@tool.placeList marker=marker object=object name="sumCelkem" it=banPokIt items=banPokIt.devDocItemsForHtmlMap />
                        <@tool.menaSymbol object.mena/>
                    </#if>
                    </span>

                    <br/>

                    <#if isMobile == false || isTablet == true>
                    <#if object['sumZalohyMen']?? && object['sumZalohyMen'] != 0>
                        <small>Záloha <@tool.placeList marker=marker object=object name="sumZalohyMen" it=banPokIt items=banPokIt.devDocItemsForHtmlMap />
                        <@tool.menaSymbol object.mena/></small><br/>
                    <#elseif object['sumZalohy']?? && object['sumZalohy'] != 0>
                        <small>Záloha <@tool.placeList marker=marker object=object name="sumZalohy" it=banPokIt items=banPokIt.devDocItemsForHtmlMap />
                        <@tool.menaSymbol object.mena/></small><br/>
                    </#if>
                    </#if>

                    <small>
                    <#if object['stavUhrK']?exists>
                        <span style="color:green"><@tool.placeList marker=marker object=object name="stavUhrK" it=banPokIt items=banPokIt.devDocItemsForHtmlMap /> <@tool.placeList marker=marker object=object name="datUhr" it=banPokIt items=banPokIt.devDocItemsForHtmlMap /></span>
                        <#if object['zbyvaUhradit'] &gt; 0.01 || object['zbyvaUhradit'] &lt; -0.01>
                            <#if object['mena'] != ''>
                                <@tool.placeList marker=marker object=object name="zbyvaUhraditMen" it=banPokIt items=banPokIt.devDocItemsForHtmlMap /> <@tool.menaSymbol object.mena/>
                            <#else>
                                <@tool.placeList marker=marker object=object name="zbyvaUhradit" it=banPokIt items=banPokIt.devDocItemsForHtmlMap /> <@tool.menaSymbol object.mena/>
                            </#if>
                        </#if>
                    </#if>

                    <#if !object['stavUhrK']?exists || (object['stavUhrK'] != 'stavUhr.uhrazeno' && object['stavUhrK'] != 'stavUhr.uhrazenoRucne')>
                        <#if object['stavUhrK']?exists><br/></#if>
                        <#if object['datSplat']?exists>
                            <#if object['datSplat'].time?date &gt; now?date>
                                Splatnost <@tool.placeList marker=marker object=object name="datSplat" it=banPokIt items=banPokIt.devDocItemsForHtmlMap />
                            <#else>
                                <span style="color:red">po splatnosti <@tool.placeList marker=marker object=object name="datSplat" it=banPokIt items=banPokIt.devDocItemsForHtmlMap /></span>
                            </#if>
                        </#if>
                    </#if>
                    </small>

                    </span>

                    <br/>

                    <small style="color:gray">
                        <#assign itemCnt = 0 />
                        <#assign maxCount = 5/>
                        <#if isMobile && !isTablet>
                            <#assign maxCount = 3/>
                        </#if>
                        <#if object['popis']?exists && object['popis'] != ''>
                            <@tool.placeList marker=marker object=object name="popis" it=banPokIt items=banPokIt.devDocItemsForHtmlMap />
                            <#assign itemCnt = itemCnt + 1/>
                        </#if>
                        <#list object['polozkyDokladu'] as item>
                            <#if !item.autogen && !item.autoZaokr && itemCnt &lt; maxCount && item.typPolozkyK != 'typPolozky.ucetni'>
                                <#if itemCnt &gt; 0>,</#if>
                                <#assign itemCnt = itemCnt + 1/>
                                ${item.nazev}
                            </#if>
                        </#list>
                    </small>
                    -->
                </a>
            </td>
        </tr>
        </#list>
    </tbody>
    </table>
    <#include "/evidence/i-list-end.ftl" />
    <#include "/footer.ftl" />
</#if>

<script type="text/javascript">
    $(function() {
        $("form").submit(function() {
            <#--
              - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
              - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
              -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>
</#escape>