<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign myTitle="Hotovostní úhrada"/>
<#include "/i-header-edit.ftl"/>

    <#if hotovnostniUhradaOk?? && hotovnostniUhradaOk>
        <div class="alert alert-success">
            <p><strong>Byl vytvořen pokladní doklad <a href="${vygenerovanyPokladniDokladUrl}">${vygenerovanyPokladniDokladKod}</a>.</strong></p>
        </div>
    </#if>

    <#if nelzeUhraditFakturu??>
        <div class="alert alert-danger">
            <ul class="flexibee-errors">${nelzeUhraditFakturu}</ul>
        </div>
    </#if>

    <#if !showDialog>
        <@edit.buttonsBegin/>
            <a class="flexibee-focus btn btn-primary flexibee-clickable"  data-dismiss="modal"  href="${it.goBackURI}"><span>${lang('labels', 'backToDocument', 'Zpět na doklad')}</span></a>
        <@edit.buttonsEnd/>
    </#if>

<#if showDialog>

<#assign cnt = 1/>
<@edit.beginForm />
<div class="flexibee-steps">
    <#if typyPokladnichPohybu?size &gt; 1>
        <h2 class="flexibee-step${cnt}<#assign cnt = cnt + 1/>">Typ dokladu:</h2>
        <div>
                <table class="flexibee-tbl-1">
                <col width="10%"/><col width="90%"/>
                <#list typyPokladnichPohybu as typPokladnihoPohybu>
                            <tr>
                                <td>
                                    <input id="typDokladu${typPokladnihoPohybu.id}" type="radio" name="typPokladnihoPohybu" value="${typPokladnihoPohybu.id}"
                                           <#if (vybranyTypPokladnihoPohybu?? && vybranyTypPokladnihoPohybu == typPokladnihoPohybu.id) || (!vybranyTypPokladnihoPohybu?? && typPokladnihoPohybu_index == 0)>checked</#if>>
                                </td>
                                <td><label for="typDokladu${typPokladnihoPohybu.id}">${typPokladnihoPohybu.nazev}</label></td>
                            </tr>
                </#list>
                </table>
            </ul>
        </div>
    <#else>
        <input type="hidden" name="typPokladnihoPohybu" value="${typyPokladnichPohybu?first.id}">
    </#if>

    <h2 class="flexibee-step${cnt}<#assign cnt = cnt + 1/>">Pokladna:</h2>
    <div>
        <table class="flexibee-tbl-1">
            <col width="10%"/><col width="90%"/>
            <#list pokladny as pokladna>
                        <tr>
                            <td>
                                <input id="pokladnaId" type="radio" name="pokladna" id="pokladna" value="${pokladna.id}"
                                       <#if (vybranaPokladna?? && vybranaPokladna == pokladna.id) || (!vybranaPokladna?? && pokladna_index == 0)>checked</#if>>
                            </td>
                            <td><label for="pokladnaId">${pokladna.nazev}</label></td>
                        </tr>
            </#list>
            </table>
        <@tool.showRelating "POKLADNA" "pokladna" />
    </div>

    <#assign primarniMena><@tool.menaSymbol settings(object.datVyst).mena/></#assign>

    <div class="removeStepsBorder">
    <h2 class="flexibee-step${cnt}<#assign cnt = cnt + 1/>">Úhrada</h2>
    <div>
                <table class="flexibee-tbl-1">
                    <col width="20%"/><col width="40%"/><col width="15%"/><col width="10%"/><col width="15%"/>
                    <tr>
                        <td><label>Zbývá k úhradě:</label></td>
                        <td colspan="4"><span>
                            <#if object.zbyvaUhraditMen != 0>
                                <strong><@tool.place object=object name='zbyvaUhraditMen' items=items marker=marker /><@tool.menaSymbol object.mena/></strong>
                                <br/>
                            <#else>
                                <strong><@tool.place object=object name='zbyvaUhradit' items=items marker=marker /> ${primarniMena}</strong>
                            </#if>
                        </td>
                    </tr>
                <#if object.zbyvaUhraditMen != 0>
                    <#list typyKurzu as typKurzu>
                            <tr>
                                <td style="text-align:right;">
                                    <input id="typKurzu${typKurzu.getVal()}" type="radio" name="typKurzu" value="${typKurzu.getVal()}"
                                           <#if (vybranyTypKurzu?? && vybranyTypKurzu == typKurzu.getVal()) || (!vybranyTypKurzu?? && typKurzu_index == 0)>checked</#if>>
                                </td>
                                <td><label for="typKurzu${typKurzu.getVal()}">${lang('labels', '${typKurzu.key}', '${typKurzu.label}')}</label> </td>
                                <#if typKurzu.getVal() == 'FAKTURY'>
                                    <td colspan="3">
                                        <i style="padding-left:2em">dle kurzu ${object.kurzMnozstvi} <@tool.menaSymbol object.mena/> = ${object.kurz} ${primarniMena}</i>
                                    </td>
                                <#elseif typKurzu.getVal() == 'UHRADY'>
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                <#else>
                                    <td>
                                         <div class="input-group">
                                            <input type="text" name="kurzKoef" id="flexibee-inp-kurzKoef" <#if kurzKoef??>value="${kurzKoef}"</#if> class="flexibee-inp flexibee-level-3  form-control">
                                        </div>
                                    </td>
                                    <td>
                                         <div class="input-group">
                                            ${primarniMena}&nbsp;=&nbsp;
                                        </div>
                                    </td>
                                    <td>
                                         <div class="input-group">
                                            <input type="text" name="kurzMnozstvi" id="flexibee-inp-castka" <#if kurzMnozstvi??>value="${kurzMnozstvi}"</#if> class="flexibee-inp  flexibee-level-1  form-control">
                                        </div>
                                    </td>
                                    <td>
                                         <div class="input-group">
                                            ${object.mena.symbol}
                                        </div>
                                    </td>
                                </#if>
                            </tr>
                    </#list>

                    </#if>
                    <tr>
                        <td><label for="flexibee-inp-castka">Částka k úhradě:</label></td>
                        <td colspan="4">
                            <div class="input-group">
                                <input type="text" name="castka" id="flexibee-inp-castka" <#if vybranaCastka??>value="${vybranaCastka}"</#if> class="flexibee-inp form-control">
                                <span class="input-group-addon"><@tool.menaSymbol object.mena/></div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="flexibee-inp-datumUhrady">Datum úhrady:</label></td>
                        <td colspan="4">
                            <input type="text" format="dd.mm.yyyy" name="datumUhrady" id="flexibee-inp-datumUhrady"
                                   value="<#if vybraneDatumUhrady??>${vybraneDatumUhrady}</#if>" class="flexibee-edit-date flexibee-inp form-control">
                        </td>
                    </tr>
                </table>
    </div>

    </div> <#-- remove-steps-border -->
</div> <#-- flexibee-steps -->

<@edit.okCancelButton />

<@edit.endForm />
</#if>

<#include "/i-footer-edit.ftl" />
</#escape>