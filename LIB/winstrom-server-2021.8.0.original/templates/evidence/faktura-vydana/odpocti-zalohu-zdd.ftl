<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

<#--    V OdpocetArrayConstants.java je duplikát, protože není žádný hezký způsob,
        jak tento interface využít ve FreeMarker -->
<#assign ODPOCET_OSV = 0>
<#assign ODPOCET_ZKL_SNIZ = 1>
<#assign ODPOCET_DPH_SNIZ = 2>
<#assign ODPOCET_CEKL_SNIZ = 3>
<#assign ODPOCET_ZKL_ZAKL = 4>
<#assign ODPOCET_DPH_ZAKL = 5>
<#assign ODPOCET_CEKL_ZAKL = 6>
<#assign ODPOCET_ZKL_SNIZ2 = 7>
<#assign ODPOCET_DPH_SNIZ2 = 8>
<#assign ODPOCET_CEKL_SNIZ2 = 9>
<#assign ODPOCET_SAZBA_ZAKL = 10>
<#assign ODPOCET_SAZBA_SNIZ = 11>
<#assign ODPOCET_SAZBA_SNIZ2 = 12>

<#-- Obsluha chyby -->
<#if chyba??>

    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            ${chyba}
        </ul>
    </div>

    <@edit.buttonsBegin/>
    <a class="flexibee-focus btn btn-primary flexibee-clickable"  data-dismiss="modal"  href="${it.goBackURI}"><span>${lang('labels', 'backToDocument', 'Zpět na doklad')}</span></a>
    <@edit.buttonsEnd/>

</#if>

<#-- seznam dokladu pro vyber -->
<#if doklady?? && !chyba??>

    <#include "/evidence/i-list-begin.ftl" />
    <table class="table flexibee-tbl-list table-striped table-hover table-condensed" summary="${lang('dialogs', favIt.name, favIt.name)}">
    <thead>
        <tr>
            <th><@tool.listHeader name="kod"       items=favIt.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
            <th><@tool.listHeader name="firma"     items=favIt.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
            <th><@tool.listHeader name="datVyst"   items=favIt.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
            <th><@tool.listHeader name="sumCelkem" items=favIt.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
        </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
        <#list doklady as object>
        <#assign dataUrl = "/c/" + it.companyResource.companyId + "/" + it.evidenceName + "/" + id + "/odpocti-zalohu-zdd/" + object.id?string('0') />
        <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
            <td colspan="4">
                <a href="${dataUrl}">
                    <small><@tool.placeList marker=marker object=object name="kod" items=favIt.devDocItemsForHtmlMap /></small>
                    &nbsp;
                    <@tool.placeList marker=marker object=object name="firma" items=favIt.devDocItemsForHtmlMap />

                    &nbsp;
                    <small>vystaveno <@tool.placeList marker=marker object=object name="datVyst" items=favIt.devDocItemsForHtmlMap /></small>

                    <span style="float:right;text-align:right">
                    <span class="flexibee-big">
                    <#if object['sumCelkemMen'] != 0>
                        <@tool.placeList marker=marker object=object name="sumCelkemMen" items=favIt.devDocItemsForHtmlMap />
                        <@tool.menaSymbol object.mena/>
                    <#else>
                        <@tool.placeList marker=marker object=object name="sumCelkem" items=favIt.devDocItemsForHtmlMap />
                        <@tool.menaSymbol object.mena/>
                    </#if>
                    </span><br/>

                    <#if isMobile == false || isTablet == true>
                    <#if object['sumZalohyMen']?? && object['sumZalohyMen'] != 0>
                        <small>Záloha <@tool.placeList marker=marker object=object name="sumZalohyMen" items=favIt.devDocItemsForHtmlMap />
                        <@tool.menaSymbol object.mena/></small><br/>
                    <#elseif object['sumZalohy']?? && object['sumZalohy'] != 0>
                        <small>Záloha <@tool.placeList marker=marker object=object name="sumZalohy" items=favIt.devDocItemsForHtmlMap />
                        <@tool.menaSymbol object.mena/></small><br/>
                    </#if>
                    </#if>


                    <small>
                    <#if object['stavUhrK']?exists>
                        <span style="color:green"><@tool.placeList marker=marker object=object name="stavUhrK" items=favIt.devDocItemsForHtmlMap /> <@tool.placeList marker=marker object=object name="datUhr" items=favIt.devDocItemsForHtmlMap /></span>
                        <#if object['zbyvaUhradit'] &gt; 0.01 || object['zbyvaUhradit'] &lt; -0.01>
                            <#if object['mena'] != ''>
                                <@tool.placeList marker=marker object=object name="zbyvaUhraditMen" items=favIt.devDocItemsForHtmlMap /> <@tool.menaSymbol object.mena/>
                            <#else>
                                <@tool.placeList marker=marker object=object name="zbyvaUhradit" items=favIt.devDocItemsForHtmlMap /> <@tool.menaSymbol object.mena/>
                            </#if>
                        </#if>
                    </#if>

                    <#if !object['stavUhrK']?exists || (object['stavUhrK'] != 'stavUhr.uhrazeno' && object['stavUhrK'] != 'stavUhr.uhrazenoRucne')>
                        <#if object['stavUhrK']?exists><br/></#if>
                        <#if object['datSplat']?exists>
                            <#if object['datSplat'].time?date &gt; now?date>
                                Splatnost <@tool.placeList marker=marker object=object name="datSplat" items=favIt.devDocItemsForHtmlMap />
                            <#else>
                                <span style="color:red">po splatnosti <@tool.placeList marker=marker object=object name="datSplat" items=favIt.devDocItemsForHtmlMap /></span>
                            </#if>
                        </#if>
                    </#if></small></span>



                    <br/>
                    <small style="color:gray"><#assign itemCnt = 0 />
                        <#assign maxCount = 5/>
                        <#if isMobile == true && isTablet == false>
                            <#assign maxCount = 3/>
                        </#if>
                        <#if object['popis']?exists && object['popis'] != ''>
                            <@tool.placeList marker=marker object=object name="popis" items=favIt.devDocItemsForHtmlMap />
                            <#assign itemCnt = itemCnt + 1/>
                        </#if>
                        <#list object['polozkyDokladu'] as item>
                                <#if item.autogen == false && item.autoZaokr == false && itemCnt &lt; maxCount && item.typPolozkyK != 'typPolozky.ucetni'>
                                    <#if itemCnt &gt; 0>,</#if>
                                    <#assign itemCnt = itemCnt + 1/>
                                    ${item.nazev}
                                </#if>
                        </#list>
                    </small>
                </a>
            </td>
        </tr>
        </#list>
    </tbody>
    </table>

</#if>

<#if !chyba?? && castka??>
    <@edit.beginForm />
        <table>
        <tr>
            <td><label>${lang('labels', 'odpocetZalohy', 'Zvolte částku pro odpočet od 0 do')} ${castka}:</label></td>
            <td><input type="number" name="castka" class="flexibee-inp form-control" value="${castka?c}"></td>
        </tr>
        </table>
        <@edit.okCancelButton showCancel=false />
    <@edit.endForm />
</#if>

<#if !chyba?? && castky??>
    <@edit.beginForm />
        <table>
        <th colspan="4"><label>${lang('labels', 'odpocetZalohy', 'Zvolte částku pro odpočet od 0 do')}</label></th>
        <tr class="flexibee-sep0">
            <td></td>
            <#if isMobile></tr><tr></#if>
            <td>Základ</td>
            <#if isMobile></tr><tr></#if>
            <td>DPH</td>
            <#if isMobile></tr><tr></#if>
            <td>Celkem</td>
        </tr>

        <tr class="flexibee-sep">
            <td>0 %</td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="osv" class="flexibee-inp form-control" value="${castky[ODPOCET_OSV]?c}"></td>
            <#if isMobile></tr><tr></#if>
            <td></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="osv" class="flexibee-inp form-control" value="${castky[ODPOCET_OSV]?c}"></td>
        </tr>

        <tr class="flexibee-sep">
            <td>${castky[ODPOCET_SAZBA_SNIZ2]} %</td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="zklSniz2" class="flexibee-inp form-control" value="${castky[ODPOCET_ZKL_SNIZ2]?c}"></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="dphSniz2" class="flexibee-inp form-control" value="${castky[ODPOCET_DPH_SNIZ2]?c}"></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="celkSniz2" class="flexibee-inp form-control" value="${castky[ODPOCET_CEKL_SNIZ2]?c}"></td>
        </tr>

        <tr class="flexibee-sep">
            <td>${castky[ODPOCET_SAZBA_SNIZ]} %</td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="zklSniz" class="flexibee-inp form-control" value="${castky[ODPOCET_ZKL_SNIZ]?c}"></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="dphSniz" class="flexibee-inp form-control" value="${castky[ODPOCET_DPH_SNIZ]?c}"></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="celkSniz" class="flexibee-inp form-control" value="${castky[ODPOCET_CEKL_SNIZ]?c}"></td>
        </tr>

        <tr class="flexibee-sep">
            <td>${castky[ODPOCET_SAZBA_ZAKL]} %</td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="zklZakl" class="flexibee-inp form-control" value="${castky[ODPOCET_ZKL_ZAKL]?c}"></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="dphZakl" class="flexibee-inp form-control" value="${castky[ODPOCET_DPH_ZAKL]?c}"></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="number" name="celkZakl" class="flexibee-inp form-control" value="${castky[ODPOCET_CEKL_ZAKL]?c}"></td>
        </tr>
        </table>

        <@edit.okCancelButton showCancel=false />
    <@edit.endForm />
</#if>

<script type="text/javascript">
    $(function() {
        $("form").submit(function() {
            <#--
              - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
              - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
              -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>