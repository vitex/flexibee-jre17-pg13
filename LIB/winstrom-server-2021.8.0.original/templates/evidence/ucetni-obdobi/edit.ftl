<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('chybaPreceneni')}
${marker.mark('zmenaZaver')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'rokProRadu', 'platiOdData', 'platiDoData'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['kod', 'rokProRadu'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['kod'])>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['rokProRadu'])>
            <td><@edit.label name='rokProRadu' items=items /></td><td><@edit.place object=object name='rokProRadu' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['platiOdData'])>
        <tr>
            <td><@edit.label name='platiOdData' items=items /></td><td><@edit.place object=object name='platiOdData' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['platiDoData'])>
        <tr>
            <td><@edit.label name='platiDoData' items=items /></td><td><@edit.place object=object name='platiDoData' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
    <h4>${lang('labels', 'textyPN')}</h4>
    <div id="edit-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
        <table class="flexibee-tbl-1">
            <#if canEditAtLeastOne(items, object, ['popis'])>
            <tr>
                <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['poznam'])>
            <tr>
                <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
