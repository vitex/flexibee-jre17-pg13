<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('typOrganizace')}


<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['nazev', 'kod'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['autoProdl', 'typDoklFak', 'autoZakazka', 'den', 'autoGen', 'mesic', 'autoMail', 'zpusFaktK', 'varSymFakt', 'dnyFakt', 'preplatky', 'posUpom', 'dnyUpom', 'genPenale', 'procPenale', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['autoProdl', 'typDoklFak', 'autoZakazka', 'den', 'autoGen', 'mesic', 'autoMail', 'zpusFaktK', 'varSymFakt', 'dnyFakt', 'preplatky', 'posUpom', 'dnyUpom', 'genPenale', 'procPenale'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>Základní</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['autoProdl', 'typDoklFak', 'autoZakazka', 'den', 'autoGen', 'mesic', 'autoMail', 'zpusFaktK', 'varSymFakt', 'dnyFakt', 'preplatky', 'posUpom', 'dnyUpom', 'genPenale', 'procPenale'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['autoProdl', 'typDoklFak', 'autoZakazka', 'den', 'autoGen', 'mesic', 'autoMail', 'zpusFaktK', 'varSymFakt', 'dnyFakt', 'preplatky', 'posUpom', 'dnyUpom', 'genPenale', 'procPenale'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['autoProdl', 'typDoklFak'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['autoProdl'])>
                        <td><@edit.label name='autoProdl' items=items /></td><td><@edit.place object=object name='autoProdl' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['typDoklFak'])>
                        <td><@edit.label name='typDoklFak' items=items /></td><td><@edit.place object=object name='typDoklFak' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['autoZakazka', 'den'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['autoZakazka'])>
                        <td><@edit.label name='autoZakazka' items=items /></td><td><@edit.place object=object name='autoZakazka' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['den'])>
                        <td><@edit.label name='den' items=items /></td><td><@edit.place object=object name='den' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['autoGen', 'mesic'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['autoGen'])>
                        <td><@edit.label name='autoGen' items=items /></td><td><@edit.place object=object name='autoGen' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mesic'])>
                        <td><@edit.label name='mesic' items=items /></td><td><@edit.place object=object name='mesic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['autoMail', 'zpusFaktK'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['autoMail'])>
                        <td><@edit.label name='autoMail' items=items /></td><td><@edit.place object=object name='autoMail' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['zpusFaktK'])>
                        <td><@edit.label name='zpusFaktK' items=items /></td><td><@edit.place object=object name='zpusFaktK' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['varSymFakt', 'dnyFakt'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['varSymFakt'])>
                        <td><@edit.label name='varSymFakt' items=items /></td><td><@edit.place object=object name='varSymFakt' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dnyFakt'])>
                        <td><@edit.label name='dnyFakt' items=items /></td><td><@edit.place object=object name='dnyFakt' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['preplatky'])>
                    <tr>
                        <td><@edit.label name='preplatky' items=items /></td><td><@edit.place object=object name='preplatky' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['posUpom', 'dnyUpom'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['posUpom'])>
                        <td><@edit.label name='posUpom' items=items /></td><td><@edit.place object=object name='posUpom' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dnyUpom'])>
                        <td><@edit.label name='dnyUpom' items=items /></td><td><@edit.place object=object name='dnyUpom' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['genPenale', 'procPenale'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['genPenale'])>
                        <td><@edit.label name='genPenale' items=items /></td><td><@edit.place object=object name='genPenale' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['procPenale'])>
                        <td><@edit.label name='procPenale' items=items /></td><td><@edit.place object=object name='procPenale' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
