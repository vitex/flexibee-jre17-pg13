<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-list.ftl" />
<#include "/evidence/i-list-begin.ftl" />

<#if it.rowCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed" summary="${lang('dialogs', it.name, it.name)}">
    <thead>
    <tr>
        <#list it.devDocItemsForHtml as item>
        <#if item.showToUser && item.isVisible>
            <th><@tool.listHeader name=item.propertyName items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection /></th>
        </#if>
        </#list>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
    <#list it.list as object>
    <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
        <#list it.devDocItemsForHtml as item>
        <#if item.showToUser && item.isVisible>
            <td <#if item.type=='numeric'>class="flexibee-r"</#if>><a href="/c/${it.companyResource.companyId}/${it.evidenceType.path}/${object.id?string('0')}">
            <@tool.placeList marker=marker object=object name=item.propertyName items=it.devDocItemsForHtmlMap />
            </a></td>
        </#if>
        </#list>
        <td><a href="/c/${it.companyResource.companyId}/${it.evidenceType.path}/${object.id?c}/content">Stáhnout</a></td>
    </tr>
        </#list>
    </tbody>
</table>
</#if>

<#include "/evidence/i-list-end.ftl" />


<#include "/i-footer-view.ftl" />
</#escape>