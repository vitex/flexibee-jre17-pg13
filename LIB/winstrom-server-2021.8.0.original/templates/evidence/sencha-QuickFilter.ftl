<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.QuickFilter', {
    extend: 'Ext.toolbar.Toolbar',
    alias: 'widget.fbQuickFilter${it.senchaName?cap_first}',

    requires: [
    ],

    <#assign showFilters = false/>
    <#if it.evidenceResource.filterGroups?? && it.evidenceResource.filterGroups?size &gt; 0>
        <#list it.evidenceResource.filterGroups as group>
            <#if group.allItems?size &gt; 1><#assign showFilters = true/></#if>
        </#list>
    </#if>

    <#if showFilters>
        items: [
            {
                xtype: 'component',
                html: '${lang('labels', 'filter', '')}',
                margin: '0 10 0 10'
            },
            <#list it.evidenceResource.filterGroups as group><#if group.allItems?size &gt; 1>
            {
                xtype: 'combobox',
                allowBlank: false,
                editable: false,
                forceSelection: true,
                multiSelect: false,
                queryMode: 'local',
                store: Ext.create('Ext.data.Store', {
                    fields: ['key', 'value'],
                    data : [
                        <#list group.allItems as filter>
                        {
                            key: '${filter.key}',
                            value: '${filter.title}'
                        },
                        <#-- <option value="<#if filter.key != group.neomezenoItem.key>${filter.key}</#if>" <#if group.selectedItem?? && group.selectedItem.key == filter.key>selected="selected"</#if> <#if filter.key?starts_with("separator")>disabled="disabled"</#if>>
                            <#if !filter.key?starts_with("separator")>${filter.title}</#if>
                        </option> -->
                        </#list>
                    ]
                }),
                displayField: 'value',
                valueField: 'key',
                <#if group.selectedItem??>
                value: '${group.selectedItem.key}',
                <#else>
                value: '${group.allItems[0].key}',
                </#if>

                quickFilterGroup: '${group.name}',
                quickFilterNeomezenoItem: '${group.neomezenoItem.key}',
            },
            </#if></#list>
            {
                xtype: 'button',
                text: '${lang('actions', 'cancel', '')}',
                itemId: 'cancelQuickFilter',
                margin: '0 0 0 5'
            }
        ],
    </#if> <#-- quick filter -->

    initComponent: function() {
        this.callParent(arguments);

        <#if !showFilters>
        /* pokud nemáme žádné filtry, panel úplně skryjeme */
            this.hide();
        </#if>
    }
});

</#escape>
