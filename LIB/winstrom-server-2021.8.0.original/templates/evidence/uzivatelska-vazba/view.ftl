<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<@tool.listItems items=items object=object marker=marker />


<ul class="flexibee-rounded">
    <#if it.evidenceResource.hasParent>
    <#assign parentUrl = it.evidenceResource.parentUrl/>
    <li class="flexibee-keynav"><a class="flexibee-clickable" href="${parentUrl}"><span>${lang('labels', 'backToDocument', 'Zpět na doklad')}</span></a></li>
    <li class="flexibee-keynav"><a class="flexibee-clickable" href="${parentUrl}/uzivatelske-vazby"><span>${lang('labels', 'backToUserAttachments', 'Zpět na vazby')}</span></a></li>
    </#if>
    <#assign otherType = it.evidenceResource.getEvidenceTypeFor(object.beanKey)/>
    <#assign otherUrl = formListUrl(it.companyResource, otherType, object.vazId?c)/>
    <li class="flexibee-keynav"><a class="flexibee-clickable" href="${otherUrl}"><span>${lang('labels', 'openRecord', 'Otevřít záznam')}</span></a></li>
</ul>

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />


<#include "/footer.ftl" />
</#escape>