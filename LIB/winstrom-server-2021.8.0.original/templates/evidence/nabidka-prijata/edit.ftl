<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('sumZklCelkem')}
${marker.mark('sumDphCelkem')}
${marker.mark('sumCelkem')}

<#-- naprosto ořezaná verze -->
${marker.mark('zamekK')}
${marker.mark('datObj')}
${marker.mark('duzpPuv')}
${marker.mark('duzpUcto')}
${marker.mark('datUhr')}
${marker.mark('datTermin')}
${marker.mark('datReal')}
${marker.mark('sumOsvMen')}
${marker.mark('sumZklSnizMen')}
${marker.mark('sumZklZaklMen')}
${marker.mark('sumZklCelkemMen')}
${marker.mark('sumDphZaklMen')}
${marker.mark('sumDphSnizMen')}
${marker.mark('sumDphCelkemMen')}
${marker.mark('sumCelkSnizMen')}
${marker.mark('sumCelkZaklMen')}
${marker.mark('sumCelkemMen')}
${marker.mark('kurz')}
${marker.mark('kurzMnozstvi')}
${marker.mark('stavUzivK')}
${marker.mark('eanKod')}
${marker.mark('faEanKod')}
${marker.mark('bezPolozek')}
${marker.mark('ucetni')}
${marker.mark('szbDphSniz')}
${marker.mark('szbDphZakl')}
${marker.mark('zuctovano')}
${marker.mark('datUcto')}
${marker.mark('storno')}
${marker.mark('zaokrJakSumK')}
${marker.mark('zaokrNaSumK')}
${marker.mark('zaokrJakDphK')}
${marker.mark('zaokrNaDphK')}
${marker.mark('stitky')}
${marker.mark('mena')}
${marker.mark('mistUrc')}
${marker.mark('typUcOp')}
${marker.mark('primUcet')}
${marker.mark('protiUcet')}
${marker.mark('dphZaklUcet')}
${marker.mark('dphSnizUcet')}
${marker.mark('statDph')}
${marker.mark('clenDph')}
${marker.mark('stredisko')}
${marker.mark('cinnost')}
${marker.mark('zakazka')}
${marker.mark('uzivatel')}
${marker.mark('rada')}
${marker.mark('polozkaRady')}
${marker.mark('sazbaDphOsv')}
${marker.mark('sazbaDphSniz')}
${marker.mark('sazbaDphZakl')}
${marker.mark('podpisPrik')}
${marker.mark('prikazSum')}
${marker.mark('prikazSumMen')}
${marker.mark('danEvidK')}
${marker.mark('juhSum')}
${marker.mark('juhSumMen')}
${marker.mark('juhDat')}
${marker.mark('juhDatMen')}
${marker.mark('zbyvaUhradit')}
${marker.mark('zbyvaUhraditMen')}
${marker.mark('stavUhrK')}
${marker.mark('sumZalohy')}
${marker.mark('sumZalohyMen')}
${marker.mark('stavOdpocetK')}
${marker.mark('generovatSkl')}
${marker.mark('hromFakt')}
${marker.mark('zdrojProSkl')}
${marker.mark('prodejka')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div id="creditcheck-container">
    </div>


    <#if isMobile>
    <div id="flexibee-document-header">

    <#else>
        <#-- TODO tohle by patřilo spíš do nějakého stylopisu, a vůbec nejlíp by se to rozvržení mělo řešit jinak -->
        <style type="text/css">
            table#flexibee-document-header td { vertical-align: top; }
        </style>

        <table id="flexibee-document-header">
        <col width="50%">
        <col width="50%">
        <tr><td>
    </#if>

    <#if canEditAtLeastOne(items, object, ['typDokl', 'banka', 'pokladna', 'sklad', 'typPohybuK', 'kod', 'cisDosle', 'varSym', 'popis', 'datVyst', 'datSplat', 'stavUhrK', 'cisSml', 'formaUhradyCis', 'formaUhrK', 'cisObj', 'datObj', 'cisDodak', 'formaDopravy', 'doprava', 'typDoklBan', 'bspBan', 'bankovniUcet', 'varSym', 'konSym', 'uvodTxt', 'zavTxt', 'poznam', 'datUp1', 'datUp2', 'datSmir'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['typDokl', 'banka', 'pokladna', 'sklad', 'typPohybuK', 'kod', 'cisDosle', 'varSym', 'popis', 'datVyst', 'datSplat', 'stavUhrK'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'hlavniPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['cisSml', 'formaUhradyCis', 'formaUhrK', 'cisObj', 'datObj', 'cisDodak', 'formaDopravy', 'doprava'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'dokladEBDoplnekPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['typDoklBan', 'bspBan', 'bankovniUcet', 'varSym', 'konSym'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'dokladEBPlatbaNaUcet')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['uvodTxt', 'zavTxt', 'poznam'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['datUp1', 'datUp2', 'datSmir'])>
            <li><a href="#edit-fragment-5" data-toggle="tab"><span>${lang('labels', 'dokladEBUpominkyPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['typDokl', 'banka', 'pokladna', 'sklad', 'typPohybuK', 'kod', 'cisDosle', 'varSym', 'popis', 'datVyst', 'datSplat', 'stavUhrK'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['typDokl', 'banka', 'pokladna', 'sklad', 'typPohybuK', 'kod', 'cisDosle', 'varSym', 'popis', 'datVyst', 'datSplat', 'stavUhrK'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['typDokl'])>
                    <tr>
                        <td><@edit.label name='typDokl' items=items /></td><td><@edit.place object=object name='typDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['banka'])>
                    <tr>
                        <td><@edit.label name='banka' items=items /></td><td><@edit.place object=object name='banka' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['pokladna'])>
                    <tr>
                        <td><@edit.label name='pokladna' items=items /></td><td><@edit.place object=object name='pokladna' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['sklad'])>
                    <tr>
                        <td><@edit.label name='sklad' items=items /></td><td><@edit.place object=object name='sklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typPohybuK'])>
                    <tr>
                        <td><@edit.label name='typPohybuK' items=items /></td><td><@edit.place object=object name='typPohybuK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kod'])>
                    <tr>
                        <td><@edit.label name='kod' items=items level='3' /></td><td><@edit.place object=object name='kod' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cisDosle'])>
                    <tr>
                        <td><@edit.label name='cisDosle' items=items /></td><td><@edit.place object=object name='cisDosle' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['varSym'])>
                    <tr>
                        <td><@edit.label name='varSym' items=items /></td><td><@edit.place object=object name='varSym' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /></td><td><@edit.place object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['datVyst'])>
                    <tr>
                        <td><@edit.label name='datVyst' items=items /></td><td><@edit.place object=object name='datVyst' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['datSplat'])>
                    <tr>
                        <td><@edit.label name='datSplat' items=items /></td><td><@edit.place object=object name='datSplat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stavUhrK'])>
                    <tr>
                        <td><@edit.label name='stavUhrK' items=items /></td><td><@edit.place object=object name='stavUhrK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['cisSml', 'formaUhradyCis', 'formaUhrK', 'cisObj', 'datObj', 'cisDodak', 'formaDopravy', 'doprava'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['cisSml', 'formaUhradyCis', 'formaUhrK', 'cisObj', 'datObj', 'cisDodak', 'formaDopravy', 'doprava'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['cisSml'])>
                    <tr>
                        <td><@edit.label name='cisSml' items=items /></td><td><@edit.place object=object name='cisSml' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['formaUhradyCis'])>
                    <tr>
                        <td><@edit.label name='formaUhradyCis' items=items /></td><td><@edit.place object=object name='formaUhradyCis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['formaUhrK'])>
                    <tr>
                        <td><@edit.label name='formaUhrK' items=items /></td><td><@edit.place object=object name='formaUhrK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cisObj'])>
                    <tr>
                        <td><@edit.label name='cisObj' items=items /></td><td><@edit.place object=object name='cisObj' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['datObj'])>
                    <tr>
                        <td><@edit.label name='datObj' items=items /></td><td><@edit.place object=object name='datObj' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cisDodak'])>
                    <tr>
                        <td><@edit.label name='cisDodak' items=items /></td><td><@edit.place object=object name='cisDodak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['formaDopravy'])>
                    <tr>
                        <td><@edit.label name='formaDopravy' items=items /></td><td><@edit.place object=object name='formaDopravy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['doprava'])>
                    <tr>
                        <td colspan="4"><@edit.label name='doprava' items=items /><br><@edit.textarea object=object name='doprava' items=items marker=marker rows=2 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['typDoklBan', 'bspBan', 'bankovniUcet', 'varSym', 'konSym'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['typDoklBan', 'bspBan', 'bankovniUcet', 'varSym', 'konSym'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['typDoklBan'])>
                    <tr>
                        <td><@edit.label name='typDoklBan' items=items /></td><td><@edit.place object=object name='typDoklBan' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['bspBan'])>
                    <tr>
                        <td><@edit.label name='bspBan' items=items /></td><td><@edit.place object=object name='bspBan' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['bankovniUcet'])>
                    <tr>
                        <td><@edit.label name='bankovniUcet' items=items /></td><td><@edit.place object=object name='bankovniUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['varSym'])>
                    <tr>
                        <td><@edit.label name='varSym' items=items /></td><td><@edit.place object=object name='varSym' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['konSym'])>
                    <tr>
                        <td><@edit.label name='konSym' items=items level='3' /></td><td><@edit.place object=object name='konSym' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['uvodTxt', 'zavTxt', 'poznam'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['uvodTxt', 'zavTxt', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['uvodTxt'])>
                    <tr>
                        <td><@edit.label name='uvodTxt' items=items /><br><@edit.textarea object=object name='uvodTxt' items=items marker=marker rows=4 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zavTxt'])>
                    <tr>
                        <td><@edit.label name='zavTxt' items=items /><br><@edit.textarea object=object name='zavTxt' items=items marker=marker rows=4 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['datUp1', 'datUp2', 'datSmir'])>
            <div id="edit-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['datUp1', 'datUp2', 'datSmir'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['datUp1'])>
                    <tr>
                        <td><@edit.label name='datUp1' items=items /></td><td><@edit.place object=object name='datUp1' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['datUp2'])>
                    <tr>
                        <td><@edit.label name='datUp2' items=items /></td><td><@edit.place object=object name='datUp2' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['datSmir'])>
                    <tr>
                        <td><@edit.label name='datSmir' items=items /></td><td><@edit.place object=object name='datSmir' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    <#if isMobile>
        <br>
    <#else>
        </td><td>
    </#if>

    <#if canEditAtLeastOne(items, object, ['firma', 'nazFirmy', 'ulice', 'mesto', 'psc', 'stat', 'ic', 'dic', 'vatId', 'postovniShodna', 'faNazev', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'banSpojDod', 'buc', 'smerKod', 'iban', 'bic', 'specSym'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['firma', 'nazFirmy', 'ulice', 'mesto', 'psc', 'stat', 'ic', 'dic', 'vatId'])>
            <li class="active"><a href="#edit-fragment-6" data-toggle="tab"><span>${lang('dials', 'typVztahu.dodavatel')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['postovniShodna', 'faNazev', 'faUlice', 'faMesto', 'faPsc', 'faStat'])>
            <li><a href="#edit-fragment-7" data-toggle="tab"><span>${lang('labels', 'postovniPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['banSpojDod', 'buc', 'smerKod', 'iban', 'bic', 'specSym'])>
            <li><a href="#edit-fragment-8" data-toggle="tab"><span>${lang('labels', 'dokladEBUcetDodavatele')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['firma', 'nazFirmy', 'ulice', 'mesto', 'psc', 'stat', 'ic', 'dic', 'vatId'])>
            <div id="edit-fragment-6" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['firma', 'nazFirmy', 'ulice', 'mesto', 'psc', 'stat', 'ic', 'dic', 'vatId'])>
                <table class="flexibee-tbl-2">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['firma'])>
                    <tr>
                        <td><@edit.label name='firma' items=items /></td><td class="neverHide" colspan="3"><@edit.place object=object name='firma' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['nazFirmy'])>
                    <tr>
                        <td><@edit.label name='nazFirmy' items=items level='3' /></td><td colspan="3"><@edit.place object=object name='nazFirmy' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ulice'])>
                    <tr>
                        <td><@edit.label name='ulice' items=items level='3' /></td><td colspan="3"><@edit.place object=object name='ulice' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mesto', 'psc'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mesto'])>
                        <td><@edit.label name='mesto' items=items level='3' /></td><td><@edit.place object=object name='mesto' items=items marker=marker  level='3'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['psc'])>
                        <td><@edit.label name='psc' items=items level='3' /></td><td><@edit.place object=object name='psc' items=items marker=marker  level='3'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stat'])>
                    <tr>
                        <td><@edit.label name='stat' items=items level='3' /></td><td colspan="3"><@edit.place object=object name='stat' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ic', 'dic'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ic'])>
                        <td><@edit.label name='ic' items=items level='3' /></td><td><@edit.place object=object name='ic' items=items marker=marker  level='3'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dic'])>
                        <td><@edit.label name='dic' items=items level='3' /></td><td><@edit.place object=object name='dic' items=items marker=marker  level='3'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['vatId'])>
                    <tr>
                        <td><@edit.label name='vatId' items=items level='3' /></td><td><@edit.place object=object name='vatId' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['postovniShodna', 'faNazev', 'faUlice', 'faMesto', 'faPsc', 'faStat'])>
            <div id="edit-fragment-7" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['postovniShodna', 'faNazev', 'faUlice', 'faMesto', 'faPsc', 'faStat'])>
                <table class="flexibee-tbl-2">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['postovniShodna'])>
                    <tr>
                        <td><@edit.label name='postovniShodna' items=items /></td><td colspan="3"><@edit.place object=object name='postovniShodna' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faNazev'])>
                    <tr>
                        <td><@edit.label name='faNazev' items=items level='3' /></td><td colspan="3"><@edit.place object=object name='faNazev' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faUlice'])>
                    <tr>
                        <td><@edit.label name='faUlice' items=items level='3' /></td><td colspan="3"><@edit.place object=object name='faUlice' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faMesto', 'faPsc'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['faMesto'])>
                        <td><@edit.label name='faMesto' items=items level='3' /></td><td><@edit.place object=object name='faMesto' items=items marker=marker  level='3'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['faPsc'])>
                        <td><@edit.label name='faPsc' items=items level='3' /></td><td><@edit.place object=object name='faPsc' items=items marker=marker  level='3'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faStat'])>
                    <tr>
                        <td><@edit.label name='faStat' items=items level='3' /></td><td colspan="3"><@edit.place object=object name='faStat' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['banSpojDod', 'buc', 'smerKod', 'iban', 'bic', 'specSym'])>
            <div id="edit-fragment-8" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['banSpojDod', 'buc', 'smerKod', 'iban', 'bic', 'specSym'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['banSpojDod'])>
                    <tr>
                        <td><@edit.label name='banSpojDod' items=items /></td><td><@edit.place object=object name='banSpojDod' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['buc'])>
                    <tr>
                        <td><@edit.label name='buc' items=items /></td><td><@edit.place object=object name='buc' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['smerKod'])>
                    <tr>
                        <td><@edit.label name='smerKod' items=items /></td><td><@edit.place object=object name='smerKod' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['iban'])>
                    <tr>
                        <td><@edit.label name='iban' items=items /></td><td><@edit.place object=object name='iban' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['bic'])>
                    <tr>
                        <td><@edit.label name='bic' items=items /></td><td><@edit.place object=object name='bic' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['specSym'])>
                    <tr>
                        <td><@edit.label name='specSym' items=items /></td><td><@edit.place object=object name='specSym' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    <#if isMobile>
        <br>
    <#else>
        </td></tr><tr><td>
    </#if>

    <#if canEditAtLeastOne(items, object, ['typUcOp', 'typDoklNabFak', 'stredisko', 'cinnost', 'zakazka', 'danEvidK', 'duzpPuv', 'duzpUcto', 'primUcet', 'protiUcet', 'vyloucitSaldo', 'dphZaklUcet', 'dphSnizUcet', 'dphSniz2Ucet', 'clenDph', 'statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh', 'zodpOsoba', 'kontaktOsoba', 'kontaktJmeno', 'kontaktEmail', 'kontaktTel'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['typUcOp', 'typDoklNabFak', 'stredisko', 'cinnost', 'zakazka'])>
            <li class="active"><a href="#edit-fragment-9" data-toggle="tab"><span>${lang('labels', 'specifikacePN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['danEvidK', 'duzpPuv', 'duzpUcto', 'primUcet', 'protiUcet', 'vyloucitSaldo', 'dphZaklUcet', 'dphSnizUcet', 'dphSniz2Ucet', 'clenDph'])>
            <li><a href="#edit-fragment-10" data-toggle="tab"><span>${lang('labels', 'uctoADphPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'])>
            <li><a href="#edit-fragment-11" data-toggle="tab"><span>${lang('labels', 'intrastatPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['zodpOsoba', 'kontaktOsoba', 'kontaktJmeno', 'kontaktEmail', 'kontaktTel'])>
            <li><a href="#edit-fragment-12" data-toggle="tab"><span>${lang('labels', 'zodpOsobaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['typUcOp', 'typDoklNabFak', 'stredisko', 'cinnost', 'zakazka'])>
            <div id="edit-fragment-9" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['typUcOp', 'typDoklNabFak', 'stredisko', 'cinnost', 'zakazka'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['typUcOp'])>
                    <tr>
                        <td><@edit.label name='typUcOp' items=items /></td><td><@edit.place object=object name='typUcOp' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typDoklNabFak'])>
                    <tr>
                        <td><@edit.label name='typDoklNabFak' items=items /></td><td><@edit.place object=object name='typDoklNabFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stredisko'])>
                    <tr>
                        <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cinnost'])>
                    <tr>
                        <td><@edit.label name='cinnost' items=items /></td><td><@edit.place object=object name='cinnost' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zakazka'])>
                    <tr>
                        <td><@edit.label name='zakazka' items=items /></td><td><@edit.place object=object name='zakazka' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['danEvidK', 'duzpPuv', 'duzpUcto', 'primUcet', 'protiUcet', 'vyloucitSaldo', 'dphZaklUcet', 'dphSnizUcet', 'dphSniz2Ucet', 'clenDph'])>
            <div id="edit-fragment-10" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['danEvidK', 'duzpPuv', 'duzpUcto', 'primUcet', 'protiUcet', 'vyloucitSaldo', 'dphZaklUcet', 'dphSnizUcet', 'dphSniz2Ucet', 'clenDph'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if project().danEvid>
                    <#if canEditAtLeastOne(items, object, ['danEvidK'])>
                    <tr>
                        <td><@edit.label name='danEvidK' items=items /></td><td><@edit.place object=object name='danEvidK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['duzpPuv'])>
                    <tr>
                        <td><@edit.label name='duzpPuv' items=items /></td><td><@edit.place object=object name='duzpPuv' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['duzpUcto'])>
                    <tr>
                        <td><@edit.label name='duzpUcto' items=items /></td><td><@edit.place object=object name='duzpUcto' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['primUcet'])>
                    <tr>
                        <td><@edit.label name='primUcet' items=items /></td><td><@edit.place object=object name='primUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['protiUcet'])>
                    <tr>
                        <td><@edit.label name='protiUcet' items=items /></td><td><@edit.place object=object name='protiUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['vyloucitSaldo'])>
                    <tr>
                        <td><@edit.label name='vyloucitSaldo' items=items /></td><td><@edit.place object=object name='vyloucitSaldo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['dphZaklUcet'])>
                    <tr>
                        <td><@edit.label name='dphZaklUcet' items=items /></td><td><@edit.place object=object name='dphZaklUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['dphSnizUcet'])>
                    <tr>
                        <td><@edit.label name='dphSnizUcet' items=items /></td><td><@edit.place object=object name='dphSnizUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['dphSniz2Ucet'])>
                    <tr>
                        <td><@edit.label name='dphSniz2Ucet' items=items /></td><td><@edit.place object=object name='dphSniz2Ucet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['clenDph'])>
                    <tr>
                        <td><@edit.label name='clenDph' items=items /></td><td><@edit.place object=object name='clenDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'])>
            <div id="edit-fragment-11" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['statOdesl', 'obchTrans', 'statUrc', 'dodPodm', 'statPuvod', 'druhDopr', 'krajUrc', 'zvlPoh'])>
                <table class="flexibee-tbl-2">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['statOdesl', 'obchTrans'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['statOdesl'])>
                        <td><@edit.label name='statOdesl' items=items /></td><td><@edit.place object=object name='statOdesl' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['obchTrans'])>
                        <td><@edit.label name='obchTrans' items=items /></td><td><@edit.place object=object name='obchTrans' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['statUrc', 'dodPodm'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['statUrc'])>
                        <td><@edit.label name='statUrc' items=items /></td><td><@edit.place object=object name='statUrc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dodPodm'])>
                        <td><@edit.label name='dodPodm' items=items /></td><td><@edit.place object=object name='dodPodm' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['statPuvod', 'druhDopr'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['statPuvod'])>
                        <td><@edit.label name='statPuvod' items=items /></td><td><@edit.place object=object name='statPuvod' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['druhDopr'])>
                        <td><@edit.label name='druhDopr' items=items /></td><td><@edit.place object=object name='druhDopr' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['krajUrc', 'zvlPoh'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['krajUrc'])>
                        <td><@edit.label name='krajUrc' items=items /></td><td><@edit.place object=object name='krajUrc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['zvlPoh'])>
                        <td><@edit.label name='zvlPoh' items=items /></td><td><@edit.place object=object name='zvlPoh' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['zodpOsoba', 'kontaktOsoba', 'kontaktJmeno', 'kontaktEmail', 'kontaktTel'])>
            <div id="edit-fragment-12" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['zodpOsoba', 'kontaktOsoba', 'kontaktJmeno', 'kontaktEmail', 'kontaktTel'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['zodpOsoba'])>
                    <tr>
                        <td><@edit.label name='zodpOsoba' items=items /></td><td><@edit.place object=object name='zodpOsoba' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kontaktOsoba'])>
                    <tr>
                        <td><@edit.label name='kontaktOsoba' items=items /></td><td><@edit.place object=object name='kontaktOsoba' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kontaktJmeno'])>
                    <tr>
                        <td><@edit.label name='kontaktJmeno' items=items /></td><td><@edit.place object=object name='kontaktJmeno' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kontaktEmail'])>
                    <tr>
                        <td><@edit.label name='kontaktEmail' items=items /></td><td><@edit.place object=object name='kontaktEmail' items=items marker=marker  type='email'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kontaktTel'])>
                    <tr>
                        <td><@edit.label name='kontaktTel' items=items /></td><td><@edit.place object=object name='kontaktTel' items=items marker=marker  type='tel'/></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    <#if isMobile>
        <br>
    <#else>
        </td><td>
    </#if>

    <#if canEditAtLeastOne(items, object, ['slevaDokl', 'sumCelkem', 'sumZklCelkem', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumZklSniz', 'sumZklZakl', 'sumOsv', 'sumCelkemMen', 'sumZklCelkemMen', 'mena', 'kurz', 'kurzMnozstvi', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumZklSnizMen', 'sumZklZaklMen', 'sumOsvMen', 'zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK', 'eetProvozovna', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetTypK', 'eetStavK', 'eetFik'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['slevaDokl', 'sumCelkem', 'sumZklCelkem', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumZklSniz', 'sumZklZakl', 'sumOsv'])>
            <li class="active"><a href="#edit-fragment-17" data-toggle="tab"><span><@tool.menaSymbol settings(object.datVyst).mena/></span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['sumCelkemMen', 'sumZklCelkemMen', 'mena', 'kurz', 'kurzMnozstvi', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumZklSnizMen', 'sumZklZaklMen', 'sumOsvMen'])>
            <li><a href="#edit-fragment-22" data-toggle="tab"><span>${lang('labels', 'dokladEBMenaPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
            <li><a href="#edit-fragment-23" data-toggle="tab"><span>${lang('labels', 'typDoklEBzaokrouhleniObPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetTypK', 'eetStavK', 'eetFik'])>
            <li><a href="#edit-fragment-24" data-toggle="tab"><span>${lang('labels', 'dokladEBEetPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['slevaDokl', 'sumCelkem', 'sumZklCelkem', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl', 'sumOsv', 'sumOsv', 'sumZklSniz2', 'sumZklSniz', 'sumZklZakl', 'sumOsv'])>
            <div id="edit-fragment-17" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['slevaDokl', 'sumCelkem', 'sumZklCelkem'])>
                <table class="flexibee-tbl-1">
                    <col width="30%" /><col width="30%" /><col width="30%" /><col width="10%" />
                    <#if canEditAtLeastOne(items, object, ['slevaDokl'])>
                    <tr>
                        <td><@edit.label name='slevaDokl' items=items /></td><td><@edit.place object=object name='slevaDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['sumCelkem', 'sumZklCelkem'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['sumCelkem'])>
                        <td><label for="flexibee-inp-sumCelkem"><#if settings(object.datVyst).platceDph>Celkem s DPH<#else>Celkem</#if>:</label></td><td><@edit.place object=object name='sumCelkem' items=items marker=marker  level='0' onlyShow=true/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklCelkem'])>
                        <td><label for="flexibee-inp-sumZklCelkem">${lang('messages', 'sumDoklZklCelk')}:</label></td><td><@edit.place object=object name='sumZklCelkem' items=items marker=marker  level='3' onlyShow=true/></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if isMobile == false>

                <div class="flexibee-doklad-sumy-platce-dph">

                <#if canEditAtLeastOne(items, object, ['sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl'])>
                <div id="edit-fragment-13" class="tab-pane">
                    <div class="flexibee-table-border-tbl">
                    <#if canEditAtLeastOne(items, object, ['sumOsv', 'sumOsv', 'sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2', 'sumZklSniz', 'sumDphSniz', 'sumCelkSniz', 'sumZklZakl', 'sumDphZakl', 'sumCelkZakl'])>
                    <table class="flexibee-tbl-1">
                        <col width="25%" /><col width="25%" /><col width="25%" /><col width="25%" />
                        <tr class="flexibee-sep0">
                            <td></td>
                            <#if isMobile></tr><tr></#if>
                            <td>Základ</td>
                            <#if isMobile></tr><tr></#if>
                            <td>DPH</td>
                            <#if isMobile></tr><tr></#if>
                            <td>Celkem</td>
                        </tr>

                        <#if canEditAtLeastOne(items, object, ['sumOsv', 'sumOsv'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumOsv">  Osv. </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                            <td><@edit.place object=object name='sumOsv' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <td></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                            <td><@edit.place object=object name='sumOsv' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklSniz2', 'sumDphSniz2', 'sumCelkSniz2'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz2">  2. sníž. </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklSniz2'])>
                            <td><@edit.place object=object name='sumZklSniz2' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumDphSniz2'])>
                            <td><@edit.place object=object name='sumDphSniz2' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumCelkSniz2'])>
                            <td><@edit.place object=object name='sumCelkSniz2' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklSniz', 'sumDphSniz', 'sumCelkSniz'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz">  Sníž. </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklSniz'])>
                            <td><@edit.place object=object name='sumZklSniz' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumDphSniz'])>
                            <td><@edit.place object=object name='sumDphSniz' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumCelkSniz'])>
                            <td><@edit.place object=object name='sumCelkSniz' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklZakl', 'sumDphZakl', 'sumCelkZakl'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklZakl">  Zákl. </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklZakl'])>
                            <td><@edit.place object=object name='sumZklZakl' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumDphZakl'])>
                            <td><@edit.place object=object name='sumDphZakl' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumCelkZakl'])>
                            <td><@edit.place object=object name='sumCelkZakl' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <tr class="flexibee-sep2">
                            <td>Suma</td>
                            <#if isMobile></tr><tr></#if>
                            <td><span id="flexibee-inp-sumZklCelkem" class="flexibee-computed-value"></td>
                            <#if isMobile></tr><tr></#if>
                            <td><span id="flexibee-inp-sumDphCelkem" class="flexibee-computed-value"></td>
                            <#if isMobile></tr><tr></#if>
                            <td><span id="flexibee-inp-sumCelkem-dup1" class="flexibee-dup flexibee-computed-value"></td>
                        </tr>

                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div><div class="flexibee-doklad-sumy-neplatce-dph" style="display: none">

                <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                <div id="edit-fragment-14" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                    <table class="flexibee-tbl-1">
                        <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                        <tr>
                            <td><span id="flexibee-lbl-sumOsv">  Osv. </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                            <td><@edit.place object=object name='sumOsv' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div>

                <#else>

                <div class="flexibee-doklad-sumy-platce-dph">

                <#if canEditAtLeastOne(items, object, ['sumOsv', 'sumZklSniz2', 'sumZklSniz', 'sumZklZakl'])>
                <div id="edit-fragment-15" class="tab-pane">
                    <div class="flexibee-table-border-tbl">
                    <#if canEditAtLeastOne(items, object, ['sumOsv', 'sumZklSniz2', 'sumZklSniz', 'sumZklZakl'])>
                    <table class="flexibee-tbl-1">
                        <col width="35%" /><col width="65%" />
                        <tr class="flexibee-sep0">
                            <td>&nbsp; </td><td> Základ (bez DPH)</td>
                        </tr>

                        <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumOsv">Osv.</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                            <td><@edit.place object=object name='sumOsv' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklSniz2'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz2">2. sníž.</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklSniz2'])>
                            <td><@edit.place object=object name='sumZklSniz2' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklSniz'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz">Sníž.</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklSniz'])>
                            <td><@edit.place object=object name='sumZklSniz' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklZakl'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklZakl">Zákl.</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklZakl'])>
                            <td><@edit.place object=object name='sumZklZakl' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <tr class="flexibee-sep2">
                            <td>Suma </td><td> <span id="flexibee-inp-sumZklCelkem" class="flexibee-computed-value"></td>
                        </tr>

                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div><div class="flexibee-doklad-sumy-neplatce-dph" style="display: none">

                <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                <div id="edit-fragment-16" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                    <table class="flexibee-tbl-1">
                        <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                        <tr>
                            <td><span id="flexibee-lbl-sumOsv">Osv.</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsv'])>
                            <td><@edit.place object=object name='sumOsv' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div>

                </#if>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['sumCelkemMen', 'sumZklCelkemMen', 'mena', 'kurz', 'kurzMnozstvi', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen', 'sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumZklSnizMen', 'sumZklZaklMen', 'sumOsvMen'])>
            <div id="edit-fragment-22" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['sumCelkemMen', 'sumZklCelkemMen', 'mena', 'kurz', 'kurzMnozstvi'])>
                <table class="flexibee-tbl-1">
                    <col width="30%" /><col width="30%" /><col width="30%" /><col width="10%" />
                    <#if canEditAtLeastOne(items, object, ['sumCelkemMen', 'sumZklCelkemMen'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['sumCelkemMen'])>
                        <td><label for="flexibee-inp-sumCelkemMen"><#if settings(object.datVyst).platceDph>Celkem s DPH<#else>Celkem</#if>:</label></td><td><@edit.place object=object name='sumCelkemMen' items=items marker=marker  level='0' onlyShow=true/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklCelkemMen'])>
                        <td><label for="flexibee-inp-sumZklCelkemMen">${lang('messages', 'sumDoklZklCelk')}:</label></td><td><@edit.place object=object name='sumZklCelkemMen' items=items marker=marker  level='3' onlyShow=true/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mena'])>
                    <tr>
                        <td><@edit.label name='mena' items=items level='0' /></td><td colspan="3"><@edit.place object=object name='mena' items=items marker=marker  level='0'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kurz', 'kurzMnozstvi'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['kurz'])>
                        <td><@edit.label name='kurz' items=items level='3' /></td><td><@edit.place object=object name='kurz' items=items marker=marker  level='3'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['kurzMnozstvi'])>
                        <td><@edit.label name='kurzMnozstvi' items=items level='3' /></td><td><@edit.place object=object name='kurzMnozstvi' items=items marker=marker  level='3'/></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if isMobile == false>

                <div class="flexibee-doklad-sumy-men-platce-dph">

                <#if canEditAtLeastOne(items, object, ['sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen'])>
                <div id="edit-fragment-18" class="tab-pane">
                    <div class="flexibee-table-border-tbl">
                    <#if canEditAtLeastOne(items, object, ['sumOsvMen', 'sumOsvMen', 'sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men', 'sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen', 'sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen'])>
                    <table class="flexibee-tbl-1">
                        <col width="25%" /><col width="25%" /><col width="25%" /><col width="25%" />
                        <tr class="flexibee-sep0">
                            <td></td>
                            <#if isMobile></tr><tr></#if>
                            <td>Základ</td>
                            <#if isMobile></tr><tr></#if>
                            <td>DPH</td>
                            <#if isMobile></tr><tr></#if>
                            <td>Celkem</td>
                        </tr>

                        <#if canEditAtLeastOne(items, object, ['sumOsvMen', 'sumOsvMen'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumOsvMen">  Osv. měn </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                            <td><@edit.place object=object name='sumOsvMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <td></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                            <td><@edit.place object=object name='sumOsvMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklSniz2Men', 'sumDphSniz2Men', 'sumCelkSniz2Men'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz2Men">  2. sníž. měn </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklSniz2Men'])>
                            <td><@edit.place object=object name='sumZklSniz2Men' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumDphSniz2Men'])>
                            <td><@edit.place object=object name='sumDphSniz2Men' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumCelkSniz2Men'])>
                            <td><@edit.place object=object name='sumCelkSniz2Men' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklSnizMen', 'sumDphSnizMen', 'sumCelkSnizMen'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSnizMen">  Sníž. měn </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklSnizMen'])>
                            <td><@edit.place object=object name='sumZklSnizMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumDphSnizMen'])>
                            <td><@edit.place object=object name='sumDphSnizMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumCelkSnizMen'])>
                            <td><@edit.place object=object name='sumCelkSnizMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklZaklMen', 'sumDphZaklMen', 'sumCelkZaklMen'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklZaklMen">  Zákl. měn </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklZaklMen'])>
                            <td><@edit.place object=object name='sumZklZaklMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumDphZaklMen'])>
                            <td><@edit.place object=object name='sumDphZaklMen' items=items marker=marker /></td>
                            </#if>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumCelkZaklMen'])>
                            <td><@edit.place object=object name='sumCelkZaklMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <tr class="flexibee-sep2">
                            <td>Suma</td>
                            <#if isMobile></tr><tr></#if>
                            <td><span id="flexibee-inp-sumZklCelkemMen" class="flexibee-computed-value"></td>
                            <#if isMobile></tr><tr></#if>
                            <td><span id="flexibee-inp-sumDphCelkemMen" class="flexibee-computed-value"></td>
                            <#if isMobile></tr><tr></#if>
                            <td><span id="flexibee-inp-sumCelkemMen-dup1" class="flexibee-dup flexibee-computed-value"></td>
                        </tr>

                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div><div class="flexibee-doklad-sumy-men-neplatce-dph" style="display: none">

                <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                <div id="edit-fragment-19" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                    <table class="flexibee-tbl-1">
                        <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                        <tr>
                            <td><span id="flexibee-lbl-sumOsvMen">  Osv. měn </span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                            <td><@edit.place object=object name='sumOsvMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div>

                <#else>

                <div class="flexibee-doklad-sumy-men-platce-dph">

                <#if canEditAtLeastOne(items, object, ['sumOsvMen', 'sumZklSniz2Men', 'sumZklSnizMen', 'sumZklZaklMen'])>
                <div id="edit-fragment-20" class="tab-pane">
                    <div class="flexibee-table-border-tbl">
                    <#if canEditAtLeastOne(items, object, ['sumOsvMen', 'sumZklSniz2Men', 'sumZklSnizMen', 'sumZklZaklMen'])>
                    <table class="flexibee-tbl-1">
                        <col width="35%" /><col width="65%" />
                        <tr class="flexibee-sep0">
                            <td>&nbsp; </td><td> Základ (bez DPH)</td>
                        </tr>

                        <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumOsvMen">Osv. měn</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                            <td><@edit.place object=object name='sumOsvMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklSniz2Men'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSniz2Men">2. sníž. měn</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklSniz2Men'])>
                            <td><@edit.place object=object name='sumZklSniz2Men' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklSnizMen'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklSnizMen">Sníž. měn</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklSnizMen'])>
                            <td><@edit.place object=object name='sumZklSnizMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sumZklZaklMen'])>
                        <tr class="flexibee-sep">
                            <td><span id="flexibee-lbl-sumZklZaklMen">Zákl. měn</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumZklZaklMen'])>
                            <td><@edit.place object=object name='sumZklZaklMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                        <tr class="flexibee-sep2">
                            <td>Suma </td><td> <span id="flexibee-inp-sumZklCelkemMen" class="flexibee-computed-value"></td>
                        </tr>

                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div><div class="flexibee-doklad-sumy-men-neplatce-dph" style="display: none">

                <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                <div id="edit-fragment-21" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                    <table class="flexibee-tbl-1">
                        <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                        <tr>
                            <td><span id="flexibee-lbl-sumOsvMen">Osv. měn</span></td>
                            <#if isMobile></tr><tr></#if>
                            <#if canEditAtLeastOne(items, object, ['sumOsvMen'])>
                            <td><@edit.place object=object name='sumOsvMen' items=items marker=marker /></td>
                            </#if>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                </div>

                </#if>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
            <div id="edit-fragment-23" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['zaokrJakDphK'])>
                    <tr>
                        <td><@edit.label name='zaokrJakDphK' items=items level='3' /></td><td><@edit.place object=object name='zaokrJakDphK' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrNaDphK'])>
                    <tr>
                        <td><@edit.label name='zaokrNaDphK' items=items level='3' /></td><td><@edit.place object=object name='zaokrNaDphK' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrJakSumK'])>
                    <tr>
                        <td><@edit.label name='zaokrJakSumK' items=items level='3' /></td><td><@edit.place object=object name='zaokrJakSumK' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrNaSumK'])>
                    <tr>
                        <td><@edit.label name='zaokrNaSumK' items=items level='3' /></td><td><@edit.place object=object name='zaokrNaSumK' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetTypK', 'eetStavK', 'eetFik'])>
            <div id="edit-fragment-24" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetPokladniZarizeni', 'eetDicPoverujiciho', 'eetTypK', 'eetStavK', 'eetFik'])>
                <table class="flexibee-tbl-1">
                    <col width="35%" /><col width="65%" />
                    <#if canEditAtLeastOne(items, object, ['eetProvozovna'])>
                    <tr>
                        <td><@edit.label name='eetProvozovna' items=items level='3' /></td><td><@edit.place object=object name='eetProvozovna' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['eetPokladniZarizeni'])>
                    <tr>
                        <td><@edit.label name='eetPokladniZarizeni' items=items level='3' /></td><td><@edit.place object=object name='eetPokladniZarizeni' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['eetDicPoverujiciho'])>
                    <tr>
                        <td><@edit.label name='eetDicPoverujiciho' items=items level='3' /></td><td><@edit.place object=object name='eetDicPoverujiciho' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['eetTypK'])>
                    <tr>
                        <td><@edit.label name='eetTypK' items=items level='3' /></td><td><@edit.place object=object name='eetTypK' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['eetStavK'])>
                    <tr>
                        <td><@edit.label name='eetStavK' items=items level='3' /></td><td><@edit.place object=object name='eetStavK' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['eetFik'])>
                    <tr>
                        <td><@edit.label name='eetFik' items=items level='3' /></td><td><@edit.place object=object name='eetFik' items=items marker=marker  level='3'/></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    <#if isMobile>
        </div>
    <#else>
        </td></tr></table>
    </#if>

    <#assign oldIt=it, oldItems=items, oldObject=object, oldMarker=marker />
    <#global it=it.getSubEvidence(it.evidenceName+"-polozka") />
    <#assign items=it.editHandler.propertiesMap, object=it.editHandler.object, marker=newMarker() />

    <div id="flexibee-doklad-polozky" style="display:none">

    <br><br>

    <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'mj', 'cenaMj', 'typCenyDphK', 'typSzbDphK', 'szbDph', 'clenKonVykDph', 'kopClenKonVykDph'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#edit-fragment-25" data-toggle="tab"><span>${lang('labels', 'dokladEBPolozky')}</span></a></li>

            <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'mj', 'cenaMj', 'typCenyDphK', 'typSzbDphK', 'szbDph', 'clenKonVykDph', 'kopClenKonVykDph'])>
            <li><a href="#edit-fragment-26" data-toggle="tab"><span>+</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <div id="edit-fragment-25" class="tab-pane active">
                <div id="flexibee-doklad-polozky-list" class="flexibee-table-border">
                      <table>
                        <col width="60%"/>
                        <col width="10%"/>
                        <col width="20%"/>
                        <col width="10%"/>
                      </table>
                    </div>

            </div>

            <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'mj', 'cenaMj', 'typCenyDphK', 'typSzbDphK', 'szbDph', 'clenKonVykDph', 'kopClenKonVykDph'])>
            <div id="edit-fragment-26" class="tab-pane">
                <div id="flexibee-doklad-polozky-edit">

                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'mj', 'cenaMj', 'typCenyDphK', 'typSzbDphK', 'szbDph', 'clenKonVykDph', 'kopClenKonVykDph'])>
                <table class="flexibee-tbl-1">
                    <col width="10%" /><col width="30%" /><col width="30%" /><col width="30%" />
                    <#if canEditAtLeastOne(items, object, ['cenik'])>
                    <tr>
                        <td><@edit.label name='cenik' items=items prefix="polozkyDokladu__" /></td><td colspan="3"><@edit.place object=object name='cenik' items=items marker=marker prefix="polozkyDokladu__" /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kod', 'sklad'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['kod'])>
                        <td><@edit.label name='kod' items=items prefix="polozkyDokladu__" /></td><td><@edit.place object=object name='kod' items=items marker=marker prefix="polozkyDokladu__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['sklad'])>
                        <td><@edit.label name='sklad' items=items prefix="polozkyDokladu__" /></td><td><@edit.place object=object name='sklad' items=items marker=marker prefix="polozkyDokladu__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['nazev'])>
                    <tr>
                        <td><@edit.label name='nazev' items=items prefix="polozkyDokladu__" /></td><td colspan="3"><@edit.place object=object name='nazev' items=items marker=marker prefix="polozkyDokladu__" /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mnozMj', 'mj'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mnozMj'])>
                        <td><@edit.label name='mnozMj' items=items prefix="polozkyDokladu__" /></td><td><@edit.place object=object name='mnozMj' items=items marker=marker prefix="polozkyDokladu__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mj'])>
                        <td><@edit.place object=object name='mj' items=items marker=marker prefix="polozkyDokladu__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cenaMj'])>
                    <tr>
                        <td><@edit.label name='cenaMj' items=items prefix="polozkyDokladu__" /></td><td><@edit.place object=object name='cenaMj' items=items marker=marker prefix="polozkyDokladu__" /></td>
                    </tr>
                    </#if>
                    <#if settings(oldObject.datumDokladu).platceDph>
                    <#if canEditAtLeastOne(items, object, ['typCenyDphK'])>
                    <tr>
                        <td><@edit.label name='typCenyDphK' items=items prefix="polozkyDokladu__" level='3' /></td><td><@edit.place object=object name='typCenyDphK' items=items marker=marker prefix="polozkyDokladu__"  level='3'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typSzbDphK', 'szbDph'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['typSzbDphK'])>
                        <td><@edit.label name='typSzbDphK' items=items prefix="polozkyDokladu__" level='3' /></td><td><@edit.place object=object name='typSzbDphK' items=items marker=marker prefix="polozkyDokladu__"  level='3'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['szbDph'])>
                        <td><@edit.label name='szbDph' items=items prefix="polozkyDokladu__" level='3' /></td><td><@edit.place object=object name='szbDph' items=items marker=marker prefix="polozkyDokladu__"  level='3' onlyShow=true/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['clenKonVykDph', 'kopClenKonVykDph'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['clenKonVykDph'])>
                        <td><@edit.label name='clenKonVykDph' items=items prefix="polozkyDokladu__" /></td><td><@edit.place object=object name='clenKonVykDph' items=items marker=marker prefix="polozkyDokladu__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['kopClenKonVykDph'])>
                        <td><@edit.label name='kopClenKonVykDph' items=items prefix="polozkyDokladu__" /></td><td><@edit.place object=object name='kopClenKonVykDph' items=items marker=marker prefix="polozkyDokladu__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    </#if>
                </table>
                </#if>
                </div>


                <div class="flexibee-table-border flexibee-tab">
                <table class="flexibee-tbl-1">
                    <col width="10%" /><col width="30%" /><col width="30%" /><col width="30%" />
                    <#if settings(oldObject.datumDokladu).platceDph>
                    <tr class="flexibee-sep0">
                        <td>&nbsp; </td><td> Základ </td><td> DPH </td><td> Celkem</td>
                    </tr>

                    <tr class="flexibee-sep">
                        <td>Suma </td><td> <span id="flexibee-inp-polozkyDokladu__sumZkl" class="flexibee-computed-value"></span> </td><td> <span id="flexibee-inp-polozkyDokladu__sumDph" class="flexibee-computed-value"></span> </td><td> <span id="flexibee-inp-polozkyDokladu__sumCelkem" class="flexibee-computed-value"></span></td>
                    </tr>

                    <tr class="flexibee-sep">
                        <td>Suma [měna] </td><td> <span id="flexibee-inp-polozkyDokladu__sumZklMen" class="flexibee-computed-value"></span> </td><td> <span id="flexibee-inp-polozkyDokladu__sumDphMen" class="flexibee-computed-value"></span> </td><td> <span id="flexibee-inp-polozkyDokladu__sumCelkemMen" class="flexibee-computed-value"></span></td>
                    </tr>

                    <#else>
                    <tr class="flexibee-sep0">
                        <td>&nbsp; </td><td> Celkem</td>
                    </tr>

                    <tr class="flexibee-sep">
                        <td>Suma </td><td> <span id="flexibee-inp-polozkyDokladu__sumCelkem" class="flexibee-computed-value"></span></td>
                    </tr>

                    <tr class="flexibee-sep">
                        <td>Suma [měna] </td><td> <span id="flexibee-inp-polozkyDokladu__sumCelkemMen" class="flexibee-computed-value"></span></td>
                    </tr>

                    </#if>
                </table>

                </div>

                <div class="flexibee-buttons" <#if isMobile>data-role="controlgroup"  data-type="horizontal"</#if>>
                        <input type="button" name="ok" value="OK" class="flexibee-button btn btn-primary" id="flexibee-doklad-polozky-edit-ok" />
                        <input type="button" name="cancel" value="Zrušit" class="flexibee-button negative btn btn-default" id="flexibee-doklad-polozky-edit-cancel" />
                      </div>
                    </div>

                    <#-- uklizení -->
                    <#global it=oldIt />
                    <#assign items=oldItems, object=oldObject, marker=oldMarker />

            </div>
            </#if>
        </div>
    </#if>

    </div>


    <@tool.showRelating "RADA" "flexibee-inp-kod" />

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />


<#import "/evidence/l-doklad-edit.ftl" as dokladEdit />
<@dokladEdit.generateJavaScript it=it object=object polozkyPropName="polozkyObchDokladu" />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
