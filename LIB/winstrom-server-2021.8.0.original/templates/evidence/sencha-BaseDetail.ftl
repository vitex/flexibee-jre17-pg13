<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.BaseDetail', {
    extend: 'Ext.container.Container',

    requires: [
        'FlexiBee.${it.senchaName}.EditForm',
        'FlexiBee.${it.senchaName}.Toolbar',
        'FlexiBee.${it.senchaName}.Relations'
    ],

    showToolbar: true,
    showRelations: true,

    layout: {
        type: 'border'
    },

    initComponent: function() {
        var me = this;

        var centerContainerItems = [];

        if (me.showToolbar) {
            centerContainerItems.push({
                xtype: 'fbToolbar${it.senchaName?cap_first}',
                isDetail: true
            });
        }

        centerContainerItems.push({
            xtype: 'fbEditForm${it.senchaName?cap_first}',
            autoScroll: true,
            flex: 1
        });

        if (me.showRelations) {
            centerContainerItems.push({
                xtype: 'fbRelations${it.senchaName?cap_first}'
            });
        }

        me.items = [
            {
                xtype: 'container',
                layout: {
                    type: 'vbox',
                    align: 'stretch'
                },
                region: 'center',
                items: centerContainerItems
            }
        ];

        var additionalItems = FlexiBee.Plugins.invoke(me, 'defineAdditionalItems');
        Ext.iterate(additionalItems, function(additionalItem) {
            if (Ext.isArray(additionalItem)) {
                me.items = me.items.concat(additionalItem);
            } else {
                me.items.push(additionalItem);
            }
        });

        me.callParent(arguments);
    }
});


<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="${it.senchaName}.Detail"/>


</#escape>
