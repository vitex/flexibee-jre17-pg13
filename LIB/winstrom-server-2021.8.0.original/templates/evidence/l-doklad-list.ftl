<#ftl encoding="utf-8" strip_whitespace=true />
<#escape x as x?default("")?html>

<#macro dokladInList object iteratorIndex marker items>

<#import "/evidence/l-doklad-list.ftl" as my/>

<a href="/c/${it.companyResource.companyId}/${it.evidenceType.path}/${object.id?c}">
    <#-- kód -->
    <small><@tool.placeList marker=marker object=object name="kod" items=items /></small>

    &nbsp;

    <#-- firma -->
    <@tool.placeList marker=marker object=object name="firma" items=items />

    &nbsp;

    <#-- datum vystavení -->
    <small>vystaveno <@tool.placeList marker=marker object=object name="datVyst" items=items /></small>

    <span style="float:right;text-align:right">
        <#-- celková částka -->
        <span class="flexibee-big">
            <#if object['sumCelkemMen'] != 0>
                <@tool.placeList marker=marker object=object name="sumCelkemMen" items=items />
            <#else>
                <@tool.placeList marker=marker object=object name="sumCelkem" items=items />
            </#if>
            <@tool.menaSymbol object.mena/>
        </span>

        <br/>

        <small>
            <#-- zálohy -->
            <#if isMobile == false || isTablet == true>
                <#if object['sumZalohyMen']?? && object['sumZalohyMen'] != 0>
                    Záloha <@tool.placeList marker=marker object=object name="sumZalohyMen" items=items />
                    <@tool.menaSymbol object.mena/><br/>
                <#elseif object['sumZalohy']?? && object['sumZalohy'] != 0>
                    Záloha <@tool.placeList marker=marker object=object name="sumZalohy" items=items />
                    <@tool.menaSymbol object.mena/><br/>
                </#if>
            </#if>

            <#-- stav úhrady -->
            <#if object['stavUhrK']??>
                <span style="color:green"><@tool.placeList marker=marker object=object name="stavUhrK" items=items /> <@tool.placeList marker=marker object=object name="datUhr" items=items /></span>
                <#if object['zbyvaUhraditMen'] != 0>
                    | zbývá <@tool.placeList marker=marker object=object name="zbyvaUhraditMen" items=items /> <@tool.menaSymbol object.mena/>
                <#elseif object['zbyvaUhradit'] != 0>
                    | zbývá <@tool.placeList marker=marker object=object name="zbyvaUhradit" items=items /> <@tool.menaSymbol object.mena/>
                </#if>
            </#if>

            <#-- splatnost -->
            <#if !object['stavUhrK']?? || (object['stavUhrK'] != 'stavUhr.uhrazeno' && object['stavUhrK'] != 'stavUhr.uhrazenoRucne')>
                <#if object['stavUhrK']??><br/></#if>
                <#if object['datSplat']??>
                    <#if object['datSplat'].time?date &gt; now?date>
                        Splatnost <@tool.placeList marker=marker object=object name="datSplat" items=items />
                    <#else>
                        <span style="color:red">po splatnosti <@tool.placeList marker=marker object=object name="datSplat" items=items /></span>
                    </#if>
                </#if>
            </#if>
        </small>
    </span>

    <br/>

    <#-- popis, položky -->
    <small style="color:gray">
        <#assign maxCount = 5/>
        <#if isMobile == true && isTablet == false><#assign maxCount = 3/></#if>

        <#assign itemCnt = 0 />

        <#if object['popis']?? && object['popis'] != ''>
            <@tool.placeList marker=marker object=object name="popis" items=items />
            <#assign itemCnt = itemCnt + 1/>
        </#if>

        <#list object['polozkyDokladu'] as item>
            <#if item.autogen == false && item.autoZaokr == false && itemCnt &lt; maxCount && item.typPolozkyK != 'typPolozky.ucetni'>
                <#if itemCnt &gt; 0>,</#if>
                ${item.nazev}
                <#assign itemCnt = itemCnt + 1/>
            </#if>
        </#list>
    </small>
</a>

</#macro>

</#escape>
