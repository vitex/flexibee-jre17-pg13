<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

<@edit.beginForm />
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />


<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>