<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign myTitle = "Načtení online výpisů" />
<#include "/i-header-edit.ftl"/>

<#if vysledekOnlineNacitani??>
    <div class="alert alert-success">
        <strong>Načtení online výpisů úspěšně dokončeno.</strong>
    </div>
    <div class="alert alert-success">
        ${vysledekOnlineNacitani}
    </div>

    <@edit.buttonsBegin/>
        <a class="flexibee-focus btn btn-primary flexibee-clickable"  data-dismiss="modal"  href="${it.goBackURI}"><span>${lang('labels', 'backToDocument', 'Zpět na doklad')}</span></a>
    <@edit.buttonsEnd/>
<#else>
    <@edit.beginForm />
      <p>Provést načtení online výpisů?</p>
      <@edit.okCancelButton />
    <@edit.endForm />
</#if>

<#include "/i-footer-edit.ftl" />
</#escape>