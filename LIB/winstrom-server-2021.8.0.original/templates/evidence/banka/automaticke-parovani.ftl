<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign myTitle = "Automatické párování" />
<#assign hideToolbar=true/>
<#assign title3=""/>
<#include "/i-header-edit.ftl"/>

<#if automatickeParovaniOk?? && automatickeParovaniOk>
    <div class="alert alert-success">
        <strong>Automatické párování úspěšně dokončeno.</strong>
    </div>

    <@edit.backButton/>
<#else>

    <#if automatickeParovaniNeuspesne??>
        <div class="alert alert-danger">
            <ul class="flexibee-errors">
                <li>${automatickeParovaniNeuspesne}</li>
            </ul>
        </div>
        <@edit.backButton/>

    <#else>
        <@edit.beginForm />
            <p>Provést automatické párování plateb?</p>
            <@edit.okCancelButton />
        <@edit.endForm />
    </#if>
</#if>


<#include "/i-footer-edit.ftl" />
</#escape>