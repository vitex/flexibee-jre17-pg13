<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
Ext.onReady(function(){
    <#assign writeSupport = true />

    if (!globalUrl) {
        var globalUrl = '';
    }

    // load data from server
    var winstromProxy = new Ext.data.HttpProxy({
        url: globalUrl + "/c/${it.companyResource.companyId}/${it.evidenceName}<#if it.listParameters.filterRules?exists>/(${it.listParameters.filterRules?url})</#if>",
    });

    Ext.Ajax.defaultHeaders = {
        'Accept': 'application/json'
    };


    Ext.Ajax.extraParams = {
        'add-row-count': true,
        <#if it.detailParameters.info?exists>detail: '${it.detailParameters.info}'</#if>
    };

    <#include "i-extjs.ftl"/>

<#if writeSupport>
    // The new DataWriter component.
    var winstromWriter = new Ext.data.JsonWriter();
</#if>

    // Typical JsonReader.  Notice additional meta-data params for defining the core attributes of your json-response
    var winstromReader = new Ext.data.JsonReader({
        root: 'winstrom.${it.evidenceResource.tagName}',
        totalProperty: 'winstrom.@rowCount',
        idProperty: 'id',
    }, winstrom${it.evidenceResource.tagName}Fields);


    // create the Data Store for WinStrom ${it.name}
    var winstromStore = new Ext.data.Store({
        remoteSort : true,
        proxy      : winstromProxy,
        reader     : winstromReader,
<#if writeSupport>        writer     : winstromWriter,
        autoSave   : true,
</#if>        restful    : true,
        fields     : winstrom${it.evidenceResource.tagName}Fields,
    });

    var grid = new Ext.grid.<#if writeSupport>Editor</#if>GridPanel({
        el             : 'content',
        width          : '98%',
        height         : 550,
        title          : '${it.name}: WinStrom',
        store          : winstromStore,
        trackMouseOver : true,
        loadMask       : true,
<#if writeSupport>        sm             : new Ext.grid.RowSelectionModel({singleSelect:true}),
</#if>        // grid columns
        columns        : winstrom${it.evidenceResource.tagName}Columns,
<#if writeSupport>        tbar: [{
            text: 'Add',
            iconCls: 'silk-add',
            handler: onAdd
        }, '-', {
            text: 'Delete',
            iconCls: 'silk-delete',
            handler: onDelete
        }, '-'],
</#if>

        // paging bar on the bottom
        bbar: new Ext.PagingToolbar({
            pageSize: ${it.defaultLimit},
            store: winstromStore,
            displayInfo: false,
        })
    });

    // custom renderers
    function renderBoolean(value, p, r) {
        if (value) {
            return 'Ano';
        }
        return 'Ne';
    }

    function renderSelect(value, p, r) {
        if (value) {
            return grid.getColumnModel().getColumnById(p.id).possibleValues[value];
        } else {
            return '';
        }
    }

    function renderRelation(value, p, r) {
        if (value) {
            return value['@showAs'];
        }
    }

    function renderMoney(value, p, r) {
        return value;
    }

    function renderDate(value, p, r) {
        return value;
    }
    function renderDateTime(value, p, r) {
        return value;
    }

<#if writeSupport>
    /**
     * onAdd
     */
    function onAdd(btn, ev) {
        var u = new grid.store.recordType({});
        grid.store.insert(0, u);
    }
    /**
     * onDelete
     */
    function onDelete() {
        var rec = grid.getSelectionModel().getSelected();
        if (!rec) {
            return false;
        }
        grid.store.remove(rec);
    }
</#if>

    // render it
    grid.render();

    // trigger the data store load
    winstromStore.load();
});
</#escape>
