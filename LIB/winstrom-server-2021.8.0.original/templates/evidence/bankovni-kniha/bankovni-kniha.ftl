<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-list.ftl"/>
<#import "/l-edits.ftl" as edit />

<#if chyba??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            ${chyba}
        </ul>
    </div>
</#if>

<@edit.beginForm />

    <div class="flexibee-steps">
    <h2 class="flexibee-step1">${lang('labels', 'bankovniDenik.zvolteTypDokl', 'Bankovní účet:')}</h2>
    <div>
        <div class="form-group">
              <div class="col-sm-2">
                <label for="banka">${lang('labels', 'bankovniDenik.zvolteTypDokl', 'Bankovní účet:')}</label>
              </div>
                <div class="col-sm-10">
                <select name="banka" class="flexibee-rounded-form-select flexibee-inp form-control">
                <#list banky as b>
                    <option value="${b.id}" <#if (vybranaBanka?? && vybranaBanka == b.id)>selected</#if>>${b.name}</option>
                </#list>
                </select>
            </div>
        </div>
    </div>

<div class="removeStepsBorder">
    <h2 class="flexibee-step2">${lang('labels', 'vybraneDatumyVystaveni', 'Datum vystavení')}</h2>
    <div>
        <div class="form-group">
            <div class="col-sm-2">
                <label for="datVystOd">${lang('labels', 'od', 'Od')}:</label>
            </div>
            <div class="col-sm-10">
                <input type="text" format="dd.mm.yyyy" name="datVystOd" id="datVystOd" value="<#if datVystOd??>${datVystOd}</#if>" class="flexibee-edit-date flexibee-inp form-control">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-2">
                <label for="datVystDo">${lang('labels', 'do', 'Do')}:</label>
            </div>
            <div class="col-sm-10">
                <input type="text" format="dd.mm.yyyy" name="datVystDo" id="datVystDo" value="<#if datVystDo??>${datVystDo}</#if>" class="flexibee-edit-date flexibee-inp form-control">
                    </div>
                </div>
    </div>

    </div>
        </div> <#-- flexibee-steps -->
        <@edit.okCancelButton showCancel=false />
<@edit.endForm />

<script type="text/javascript">
    $(function() {
        $("form").submit(function() {
            <#--
              - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
              - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
              -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>