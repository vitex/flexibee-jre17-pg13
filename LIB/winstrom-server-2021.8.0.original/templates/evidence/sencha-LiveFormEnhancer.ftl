<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

var token = '';
var tokenNode = Ext.dom.Query.selectNode('meta[name="CSRF-Token"]');
if (tokenNode) {
    token = Ext.get(tokenNode).getAttribute('content');
}

Ext.define('FlexiBee.${it.senchaName}.LiveFormEnhancer', {
    _conn: Ext.create('Ext.data.Connection', {
        autoAbort: true,
        extraParams: {
            'dry-run': 'true',
            'as-gui': 'true',
            'no-http-errors': 'true',
            'use-internal-id': 'true',
            'stitky-as-ids': 'true',
            'token': token
        },

        defaultHeaders: {
            'Accept': 'application/json'
        }
    }),

    _writer: Ext.create('FlexiBee.data.writer.Json', {
        root: 'winstrom.${it.tagName}'
    }),

    _request: function(record, callback) {
        var request = Ext.create('Ext.data.Request', {
            url: '/c/${it.companyId}/${it.tagName}',
            method: 'PUT',
            operation: Ext.create('Ext.data.Operation', {
                records: [record]
            }),
            success: function(response) {
                var json = Ext.decode(response.responseText);
                if (callback) {
                    callback(json);
                }
            }
        });

        this._conn.request(this._writer.write(request));
    },

    _handler: function(field, newValue, oldValue) {
        var me = this;

        var form = field.findParentByType('form').getForm();
        var model = form.getRecord();
        var copy = model.copy();
        form.updateRecord(copy);

        me._request(copy, function(result) {
            var success = result.winstrom.success === "true";

            if (success) {
                // TODO check if this is enough

                var record = result.winstrom.results[0].content["${it.tagName}"];
                Ext.iterate(record, function(key, value) {
                    if (/@internalId$/.test(key)) {
                        record[key.replace('@internalId', '')] = value;
                    }
                });
                form.setValues(record);
            } else {
                // TODO check if this is enough
                // TODO implement the case where the error doesn't belong to any field

                var invalidFields = [];
                Ext.iterate(result.winstrom.results[0].errors, function(error) {
                    var message = Ext.String.trim(error.message.replace(/\[[^\]]*\]$/, ""));
                    var field = error.for;
                    if (field) {
                        invalidFields.push({ id: field, msg: message });
                    }
                });
                if (invalidFields.length > 0) {
                    form.markInvalid(invalidFields);
                }
            }

            // TODO accessibility
        });
    },

    getHandler: function() {
        return Ext.bind(this._handler, this);
    }
});

</#escape>
