<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>


${marker.mark('stitky')}
${marker.mark('primarni')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['firma', 'prijmeni', 'titul', 'jmeno', 'titulZa', 'osloveni', 'primarni'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['firma'])>
        <tr>
            <td><@edit.label name='firma' items=items /></td><td><@edit.place object=object name='firma' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['prijmeni', 'titul'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['prijmeni'])>
            <td><@edit.label name='prijmeni' items=items /></td><td><@edit.place object=object name='prijmeni' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['titul'])>
            <td><@edit.label name='titul' items=items /></td><td><@edit.place object=object name='titul' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['jmeno', 'titulZa'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['jmeno'])>
            <td><@edit.label name='jmeno' items=items /></td><td><@edit.place object=object name='jmeno' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['titulZa'])>
            <td><@edit.label name='titulZa' items=items /></td><td><@edit.place object=object name='titulZa' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['osloveni'])>
        <tr>
            <td><@edit.label name='osloveni' items=items /></td><td><@edit.place object=object name='osloveni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['primarni'])>
        <tr>
            <td><@edit.label name='primarni' items=items /></td><td><@edit.place object=object name='primarni' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['funkce', 'oddeleni', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'odesilatFak', 'rodCis', 'odesilatObj', 'datNaroz', 'odesilatNab', 'odesilatPpt', 'odesilatPok', 'odesilatSkl', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['funkce', 'oddeleni', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'zaklInformacePN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['odesilatFak', 'rodCis', 'odesilatObj', 'datNaroz', 'odesilatNab', 'odesilatPpt', 'odesilatPok', 'odesilatSkl'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'upresneniPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['funkce', 'oddeleni', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['funkce', 'oddeleni', 'ic', 'dic', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['funkce', 'oddeleni'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['funkce'])>
                        <td><@edit.label name='funkce' items=items /></td><td><@edit.place object=object name='funkce' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['oddeleni'])>
                        <td><@edit.label name='oddeleni' items=items /></td><td><@edit.place object=object name='oddeleni' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ic', 'dic'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ic'])>
                        <td><@edit.label name='ic' items=items /></td><td><@edit.place object=object name='ic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dic'])>
                        <td><@edit.label name='dic' items=items /></td><td><@edit.place object=object name='dic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ulice'])>
                    <tr>
                        <td><@edit.label name='ulice' items=items /></td><td colspan="3"><@edit.place object=object name='ulice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mesto', 'psc'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mesto'])>
                        <td><@edit.label name='mesto' items=items /></td><td><@edit.place object=object name='mesto' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['psc'])>
                        <td><@edit.label name='psc' items=items /></td><td><@edit.place object=object name='psc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stat'])>
                    <tr>
                        <td><@edit.label name='stat' items=items /></td><td colspan="3"><@edit.place object=object name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['tel'])>
                    <tr>
                        <td><@edit.label name='tel' items=items /></td><td><@edit.place object=object name='tel' items=items marker=marker  type='tel'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mobil', 'fax'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mobil'])>
                        <td><@edit.label name='mobil' items=items /></td><td><@edit.place object=object name='mobil' items=items marker=marker  type='tel'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['fax'])>
                        <td><@edit.label name='fax' items=items /></td><td><@edit.place object=object name='fax' items=items marker=marker  type='fax'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['email', 'www'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['email'])>
                        <td><@edit.label name='email' items=items /></td><td><@edit.place object=object name='email' items=items marker=marker  type='email'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['www'])>
                        <td><@edit.label name='www' items=items /></td><td><@edit.place object=object name='www' items=items marker=marker  type='url'/></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['odesilatFak', 'rodCis', 'odesilatObj', 'datNaroz', 'odesilatNab', 'odesilatPpt', 'odesilatPok', 'odesilatSkl'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['odesilatFak', 'rodCis', 'odesilatObj', 'datNaroz', 'odesilatNab', 'odesilatPpt', 'odesilatPok', 'odesilatSkl'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['odesilatFak', 'rodCis'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['odesilatFak'])>
                        <td><@edit.label name='odesilatFak' items=items /></td><td><@edit.place object=object name='odesilatFak' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['rodCis'])>
                        <td><@edit.label name='rodCis' items=items /></td><td><@edit.place object=object name='rodCis' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['odesilatObj', 'datNaroz'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['odesilatObj'])>
                        <td><@edit.label name='odesilatObj' items=items /></td><td><@edit.place object=object name='odesilatObj' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['datNaroz'])>
                        <td><@edit.label name='datNaroz' items=items /></td><td><@edit.place object=object name='datNaroz' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['odesilatNab'])>
                    <tr>
                        <td><@edit.label name='odesilatNab' items=items /></td><td><@edit.place object=object name='odesilatNab' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['odesilatPpt'])>
                    <tr>
                        <td><@edit.label name='odesilatPpt' items=items /></td><td><@edit.place object=object name='odesilatPpt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['odesilatPok'])>
                    <tr>
                        <td><@edit.label name='odesilatPok' items=items /></td><td><@edit.place object=object name='odesilatPok' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['odesilatSkl'])>
                    <tr>
                        <td><@edit.label name='odesilatSkl' items=items /></td><td><@edit.place object=object name='odesilatSkl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />


<@edit.generateNaiveJavascript object=object it=it />


<#if isSlowMobile == false>
<script type="text/javascript">
    $(function() {

    <#assign pscUrl = formListUrl(it.companyResource, 'PSC')!"" />
    $("#flexibee-inp-psc, #flexibee-inp-mesto").suggestPsc('${pscUrl}', '#flexibee-inp-stat', function(data) {
            $("#flexibee-inp-mesto").val(data.nazev).change();
            $("#flexibee-inp-psc").val(data.kod).change();
    });
    });
</script>
</#if>

<#include "/parts/i-dalsi-informace.ftl" />




<#include "/i-footer-edit.ftl" />
</#escape>
