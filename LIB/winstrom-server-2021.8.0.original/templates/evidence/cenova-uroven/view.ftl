<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['kod', 'nazev', 'docasnost', 'platiOdData', 'platiDoData', 'stredisko', 'rucneVybrat'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td colspan="3"><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['docasnost'], it)>
        <tr>
            <td><@tool.label name='docasnost' items=items />:</td><td colspan="3"><@tool.place object=object name='docasnost' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['platiOdData', 'platiDoData'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['platiOdData'], it)>
            <td><@tool.label name='platiOdData' items=items />:</td><td><@tool.place object=object name='platiOdData' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['platiDoData'], it)>
            <td><@tool.label name='platiDoData' items=items />:</td><td><@tool.place object=object name='platiDoData' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['stredisko', 'rucneVybrat'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['stredisko'], it)>
            <td><@tool.label name='stredisko' items=items />:</td><td><@tool.place object=object name='stredisko' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['rucneVybrat'], it)>
            <td><@tool.label name='rucneVybrat' items=items />:</td><td><@tool.place object=object name='rucneVybrat' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['zaokrJakK', 'zaokrNaK', 'procZakl', 'typCenyVychoziK', 'typVypCenyK', 'typCenyVychozi25K', 'typVypCeny25K', 'limMnoz2', 'procento2', 'limMnoz3', 'procento3', 'limMnoz4', 'procento4', 'limMnoz5', 'procento5', 'a', 'vsechnySkupZboz', 'vsechnySkupFir', 'popis', 'poznam', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['zaokrJakK', 'zaokrNaK', 'procZakl', 'typCenyVychoziK', 'typVypCenyK', 'typCenyVychozi25K', 'typVypCeny25K', 'limMnoz2', 'procento2', 'limMnoz3', 'procento3', 'limMnoz4', 'procento4', 'limMnoz5', 'procento5'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'cenHladEBCenotvorbaPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['a', 'vsechnySkupZboz', 'vsechnySkupFir'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'cenHladEBVybraneSkupinyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-5" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-6" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['zaokrJakK', 'zaokrNaK', 'procZakl', 'typCenyVychoziK', 'typVypCenyK', 'typCenyVychozi25K', 'typVypCeny25K', 'limMnoz2', 'procento2', 'limMnoz3', 'procento3', 'limMnoz4', 'procento4', 'limMnoz5', 'procento5'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['zaokrJakK', 'zaokrNaK', 'procZakl', 'typCenyVychoziK', 'typVypCenyK', 'typCenyVychozi25K', 'typVypCeny25K', 'limMnoz2', 'procento2', 'limMnoz3', 'procento3', 'limMnoz4', 'procento4', 'limMnoz5', 'procento5'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['zaokrJakK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrJakK' items=items />:</td><td><@tool.place object=object name='zaokrJakK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaokrNaK', 'procZakl'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['zaokrNaK'], it)>
                        <td><@tool.label name='zaokrNaK' items=items />:</td><td><@tool.place object=object name='zaokrNaK' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['procZakl'], it)>
                        <td><@tool.label name='procZakl' items=items />:</td><td><@tool.place object=object name='procZakl' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typCenyVychoziK', 'typVypCenyK'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['typCenyVychoziK'], it)>
                        <td><@tool.label name='typCenyVychoziK' items=items />:</td><td><@tool.place object=object name='typCenyVychoziK' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['typVypCenyK'], it)>
                        <td><@tool.label name='typVypCenyK' items=items />:</td><td><@tool.place object=object name='typVypCenyK' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typCenyVychozi25K', 'typVypCeny25K'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['typCenyVychozi25K'], it)>
                        <td><@tool.label name='typCenyVychozi25K' items=items />:</td><td><@tool.place object=object name='typCenyVychozi25K' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['typVypCeny25K'], it)>
                        <td><@tool.label name='typVypCeny25K' items=items />:</td><td><@tool.place object=object name='typVypCeny25K' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['limMnoz2', 'procento2'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['limMnoz2'], it)>
                        <td><@tool.label name='limMnoz2' items=items />:</td><td><@tool.place object=object name='limMnoz2' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['procento2'], it)>
                        <td><@tool.label name='procento2' items=items />:</td><td><@tool.place object=object name='procento2' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['limMnoz3', 'procento3'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['limMnoz3'], it)>
                        <td><@tool.label name='limMnoz3' items=items />:</td><td><@tool.place object=object name='limMnoz3' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['procento3'], it)>
                        <td><@tool.label name='procento3' items=items />:</td><td><@tool.place object=object name='procento3' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['limMnoz4', 'procento4'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['limMnoz4'], it)>
                        <td><@tool.label name='limMnoz4' items=items />:</td><td><@tool.place object=object name='limMnoz4' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['procento4'], it)>
                        <td><@tool.label name='procento4' items=items />:</td><td><@tool.place object=object name='procento4' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['limMnoz5', 'procento5'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['limMnoz5'], it)>
                        <td><@tool.label name='limMnoz5' items=items />:</td><td><@tool.place object=object name='limMnoz5' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['procento5'], it)>
                        <td><@tool.label name='procento5' items=items />:</td><td><@tool.place object=object name='procento5' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['a', 'vsechnySkupZboz', 'vsechnySkupFir'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['a'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['a'], it)>
                    <tr>
                        <td><@tool.label name='a' items=items />:</td><td><@tool.place object=object name='a' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canShowAtLeastOne(items, object, ['vsechnySkupZboz', 'vsechnySkupFir'], it)>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canShowAtLeastOne(items, object, ['vsechnySkupZboz'], it)>
                        <li class="active"><a href="#view-fragment-2" data-toggle="tab">Skupiny zboží</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['vsechnySkupFir'], it)>
                        <li><a href="#view-fragment-3" data-toggle="tab">Skupiny firem</a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canShowAtLeastOne(items, object, ['vsechnySkupZboz'], it)>
                        <div id="view-fragment-2" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['vsechnySkupZboz'], it)>
                            <table class="flexibee-tbl-1">
                                <#if canShowAtLeastOne(items, object, ['vsechnySkupZboz'], it)>
                                <tr>
                                    <td><@tool.label name='vsechnySkupZboz' items=items />:</td><td><@tool.place object=object name='vsechnySkupZboz' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['vsechnySkupFir'], it)>
                        <div id="view-fragment-3" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['vsechnySkupFir'], it)>
                            <table class="flexibee-tbl-1">
                                <#if canShowAtLeastOne(items, object, ['vsechnySkupFir'], it)>
                                <tr>
                                    <td><@tool.label name='vsechnySkupFir' items=items />:</td><td><@tool.place object=object name='vsechnySkupFir' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-6" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>