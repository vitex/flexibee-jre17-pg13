<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>
<#import "/l-edits.ftl" as edit />

<#if chyba??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            ${chyba}
        </ul>
    </div>
</#if>

<@edit.beginForm />

    <div class="flexibee-steps">
    <h2 class="flexibee-step1">${lang('labels', 'ucetniDenik.infoDatum', 'Zadejte časový rozsah analýzy ....')}</h2>
    <div>
        <table class="flexibee-tbl-1">
            <tr>
                <td><label>${lang('labels', 'ucetniDenik.datumUctovani', 'Datum účtování od-do:')}</label></td>
                <td><input type="text" format="dd.mm.yyyy" name="datUctoOd" id="datUctoOd" value="<#if datUctoOd??>${datUctoOd}</#if>" class="flexibee-edit-date flexibee-inp form-control"></td>
                <td><input type="text" format="dd.mm.yyyy" name="datUctoDo" id="datUctoDo" value="<#if datUctoDo??>${datUctoDo}</#if>" class="flexibee-edit-date flexibee-inp form-control"></td>
            </tr>
            <tr>
                <td><label>${lang('labels', 'ucetniDenik.datumVystaveni', 'Datum vystavení dokladu od-do:')}</label></td>
                <td><input type="text" format="dd.mm.yyyy" name="datVystOd" id="datVystOd" value="<#if datVystOd??>${datVystOd}</#if>" class="flexibee-edit-date flexibee-inp form-control"></td>
                <td><input type="text" format="dd.mm.yyyy" name="datVystDo" id="datVystDo" value="<#if datVystDo??>${datVystDo}</#if>" class="flexibee-edit-date flexibee-inp form-control"></td>
            </tr>
            <tr>
                <td><label>${lang('labels', 'ucetniDenik.datumPlneni', 'Datum zdanitelného plnění od-do:')}</label></td>
                <td><input type="text" format="dd.mm.yyyy" name="duzpUctoOd" id="duzpUctoOd" value="<#if duzpUctoOd??>${duzpUctoOd}</#if>" class="flexibee-edit-date flexibee-inp form-control"></td>
                <td><input type="text" format="dd.mm.yyyy" name="duzpUctoDo" id="duzpUctoDo" value="<#if duzpUctoDo??>${duzpUctoDo}</#if>" class="flexibee-edit-date flexibee-inp form-control"></td>
            </tr>
            <tr>
                <td><label>${lang('labels', 'ucetniDenik.okUctovano', 'OK účtováno:')}</label></td>
                <td>
                    <select name="okUctovano" class="flexibee-rounded-form-select flexibee-inp form-control">
                    <#list okUctovanoValues as val>
                        <option value="${val.id}" <#if (vybranOkUctovano?? && vybranOkUctovano == val.id)>selected</#if>>${val.name}</option>
                    </#list>
                    </select>
                </td>
            </tr>
        </table>
    </div>

    <h2 class="flexibee-step2">${lang('labels', 'dokladEBIntUcty', 'Účty')}</h2>
    <div>
        <ul class="flexibee-rounded flexibee-rounded-form">
            <table class="flexibee-tbl-1">
                <tr>
                    <td valign="top"><label>${lang('labels', 'ucetniDenik.uctyMD', 'Účty má dáti:')}</label></td>
                    <td valign="top"><input type="text" name="ucetMd" id="ucetMd" value="<#if ucetMd??>${ucetMd}</#if>" class="flexibee-inp form-control"></td>
                    <td>
                        <select name="uctyMd" multiple="yes" size="3">
                        <#list ucetMdValues as ucet>
                            <option value="${ucet.id}">${ucet.name}</option>
                        </#list>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><label>${lang('labels', 'ucetniDenik.uctyDal', 'Účty dal:')}</label></td>
                    <td valign="top"><input type="text" name="ucetDal" id="ucetDal" value="<#if ucetDal??>${ucetDal}</#if>" class="flexibee-inp form-control"></td>
                    <td>
                        <select name="uctyDal" multiple="yes" size="3">
                        <#list ucetDalValues as ucet>
                            <option value="${ucet.id}">${ucet.name}</option>
                        </#list>
                        </select>
                    </td>
                </tr>
            </table>
    </ul>
    </div>

    <h2 class="flexibee-step3">${lang('labels', 'ucetni.denik.castky', 'Částky')}</h2>
    <div>
        <ul class="flexibee-rounded flexibee-rounded-form">
            <table class="flexibee-tbl-1">
                <tr>
                    <td><label>${lang('labels', 'ucetniDenik.castka', 'Částka od-do:')}</label></td>
                    <td><input type="number" name="castkaOd" class="flexibee-inp form-control" <#if castkaOd??>value="${castkaOd}"</#if>></td>
                    <td><input type="number" name="castkaDo" class="flexibee-inp form-control" <#if castkaDo??>value="${castkaDo}"</#if>></td>
                </tr>
                <tr>
                    <td><label>${lang('labels', 'ucetniDenik.castkaMen', 'Částka v měně od-do:')}</label></td>
                    <td><input type="number" name="castkaMenOd" class="flexibee-inp form-control" <#if castkaMenOd??>value="${castkaMenOd}"</#if>></td>
                    <td><input type="number" name="castkaMenDo" class="flexibee-inp form-control" <#if castkaMenDo??>value="${castkaMenDo}"</#if>></td>
                </tr>
            </table>
                </ul>
    </div>

    <h2 class="flexibee-step4">${lang('labels', 'ostatniPN', 'Ostatní')}</h2>
    <div>
        <ul class="flexibee-rounded flexibee-rounded-form">
            <table class="flexibee-tbl-1">
                <tr>
                    <td valign="top"><label>${lang('labels', 'ucetniDenik.vybraneModuly', 'Vybrané moduly:')}</label></td>
                    <td>
                        <select name="moduly" multiple="yes" size="3" class=" form-control">
                        <#list modulyValues as ucet>
                            <option value="${ucet.id}">${ucet.name}</option>
                        </#list>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><label>${lang('labels', 'ucetniDenik.vybranaStrediska', 'Vybraná střediska:')}</label></td>
                    <td>
                        <select name="strediska" multiple="yes" size="3" class=" form-control">
                        <#list strediskaValues as ucet>
                            <option value="${ucet.id}">${ucet.name}</option>
                        </#list>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><label>${lang('labels', 'ucetniDenik.vybraneZakazky', 'Vybrané zakázky:')}</label></td>
                    <td>
                        <select name="zakazky" multiple="yes" size="3" class=" form-control">
                        <#list zakazkyValues as ucet>
                            <option value="${ucet.id}">${ucet.name}</option>
                        </#list>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><label>${lang('labels', 'ucetniDenik.vybraneFirmy', 'Vybrané firmy:')}</label></td>
                    <td>
                        <select name="firmy" multiple="yes" size="3" class=" form-control">
                        <#list firmyValues as ucet>
                            <option value="${ucet.id}">${ucet.name}</option>
                        </#list>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td valign="top"><label>${lang('labels', 'ucetniDenik.vybraneCinnosti', 'Vybrané činnosti:')}</label></td>
                    <td>
                        <select name="cinnosti" multiple="yes" size="3" class=" form-control">
                        <#list cinnostiValues as ucet>
                            <option value="${ucet.id}">${ucet.name}</option>
                        </#list>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td><label>${lang('labels', 'ucetniDenik.popis', '')}</label></td>
                    <td><input type="text" name="popis" id="popis" value="<#if popis??>${popis}</#if>" class="flexibee-inp form-control"></td>
                </tr>
            </table>
    </ul>
    </div>

    </div> <#-- flexibee-steps -->
    <@edit.okCancelButton showCancel=false />
<@edit.endForm />

<script type="text/javascript">
    $(function() {
        $("form").submit(function() {
            <#--
              - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
              - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
              -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>