<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['kod', 'nazev'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['vsbAdr', 'vsbKatalog', 'vsbSkl', 'vsbFav', 'vsbPhl', 'vsbFap', 'vsbZav', 'vsbBan', 'vsbPok', 'vsbInt', 'vsbMaj', 'vsbMzd', 'vsbNap', 'vsbObp', 'vsbPpp', 'vsbNav', 'vsbObv', 'vsbPpv'], it)>
    <h4>${lang('labels', 'vybKliceViditelnost')}</h4>
    <div id="view-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['vsbAdr', 'vsbKatalog', 'vsbSkl', 'vsbFav', 'vsbPhl', 'vsbFap', 'vsbZav', 'vsbBan', 'vsbPok', 'vsbInt', 'vsbMaj', 'vsbMzd', 'vsbNap', 'vsbObp', 'vsbPpp', 'vsbNav', 'vsbObv', 'vsbPpv'], it)>
        <table class="flexibee-tbl-1">
            <col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" />
            <#if canShowAtLeastOne(items, object, ['vsbAdr', 'vsbKatalog', 'vsbSkl'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbAdr'], it)>
                <td><@tool.label name='vsbAdr' items=items />:</td><td><@tool.place object=object name='vsbAdr' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbKatalog'], it)>
                <td><@tool.label name='vsbKatalog' items=items />:</td><td><@tool.place object=object name='vsbKatalog' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbSkl'], it)>
                <td><@tool.label name='vsbSkl' items=items />:</td><td><@tool.place object=object name='vsbSkl' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbFav', 'vsbPhl'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbFav'], it)>
                <td><@tool.label name='vsbFav' items=items />:</td><td><@tool.place object=object name='vsbFav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPhl'], it)>
                <td><@tool.label name='vsbPhl' items=items />:</td><td><@tool.place object=object name='vsbPhl' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbFap', 'vsbZav'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbFap'], it)>
                <td><@tool.label name='vsbFap' items=items />:</td><td><@tool.place object=object name='vsbFap' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbZav'], it)>
                <td><@tool.label name='vsbZav' items=items />:</td><td><@tool.place object=object name='vsbZav' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbBan', 'vsbPok', 'vsbInt'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbBan'], it)>
                <td><@tool.label name='vsbBan' items=items />:</td><td><@tool.place object=object name='vsbBan' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPok'], it)>
                <td><@tool.label name='vsbPok' items=items />:</td><td><@tool.place object=object name='vsbPok' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbInt'], it)>
                <td><@tool.label name='vsbInt' items=items />:</td><td><@tool.place object=object name='vsbInt' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbMaj', 'vsbMzd'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbMaj'], it)>
                <td><@tool.label name='vsbMaj' items=items />:</td><td><@tool.place object=object name='vsbMaj' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbMzd'], it)>
                <td><@tool.label name='vsbMzd' items=items />:</td><td><@tool.place object=object name='vsbMzd' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbNap', 'vsbObp', 'vsbPpp'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbNap'], it)>
                <td><@tool.label name='vsbNap' items=items />:</td><td><@tool.place object=object name='vsbNap' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbObp'], it)>
                <td><@tool.label name='vsbObp' items=items />:</td><td><@tool.place object=object name='vsbObp' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPpp'], it)>
                <td><@tool.label name='vsbPpp' items=items />:</td><td><@tool.place object=object name='vsbPpp' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['vsbNav', 'vsbObv', 'vsbPpv'], it)>
            <tr>
                <#if canShowAtLeastOne(items, object, ['vsbNav'], it)>
                <td><@tool.label name='vsbNav' items=items />:</td><td><@tool.place object=object name='vsbNav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbObv'], it)>
                <td><@tool.label name='vsbObv' items=items />:</td><td><@tool.place object=object name='vsbObv' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canShowAtLeastOne(items, object, ['vsbPpv'], it)>
                <td><@tool.label name='vsbPpv' items=items />:</td><td><@tool.place object=object name='vsbPpv' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canShowAtLeastOne(items, object, ['popis', 'poznam', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li class="active"><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-2" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>