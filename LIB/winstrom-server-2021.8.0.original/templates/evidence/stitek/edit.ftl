<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'nazev'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['vsbAdr', 'vsbKatalog', 'vsbSkl', 'vsbFav', 'vsbPhl', 'vsbFap', 'vsbZav', 'vsbBan', 'vsbPok', 'vsbInt', 'vsbMaj', 'vsbMzd', 'vsbNap', 'vsbObp', 'vsbPpp', 'vsbNav', 'vsbObv', 'vsbPpv'])>
    <h4>${lang('labels', 'vybKliceViditelnost')}</h4>
    <div id="edit-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['vsbAdr', 'vsbKatalog', 'vsbSkl', 'vsbFav', 'vsbPhl', 'vsbFap', 'vsbZav', 'vsbBan', 'vsbPok', 'vsbInt', 'vsbMaj', 'vsbMzd', 'vsbNap', 'vsbObp', 'vsbPpp', 'vsbNav', 'vsbObv', 'vsbPpv'])>
        <table class="flexibee-tbl-1">
            <col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" />
            <#if canEditAtLeastOne(items, object, ['vsbAdr', 'vsbKatalog', 'vsbSkl'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbAdr'])>
                <td><@edit.label name='vsbAdr' items=items /></td><td><@edit.place object=object name='vsbAdr' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbKatalog'])>
                <td><@edit.label name='vsbKatalog' items=items /></td><td><@edit.place object=object name='vsbKatalog' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbSkl'])>
                <td><@edit.label name='vsbSkl' items=items /></td><td><@edit.place object=object name='vsbSkl' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbFav', 'vsbPhl'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbFav'])>
                <td><@edit.label name='vsbFav' items=items /></td><td><@edit.place object=object name='vsbFav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPhl'])>
                <td><@edit.label name='vsbPhl' items=items /></td><td><@edit.place object=object name='vsbPhl' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbFap', 'vsbZav'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbFap'])>
                <td><@edit.label name='vsbFap' items=items /></td><td><@edit.place object=object name='vsbFap' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbZav'])>
                <td><@edit.label name='vsbZav' items=items /></td><td><@edit.place object=object name='vsbZav' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbBan', 'vsbPok', 'vsbInt'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbBan'])>
                <td><@edit.label name='vsbBan' items=items /></td><td><@edit.place object=object name='vsbBan' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPok'])>
                <td><@edit.label name='vsbPok' items=items /></td><td><@edit.place object=object name='vsbPok' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbInt'])>
                <td><@edit.label name='vsbInt' items=items /></td><td><@edit.place object=object name='vsbInt' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbMaj', 'vsbMzd'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbMaj'])>
                <td><@edit.label name='vsbMaj' items=items /></td><td><@edit.place object=object name='vsbMaj' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbMzd'])>
                <td><@edit.label name='vsbMzd' items=items /></td><td><@edit.place object=object name='vsbMzd' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbNap', 'vsbObp', 'vsbPpp'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbNap'])>
                <td><@edit.label name='vsbNap' items=items /></td><td><@edit.place object=object name='vsbNap' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbObp'])>
                <td><@edit.label name='vsbObp' items=items /></td><td><@edit.place object=object name='vsbObp' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPpp'])>
                <td><@edit.label name='vsbPpp' items=items /></td><td><@edit.place object=object name='vsbPpp' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vsbNav', 'vsbObv', 'vsbPpv'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['vsbNav'])>
                <td><@edit.label name='vsbNav' items=items /></td><td><@edit.place object=object name='vsbNav' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbObv'])>
                <td><@edit.label name='vsbObv' items=items /></td><td><@edit.place object=object name='vsbObv' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['vsbPpv'])>
                <td><@edit.label name='vsbPpv' items=items /></td><td><@edit.place object=object name='vsbPpv' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li class="active"><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-2" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
