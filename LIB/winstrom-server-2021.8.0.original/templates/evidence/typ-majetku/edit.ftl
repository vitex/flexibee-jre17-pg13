<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('typOrganizace')}


<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'nazev'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['druhK', 'jeOdpis', 'primarniUcet', 'odpisovyUcet', 'protiUcetZarazeni', 'opravnyUcet', 'stredisko', 'zakazka', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['druhK', 'jeOdpis', 'primarniUcet', 'odpisovyUcet', 'protiUcetZarazeni', 'opravnyUcet', 'stredisko', 'zakazka'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>Odpisování a účtování</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['druhK', 'jeOdpis', 'primarniUcet', 'odpisovyUcet', 'protiUcetZarazeni', 'opravnyUcet', 'stredisko', 'zakazka'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['druhK', 'jeOdpis', 'primarniUcet', 'odpisovyUcet', 'protiUcetZarazeni', 'opravnyUcet', 'stredisko', 'zakazka'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['druhK'])>
                    <tr>
                        <td><@edit.label name='druhK' items=items /></td><td><@edit.place object=object name='druhK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['jeOdpis'])>
                    <tr>
                        <td><@edit.label name='jeOdpis' items=items /></td><td><@edit.place object=object name='jeOdpis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['primarniUcet'])>
                    <tr>
                        <td><@edit.label name='primarniUcet' items=items /></td><td><@edit.place object=object name='primarniUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['odpisovyUcet'])>
                    <tr>
                        <td><@edit.label name='odpisovyUcet' items=items /></td><td><@edit.place object=object name='odpisovyUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['protiUcetZarazeni'])>
                    <tr>
                        <td><@edit.label name='protiUcetZarazeni' items=items /></td><td><@edit.place object=object name='protiUcetZarazeni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['opravnyUcet'])>
                    <tr>
                        <td><@edit.label name='opravnyUcet' items=items /></td><td><@edit.place object=object name='opravnyUcet' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stredisko'])>
                    <tr>
                        <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zakazka'])>
                    <tr>
                        <td><@edit.label name='zakazka' items=items /></td><td><@edit.place object=object name='zakazka' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
