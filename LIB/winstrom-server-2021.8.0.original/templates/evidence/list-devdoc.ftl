<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Dokumentace výpisu "+lang('dialogs', it.name, it.name) />
<#assign titleId="root"/>
<#include "/header.ftl" />

<p>Dokumentace výpisu ${it.name}</p>
<ul>
    <li>Maximálně záznamů: <#if it.limit?exists && it.limit != 0><code>${it.limit}</code><#else>neomezeno</#if> (<a href="/devdoc/paging">Stránkování</a>)</li>
    <li>Od záznamu č.: <code>${it.start}</code></li>
    <#if it.listParameters.order?exists>
    <li><a href="/devdoc/ordering">Řazení</a>:
        <ul>
        <#list it.listParameters.order as order>
            <li><code>${order.propertyName}</code></li>
        </#list>
        </ul>
    </li>
    </#if>
    <#if it.listParameters.filterRules?exists>
    <li><a href="/devdoc/filters">Filtr</a>: <code>${it.listParameters.filterRules}</code>: ${it.localizedFilter}</li>
    </#if>

    <li><a href="/devdoc/detail-levels">Úroveň detailu</a>: <code>${it.detailParameters.info}</code></li>
</ul>

<ul>
    <li><a href="${it.evidenceName}/properties">${lang('dialogs', 'properties.title', 'Přehled položek')}</a></li>
    <li><a href="${it.evidenceName}/relations">${lang('dialogs', 'relations.title', 'Přehled relací')}</a></li>
    <li><a href="${it.evidenceName}/actions">${lang('dialogs', 'actions.title', 'Přehled akcí')}</a></li>
    <li><a href="${it.evidenceName}/reports">${lang('dialogs', 'reports.title', 'Přehled tiskových sestav')}</a></li>
</ul>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl" />
</#escape>
