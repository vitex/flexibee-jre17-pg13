<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['cenikOtec', 'cenik', 'mnozMj', 'poznam'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['cenikOtec'])>
        <tr>
            <td><@edit.label name='cenikOtec' items=items /></td><td colspan="3"><@edit.place object=object name='cenikOtec' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['cenik'])>
        <tr>
            <td><@edit.label name='cenik' items=items /></td><td colspan="3"><@edit.place object=object name='cenik' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['mnozMj'])>
        <tr>
            <td><@edit.label name='mnozMj' items=items /></td><td colspan="3"><@edit.place object=object name='mnozMj' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['poznam'])>
        <tr>
            <td colspan="4"><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />


<#include "/i-footer-edit.ftl" />
</#escape>
