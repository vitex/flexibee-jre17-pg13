<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['mena', 'nbStred', 'kurzMnozstvi', 'platiOdData'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['mena'], it)>
        <tr>
            <td><@tool.label name='mena' items=items />:</td><td colspan="3"><@tool.place object=object name='mena' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nbStred', 'kurzMnozstvi'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['nbStred'], it)>
            <td><@tool.label name='nbStred' items=items />:</td><td><@tool.place object=object name='nbStred' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['kurzMnozstvi'], it)>
            <td><@tool.label name='kurzMnozstvi' items=items />:</td><td><@tool.place object=object name='kurzMnozstvi' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['platiOdData'], it)>
        <tr>
            <td><@tool.label name='platiOdData' items=items />:</td><td colspan="3"><@tool.place object=object name='platiOdData' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>