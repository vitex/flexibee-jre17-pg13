<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

/**
 * Simple class that adds some defaults (anchor, layout, margin)
 */
Ext.define('FlexiBee.main.FieldPanel', {
    extend: 'Ext.container.Container',
    alias: 'widget.fbFieldPanel',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        layout: {
            xtype: 'container',
            type: 'hbox',
            align: 'stretch'
        },
        anchor: '100%',
        margin: '0 0 5 0'
    }
});

Ext.define('FlexiBee.main.FieldSet', {
    extend: 'Ext.form.FieldSet',
    alias: 'widget.fbFieldSet',

   layout: {
       type: 'vbox',
       align: 'stretch'
   },
   fieldDefaults: {
       labelWidth: 90
   },
   defaults: {
        flex: 1
   }
});

Ext.define('FlexiBee.main.FieldSet', {
    extend: 'Ext.form.FieldSet',
    alias: 'widget.fbFieldSetVertical',

   layout: {
       type: 'hbox',
       align: 'stretch'
   },
    fieldDefaults: {
       labelWidth: 80
    },
    defaults: {
        flex: 1,
        layout: {
            xtype: 'container',
            type: 'hbox',
            align: 'stretch'
        },
        anchor: '100%',
    }
});


Ext.define('FlexiBee.main.MultiFields', {
    extend: 'Ext.container.Container',
    alias: 'widget.fbMultiFields',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },
    defaults: {
        flex: 1
    }
});

Ext.define('FlexiBee.main.TabPanel', {
    extend: 'Ext.tab.Panel',
    alias: 'widget.fbTabPanel',

    bodyPadding: 10,
    plain: true,
    defaults: {
        xtype: 'fbFieldPanel'
    }
});

Ext.define('FlexiBee.main.PreparedPanels', {
    createPopisAndPoznamka: function(property) {
        return {
            title: '${lang('labels', 'textyPN', '')}',
            xtype: 'fbFieldPanel',
            items: [
                property("popis", { xtype: 'textarea', labelAlign: 'top' }),
                property("poznam", { xtype: 'textarea', labelAlign: 'top' })
            ]
        };
    },
    createPopis: function(property) {
        return {
            title: '${lang('labels', 'popisPN', '')}',
            xtype: 'fbFieldPanel',
            items: [
                property("popis", { xtype: 'textarea', labelAlign: 'top' })
            ]
        };
    },
    createLocalizedNazev: function(property, nazev) {
        return property(nazev);
    },
    createPoznamka: function(property) {
        return {
            title: '${lang('labels', 'poznamkaPN', '')}',
            xtype: 'fbFieldPanel',
            items: [
                property("poznam", { xtype: 'textarea', labelAlign: 'top' })
            ]
        };
    },

    createPlatnost: function(property) {
        return {
            title: '${lang('labels', 'spravaPN', '')}',
            xtype: 'fbFieldPanel',
            items: [
                {
                    xtype: 'container',
                    layout: 'fit',
                    html: '${lang('labels', 'ciselnikRokPlatnost', '')}',
                    margin: '0 0 10px 0'
                },
                {
                    xtype: 'fbMultiFields',
                    items: [
                        property("platiOd", { margin: '0 10 0 0' }),
                        property("platiDo")
                    ]
                }
            ]
        };
    },

    createStitky: function(property) {
        return {
            title: '${lang('labels', 'klicePN', '')}',
            xtype: 'fbFieldPanel',
            items: [
                property("stitky")
            ]
        };
    }
});


Ext.define('FlexiBee.${it.senchaName}.BaseEditForm', {
    extend: 'Ext.form.Panel',

    requires: [
        'Ext.ux.form.field.BoxSelect',
        'FlexiBee.main.MultiFields',
        'FlexiBee.main.FieldPanel'
    ],

    buttonAlign: 'center',
    buttons: [
        {
            text: '${lang('actions', 'save', '')}',
            itemId: 'save',
            formBind: true,
            disabled: true
        },
        {
            text: '${lang('actions', 'cancel', '')}',
            itemId: 'cancel'
        }
    ],

    fieldDefaults: {
        labelAlign: 'left',
        msgTarget: 'side',
        labelWidth: 100
    },
    layout: 'anchor',
    defaults: {
        border: false,
        layout: 'anchor',
        anchor: '100%'
    },

    border: 0,
    bodyPadding: 10,

    initComponent: function() {
        var me = this;

        me.items = Ext.create('FlexiBee.${it.senchaName}.Properties').formItems(function(property) {
            var pluginFormItems = FlexiBee.Plugins.invokeSingle(me, 'defineEditFormItems', property);
            if (pluginFormItems) {
                return Ext.Array.filter(pluginFormItems, function(pluginFormItem) {
                    return Ext.isObject(pluginFormItem);
                });
            }
            return me.defineEditFormItems(property);
        });

        me.callParent(arguments);
    },

    defineEditFormItems: function(property) {
        return null;
    }
});

<#import "/l-sencha.ftl" as sencha>
<@sencha.includePlugins path="${it.senchaName}.EditForm"/>

</#escape>
