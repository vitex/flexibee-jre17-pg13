<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-list.ftl" />

<#if isModal>
<div class="modal-header">
 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
 ${title}
 </div>
<div class="modal-body">
</#if>

<#include "/evidence/i-list-begin.ftl" />


<#if it.rowCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th>Typ vazby</th>
        <th>Částka</th>
        <#if it.evidenceResource.hasParent>
        <th>Doklad</th>
        <th>Typ dokladu</th>
        <th>Datum vystavení</th>
        <#else>
        <th>Doklad A</th>
        <th>Doklad B</th>
        </#if>
    </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
    <#if it.evidenceResource.hasParent>
        <#list it.list as object>
            <#assign parentIsA = it.evidenceResource.parentIsA(object)/>

            <#if parentIsA>
                <#assign other = it.evidenceResource.getInstanceForObjectB(object)/>
                <#assign otherType = it.evidenceResource.getEvidenceTypeForObjectB(object)/>
            <#else>
                <#assign other = it.evidenceResource.getInstanceForObjectA(object)/>
                <#assign otherType = it.evidenceResource.getEvidenceTypeForObjectA(object)/>
            </#if>

            <#if formInfo(otherType)??><#assign url = formListUrl(it.companyResource, otherType)+"/" + other.id?c /><#else><#assign url = ''/></#if>

            <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
                <td><#if url != ''><a href="${url}"></#if><@tool.placeList marker=marker object=object name="typVazbyK" items=it.devDocItemsForHtmlMap /><#if url != ''></a></#if></td>
                <td><#if url != ''><a href="${url}"></#if>${object.castka} <@tool.menaSymbol other.mena/><#if url != ''></a></#if></td>
                <td><#if url != ''><a href="${url}"></#if>${other}<#if url != ''></a></#if></td>
                <td><#if formInfo(otherType)??><#if url != ''><a href="${url}"></#if>${formInfo(otherType).name}<#if url != ''></a></#if></#if></td>
                <td><#if url != ''><a href="${url}"></#if>${other.datVyst.time?date}<#if url != ''></a></#if></td>
            </tr>
        </#list>
    <#else>
        <#list it.list as object>
            <#assign a = it.evidenceResource.getInstanceForObjectA(object)/>
            <#assign aType = it.evidenceResource.getEvidenceTypeForObjectA(object)/>
            <#if formInfo(aType)??><#assign urlA = formListUrl(it.companyResource, aType)+"/" + a.id?c /><#else><#assign urlA = ''/></#if>

            <#assign b = it.evidenceResource.getInstanceForObjectB(object)/>
            <#assign bType = it.evidenceResource.getEvidenceTypeForObjectB(object)/>
            <#if formInfo(bType)??><#assign urlB = formListUrl(it.companyResource, bType)+"/" + b.id?c /><#else><#assign urlB = ''/></#if>

            <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
                <td><@tool.placeList marker=marker object=object name="typVazbyK" items=it.devDocItemsForHtmlMap /></td>
                <td>${object.castka} <@tool.menaSymbol a.mena/></td>
                <td>
                    <#if urlA != ''><a href="${urlA}"></#if>
                    ${a} <#if formInfo(aType)??>(${formInfo(aType).name})</#if>
                    <br><small>vystaveno ${a.datVyst.time?date}</small>
                    <#if urlA != ''></a></#if>
                </td>
                <td>
                    <#if urlB != ''><a href="${urlB}"></#if>
                    ${b} <#if formInfo(bType)??>(${formInfo(bType).name})</#if>
                    <br><small>vystaveno ${b.datVyst.time?date}</small>
                    <#if urlB != ''></a></#if>
                </td>
            </tr>
        </#list>
    </#if>
    </tbody>
</table>
</#if>

<#include "/evidence/i-list-end.ftl" />

<#if isModal>
</div>
<div class="modal-footer">
    <input type="button" name="cancel" data-dismiss="modal" <#if isMobile>data-role="none"</#if> value="${actText('ok')}" class="btn btn-primary flexibee-cancel-button"/>
</div>
</#if>


<#include "/i-footer-view.ftl" />
</#escape>
