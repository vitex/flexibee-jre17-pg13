<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

// almost the same as Ext.data.writer.Json
// TODO move to a single location
Ext.define('FlexiBee.data.writer.Json', {
    extend: 'Ext.data.writer.Writer',
    alias: 'writer.fbJson',

    root: undefined,

    allowSingle: true,

    writeRecords: function(request, data) {
        var root = this.root;

        if (this.allowSingle && data.length === 1) {
            data = data[0];
        }

        request.jsonData = request.jsonData || {};
        var jsonData = request.jsonData;
        if (root) {
            var path = root.split(".");
            Ext.iterate(path, function(element) {
                jsonData[element] = {};
                jsonData = jsonData[element];
            });
        }

        if (this.allowSingle && !Ext.isArray(data)) { // TODO currently, this is always true -- but what if it isn't?
            Ext.iterate(data, function(key, value) {
                if (value === null || value === undefined) {
                    delete data[key];
                }
            });
        }

        if (data["stitky"] !== undefined) {
            data["stitky@removeAll"] = 'true';
        }

        Ext.apply(jsonData, data);

        return request;
    }
});

Ext.define('FlexiBee.${it.senchaName}.Proxy', {
    extend: 'Ext.data.proxy.Rest',
    alias: 'proxy.fb${it.senchaName?cap_first}',

    headers: {
        'Accept': 'application/json'
    },

    quickFilters: {},

    buildUrl: function(request) {
        var url = this.callParent(arguments);
        Ext.iterate(this.quickFilters, function(key, value) {
            if (value) {
                url = Ext.urlAppend(url, Ext.String.format("{0}={1}",
                        encodeURIComponent("filter[" + key + "]"),
                        encodeURIComponent(value)));
            }
        });

        var tokenNode = Ext.dom.Query.selectNode('meta[name="CSRF-Token"]');
        var csrfToken = '';

        if (tokenNode) {
            csrfToken = Ext.get(tokenNode).getAttribute('content');
            url = Ext.urlAppend(url, Ext.String.format("{0}={1}", "token", encodeURIComponent(csrfToken)));
        }


        return url;
    }
});

Ext.define('FlexiBee.${it.senchaName}.Model', {
    extend: 'Ext.data.Model',

    idProperty: 'id',

    fields: Ext.create('FlexiBee.${it.senchaName}.Properties').asModelFields(),

    validations: Ext.create('FlexiBee.${it.senchaName}.Properties').asModelValidations(),

    proxy: {
        type: 'fb${it.senchaName?cap_first}',

        url: '/c/${it.companyId}/${it.tagName}',
        extraParams: {
            'add-row-count': 'true',
            'detail': 'full',
            'use-internal-id': 'true',
            'stitky-as-ids': 'true'
        },
        format: 'json',
        simpleSortMode: true,
        // startParam, limitParam, sortParam a directionParam mají defaultní hodnoty kompatibilní s našimi názvy
        pageParam: undefined,
        groupParam: undefined,
        filterParam: undefined,
        reader: {
            type: 'json',
            root: function(data) {
                if (data.winstrom) {
                    return data.winstrom['${it.tagName}'];
                } else {
                    return [];
                }
            },
            totalProperty: function(data) {
                if (data.winstrom) {
                    return data.winstrom['@rowCount'];
                } else {
                    return 0;
                }
            }
        },
        writer: {
            type: 'fbJson',
            root: 'winstrom.${it.tagName}'
        }
    }
});

</#escape>
