<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.${it.senchaName}.Store', {
    extend: 'Ext.data.Store',

    requires: [
        'FlexiBee.${it.senchaName}.Model'
    ],

    model: 'FlexiBee.${it.senchaName}.Model',
    remoteSort: true,
    remoteFilter: true,
    autoLoad: true, // since ExtJS 4.1.1 RC2, this causes quickFilters malfunction (can filter only once)
    buffered: true,
    pageSize: 100,

    changeQuickFilter: function(key, value) {
        this.proxy.quickFilters[key] = value;
        this.load();
    },

    clearQuickFilter: function() {
        if (Ext.Object.getSize(this.proxy.quickFilters) > 0) {
            this.proxy.quickFilters = {};
            this.load();
        }
    }
});

</#escape>
