<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('visible')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['nazev', 'kod'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['typZakazky', 'zodpPrac', 'stredisko', 'kontaktOsoba', 'firma', 'stavZakazky', 'datZahaj', 'datKonec', 'termin', 'splatDny', 'vyhZakazky', 'procVyh', 'rozsah', 'varSym', 'nakladyPredpoklad', 'mena', 'ziskPredpoklad', 'mistUrc', 'popis', 'poznam', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['typZakazky', 'zodpPrac', 'stredisko', 'kontaktOsoba', 'firma', 'stavZakazky', 'datZahaj', 'datKonec', 'termin', 'splatDny', 'vyhZakazky', 'procVyh', 'rozsah', 'varSym', 'nakladyPredpoklad', 'mena', 'ziskPredpoklad', 'mistUrc'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'zaklInformacePN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['typZakazky', 'zodpPrac', 'stredisko', 'kontaktOsoba', 'firma', 'stavZakazky', 'datZahaj', 'datKonec', 'termin', 'splatDny', 'vyhZakazky', 'procVyh', 'rozsah', 'varSym', 'nakladyPredpoklad', 'mena', 'ziskPredpoklad', 'mistUrc'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['typZakazky', 'zodpPrac', 'stredisko', 'kontaktOsoba', 'firma', 'stavZakazky', 'datZahaj', 'datKonec', 'termin', 'splatDny', 'vyhZakazky', 'procVyh', 'rozsah', 'varSym', 'nakladyPredpoklad', 'mena', 'ziskPredpoklad', 'mistUrc'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['typZakazky', 'zodpPrac'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['typZakazky'], it)>
                        <td><@tool.label name='typZakazky' items=items />:</td><td><@tool.place object=object name='typZakazky' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['zodpPrac'], it)>
                        <td><@tool.label name='zodpPrac' items=items />:</td><td><@tool.place object=object name='zodpPrac' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stredisko', 'kontaktOsoba'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['stredisko'], it)>
                        <td><@tool.label name='stredisko' items=items />:</td><td><@tool.place object=object name='stredisko' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['kontaktOsoba'], it)>
                        <td><@tool.label name='kontaktOsoba' items=items />:</td><td><@tool.place object=object name='kontaktOsoba' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['firma', 'stavZakazky'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['firma'], it)>
                        <td><@tool.label name='firma' items=items />:</td><td><@tool.place object=object name='firma' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['stavZakazky'], it)>
                        <td><@tool.label name='stavZakazky' items=items />:</td><td><@tool.place object=object name='stavZakazky' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['datZahaj', 'datKonec'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['datZahaj'], it)>
                        <td><@tool.label name='datZahaj' items=items />:</td><td><@tool.place object=object name='datZahaj' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['datKonec'], it)>
                        <td><@tool.label name='datKonec' items=items />:</td><td><@tool.place object=object name='datKonec' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['termin', 'splatDny'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['termin'], it)>
                        <td><@tool.label name='termin' items=items />:</td><td><@tool.place object=object name='termin' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['splatDny'], it)>
                        <td><@tool.label name='splatDny' items=items />:</td><td><@tool.place object=object name='splatDny' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['vyhZakazky', 'procVyh'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['vyhZakazky'], it)>
                        <td><@tool.label name='vyhZakazky' items=items />:</td><td><@tool.place object=object name='vyhZakazky' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['procVyh'], it)>
                        <td><@tool.label name='procVyh' items=items />:</td><td><@tool.place object=object name='procVyh' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['rozsah', 'varSym'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['rozsah'], it)>
                        <td><@tool.label name='rozsah' items=items />:</td><td><@tool.place object=object name='rozsah' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['varSym'], it)>
                        <td><@tool.label name='varSym' items=items />:</td><td><@tool.place object=object name='varSym' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['nakladyPredpoklad', 'mena'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['nakladyPredpoklad'], it)>
                        <td><@tool.label name='nakladyPredpoklad' items=items />:</td><td><@tool.place object=object name='nakladyPredpoklad' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mena'], it)>
                        <td><@tool.label name='mena' items=items />:</td><td><@tool.place object=object name='mena' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ziskPredpoklad', 'mistUrc'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ziskPredpoklad'], it)>
                        <td><@tool.label name='ziskPredpoklad' items=items />:</td><td><@tool.place object=object name='ziskPredpoklad' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mistUrc'], it)>
                        <td><@tool.label name='mistUrc' items=items />:</td><td><@tool.place object=object name='mistUrc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>
