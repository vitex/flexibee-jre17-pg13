<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>
<#import "/l-edits.ftl" as edit />

<#if chyba??>
    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            ${chyba}
        </ul>
    </div>
</#if>

<@edit.beginForm />

    <div class="flexibee-steps">
    <h2 class="flexibee-step1">${lang('buttons', 'parovaniZalohPokladna', 'Pokladna')}</h2>
    <div>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-xs-4">
                    <label for="pokladna" class="control-label">${lang('labels', 'pokladniDenik.zvolteTypDokl', 'Pokladna')}</label>
                </div>
                <div class="col-xs-8">
                        <select name="pokladna" class="flexibee-rounded-form-select flexibee-inp form-control">
                            <#list pokladny as pokl>
                                <option value="${pokl.id}" <#if (vybranaPokladna?? && vybranaPokladna == pokl.id)>selected</#if>>${pokl.name}</option>
                            </#list>
                        </select>
                </div>
            </div>
        </div>
    </div>

<div class="removeStepsBorder">
    <h2 class="flexibee-step2">${lang('labels', 'ucetniDenik.datumVystaveni', 'Datum vystavení dokladu od-do:')}</h2>
    <div>
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-xs-4">
                       <label for="datVystOd" class="control-label">${lang('labels', 'od', 'Od')}</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" format="dd.mm.yyyy" name="datVystOd" id="datVystOd" value="<#if datVystOd??>${datVystOd}</#if>" class="flexibee-edit-date flexibee-inp form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-xs-4">
                    <label for="datVystDo" class="control-label">${lang('labels', 'Do', 'Do')}</label>
                </div>
                <div class="col-xs-8">
                    <input type="text" format="dd.mm.yyyy" name="datVystDo" id="datVystDo" value="<#if datVystDo??>${datVystDo}</#if>" class="flexibee-edit-date flexibee-inp form-control">
                </div>
            </div>
        </div>
    </div>


    </div>
        </div> <#-- flexibee-steps -->
        <@edit.okCancelButton showCancel=false />
<@edit.endForm />

<script type="text/javascript">
    $(function() {
        $("form").submit(function() {
            <#--
              - hack, ale ono se to pořádně řešit nedá; lepší hack používá cookies
              - http://geekswithblogs.net/GruffCode/archive/2010/10/28/detecting-the-file-download-dialog-in-the-browser.aspx
              -->
            setTimeout(function() {
                $.hideWait();
            }, 5000);
        });
    });
</script>

<#include "/i-footer-edit.ftl" />
</#escape>