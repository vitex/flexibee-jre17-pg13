<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#macro generateJavaScript it object polozkyPropName>

<script type="text/javascript">
    function polozkyRady_renderItem(polozka, editId, deleteId) {
        //console.log("rendering ", polozka);

        return '' +
                '<tbody>\n' +
                '    <tr>\n' +
                '        <td>Účetní období:</td><td>' + (polozka.ucetObdobi$kod || '') + '</td>\n' +
                '        <td>Prefix:</td><td>' + (polozka.prefix || '') + '</td>\n' +
                '        <td>Postfix:</td><td>' + (polozka.postfix || '') + '</td>\n' +
                '        <td colspan="2">&nbsp;</td>\n' +
                '        <td><span class="flexibee-js-action flexibee-button" id="' + editId + '" <#if isMobile>data-role="button"</#if>>Upravit<'+'/span><'+'/td>\n' +
                '    </tr>\n' +
                '    <tr>\n' +
                '        <td>Akt. číslo:</td><td>' + (polozka.cisAkt || '') + '</td>\n' +
                '        <td>Počátek:</td><td>' + (polozka.cisPoc || '') + '</td>\n' +
                '        <td>Délka čísla:</td><td>' + (polozka.cisDelka || '') + '</td>\n' +
                '        <td>Zobrazit nuly:</td><td>' + (polozka.zobrazNuly ? 'Ano' : 'Ne') + '</td>\n' +
                '        <td><span class="flexibee-js-action flexibee-button negative" id="' + deleteId + '" <#if isMobile>data-role="button"</#if>>Smazat<'+'/span><'+'/td>\n' +
                '    </tr>' +
                '</tbody>';
    }

    function polozkyRady_postConvertForm2Json(form, json, jsonFromServer) {
        if ($("#flexibee-inp-polozkyRady__ucetObdobi-flexbox_input", form).length) {
            json.ucetObdobi$kod = $("#flexibee-inp-polozkyRady__ucetObdobi-flexbox_input", form).val().replace(/:.+?$/, "");
        } else {
            json.ucetObdobi$kod = $("#flexibee-inp-polozkyRady__ucetObdobi :selected", form).text();
        }

        json.zobrazNuly = (json.zobrazNuly == 'true');

        $("#flexibee-inp-polozkyRady__preview", form).text('');
    }

    function polozkyRady_postConvertJson2Form(json, form) {
        if (json.ucetObdobi$kod && $("#flexibee-inp-polozkyRady__ucetObdobi-flexbox_input", form).length) {
            $("#flexibee-inp-polozkyRady__ucetObdobi-flexbox_input", form).val(json.ucetObdobi$kod).removeClass('watermark');
        }
    }

    $(function() {
        <#if object.polozkyRady??>
            $("#flexibee-rada-polozky").show();
        </#if>

        var initialItems = [
        <#if object.polozkyRady??>
        <#escape x as x?default("")?js_string?html>
            <#list object.polozkyRady as polozka>
            {
                id:             "${polozka.id?c}",
                <#if polozka.ucetObdobi??>
                ucetObdobi:     "${polozka.ucetObdobi.id?c}",
                ucetObdobi$kod: "${polozka.ucetObdobi.kod}",
                </#if>
                prefix:         "${polozka.prefix}",
                postfix:        "${polozka.postfix}",
                cisAkt:         "${polozka.cisAkt}",
                cisPoc:         "${polozka.cisPoc}",
                cisDelka:       "${polozka.cisDelka}",
                zobrazNuly:     <#if polozka.zobrazNuly>true<#else>false</#if>
            }<#if polozka_has_next>,</#if>
            </#list>
        </#escape>
        </#if>
        ];

        <#assign polozkyIt = it.getSubEvidence("rocni-rada") />

        var items = new flexibee.ItemsHelper({
            evidence: {
                name: "${polozkyIt.evidenceResource.evidenceName}",
                props: <@edit.propsTypes restActionHelper=polozkyIt/>,
                prefix: "polozkyRady__"
            },
            page: {
                list: $("#flexibee-rada-polozky-list"),
                edit: $("#flexibee-rada-polozky-edit"),
                activateList: function() { $("#flexibee-rada-polozky ul.ui-tabs-nav li:eq(0) a").click(); },
                activateEdit: function() { $("#flexibee-rada-polozky ul.ui-tabs-nav li:eq(1) a").click(); },
                changeEditTitle: function(title) { $("#flexibee-rada-polozky ul.ui-tabs-nav li:eq(1) a span").text(title); }
            },
            data: {
                renderItem: polozkyRady_renderItem,
                postConvertForm2Json: polozkyRady_postConvertForm2Json,
                postConvertJson2Form: polozkyRady_postConvertJson2Form,
                itemTitle: function(item) { return item.ucetObdobi$kod; },
                isItemEmpty: function(item) { return !(item.cisAkt || item.cisPoc || item.cisDelka) },
                initialData: initialItems
            }
        });

        // ---

        var defaults = {
            <#if object.id?? && object.id != -1>
            id: "${object.id?c}"
            </#if>
        };

        var form = new flexibee.FormHelper({
            evidence: {
                name: "${it.evidenceResource.evidenceName}",
                url: "${it.evidenceResource.listURI}",
                props: <@edit.propsTypes restActionHelper=it/>,
                filledProps: <@edit.filledProps object=object restActionHelper=it/>,
                defaults: defaults
            },
            page: {
                form: $("#flexibee-rada-hlavicka")
            },
            items: {
                ${polozkyPropName}: items
            },
            csrfToken: "${csrfToken}"
        });
        form.start();
    });
</script>

</#macro>

</#escape>