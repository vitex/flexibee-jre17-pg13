<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}


<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['firma', 'nazev', 'tel', 'stat', 'mobil', 'mesto', 'psc', 'fax', 'ulice', 'email', 'eanKod', 'www', 'prijmeni', 'primarni', 'kontaktOsoba'], it)>
    <table class="flexibee-tbl-1">
        <col width="10%" /><col width="23%" /><col width="10%" /><col width="23%" /><col width="10%" /><col width="23%" />
        <#if canShowAtLeastOne(items, object, ['firma'], it)>
        <tr>
            <td><@tool.label name='firma' items=items />:</td><td colspan="5"><@tool.place object=object name='firma' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazev', 'tel'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['nazev'], it)>
            <td><@tool.label name='nazev' items=items />:</td><td colspan="3"><@tool.place object=object name='nazev' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['tel'], it)>
            <td><@tool.label name='tel' items=items />:</td><td><@tool.placeTel object=object name='tel' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['stat', 'mobil'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['stat'], it)>
            <td><@tool.label name='stat' items=items />:</td><td colspan="3"><@tool.place object=object name='stat' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['mobil'], it)>
            <td><@tool.label name='mobil' items=items />:</td><td><@tool.placeTel object=object name='mobil' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['mesto', 'psc', 'fax'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['mesto'], it)>
            <td><@tool.label name='mesto' items=items />:</td><td><@tool.place object=object name='mesto' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['psc'], it)>
            <td><@tool.label name='psc' items=items />:</td><td><@tool.place object=object name='psc' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['fax'], it)>
            <td><@tool.label name='fax' items=items />:</td><td><@tool.place object=object name='fax' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['ulice', 'email'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['ulice'], it)>
            <td><@tool.label name='ulice' items=items />:</td><td colspan="3"><@tool.place object=object name='ulice' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['email'], it)>
            <td><@tool.label name='email' items=items />:</td><td><@tool.placeEmail object=object name='email' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['eanKod', 'www'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['eanKod'], it)>
            <td><@tool.label name='eanKod' items=items />:</td><td colspan="3"><@tool.place object=object name='eanKod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['www'], it)>
            <td><@tool.label name='www' items=items />:</td><td><@tool.place object=object name='www' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['prijmeni', 'primarni'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['prijmeni'], it)>
            <td><@tool.label name='prijmeni' items=items />:</td><td colspan="3"><@tool.place object=object name='prijmeni' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['primarni'], it)>
            <td><@tool.label name='primarni' items=items />:</td><td><@tool.place object=object name='primarni' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kontaktOsoba'], it)>
        <tr>
            <td><@tool.label name='kontaktOsoba' items=items />:</td><td><@tool.place object=object name='kontaktOsoba' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
    <h4>${lang('labels', 'textyPN')}</h4>
    <div id="view-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
        <table class="flexibee-tbl-1">
            <#if canShowAtLeastOne(items, object, ['popis'], it)>
            <tr>
                <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canShowAtLeastOne(items, object, ['poznam'], it)>
            <tr>
                <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>