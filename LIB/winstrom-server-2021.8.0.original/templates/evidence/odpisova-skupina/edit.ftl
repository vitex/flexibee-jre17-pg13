<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('zamek')}


<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'nazev', 'typOdpK', 'zmena'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['typOdpK'])>
        <tr>
            <td><@edit.label name='typOdpK' items=items /></td><td><@edit.place object=object name='typOdpK' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['zmena'])>
        <tr>
            <td><@edit.label name='zmena' items=items /></td><td><@edit.place object=object name='zmena' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['dobaOdp', 'minDobaOdp', 'koefZrOdp1', 'koefZrOdpDalsi', 'koefZrOdpZvCeny', 'prcRoOdp1', 'prcRoOdpDalsi', 'prcRoOdpZvCeny', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['dobaOdp', 'minDobaOdp', 'koefZrOdp1', 'koefZrOdpDalsi', 'koefZrOdpZvCeny', 'prcRoOdp1', 'prcRoOdpDalsi', 'prcRoOdpZvCeny'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'koeficientyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['dobaOdp', 'minDobaOdp', 'koefZrOdp1', 'koefZrOdpDalsi', 'koefZrOdpZvCeny', 'prcRoOdp1', 'prcRoOdpDalsi', 'prcRoOdpZvCeny'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['dobaOdp', 'minDobaOdp', 'koefZrOdp1', 'koefZrOdpDalsi', 'koefZrOdpZvCeny', 'prcRoOdp1', 'prcRoOdpDalsi', 'prcRoOdpZvCeny'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['dobaOdp'])>
                    <tr>
                        <td><@edit.label name='dobaOdp' items=items /></td><td><@edit.place object=object name='dobaOdp' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['minDobaOdp'])>
                    <tr>
                        <td><@edit.label name='minDobaOdp' items=items /></td><td><@edit.place object=object name='minDobaOdp' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['koefZrOdp1'])>
                    <tr>
                        <td><@edit.label name='koefZrOdp1' items=items /></td><td><@edit.place object=object name='koefZrOdp1' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['koefZrOdpDalsi'])>
                    <tr>
                        <td><@edit.label name='koefZrOdpDalsi' items=items /></td><td><@edit.place object=object name='koefZrOdpDalsi' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['koefZrOdpZvCeny'])>
                    <tr>
                        <td><@edit.label name='koefZrOdpZvCeny' items=items /></td><td><@edit.place object=object name='koefZrOdpZvCeny' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['prcRoOdp1'])>
                    <tr>
                        <td><@edit.label name='prcRoOdp1' items=items /></td><td><@edit.place object=object name='prcRoOdp1' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['prcRoOdpDalsi'])>
                    <tr>
                        <td><@edit.label name='prcRoOdpDalsi' items=items /></td><td><@edit.place object=object name='prcRoOdpDalsi' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['prcRoOdpZvCeny'])>
                    <tr>
                        <td><@edit.label name='prcRoOdpZvCeny' items=items /></td><td><@edit.place object=object name='prcRoOdpZvCeny' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
