<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('firmaUUID')}
${marker.mark('platiOdData')}
${marker.mark('sklUcto')}
${marker.mark('intrastat')}
${marker.mark('postMigrace')}
${marker.mark('elPokExport')}
${marker.mark('elPokImport')}
${marker.mark('elPokIni')}
${marker.mark('logoPoziceK')}
${marker.mark('zobrLogoK')}
${marker.mark('avTranKod')}
${marker.mark('avSmerKod')}
${marker.mark('statLegislativa')}
${marker.mark('podvojUcto')}
${marker.mark('moss')}
${marker.mark('omezovatVyberStatu')}


<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <#if canEditAtLeastOne(items, object, ['nazFirmy', 'ic', 'dic', 'vatId', 'stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka', 'postovniShodna', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr', 'fakturacniShodna', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr', 'fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa', 'cinnost', 'finUrad', 'oprPostav', 'oprJmeno', 'oprPrijmeni', 'zastupceKod', 'zastupceNazev', 'zastupceIc', 'spisZnac', 'statLegislativa', 'typOrganizace', 'mena', 'menaDual', 'kurzDualMena', 'platceDph', 'moss', 'omezovatVyberStatu', 'fyzOsoba', 'pravFormaPodnik', 'osobUpravaDph', 'adrKontrolovatFirmy', 'sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK', 'splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav', 'splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap', 'jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej', 'jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK', 'mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka', 'tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad', 'eetProvozovna', 'eetLimit', 'eetModK', 'denniKurzK', 'rocniKurzK', 'manualCisDokl'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['nazFirmy', 'ic', 'dic', 'vatId', 'stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka', 'postovniShodna', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr', 'fakturacniShodna', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr', 'fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'])>
            <li class="active"><a href="#edit-fragment-5" data-toggle="tab"><span>Firma</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['cinnost', 'finUrad', 'oprPostav', 'oprJmeno', 'oprPrijmeni', 'zastupceKod', 'zastupceNazev', 'zastupceIc', 'spisZnac'])>
            <li><a href="#edit-fragment-8" data-toggle="tab"><span>${lang('labels', 'upresneniPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['statLegislativa', 'typOrganizace', 'mena', 'menaDual', 'kurzDualMena', 'platceDph', 'moss', 'omezovatVyberStatu', 'fyzOsoba', 'pravFormaPodnik', 'osobUpravaDph'])>
            <li><a href="#edit-fragment-9" data-toggle="tab"><span>Legislativa</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['adrKontrolovatFirmy', 'sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK', 'splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav', 'splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap', 'jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej', 'jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK', 'mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
            <li><a href="#edit-fragment-36" data-toggle="tab"><span>Moduly</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad', 'eetProvozovna', 'eetLimit', 'eetModK'])>
            <li><a href="#edit-fragment-45" data-toggle="tab"><span>Automatické operace</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['denniKurzK', 'rocniKurzK', 'manualCisDokl'])>
            <li><a href="#edit-fragment-47" data-toggle="tab"><span>${lang('labels', 'ostatniPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['nazFirmy', 'ic', 'dic', 'vatId', 'stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka', 'postovniShodna', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr', 'fakturacniShodna', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr', 'fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'])>
            <div id="edit-fragment-5" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['nazFirmy', 'ic', 'dic', 'vatId'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['nazFirmy'])>
                    <tr>
                        <td><@edit.label name='nazFirmy' items=items /></td><td colspan="5"><@edit.place object=object name='nazFirmy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ic', 'dic', 'vatId'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ic'])>
                        <td><label for="flexibee-inp-ic">IČO:</label></td><td><@edit.place object=object name='ic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dic'])>
                        <td><@edit.label name='dic' items=items /></td><td><@edit.place object=object name='dic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['vatId'])>
                        <td><@edit.label name='vatId' items=items /></td><td><@edit.place object=object name='vatId' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canEditAtLeastOne(items, object, ['stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka', 'postovniShodna', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr', 'fakturacniShodna', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr', 'fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'])>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canEditAtLeastOne(items, object, ['stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka'])>
                        <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>Sídlo / trvalé bydliště</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['postovniShodna', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr'])>
                        <li><a href="#edit-fragment-2" data-toggle="tab"><span>Poštovní adresa</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['fakturacniShodna', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr'])>
                        <li><a href="#edit-fragment-3" data-toggle="tab"><span>Fakturační adresa</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'])>
                        <li><a href="#edit-fragment-4" data-toggle="tab"><span>Fyzická osoba</span></a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canEditAtLeastOne(items, object, ['stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka'])>
                        <div id="edit-fragment-1" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka'])>
                            <table class="flexibee-tbl-1">
                                <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
                                <#if canEditAtLeastOne(items, object, ['stat'])>
                                <tr>
                                    <td><@edit.label name='stat' items=items /></td><td colspan="5"><@edit.place object=object name='stat' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['mesto', 'psc'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['mesto'])>
                                    <td><@edit.label name='mesto' items=items /></td><td><@edit.place object=object name='mesto' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['psc'])>
                                    <td><@edit.label name='psc' items=items /></td><td><@edit.place object=object name='psc' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['uliceNazev', 'cisPop', 'cisOr'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['uliceNazev'])>
                                    <td><@edit.label name='uliceNazev' items=items /></td><td><@edit.place object=object name='uliceNazev' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['cisPop'])>
                                    <td><@edit.label name='cisPop' items=items /></td><td><@edit.place object=object name='cisPop' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['cisOr'])>
                                    <td><@edit.label name='cisOr' items=items /></td><td><@edit.place object=object name='cisOr' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['tel', 'mobil', 'fax'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['tel'])>
                                    <td><@edit.label name='tel' items=items /></td><td><@edit.place object=object name='tel' items=items marker=marker  type='tel'/></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['mobil'])>
                                    <td><@edit.label name='mobil' items=items /></td><td><@edit.place object=object name='mobil' items=items marker=marker  type='tel'/></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['fax'])>
                                    <td><@edit.label name='fax' items=items /></td><td><@edit.place object=object name='fax' items=items marker=marker  type='fax'/></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['www', 'email', 'datovaSchranka'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['www'])>
                                    <td><@edit.label name='www' items=items /></td><td><@edit.place object=object name='www' items=items marker=marker  type='url'/></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['email'])>
                                    <td><@edit.label name='email' items=items /></td><td><@edit.place object=object name='email' items=items marker=marker  type='email'/></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['datovaSchranka'])>
                                    <td><@edit.label name='datovaSchranka' items=items /></td><td><@edit.place object=object name='datovaSchranka' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['postovniShodna', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr'])>
                        <div id="edit-fragment-2" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['postovniShodna', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr'])>
                            <table class="flexibee-tbl-1">
                                <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
                                <#if canEditAtLeastOne(items, object, ['postovniShodna'])>
                                <tr>
                                    <td><label for="flexibee-inp-postovniShodna">Poštovní adresa shodná se sídlem:</label></td><td colspan="5"><@edit.place object=object name='postovniShodna' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['statPostovniAdresy'])>
                                <tr>
                                    <td><@edit.label name='statPostovniAdresy' items=items /></td><td colspan="5"><@edit.place object=object name='statPostovniAdresy' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['postMesto', 'postPsc'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['postMesto'])>
                                    <td><@edit.label name='postMesto' items=items /></td><td><@edit.place object=object name='postMesto' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['postPsc'])>
                                    <td><@edit.label name='postPsc' items=items /></td><td><@edit.place object=object name='postPsc' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['postUliceNazev', 'postCisPop', 'postCisOr'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['postUliceNazev'])>
                                    <td><@edit.label name='postUliceNazev' items=items /></td><td><@edit.place object=object name='postUliceNazev' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['postCisPop'])>
                                    <td><@edit.label name='postCisPop' items=items /></td><td><@edit.place object=object name='postCisPop' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['postCisOr'])>
                                    <td><@edit.label name='postCisOr' items=items /></td><td><@edit.place object=object name='postCisOr' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['fakturacniShodna', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr'])>
                        <div id="edit-fragment-3" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['fakturacniShodna', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr'])>
                            <table class="flexibee-tbl-1">
                                <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
                                <#if canEditAtLeastOne(items, object, ['fakturacniShodna'])>
                                <tr>
                                    <td><label for="flexibee-inp-fakturacniShodna">Fakturační adresa shodná se sídlem:</label></td><td colspan="5"><@edit.place object=object name='fakturacniShodna' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['statFakturacniAdresy'])>
                                <tr>
                                    <td><@edit.label name='statFakturacniAdresy' items=items /></td><td colspan="5"><@edit.place object=object name='statFakturacniAdresy' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['faMesto', 'faPsc'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['faMesto'])>
                                    <td><@edit.label name='faMesto' items=items /></td><td><@edit.place object=object name='faMesto' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['faPsc'])>
                                    <td><@edit.label name='faPsc' items=items /></td><td><@edit.place object=object name='faPsc' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['faUliceNazev', 'faCisPop', 'faCisOr'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['faUliceNazev'])>
                                    <td><@edit.label name='faUliceNazev' items=items /></td><td><@edit.place object=object name='faUliceNazev' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['faCisPop'])>
                                    <td><@edit.label name='faCisPop' items=items /></td><td><@edit.place object=object name='faCisPop' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['faCisOr'])>
                                    <td><@edit.label name='faCisOr' items=items /></td><td><@edit.place object=object name='faCisOr' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'])>
                        <div id="edit-fragment-4" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'])>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canEditAtLeastOne(items, object, ['fyzOsTitul'])>
                                <tr>
                                    <td><@edit.label name='fyzOsTitul' items=items /></td><td><@edit.place object=object name='fyzOsTitul' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['fyzOsJmeno'])>
                                <tr>
                                    <td><@edit.label name='fyzOsJmeno' items=items /></td><td><@edit.place object=object name='fyzOsJmeno' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['fyzOsPrijmeni'])>
                                <tr>
                                    <td><@edit.label name='fyzOsPrijmeni' items=items /></td><td><@edit.place object=object name='fyzOsPrijmeni' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['fyzOsTitulZa'])>
                                <tr>
                                    <td><@edit.label name='fyzOsTitulZa' items=items /></td><td><@edit.place object=object name='fyzOsTitulZa' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['cinnost', 'finUrad', 'oprPostav', 'oprJmeno', 'oprPrijmeni', 'zastupceKod', 'zastupceNazev', 'zastupceIc', 'spisZnac'])>
            <div id="edit-fragment-8" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['cinnost', 'finUrad'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['cinnost'])>
                    <tr>
                        <td><@edit.label name='cinnost' items=items /></td><td><@edit.place object=object name='cinnost' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['finUrad'])>
                    <tr>
                        <td><@edit.label name='finUrad' items=items /></td><td><@edit.place object=object name='finUrad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canEditAtLeastOne(items, object, ['oprPostav', 'oprJmeno', 'oprPrijmeni', 'zastupceKod', 'zastupceNazev', 'zastupceIc'])>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canEditAtLeastOne(items, object, ['oprPostav', 'oprJmeno', 'oprPrijmeni'])>
                        <li class="active"><a href="#edit-fragment-6" data-toggle="tab"><span>Oprávněná osoba</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['zastupceKod', 'zastupceNazev', 'zastupceIc'])>
                        <li><a href="#edit-fragment-7" data-toggle="tab"><span>Zástupce</span></a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canEditAtLeastOne(items, object, ['oprPostav', 'oprJmeno', 'oprPrijmeni'])>
                        <div id="edit-fragment-6" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['oprPostav', 'oprJmeno', 'oprPrijmeni'])>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canEditAtLeastOne(items, object, ['oprPostav'])>
                                <tr>
                                    <td><@edit.label name='oprPostav' items=items /></td><td><@edit.place object=object name='oprPostav' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['oprJmeno'])>
                                <tr>
                                    <td><@edit.label name='oprJmeno' items=items /></td><td><@edit.place object=object name='oprJmeno' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['oprPrijmeni'])>
                                <tr>
                                    <td><@edit.label name='oprPrijmeni' items=items /></td><td><@edit.place object=object name='oprPrijmeni' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['zastupceKod', 'zastupceNazev', 'zastupceIc'])>
                        <div id="edit-fragment-7" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['zastupceKod', 'zastupceNazev', 'zastupceIc'])>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canEditAtLeastOne(items, object, ['zastupceKod'])>
                                <tr>
                                    <td><@edit.label name='zastupceKod' items=items /></td><td><@edit.place object=object name='zastupceKod' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['zastupceNazev'])>
                                <tr>
                                    <td><@edit.label name='zastupceNazev' items=items /></td><td><@edit.place object=object name='zastupceNazev' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['zastupceIc'])>
                                <tr>
                                    <td><@edit.label name='zastupceIc' items=items /></td><td><@edit.place object=object name='zastupceIc' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>

                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['spisZnac'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['spisZnac'])>
                    <tr>
                        <td><@edit.label name='spisZnac' items=items /><br><@edit.textarea object=object name='spisZnac' items=items marker=marker rows=5 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['statLegislativa', 'typOrganizace', 'mena', 'menaDual', 'kurzDualMena', 'platceDph', 'moss', 'omezovatVyberStatu', 'fyzOsoba', 'pravFormaPodnik', 'osobUpravaDph'])>
            <div id="edit-fragment-9" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['statLegislativa', 'typOrganizace', 'mena', 'menaDual', 'kurzDualMena', 'platceDph', 'moss', 'omezovatVyberStatu', 'fyzOsoba', 'pravFormaPodnik', 'osobUpravaDph'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['statLegislativa'])>
                    <tr>
                        <td><@edit.label name='statLegislativa' items=items /></td><td colspan="5"><@edit.place object=object name='statLegislativa' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typOrganizace'])>
                    <tr>
                        <td><@edit.label name='typOrganizace' items=items /></td><td colspan="5"><@edit.place object=object name='typOrganizace' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mena'])>
                    <tr>
                        <td><@edit.label name='mena' items=items /></td><td colspan="5"><@edit.place object=object name='mena' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['menaDual'])>
                    <tr>
                        <td><@edit.label name='menaDual' items=items /></td><td colspan="3"><@edit.place object=object name='menaDual' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kurzDualMena'])>
                    <tr>
                        <td><@edit.label name='kurzDualMena' items=items /></td><td><@edit.place object=object name='kurzDualMena' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platceDph'])>
                    <tr>
                        <td><@edit.label name='platceDph' items=items /></td><td colspan="5"><@edit.place object=object name='platceDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['moss'])>
                    <tr>
                        <td><@edit.label name='moss' items=items /></td><td colspan="5"><@edit.place object=object name='moss' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['omezovatVyberStatu'])>
                    <tr>
                        <td><@edit.label name='omezovatVyberStatu' items=items /></td><td><@edit.place object=object name='omezovatVyberStatu' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['fyzOsoba'])>
                    <tr>
                        <td><@edit.label name='fyzOsoba' items=items /></td><td colspan="5"><@edit.place object=object name='fyzOsoba' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['pravFormaPodnik'])>
                    <tr>
                        <td><@edit.label name='pravFormaPodnik' items=items /></td><td><@edit.place object=object name='pravFormaPodnik' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['osobUpravaDph'])>
                    <tr>
                        <td><@edit.label name='osobUpravaDph' items=items /></td><td><@edit.place object=object name='osobUpravaDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['adrKontrolovatFirmy', 'sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK', 'splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav', 'splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap', 'jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej', 'jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK', 'mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
            <div id="edit-fragment-36" class="tab-pane">
                <#if canEditAtLeastOne(items, object, ['adrKontrolovatFirmy', 'sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK', 'splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav', 'splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap', 'jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej', 'jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK', 'mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canEditAtLeastOne(items, object, ['adrKontrolovatFirmy'])>
                        <li class="active"><a href="#edit-fragment-10" data-toggle="tab"><span>${lang('labels', 'adresarPN')}</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK'])>
                        <li><a href="#edit-fragment-12" data-toggle="tab"><span>Zboží</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'])>
                        <li><a href="#edit-fragment-16" data-toggle="tab"><span>Prodej</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'])>
                        <li><a href="#edit-fragment-19" data-toggle="tab"><span>Nákup</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej'])>
                        <li><a href="#edit-fragment-22" data-toggle="tab"><span>Peníze</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK'])>
                        <li><a href="#edit-fragment-24" data-toggle="tab"><span>Účetnictví</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
                        <li><a href="#edit-fragment-35" data-toggle="tab"><span>Zaměstnanci</span></a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canEditAtLeastOne(items, object, ['adrKontrolovatFirmy'])>
                        <div id="edit-fragment-10" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['adrKontrolovatFirmy'])>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canEditAtLeastOne(items, object, ['adrKontrolovatFirmy'])>
                                <tr>
                                    <td><@edit.label name='adrKontrolovatFirmy' items=items /></td><td><@edit.place object=object name='adrKontrolovatFirmy' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK'])>
                        <div id="edit-fragment-12" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['sklZapor', 'sklFifo', 'sklAktualNakupCena'])>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canEditAtLeastOne(items, object, ['sklZapor'])>
                                <tr>
                                    <td><@edit.label name='sklZapor' items=items /></td><td><@edit.place object=object name='sklZapor' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['sklFifo'])>
                                <tr>
                                    <td><@edit.label name='sklFifo' items=items /></td><td><@edit.place object=object name='sklFifo' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['sklAktualNakupCena'])>
                                <tr>
                                    <td><@edit.label name='sklAktualNakupCena' items=items /></td><td><@edit.place object=object name='sklAktualNakupCena' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                            <#if canEditAtLeastOne(items, object, ['jakyTypSklK'])>
                            <h4>Volba typu nových dokladů</h4>
                            <div id="edit-fragment-11" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canEditAtLeastOne(items, object, ['jakyTypSklK'])>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canEditAtLeastOne(items, object, ['jakyTypSklK'])>
                                    <tr>
                                        <td><@edit.label name='jakyTypSklK' items=items /></td><td><@edit.place object=object name='jakyTypSklK' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'])>
                        <div id="edit-fragment-16" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor'])>
                            <table class="flexibee-tbl-1">
                                <col width="50%" /><col width="50%" />
                                <#if canEditAtLeastOne(items, object, ['splatDny'])>
                                <tr>
                                    <td><@edit.label name='splatDny' items=items /></td><td><@edit.place object=object name='splatDny' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['autoZakazka'])>
                                <tr>
                                    <td><@edit.label name='autoZakazka' items=items /></td><td><@edit.place object=object name='autoZakazka' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['rezimRezervaciK'])>
                                <tr>
                                    <td><@edit.label name='rezimRezervaciK' items=items /></td><td><@edit.place object=object name='rezimRezervaciK' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['rezervovatZapor'])>
                                <tr>
                                    <td><@edit.label name='rezervovatZapor' items=items /></td><td><@edit.place object=object name='rezervovatZapor' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                            <#if canEditAtLeastOne(items, object, ['jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'])>
                                <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                                    <#if canEditAtLeastOne(items, object, ['jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK'])>
                                    <li class="active"><a href="#edit-fragment-13" data-toggle="tab"><span>Volba typu nových dokladů</span></a></li>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['doplnText'])>
                                    <li><a href="#edit-fragment-14" data-toggle="tab"><span>Faktura</span></a></li>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'])>
                                    <li><a href="#edit-fragment-15" data-toggle="tab"><span>Typy dokladů pro automatickou generaci</span></a></li>
                                    </#if>
                                </ul>
                                <div class="tab-content">
                                    <#if canEditAtLeastOne(items, object, ['jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK'])>
                                    <div id="edit-fragment-13" class="tab-pane active">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK'])>
                                        <table class="flexibee-tbl-1">
                                            <#if canEditAtLeastOne(items, object, ['jakyTypFavK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypFavK' items=items /></td><td><@edit.place object=object name='jakyTypFavK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['jakyTypPppK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypPppK' items=items /></td><td><@edit.place object=object name='jakyTypPppK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['jakyTypNavK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypNavK' items=items /></td><td><@edit.place object=object name='jakyTypNavK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['jakyTypObpK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypObpK' items=items /></td><td><@edit.place object=object name='jakyTypObpK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['doplnText'])>
                                    <div id="edit-fragment-14" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['doplnText'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canEditAtLeastOne(items, object, ['doplnText'])>
                                            <tr>
                                                <td><@edit.label name='doplnText' items=items /><br><@edit.textarea object=object name='doplnText' items=items marker=marker rows=5 /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'])>
                                    <div id="edit-fragment-15" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canEditAtLeastOne(items, object, ['tdObchodObp'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodObp' items=items /></td><td><@edit.place object=object name='tdObchodObp' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodObpEdi'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodObpEdi' items=items /></td><td><@edit.place object=object name='tdObchodObpEdi' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodFav'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodFav' items=items /></td><td><@edit.place object=object name='tdObchodFav' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodFavZal'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodFavZal' items=items /></td><td><@edit.place object=object name='tdObchodFavZal' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodSklVyd'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodSklVyd' items=items /></td><td><@edit.place object=object name='tdObchodSklVyd' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodNav'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodNav' items=items /></td><td><@edit.place object=object name='tdObchodNav' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                </div>
                            </#if>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'])>
                        <div id="edit-fragment-19" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['splatDnyNakup', 'podpisPrik'])>
                            <table class="flexibee-tbl-1">
                                <col width="50%" /><col width="50%" />
                                <#if canEditAtLeastOne(items, object, ['splatDnyNakup'])>
                                <tr>
                                    <td><@edit.label name='splatDnyNakup' items=items /></td><td><@edit.place object=object name='splatDnyNakup' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['podpisPrik'])>
                                <tr>
                                    <td><@edit.label name='podpisPrik' items=items /></td><td><@edit.place object=object name='podpisPrik' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                            <#if canEditAtLeastOne(items, object, ['jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'])>
                                <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                                    <#if canEditAtLeastOne(items, object, ['jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK'])>
                                    <li class="active"><a href="#edit-fragment-17" data-toggle="tab"><span>Volba typu nových dokladů</span></a></li>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'])>
                                    <li><a href="#edit-fragment-18" data-toggle="tab"><span>Typy dokladů pro automatickou generaci</span></a></li>
                                    </#if>
                                </ul>
                                <div class="tab-content">
                                    <#if canEditAtLeastOne(items, object, ['jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK'])>
                                    <div id="edit-fragment-17" class="tab-pane active">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canEditAtLeastOne(items, object, ['jakyTypFapK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypFapK' items=items /></td><td><@edit.place object=object name='jakyTypFapK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['jakyTypPpvK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypPpvK' items=items /></td><td><@edit.place object=object name='jakyTypPpvK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['jakyTypNapK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypNapK' items=items /></td><td><@edit.place object=object name='jakyTypNapK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['jakyTypObvK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypObvK' items=items /></td><td><@edit.place object=object name='jakyTypObvK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'])>
                                    <div id="edit-fragment-18" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canEditAtLeastOne(items, object, ['tdObchodFap'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodFap' items=items /></td><td><@edit.place object=object name='tdObchodFap' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodSklPri'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodSklPri' items=items /></td><td><@edit.place object=object name='tdObchodSklPri' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodObv'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodObv' items=items /></td><td><@edit.place object=object name='tdObchodObv' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodPpv'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodPpv' items=items /></td><td><@edit.place object=object name='tdObchodPpv' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdObchodNap'])>
                                            <tr>
                                                <td><@edit.label name='tdObchodNap' items=items /></td><td><@edit.place object=object name='tdObchodNap' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                </div>
                            </#if>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej'])>
                        <div id="edit-fragment-22" class="tab-pane">
                            <#if canEditAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej'])>
                                <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                                    <#if canEditAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK'])>
                                    <li class="active"><a href="#edit-fragment-20" data-toggle="tab"><span>Volba typu nových dokladů</span></a></li>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdBanPrijem', 'tdBanVydej'])>
                                    <li><a href="#edit-fragment-21" data-toggle="tab"><span>Volba typu dokladu pro načtené bankovní výpisy</span></a></li>
                                    </#if>
                                </ul>
                                <div class="tab-content">
                                    <#if canEditAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK'])>
                                    <div id="edit-fragment-20" class="tab-pane active">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canEditAtLeastOne(items, object, ['jakyTypBanK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypBanK' items=items /></td><td><@edit.place object=object name='jakyTypBanK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['jakyTypPokK'])>
                                            <tr>
                                                <td><@edit.label name='jakyTypPokK' items=items /></td><td><@edit.place object=object name='jakyTypPokK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdBanPrijem', 'tdBanVydej'])>
                                    <div id="edit-fragment-21" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['tdBanPrijem', 'tdBanVydej'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canEditAtLeastOne(items, object, ['tdBanPrijem'])>
                                            <tr>
                                                <td><@edit.label name='tdBanPrijem' items=items /></td><td><@edit.place object=object name='tdBanPrijem' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['tdBanVydej'])>
                                            <tr>
                                                <td><@edit.label name='tdBanVydej' items=items /></td><td><@edit.place object=object name='tdBanVydej' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                </div>
                            </#if>
                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK'])>
                        <div id="edit-fragment-24" class="tab-pane">
                            <#if canEditAtLeastOne(items, object, ['jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK'])>
                            <h4>Volba typu nových dokladů</h4>
                            <div id="edit-fragment-23" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canEditAtLeastOne(items, object, ['jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK'])>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canEditAtLeastOne(items, object, ['jakyTypIntK'])>
                                    <tr>
                                        <td><@edit.label name='jakyTypIntK' items=items /></td><td><@edit.place object=object name='jakyTypIntK' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['jakyTypPhlK'])>
                                    <tr>
                                        <td><@edit.label name='jakyTypPhlK' items=items /></td><td><@edit.place object=object name='jakyTypPhlK' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['jakyTypZavK'])>
                                    <tr>
                                        <td><@edit.label name='jakyTypZavK' items=items /></td><td><@edit.place object=object name='jakyTypZavK' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>
                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
                        <div id="edit-fragment-35" class="tab-pane">
                            <#if canEditAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
                                <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                                    <#if canEditAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir'])>
                                    <li class="active"><a href="#edit-fragment-27" data-toggle="tab"><span>Pojištění</span></a></li>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez'])>
                                    <li><a href="#edit-fragment-31" data-toggle="tab"><span>Daň</span></a></li>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc'])>
                                    <li><a href="#edit-fragment-32" data-toggle="tab"><span>${lang('labels', 'ostatniPN')}</span></a></li>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov'])>
                                    <li><a href="#edit-fragment-33" data-toggle="tab"><span>Úvazek</span></a></li>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
                                    <li><a href="#edit-fragment-34" data-toggle="tab"><span>Dni splatnosti</span></a></li>
                                    </#if>
                                </ul>
                                <div class="tab-content">
                                    <#if canEditAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir'])>
                                    <div id="edit-fragment-27" class="tab-pane active">
                                        <#if canEditAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj'])>
                                        <h4>Sociální pojištění</h4>
                                        <div id="edit-fragment-25" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canEditAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj'])>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canEditAtLeastOne(items, object, ['mzdTdZavSoc'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTdZavSoc' items=items /></td><td><@edit.place object=object name='mzdTdZavSoc' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canEditAtLeastOne(items, object, ['mzdTuoZavSocZam'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTuoZavSocZam' items=items /></td><td><@edit.place object=object name='mzdTuoZavSocZam' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canEditAtLeastOne(items, object, ['mzdTuoZavSocFir'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTuoZavSocFir' items=items /></td><td><@edit.place object=object name='mzdTuoZavSocFir' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canEditAtLeastOne(items, object, ['mzdAdrSocPoj'])>
                                                <tr>
                                                    <td><@edit.label name='mzdAdrSocPoj' items=items /></td><td><@edit.place object=object name='mzdAdrSocPoj' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                        <#if canEditAtLeastOne(items, object, ['mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir'])>
                                        <h4>Zdravotní pojištění</h4>
                                        <div id="edit-fragment-26" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canEditAtLeastOne(items, object, ['mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir'])>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canEditAtLeastOne(items, object, ['mzdTdZavZdrav'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTdZavZdrav' items=items /></td><td><@edit.place object=object name='mzdTdZavZdrav' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canEditAtLeastOne(items, object, ['mzdTuoZavZdrZam'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTuoZavZdrZam' items=items /></td><td><@edit.place object=object name='mzdTuoZavZdrZam' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canEditAtLeastOne(items, object, ['mzdTuoZavZdrFir'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTuoZavZdrFir' items=items /></td><td><@edit.place object=object name='mzdTuoZavZdrFir' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                    </div>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez'])>
                                    <div id="edit-fragment-31" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['mzdAdrDan'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canEditAtLeastOne(items, object, ['mzdAdrDan'])>
                                            <tr>
                                                <td><@edit.label name='mzdAdrDan' items=items /></td><td><@edit.place object=object name='mzdAdrDan' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                        <#if canEditAtLeastOne(items, object, ['mzdTdZavDanZal', 'mzdBanDanZal'])>
                                        <h4>Daň zálohová</h4>
                                        <div id="edit-fragment-28" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canEditAtLeastOne(items, object, ['mzdTdZavDanZal', 'mzdBanDanZal'])>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canEditAtLeastOne(items, object, ['mzdTdZavDanZal'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTdZavDanZal' items=items /></td><td><@edit.place object=object name='mzdTdZavDanZal' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canEditAtLeastOne(items, object, ['mzdBanDanZal'])>
                                                <tr>
                                                    <td><@edit.label name='mzdBanDanZal' items=items /></td><td><@edit.place object=object name='mzdBanDanZal' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                        <#if canEditAtLeastOne(items, object, ['mzdTdZavDanSra', 'mzdBanDanSra'])>
                                        <h4>Dan srážková</h4>
                                        <div id="edit-fragment-29" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canEditAtLeastOne(items, object, ['mzdTdZavDanSra', 'mzdBanDanSra'])>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canEditAtLeastOne(items, object, ['mzdTdZavDanSra'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTdZavDanSra' items=items /></td><td><@edit.place object=object name='mzdTdZavDanSra' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canEditAtLeastOne(items, object, ['mzdBanDanSra'])>
                                                <tr>
                                                    <td><@edit.label name='mzdBanDanSra' items=items /></td><td><@edit.place object=object name='mzdBanDanSra' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                        <#if canEditAtLeastOne(items, object, ['mzdTdZavDanRez', 'mzdBanDanRez'])>
                                        <h4>Daň rezident</h4>
                                        <div id="edit-fragment-30" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canEditAtLeastOne(items, object, ['mzdTdZavDanRez', 'mzdBanDanRez'])>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canEditAtLeastOne(items, object, ['mzdTdZavDanRez'])>
                                                <tr>
                                                    <td><@edit.label name='mzdTdZavDanRez' items=items /></td><td><@edit.place object=object name='mzdTdZavDanRez' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canEditAtLeastOne(items, object, ['mzdBanDanRez'])>
                                                <tr>
                                                    <td><@edit.label name='mzdBanDanRez' items=items /></td><td><@edit.place object=object name='mzdBanDanRez' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                    </div>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc'])>
                                    <div id="edit-fragment-32" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="40%" /><col width="60%" />
                                            <#if canEditAtLeastOne(items, object, ['mzdDnuDovolAbs'])>
                                            <tr>
                                                <td><@edit.label name='mzdDnuDovolAbs' items=items /></td><td><@edit.place object=object name='mzdDnuDovolAbs' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdPenzPrip'])>
                                            <tr>
                                                <td><@edit.label name='mzdPenzPrip' items=items /></td><td><@edit.place object=object name='mzdPenzPrip' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdPenzPripProc'])>
                                            <tr>
                                                <td><@edit.label name='mzdPenzPripProc' items=items /></td><td><@edit.place object=object name='mzdPenzPripProc' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdPromilePovPojisteni'])>
                                            <tr>
                                                <td><@edit.label name='mzdPromilePovPojisteni' items=items /></td><td><@edit.place object=object name='mzdPromilePovPojisteni' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdTdZavZaloha'])>
                                            <tr>
                                                <td><@edit.label name='mzdTdZavZaloha' items=items /></td><td><@edit.place object=object name='mzdTdZavZaloha' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdTdZavSrazka'])>
                                            <tr>
                                                <td><@edit.label name='mzdTdZavSrazka' items=items /></td><td><@edit.place object=object name='mzdTdZavSrazka' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdTdZavDobirka'])>
                                            <tr>
                                                <td><@edit.label name='mzdTdZavDobirka' items=items /></td><td><@edit.place object=object name='mzdTdZavDobirka' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdTdIntHruba'])>
                                            <tr>
                                                <td><@edit.label name='mzdTdIntHruba' items=items /></td><td><@edit.place object=object name='mzdTdIntHruba' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdTdIntNahrad'])>
                                            <tr>
                                                <td><@edit.label name='mzdTdIntNahrad' items=items /></td><td><@edit.place object=object name='mzdTdIntNahrad' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdIspvOkres'])>
                                            <tr>
                                                <td><@edit.label name='mzdIspvOkres' items=items /></td><td><@edit.place object=object name='mzdIspvOkres' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdRefundNahrNemoc'])>
                                            <tr>
                                                <td><@edit.label name='mzdRefundNahrNemoc' items=items /></td><td><@edit.place object=object name='mzdRefundNahrNemoc' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov'])>
                                    <div id="edit-fragment-33" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="50%" /><col width="50%" />
                                            <#if canEditAtLeastOne(items, object, ['mzdSmenyHodinDenne'])>
                                            <tr>
                                                <td><@edit.label name='mzdSmenyHodinDenne' items=items /></td><td><@edit.place object=object name='mzdSmenyHodinDenne' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdSmenyDnuTydne'])>
                                            <tr>
                                                <td><@edit.label name='mzdSmenyDnuTydne' items=items /></td><td><@edit.place object=object name='mzdSmenyDnuTydne' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdSmenyPocPracDoby'])>
                                            <tr>
                                                <td><@edit.label name='mzdSmenyPocPracDoby' items=items /></td><td><@edit.place object=object name='mzdSmenyPocPracDoby' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdSmenySvatkyPrum'])>
                                            <tr>
                                                <td><@edit.label name='mzdSmenySvatkyPrum' items=items /></td><td><@edit.place object=object name='mzdSmenySvatkyPrum' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdNemocProplatAuto'])>
                                            <tr>
                                                <td><@edit.label name='mzdNemocProplatAuto' items=items /></td><td><@edit.place object=object name='mzdNemocProplatAuto' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdNemocProplatVarov'])>
                                            <tr>
                                                <td><@edit.label name='mzdNemocProplatVarov' items=items /></td><td><@edit.place object=object name='mzdNemocProplatVarov' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
                                    <div id="edit-fragment-34" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canEditAtLeastOne(items, object, ['mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'])>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canEditAtLeastOne(items, object, ['mzdSplatSocPoj'])>
                                            <tr>
                                                <td><@edit.label name='mzdSplatSocPoj' items=items /></td><td><@edit.place object=object name='mzdSplatSocPoj' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdSplatZdravPoj'])>
                                            <tr>
                                                <td><@edit.label name='mzdSplatZdravPoj' items=items /></td><td><@edit.place object=object name='mzdSplatZdravPoj' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdSplatDanZal'])>
                                            <tr>
                                                <td><@edit.label name='mzdSplatDanZal' items=items /></td><td><@edit.place object=object name='mzdSplatDanZal' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdSplatDanSra'])>
                                            <tr>
                                                <td><@edit.label name='mzdSplatDanSra' items=items /></td><td><@edit.place object=object name='mzdSplatDanSra' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdSplatDanRez'])>
                                            <tr>
                                                <td><@edit.label name='mzdSplatDanRez' items=items /></td><td><@edit.place object=object name='mzdSplatDanRez' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canEditAtLeastOne(items, object, ['mzdSplatDobirka'])>
                                            <tr>
                                                <td><@edit.label name='mzdSplatDobirka' items=items /></td><td><@edit.place object=object name='mzdSplatDobirka' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                </div>
                            </#if>
                        </div>
                        </#if>
                    </div>
                </#if>
            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad', 'eetProvozovna', 'eetLimit', 'eetModK'])>
            <div id="edit-fragment-45" class="tab-pane">
                <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad', 'eetProvozovna', 'eetLimit', 'eetModK'])>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad'])>
                        <li class="active"><a href="#edit-fragment-43" data-toggle="tab"><span>Typy dokladů a účty</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetLimit', 'eetModK'])>
                        <li><a href="#edit-fragment-44" data-toggle="tab"><span>${lang('labels', 'dokladEBEetPN')}</span></a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad'])>
                        <div id="edit-fragment-43" class="tab-pane active">
                            <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad'])>
                            <h4>Kurzové rozdíly</h4>
                            <div id="edit-fragment-37" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad'])>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilVynos'])>
                                    <tr>
                                        <td><@edit.label name='tdKurzovyRozdilVynos' items=items /></td><td><@edit.place object=object name='tdKurzovyRozdilVynos' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdKurzovyRozdilNaklad'])>
                                    <tr>
                                        <td><@edit.label name='tdKurzovyRozdilNaklad' items=items /></td><td><@edit.place object=object name='tdKurzovyRozdilNaklad' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canEditAtLeastOne(items, object, ['tdZbytekVynos', 'tdZbytekNaklad'])>
                            <h4>Zbytkové rozdíly</h4>
                            <div id="edit-fragment-38" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canEditAtLeastOne(items, object, ['tdZbytekVynos', 'tdZbytekNaklad'])>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canEditAtLeastOne(items, object, ['tdZbytekVynos'])>
                                    <tr>
                                        <td><@edit.label name='tdZbytekVynos' items=items /></td><td><@edit.place object=object name='tdZbytekVynos' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdZbytekNaklad'])>
                                    <tr>
                                        <td><@edit.label name='tdZbytekNaklad' items=items /></td><td><@edit.place object=object name='tdZbytekNaklad' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canEditAtLeastOne(items, object, ['tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl'])>
                            <h4>Zálohový daňový doklad</h4>
                            <div id="edit-fragment-39" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canEditAtLeastOne(items, object, ['tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl'])>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canEditAtLeastOne(items, object, ['tdFavZalohovyDanDokl'])>
                                    <tr>
                                        <td><@edit.label name='tdFavZalohovyDanDokl' items=items /></td><td><@edit.place object=object name='tdFavZalohovyDanDokl' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdFapZalohovyDanDokl'])>
                                    <tr>
                                        <td><@edit.label name='tdFapZalohovyDanDokl' items=items /></td><td><@edit.place object=object name='tdFapZalohovyDanDokl' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canEditAtLeastOne(items, object, ['ucetZaokrVynos', 'ucetZaokrNaklad'])>
                            <h4>Účty pro zaokrouhlovací rozdíl</h4>
                            <div id="edit-fragment-40" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canEditAtLeastOne(items, object, ['ucetZaokrVynos', 'ucetZaokrNaklad'])>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canEditAtLeastOne(items, object, ['ucetZaokrVynos'])>
                                    <tr>
                                        <td><@edit.label name='ucetZaokrVynos' items=items /></td><td><@edit.place object=object name='ucetZaokrVynos' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['ucetZaokrNaklad'])>
                                    <tr>
                                        <td><@edit.label name='ucetZaokrNaklad' items=items /></td><td><@edit.place object=object name='ucetZaokrNaklad' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canEditAtLeastOne(items, object, ['tdOsUpravaPrijem', 'tdOsUpravaVydej'])>
                            <h4>${lang('labels', 'nastaveniPomocDoklPN')}</h4>
                            <div id="edit-fragment-41" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canEditAtLeastOne(items, object, ['tdOsUpravaPrijem', 'tdOsUpravaVydej'])>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canEditAtLeastOne(items, object, ['tdOsUpravaPrijem'])>
                                    <tr>
                                        <td><@edit.label name='tdOsUpravaPrijem' items=items /></td><td><@edit.place object=object name='tdOsUpravaPrijem' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['tdOsUpravaVydej'])>
                                    <tr>
                                        <td><@edit.label name='tdOsUpravaVydej' items=items /></td><td><@edit.place object=object name='tdOsUpravaVydej' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canEditAtLeastOne(items, object, ['ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad'])>
                            <h4>${lang('labels', 'nastaveniUctyPomocDoklPN')}</h4>
                            <div id="edit-fragment-42" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canEditAtLeastOne(items, object, ['ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad'])>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canEditAtLeastOne(items, object, ['ucetOsUpravaDphSniz'])>
                                    <tr>
                                        <td><@edit.label name='ucetOsUpravaDphSniz' items=items /></td><td><@edit.place object=object name='ucetOsUpravaDphSniz' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['ucetOsUpravaDphZakl'])>
                                    <tr>
                                        <td><@edit.label name='ucetOsUpravaDphZakl' items=items /></td><td><@edit.place object=object name='ucetOsUpravaDphZakl' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canEditAtLeastOne(items, object, ['ucetOsUpravaZaklad'])>
                                    <tr>
                                        <td><@edit.label name='ucetOsUpravaZaklad' items=items /></td><td><@edit.place object=object name='ucetOsUpravaZaklad' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetLimit', 'eetModK'])>
                        <div id="edit-fragment-44" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['eetProvozovna', 'eetLimit', 'eetModK'])>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canEditAtLeastOne(items, object, ['eetProvozovna'])>
                                <tr>
                                    <td><@edit.label name='eetProvozovna' items=items /></td><td><@edit.place object=object name='eetProvozovna' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['eetLimit'])>
                                <tr>
                                    <td><@edit.label name='eetLimit' items=items /></td><td><@edit.place object=object name='eetLimit' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['eetModK'])>
                                <tr>
                                    <td><@edit.label name='eetModK' items=items /></td><td><@edit.place object=object name='eetModK' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>
            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['denniKurzK', 'rocniKurzK', 'manualCisDokl'])>
            <div id="edit-fragment-47" class="tab-pane">
                <#if canEditAtLeastOne(items, object, ['denniKurzK', 'rocniKurzK'])>
                <h4>Způsob aktualizace kurzů</h4>
                <div id="edit-fragment-46" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canEditAtLeastOne(items, object, ['denniKurzK', 'rocniKurzK'])>
                    <table class="flexibee-tbl-1">
                        <col width="20%" /><col width="80%" />
                        <#if canEditAtLeastOne(items, object, ['denniKurzK'])>
                        <tr>
                            <td><@edit.label name='denniKurzK' items=items /></td><td><@edit.place object=object name='denniKurzK' items=items marker=marker /></td>
                        </tr>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['rocniKurzK'])>
                        <tr>
                            <td><@edit.label name='rocniKurzK' items=items /></td><td><@edit.place object=object name='rocniKurzK' items=items marker=marker /></td>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['manualCisDokl'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['manualCisDokl'])>
                    <tr>
                        <td><@edit.label name='manualCisDokl' items=items /></td><td><@edit.place object=object name='manualCisDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton showSubmitAndNew=false />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

    <table class="flexibee-tbl-1">
        <col width="50%" /><col width="50%" />

        <tr>
            <td><strong>Logo</strong></td>
            <#if isMobile></tr><tr></#if>
            <td><strong>Podpis a razítko</strong></td>
        </tr>
        <tr>
            <td><div id="flexibee-nastaveni-logo">
                <#if it.evidenceResource.logoId(it.object.id)??>
                    <img src="/c/${it.companyId}/priloha/${it.evidenceResource.logoId(it.object.id)}/thumbnail"/>
                <#else>Žádné</#if>
            </div></td>
            <#if isMobile></tr><tr></#if>
            <td><div id="flexibee-nastaveni-podraz">
                <#if it.evidenceResource.podrazId(it.object.id)??>
                    <img src="/c/${it.companyId}/priloha/${it.evidenceResource.podrazId(it.object.id)}/thumbnail"/>
                <#else>Žádné</#if>
            </div></td>
        </tr>

        <tr>
            <td><input type="file" name="logo" id="flexibee-nastaveni-logo-change"/></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="file" name="podraz" id="flexibee-nastaveni-podraz-change"/></td>
        </tr>
        <tr>
            <td><input type="button" id="flexibee-nastaveni-logo-remove" value="Odstranit"/></td>
            <#if isMobile></tr><tr></#if>
            <td><input type="button" id="flexibee-nastaveni-podraz-remove" value="Odstranit"/></td>
        </tr>
    </table>

<#if isSlowMobile == false>
<script type="text/javascript">
    $(document).ready(function() {
        <#assign pscUrl = formListUrl(it.companyResource, 'PSC')!"" />
        $("#flexibee-inp-psc, #flexibee-inp-mesto").suggestPsc('${pscUrl}', '#flexibee-inp-stat', function(data) {
            $("#flexibee-inp-mesto").val(data.nazev).change();
            $("#flexibee-inp-psc").val(data.kod).change();
        });
        $("#flexibee-inp-faPsc, #flexibee-inp-faMesto").suggestPsc('${pscUrl}', '#flexibee-inp-statFakturacniAdresy', function(data) {
            $("#flexibee-inp-faMesto").val(data.nazev).change();
            $("#flexibee-inp-faPsc").val(data.kod).change();
        });
        $("#flexibee-inp-postPsc, #flexibee-inp-postMesto").suggestPsc('${pscUrl}', '#flexibee-inp-statPostovniAdresy', function(data) {
            $("#flexibee-inp-postMesto").val(data.nazev).change();
            $("#flexibee-inp-postPsc").val(data.kod).change();
        });

        // logo + podraz
        <#list ["logo", "podraz"] as priloha>
            $("#flexibee-nastaveni-${priloha}-change").change(function() {
                $.showWait();
                $(this).upload('/c/${companyId}/nastaveni/${it.object.id}/upload-${priloha}?token=${csrfToken}', '${priloha}', function(resp) {
                    var id = parseInt(resp, 10);
                    if (id) {
                        $("#flexibee-nastaveni-${priloha}").html('<img src="/c/${it.companyId}/priloha/' + id + '/thumbnail"/>');
                    } else {
                        $("#flexibee-nastaveni-${priloha}").html('Došlo k chybě. Přikládáte obrázek?');
                    }
                    $.hideWait();
                }, 'text');
                return false;
            });
            $("#flexibee-nastaveni-${priloha}-remove").click(function() {
                $.showWait();
                $.post('/c/${companyId}/nastaveni/${it.object.id}/remove-${priloha}?token=${csrfToken}', function() {
                    $("#flexibee-nastaveni-${priloha}").html("Žádné");
                    $("#flexibee-nastaveni-${priloha}-change").val("");
                    $.hideWait();
                });
                return false;
            });
        </#list>
    });


//funkce pro zajištění toho, aby byl checkbox pro moss editovatelný jen, pokud má firma nastaveno, že se jedná o plátce DPH

    function disableField(field, disable, defaultVal, edit){
        //checkbox zvl režimu nemusí na stránce být vůbec (není licence)
        if(field != null){
            if (!disable) {
                field.removeAttr("disabled");
                if(edit){
                    field.prop('checked', defaultVal);
                }
            } else {
                //v případě, že se nejedná o plátce DPH tak, disablujeme checkbox pro moss a nastavíme ho na false (aby se nám případná hodnota neuložila)
                field.attr("disabled", "disabled");
                field.prop('checked', false);
            }
        }
    }

    function enableDisableZvlRezim (edit) {
        var checked = $("#flexibee-inp-platceDph").is(":checked");
        var fieldMoss = $('input[id="flexibee-inp-moss"]');
        //checkbox zvl režimu nemusí na stránce být vůbec (není licence)
        //v případě, že se nejedná o plátce DPH tak, disablujeme checkbox pro moss
        //a nastavíme ho na false (aby se nám případná hodnota neuložila)
        disableField(fieldMoss, !checked, false, edit);

        //checkbox omezovat výběr státu dph nemusí na stránce být vůbec (není licence)
        //v případě, že se nejedná o plátce DPH tak, disablujeme checkbox pro omezení výběru státu DPH
        //a nastavíme ho na false (aby se nám případná hodnota neuložila)
        var fieldOmezovat = $('input[id="flexibee-inp-omezovatVyberStatu"]');
        disableField(fieldOmezovat, !checked, true, edit);

    }

    enableDisableZvlRezim(false);

    var enableDisableZvlRezimEdit = function () {
        enableDisableZvlRezim(true);
    };

    $("#flexibee-inp-platceDph").onClick(enableDisableZvlRezimEdit);

    function enableDisableRezervaceZapor (edit) {
        var rezimRezervaci = $("#flexibee-inp-rezimRezervaciK").val();
        var fieldRezervaceZapor = $('input[id="flexibee-inp-rezervovatZapor"]');
        disableField(fieldRezervaceZapor, rezimRezervaci && rezimRezervaci == 'rezimRezervaci.zObjPriNaskladneni', false, edit);
    }

    enableDisableRezervaceZapor(false);

    var enableDisableRezervaceZaporEdit = function () {
        enableDisableRezervaceZapor(true);
    };

    $("#flexibee-inp-rezimRezervaciK").onClick(enableDisableRezervaceZaporEdit);

</script>
</#if>


<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
