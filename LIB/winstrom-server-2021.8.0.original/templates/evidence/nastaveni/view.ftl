<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('intrastat')}
${marker.mark('zobrLogoK')}
${marker.mark('sklUcto')}
${marker.mark('postMigrace')}
${marker.mark('firmaUUID')}
${marker.mark('logoPoziceK')}


<#--AUTOGEN BEGIN-->
    <#if canShowAtLeastOne(items, object, ['nazFirmy', 'ic', 'dic', 'vatId', 'stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr', 'fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa', 'cinnost', 'finUrad', 'oprPostav', 'oprJmeno', 'oprPrijmeni', 'zastupceKod', 'zastupceNazev', 'zastupceIc', 'spisZnac', 'statLegislativa', 'typOrganizace', 'mena', 'menaDual', 'kurzDualMena', 'platceDph', 'moss', 'omezovatVyberStatu', 'fyzOsoba', 'pravFormaPodnik', 'osobUpravaDph', 'adrKontrolovatFirmy', 'sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK', 'splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav', 'splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap', 'jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej', 'jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK', 'mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka', 'tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad', 'eetProvozovna', 'eetLimit', 'eetModK', 'denniKurzK', 'rocniKurzK', 'manualCisDokl'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['nazFirmy', 'ic', 'dic', 'vatId', 'stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr', 'fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'], it)>
            <li class="active"><a href="#view-fragment-5" data-toggle="tab">Firma</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['cinnost', 'finUrad', 'oprPostav', 'oprJmeno', 'oprPrijmeni', 'zastupceKod', 'zastupceNazev', 'zastupceIc', 'spisZnac'], it)>
            <li><a href="#view-fragment-8" data-toggle="tab">${lang('labels', 'upresneniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['statLegislativa', 'typOrganizace', 'mena', 'menaDual', 'kurzDualMena', 'platceDph', 'moss', 'omezovatVyberStatu', 'fyzOsoba', 'pravFormaPodnik', 'osobUpravaDph'], it)>
            <li><a href="#view-fragment-9" data-toggle="tab">Legislativa</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['adrKontrolovatFirmy', 'sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK', 'splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav', 'splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap', 'jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej', 'jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK', 'mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
            <li><a href="#view-fragment-36" data-toggle="tab">Moduly</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad', 'eetProvozovna', 'eetLimit', 'eetModK'], it)>
            <li><a href="#view-fragment-45" data-toggle="tab">Automatické operace</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['denniKurzK', 'rocniKurzK', 'manualCisDokl'], it)>
            <li><a href="#view-fragment-47" data-toggle="tab">${lang('labels', 'ostatniPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['nazFirmy', 'ic', 'dic', 'vatId', 'stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr', 'fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'], it)>
            <div id="view-fragment-5" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['nazFirmy', 'ic', 'dic', 'vatId'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['nazFirmy'], it)>
                    <tr>
                        <td><@tool.label name='nazFirmy' items=items />:</td><td colspan="5"><@tool.place object=object name='nazFirmy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ic', 'dic', 'vatId'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ic'], it)>
                        <td>IČO:</td><td><@tool.place object=object name='ic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['dic'], it)>
                        <td><@tool.label name='dic' items=items />:</td><td><@tool.place object=object name='dic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['vatId'], it)>
                        <td><@tool.label name='vatId' items=items />:</td><td><@tool.place object=object name='vatId' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canShowAtLeastOne(items, object, ['stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka', 'statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr', 'statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr', 'fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'], it)>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canShowAtLeastOne(items, object, ['stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka'], it)>
                        <li class="active"><a href="#view-fragment-1" data-toggle="tab">Sídlo / trvalé bydliště</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr'], it)>
                        <li><a href="#view-fragment-2" data-toggle="tab">Poštovní adresa</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr'], it)>
                        <li><a href="#view-fragment-3" data-toggle="tab">Fakturační adresa</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'], it)>
                        <li><a href="#view-fragment-4" data-toggle="tab">Fyzická osoba</a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canShowAtLeastOne(items, object, ['stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka'], it)>
                        <div id="view-fragment-1" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['stat', 'mesto', 'psc', 'uliceNazev', 'cisPop', 'cisOr', 'tel', 'mobil', 'fax', 'www', 'email', 'datovaSchranka'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
                                <#if canShowAtLeastOne(items, object, ['stat'], it)>
                                <tr>
                                    <td><@tool.label name='stat' items=items />:</td><td colspan="5"><@tool.place object=object name='stat' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['mesto', 'psc'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['mesto'], it)>
                                    <td><@tool.label name='mesto' items=items />:</td><td><@tool.place object=object name='mesto' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['psc'], it)>
                                    <td><@tool.label name='psc' items=items />:</td><td><@tool.place object=object name='psc' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['uliceNazev', 'cisPop', 'cisOr'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['uliceNazev'], it)>
                                    <td><@tool.label name='uliceNazev' items=items />:</td><td><@tool.place object=object name='uliceNazev' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['cisPop'], it)>
                                    <td><@tool.label name='cisPop' items=items />:</td><td><@tool.place object=object name='cisPop' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['cisOr'], it)>
                                    <td><@tool.label name='cisOr' items=items />:</td><td><@tool.place object=object name='cisOr' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['tel', 'mobil', 'fax'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['tel'], it)>
                                    <td><@tool.label name='tel' items=items />:</td><td><@tool.placeTel object=object name='tel' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['mobil'], it)>
                                    <td><@tool.label name='mobil' items=items />:</td><td><@tool.placeTel object=object name='mobil' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['fax'], it)>
                                    <td><@tool.label name='fax' items=items />:</td><td><@tool.place object=object name='fax' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['www', 'email', 'datovaSchranka'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['www'], it)>
                                    <td><@tool.label name='www' items=items />:</td><td><@tool.placeWww object=object name='www' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['email'], it)>
                                    <td><@tool.label name='email' items=items />:</td><td><@tool.placeEmail object=object name='email' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['datovaSchranka'], it)>
                                    <td><@tool.label name='datovaSchranka' items=items />:</td><td><@tool.place object=object name='datovaSchranka' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr'], it)>
                        <div id="view-fragment-2" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['statPostovniAdresy', 'postMesto', 'postPsc', 'postUliceNazev', 'postCisPop', 'postCisOr'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
                                <#if canShowAtLeastOne(items, object, ['statPostovniAdresy'], it)>
                                <tr>
                                    <td><@tool.label name='statPostovniAdresy' items=items />:</td><td colspan="5"><@tool.place object=object name='statPostovniAdresy' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['postMesto', 'postPsc'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['postMesto'], it)>
                                    <td><@tool.label name='postMesto' items=items />:</td><td><@tool.place object=object name='postMesto' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['postPsc'], it)>
                                    <td><@tool.label name='postPsc' items=items />:</td><td><@tool.place object=object name='postPsc' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['postUliceNazev', 'postCisPop', 'postCisOr'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['postUliceNazev'], it)>
                                    <td><@tool.label name='postUliceNazev' items=items />:</td><td><@tool.place object=object name='postUliceNazev' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['postCisPop'], it)>
                                    <td><@tool.label name='postCisPop' items=items />:</td><td><@tool.place object=object name='postCisPop' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['postCisOr'], it)>
                                    <td><@tool.label name='postCisOr' items=items />:</td><td><@tool.place object=object name='postCisOr' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr'], it)>
                        <div id="view-fragment-3" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['statFakturacniAdresy', 'faMesto', 'faPsc', 'faUliceNazev', 'faCisPop', 'faCisOr'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
                                <#if canShowAtLeastOne(items, object, ['statFakturacniAdresy'], it)>
                                <tr>
                                    <td><@tool.label name='statFakturacniAdresy' items=items />:</td><td colspan="5"><@tool.place object=object name='statFakturacniAdresy' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['faMesto', 'faPsc'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['faMesto'], it)>
                                    <td><@tool.label name='faMesto' items=items />:</td><td><@tool.place object=object name='faMesto' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['faPsc'], it)>
                                    <td><@tool.label name='faPsc' items=items />:</td><td><@tool.place object=object name='faPsc' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['faUliceNazev', 'faCisPop', 'faCisOr'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['faUliceNazev'], it)>
                                    <td><@tool.label name='faUliceNazev' items=items />:</td><td><@tool.place object=object name='faUliceNazev' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['faCisPop'], it)>
                                    <td><@tool.label name='faCisPop' items=items />:</td><td><@tool.place object=object name='faCisPop' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['faCisOr'], it)>
                                    <td><@tool.label name='faCisOr' items=items />:</td><td><@tool.place object=object name='faCisOr' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'], it)>
                        <div id="view-fragment-4" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['fyzOsTitul', 'fyzOsJmeno', 'fyzOsPrijmeni', 'fyzOsTitulZa'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canShowAtLeastOne(items, object, ['fyzOsTitul'], it)>
                                <tr>
                                    <td><@tool.label name='fyzOsTitul' items=items />:</td><td><@tool.place object=object name='fyzOsTitul' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['fyzOsJmeno'], it)>
                                <tr>
                                    <td><@tool.label name='fyzOsJmeno' items=items />:</td><td><@tool.place object=object name='fyzOsJmeno' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['fyzOsPrijmeni'], it)>
                                <tr>
                                    <td><@tool.label name='fyzOsPrijmeni' items=items />:</td><td><@tool.place object=object name='fyzOsPrijmeni' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['fyzOsTitulZa'], it)>
                                <tr>
                                    <td><@tool.label name='fyzOsTitulZa' items=items />:</td><td><@tool.place object=object name='fyzOsTitulZa' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['cinnost', 'finUrad', 'oprPostav', 'oprJmeno', 'oprPrijmeni', 'zastupceKod', 'zastupceNazev', 'zastupceIc', 'spisZnac'], it)>
            <div id="view-fragment-8" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['cinnost', 'finUrad'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['cinnost'], it)>
                    <tr>
                        <td><@tool.label name='cinnost' items=items />:</td><td><@tool.place object=object name='cinnost' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['finUrad'], it)>
                    <tr>
                        <td><@tool.label name='finUrad' items=items />:</td><td><@tool.place object=object name='finUrad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canShowAtLeastOne(items, object, ['oprPostav', 'oprJmeno', 'oprPrijmeni', 'zastupceKod', 'zastupceNazev', 'zastupceIc'], it)>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canShowAtLeastOne(items, object, ['oprPostav', 'oprJmeno', 'oprPrijmeni'], it)>
                        <li class="active"><a href="#view-fragment-6" data-toggle="tab">Oprávněná osoba</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['zastupceKod', 'zastupceNazev', 'zastupceIc'], it)>
                        <li><a href="#view-fragment-7" data-toggle="tab">Zástupce</a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canShowAtLeastOne(items, object, ['oprPostav', 'oprJmeno', 'oprPrijmeni'], it)>
                        <div id="view-fragment-6" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['oprPostav', 'oprJmeno', 'oprPrijmeni'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canShowAtLeastOne(items, object, ['oprPostav'], it)>
                                <tr>
                                    <td><@tool.label name='oprPostav' items=items />:</td><td><@tool.place object=object name='oprPostav' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['oprJmeno'], it)>
                                <tr>
                                    <td><@tool.label name='oprJmeno' items=items />:</td><td><@tool.place object=object name='oprJmeno' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['oprPrijmeni'], it)>
                                <tr>
                                    <td><@tool.label name='oprPrijmeni' items=items />:</td><td><@tool.place object=object name='oprPrijmeni' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['zastupceKod', 'zastupceNazev', 'zastupceIc'], it)>
                        <div id="view-fragment-7" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['zastupceKod', 'zastupceNazev', 'zastupceIc'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canShowAtLeastOne(items, object, ['zastupceKod'], it)>
                                <tr>
                                    <td><@tool.label name='zastupceKod' items=items />:</td><td><@tool.place object=object name='zastupceKod' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['zastupceNazev'], it)>
                                <tr>
                                    <td><@tool.label name='zastupceNazev' items=items />:</td><td><@tool.place object=object name='zastupceNazev' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['zastupceIc'], it)>
                                <tr>
                                    <td><@tool.label name='zastupceIc' items=items />:</td><td><@tool.place object=object name='zastupceIc' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>

                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['spisZnac'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['spisZnac'], it)>
                    <tr>
                        <td><@tool.label name='spisZnac' items=items />:<br><@tool.placeArea object=object name='spisZnac' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['statLegislativa', 'typOrganizace', 'mena', 'menaDual', 'kurzDualMena', 'platceDph', 'moss', 'omezovatVyberStatu', 'fyzOsoba', 'pravFormaPodnik', 'osobUpravaDph'], it)>
            <div id="view-fragment-9" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['statLegislativa', 'typOrganizace', 'mena', 'menaDual', 'kurzDualMena', 'platceDph', 'moss', 'omezovatVyberStatu', 'fyzOsoba', 'pravFormaPodnik', 'osobUpravaDph'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['statLegislativa'], it)>
                    <tr>
                        <td><@tool.label name='statLegislativa' items=items />:</td><td colspan="5"><@tool.place object=object name='statLegislativa' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typOrganizace'], it)>
                    <tr>
                        <td><@tool.label name='typOrganizace' items=items />:</td><td colspan="5"><@tool.place object=object name='typOrganizace' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['mena'], it)>
                    <tr>
                        <td><@tool.label name='mena' items=items />:</td><td colspan="5"><@tool.place object=object name='mena' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['menaDual'], it)>
                    <tr>
                        <td><@tool.label name='menaDual' items=items />:</td><td colspan="3"><@tool.place object=object name='menaDual' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['kurzDualMena'], it)>
                    <tr>
                        <td><@tool.label name='kurzDualMena' items=items />:</td><td><@tool.place object=object name='kurzDualMena' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platceDph'], it)>
                    <tr>
                        <td><@tool.label name='platceDph' items=items />:</td><td colspan="5"><@tool.place object=object name='platceDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['moss'], it)>
                    <tr>
                        <td><@tool.label name='moss' items=items />:</td><td colspan="5"><@tool.place object=object name='moss' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['omezovatVyberStatu'], it)>
                    <tr>
                        <td><@tool.label name='omezovatVyberStatu' items=items />:</td><td><@tool.place object=object name='omezovatVyberStatu' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['fyzOsoba'], it)>
                    <tr>
                        <td><@tool.label name='fyzOsoba' items=items />:</td><td colspan="5"><@tool.place object=object name='fyzOsoba' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['pravFormaPodnik'], it)>
                    <tr>
                        <td><@tool.label name='pravFormaPodnik' items=items />:</td><td><@tool.place object=object name='pravFormaPodnik' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['osobUpravaDph'], it)>
                    <tr>
                        <td><@tool.label name='osobUpravaDph' items=items />:</td><td><@tool.place object=object name='osobUpravaDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['adrKontrolovatFirmy', 'sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK', 'splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav', 'splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap', 'jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej', 'jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK', 'mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
            <div id="view-fragment-36" class="tab-pane">
                <#if canShowAtLeastOne(items, object, ['adrKontrolovatFirmy', 'sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK', 'splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav', 'splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap', 'jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej', 'jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK', 'mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canShowAtLeastOne(items, object, ['adrKontrolovatFirmy'], it)>
                        <li class="active"><a href="#view-fragment-10" data-toggle="tab">${lang('labels', 'adresarPN')}</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK'], it)>
                        <li><a href="#view-fragment-12" data-toggle="tab">Zboží</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'], it)>
                        <li><a href="#view-fragment-16" data-toggle="tab">Prodej</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'], it)>
                        <li><a href="#view-fragment-19" data-toggle="tab">Nákup</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej'], it)>
                        <li><a href="#view-fragment-22" data-toggle="tab">Peníze</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK'], it)>
                        <li><a href="#view-fragment-24" data-toggle="tab">Účetnictví</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
                        <li><a href="#view-fragment-35" data-toggle="tab">Zaměstnanci</a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canShowAtLeastOne(items, object, ['adrKontrolovatFirmy'], it)>
                        <div id="view-fragment-10" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['adrKontrolovatFirmy'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canShowAtLeastOne(items, object, ['adrKontrolovatFirmy'], it)>
                                <tr>
                                    <td><@tool.label name='adrKontrolovatFirmy' items=items />:</td><td><@tool.place object=object name='adrKontrolovatFirmy' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['sklZapor', 'sklFifo', 'sklAktualNakupCena', 'jakyTypSklK'], it)>
                        <div id="view-fragment-12" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['sklZapor', 'sklFifo', 'sklAktualNakupCena'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canShowAtLeastOne(items, object, ['sklZapor'], it)>
                                <tr>
                                    <td><@tool.label name='sklZapor' items=items />:</td><td><@tool.place object=object name='sklZapor' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['sklFifo'], it)>
                                <tr>
                                    <td><@tool.label name='sklFifo' items=items />:</td><td><@tool.place object=object name='sklFifo' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['sklAktualNakupCena'], it)>
                                <tr>
                                    <td><@tool.label name='sklAktualNakupCena' items=items />:</td><td><@tool.place object=object name='sklAktualNakupCena' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                            <#if canShowAtLeastOne(items, object, ['jakyTypSklK'], it)>
                            <h4>Volba typu nových dokladů</h4>
                            <div id="view-fragment-11" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canShowAtLeastOne(items, object, ['jakyTypSklK'], it)>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canShowAtLeastOne(items, object, ['jakyTypSklK'], it)>
                                    <tr>
                                        <td><@tool.label name='jakyTypSklK' items=items />:</td><td><@tool.place object=object name='jakyTypSklK' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor', 'jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'], it)>
                        <div id="view-fragment-16" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['splatDny', 'autoZakazka', 'rezimRezervaciK', 'rezervovatZapor'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="50%" /><col width="50%" />
                                <#if canShowAtLeastOne(items, object, ['splatDny'], it)>
                                <tr>
                                    <td><@tool.label name='splatDny' items=items />:</td><td><@tool.place object=object name='splatDny' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['autoZakazka'], it)>
                                <tr>
                                    <td><@tool.label name='autoZakazka' items=items />:</td><td><@tool.place object=object name='autoZakazka' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['rezimRezervaciK'], it)>
                                <tr>
                                    <td><@tool.label name='rezimRezervaciK' items=items />:</td><td><@tool.place object=object name='rezimRezervaciK' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['rezervovatZapor'], it)>
                                <tr>
                                    <td><@tool.label name='rezervovatZapor' items=items />:</td><td><@tool.place object=object name='rezervovatZapor' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                            <#if canShowAtLeastOne(items, object, ['jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK', 'doplnText', 'tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'], it)>
                                <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                                    <#if canShowAtLeastOne(items, object, ['jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK'], it)>
                                    <li class="active"><a href="#view-fragment-13" data-toggle="tab">Volba typu nových dokladů</a></li>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['doplnText'], it)>
                                    <li><a href="#view-fragment-14" data-toggle="tab">Faktura</a></li>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'], it)>
                                    <li><a href="#view-fragment-15" data-toggle="tab">Typy dokladů pro automatickou generaci</a></li>
                                    </#if>
                                </ul>
                                <div class="tab-content">
                                    <#if canShowAtLeastOne(items, object, ['jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK'], it)>
                                    <div id="view-fragment-13" class="tab-pane active">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['jakyTypFavK', 'jakyTypPppK', 'jakyTypNavK', 'jakyTypObpK'], it)>
                                        <table class="flexibee-tbl-1">
                                            <#if canShowAtLeastOne(items, object, ['jakyTypFavK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypFavK' items=items />:</td><td><@tool.place object=object name='jakyTypFavK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['jakyTypPppK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypPppK' items=items />:</td><td><@tool.place object=object name='jakyTypPppK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['jakyTypNavK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypNavK' items=items />:</td><td><@tool.place object=object name='jakyTypNavK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['jakyTypObpK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypObpK' items=items />:</td><td><@tool.place object=object name='jakyTypObpK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['doplnText'], it)>
                                    <div id="view-fragment-14" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['doplnText'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canShowAtLeastOne(items, object, ['doplnText'], it)>
                                            <tr>
                                                <td><@tool.label name='doplnText' items=items />:<br><@tool.placeArea object=object name='doplnText' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'], it)>
                                    <div id="view-fragment-15" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['tdObchodObp', 'tdObchodObpEdi', 'tdObchodFav', 'tdObchodFavZal', 'tdObchodSklVyd', 'tdObchodNav'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canShowAtLeastOne(items, object, ['tdObchodObp'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodObp' items=items />:</td><td><@tool.place object=object name='tdObchodObp' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodObpEdi'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodObpEdi' items=items />:</td><td><@tool.place object=object name='tdObchodObpEdi' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodFav'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodFav' items=items />:</td><td><@tool.place object=object name='tdObchodFav' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodFavZal'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodFavZal' items=items />:</td><td><@tool.place object=object name='tdObchodFavZal' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodSklVyd'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodSklVyd' items=items />:</td><td><@tool.place object=object name='tdObchodSklVyd' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodNav'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodNav' items=items />:</td><td><@tool.place object=object name='tdObchodNav' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                </div>
                            </#if>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['splatDnyNakup', 'podpisPrik', 'jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'], it)>
                        <div id="view-fragment-19" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['splatDnyNakup', 'podpisPrik'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="50%" /><col width="50%" />
                                <#if canShowAtLeastOne(items, object, ['splatDnyNakup'], it)>
                                <tr>
                                    <td><@tool.label name='splatDnyNakup' items=items />:</td><td><@tool.place object=object name='splatDnyNakup' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['podpisPrik'], it)>
                                <tr>
                                    <td><@tool.label name='podpisPrik' items=items />:</td><td><@tool.place object=object name='podpisPrik' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                            <#if canShowAtLeastOne(items, object, ['jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK', 'tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'], it)>
                                <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                                    <#if canShowAtLeastOne(items, object, ['jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK'], it)>
                                    <li class="active"><a href="#view-fragment-17" data-toggle="tab">Volba typu nových dokladů</a></li>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'], it)>
                                    <li><a href="#view-fragment-18" data-toggle="tab">Typy dokladů pro automatickou generaci</a></li>
                                    </#if>
                                </ul>
                                <div class="tab-content">
                                    <#if canShowAtLeastOne(items, object, ['jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK'], it)>
                                    <div id="view-fragment-17" class="tab-pane active">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['jakyTypFapK', 'jakyTypPpvK', 'jakyTypNapK', 'jakyTypObvK'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canShowAtLeastOne(items, object, ['jakyTypFapK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypFapK' items=items />:</td><td><@tool.place object=object name='jakyTypFapK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['jakyTypPpvK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypPpvK' items=items />:</td><td><@tool.place object=object name='jakyTypPpvK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['jakyTypNapK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypNapK' items=items />:</td><td><@tool.place object=object name='jakyTypNapK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['jakyTypObvK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypObvK' items=items />:</td><td><@tool.place object=object name='jakyTypObvK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'], it)>
                                    <div id="view-fragment-18" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['tdObchodFap', 'tdObchodSklPri', 'tdObchodObv', 'tdObchodPpv', 'tdObchodNap'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canShowAtLeastOne(items, object, ['tdObchodFap'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodFap' items=items />:</td><td><@tool.place object=object name='tdObchodFap' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodSklPri'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodSklPri' items=items />:</td><td><@tool.place object=object name='tdObchodSklPri' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodObv'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodObv' items=items />:</td><td><@tool.place object=object name='tdObchodObv' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodPpv'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodPpv' items=items />:</td><td><@tool.place object=object name='tdObchodPpv' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdObchodNap'], it)>
                                            <tr>
                                                <td><@tool.label name='tdObchodNap' items=items />:</td><td><@tool.place object=object name='tdObchodNap' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                </div>
                            </#if>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej'], it)>
                        <div id="view-fragment-22" class="tab-pane">
                            <#if canShowAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK', 'tdBanPrijem', 'tdBanVydej'], it)>
                                <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                                    <#if canShowAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK'], it)>
                                    <li class="active"><a href="#view-fragment-20" data-toggle="tab">Volba typu nových dokladů</a></li>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdBanPrijem', 'tdBanVydej'], it)>
                                    <li><a href="#view-fragment-21" data-toggle="tab">Volba typu dokladu pro načtené bankovní výpisy</a></li>
                                    </#if>
                                </ul>
                                <div class="tab-content">
                                    <#if canShowAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK'], it)>
                                    <div id="view-fragment-20" class="tab-pane active">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['jakyTypBanK', 'jakyTypPokK'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canShowAtLeastOne(items, object, ['jakyTypBanK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypBanK' items=items />:</td><td><@tool.place object=object name='jakyTypBanK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['jakyTypPokK'], it)>
                                            <tr>
                                                <td><@tool.label name='jakyTypPokK' items=items />:</td><td><@tool.place object=object name='jakyTypPokK' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdBanPrijem', 'tdBanVydej'], it)>
                                    <div id="view-fragment-21" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['tdBanPrijem', 'tdBanVydej'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canShowAtLeastOne(items, object, ['tdBanPrijem'], it)>
                                            <tr>
                                                <td><@tool.label name='tdBanPrijem' items=items />:</td><td><@tool.place object=object name='tdBanPrijem' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['tdBanVydej'], it)>
                                            <tr>
                                                <td><@tool.label name='tdBanVydej' items=items />:</td><td><@tool.place object=object name='tdBanVydej' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                </div>
                            </#if>
                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK'], it)>
                        <div id="view-fragment-24" class="tab-pane">
                            <#if canShowAtLeastOne(items, object, ['jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK'], it)>
                            <h4>Volba typu nových dokladů</h4>
                            <div id="view-fragment-23" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canShowAtLeastOne(items, object, ['jakyTypIntK', 'jakyTypPhlK', 'jakyTypZavK'], it)>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canShowAtLeastOne(items, object, ['jakyTypIntK'], it)>
                                    <tr>
                                        <td><@tool.label name='jakyTypIntK' items=items />:</td><td><@tool.place object=object name='jakyTypIntK' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['jakyTypPhlK'], it)>
                                    <tr>
                                        <td><@tool.label name='jakyTypPhlK' items=items />:</td><td><@tool.place object=object name='jakyTypPhlK' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['jakyTypZavK'], it)>
                                    <tr>
                                        <td><@tool.label name='jakyTypZavK' items=items />:</td><td><@tool.place object=object name='jakyTypZavK' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>
                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
                        <div id="view-fragment-35" class="tab-pane">
                            <#if canShowAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir', 'mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez', 'mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc', 'mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov', 'mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
                                <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                                    <#if canShowAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir'], it)>
                                    <li class="active"><a href="#view-fragment-27" data-toggle="tab">Pojištění</a></li>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez'], it)>
                                    <li><a href="#view-fragment-31" data-toggle="tab">Daň</a></li>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc'], it)>
                                    <li><a href="#view-fragment-32" data-toggle="tab">${lang('labels', 'ostatniPN')}</a></li>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov'], it)>
                                    <li><a href="#view-fragment-33" data-toggle="tab">Úvazek</a></li>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
                                    <li><a href="#view-fragment-34" data-toggle="tab">Dni splatnosti</a></li>
                                    </#if>
                                </ul>
                                <div class="tab-content">
                                    <#if canShowAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj', 'mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir'], it)>
                                    <div id="view-fragment-27" class="tab-pane active">
                                        <#if canShowAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj'], it)>
                                        <h4>Sociální pojištění</h4>
                                        <div id="view-fragment-25" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canShowAtLeastOne(items, object, ['mzdTdZavSoc', 'mzdTuoZavSocZam', 'mzdTuoZavSocFir', 'mzdAdrSocPoj'], it)>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canShowAtLeastOne(items, object, ['mzdTdZavSoc'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTdZavSoc' items=items />:</td><td><@tool.place object=object name='mzdTdZavSoc' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canShowAtLeastOne(items, object, ['mzdTuoZavSocZam'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTuoZavSocZam' items=items />:</td><td><@tool.place object=object name='mzdTuoZavSocZam' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canShowAtLeastOne(items, object, ['mzdTuoZavSocFir'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTuoZavSocFir' items=items />:</td><td><@tool.place object=object name='mzdTuoZavSocFir' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canShowAtLeastOne(items, object, ['mzdAdrSocPoj'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdAdrSocPoj' items=items />:</td><td><@tool.place object=object name='mzdAdrSocPoj' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                        <#if canShowAtLeastOne(items, object, ['mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir'], it)>
                                        <h4>Zdravotní pojištění</h4>
                                        <div id="view-fragment-26" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canShowAtLeastOne(items, object, ['mzdTdZavZdrav', 'mzdTuoZavZdrZam', 'mzdTuoZavZdrFir'], it)>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canShowAtLeastOne(items, object, ['mzdTdZavZdrav'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTdZavZdrav' items=items />:</td><td><@tool.place object=object name='mzdTdZavZdrav' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canShowAtLeastOne(items, object, ['mzdTuoZavZdrZam'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTuoZavZdrZam' items=items />:</td><td><@tool.place object=object name='mzdTuoZavZdrZam' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canShowAtLeastOne(items, object, ['mzdTuoZavZdrFir'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTuoZavZdrFir' items=items />:</td><td><@tool.place object=object name='mzdTuoZavZdrFir' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                    </div>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['mzdAdrDan', 'mzdTdZavDanZal', 'mzdBanDanZal', 'mzdTdZavDanSra', 'mzdBanDanSra', 'mzdTdZavDanRez', 'mzdBanDanRez'], it)>
                                    <div id="view-fragment-31" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['mzdAdrDan'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canShowAtLeastOne(items, object, ['mzdAdrDan'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdAdrDan' items=items />:</td><td><@tool.place object=object name='mzdAdrDan' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                        <#if canShowAtLeastOne(items, object, ['mzdTdZavDanZal', 'mzdBanDanZal'], it)>
                                        <h4>Daň zálohová</h4>
                                        <div id="view-fragment-28" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canShowAtLeastOne(items, object, ['mzdTdZavDanZal', 'mzdBanDanZal'], it)>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canShowAtLeastOne(items, object, ['mzdTdZavDanZal'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTdZavDanZal' items=items />:</td><td><@tool.place object=object name='mzdTdZavDanZal' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canShowAtLeastOne(items, object, ['mzdBanDanZal'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdBanDanZal' items=items />:</td><td><@tool.place object=object name='mzdBanDanZal' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                        <#if canShowAtLeastOne(items, object, ['mzdTdZavDanSra', 'mzdBanDanSra'], it)>
                                        <h4>Dan srážková</h4>
                                        <div id="view-fragment-29" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canShowAtLeastOne(items, object, ['mzdTdZavDanSra', 'mzdBanDanSra'], it)>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canShowAtLeastOne(items, object, ['mzdTdZavDanSra'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTdZavDanSra' items=items />:</td><td><@tool.place object=object name='mzdTdZavDanSra' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canShowAtLeastOne(items, object, ['mzdBanDanSra'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdBanDanSra' items=items />:</td><td><@tool.place object=object name='mzdBanDanSra' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                        <#if canShowAtLeastOne(items, object, ['mzdTdZavDanRez', 'mzdBanDanRez'], it)>
                                        <h4>Daň rezident</h4>
                                        <div id="view-fragment-30" class="tab-pane">
                                            <div class="flexibee-table-border">
                                            <#if canShowAtLeastOne(items, object, ['mzdTdZavDanRez', 'mzdBanDanRez'], it)>
                                            <table class="flexibee-tbl-1">
                                                <col width="20%" /><col width="80%" />
                                                <#if canShowAtLeastOne(items, object, ['mzdTdZavDanRez'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdTdZavDanRez' items=items />:</td><td><@tool.place object=object name='mzdTdZavDanRez' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                                <#if canShowAtLeastOne(items, object, ['mzdBanDanRez'], it)>
                                                <tr>
                                                    <td><@tool.label name='mzdBanDanRez' items=items />:</td><td><@tool.place object=object name='mzdBanDanRez' items=items marker=marker /></td>
                                                </tr>
                                                </#if>
                                            </table>
                                            </#if>
                                            </div>

                                        </div>
                                        </#if>

                                    </div>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc'], it)>
                                    <div id="view-fragment-32" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['mzdDnuDovolAbs', 'mzdPenzPrip', 'mzdPenzPripProc', 'mzdPromilePovPojisteni', 'mzdTdZavZaloha', 'mzdTdZavSrazka', 'mzdTdZavDobirka', 'mzdTdIntHruba', 'mzdTdIntNahrad', 'mzdIspvOkres', 'mzdRefundNahrNemoc'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="40%" /><col width="60%" />
                                            <#if canShowAtLeastOne(items, object, ['mzdDnuDovolAbs'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdDnuDovolAbs' items=items />:</td><td><@tool.place object=object name='mzdDnuDovolAbs' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdPenzPrip'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdPenzPrip' items=items />:</td><td><@tool.place object=object name='mzdPenzPrip' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdPenzPripProc'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdPenzPripProc' items=items />:</td><td><@tool.place object=object name='mzdPenzPripProc' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdPromilePovPojisteni'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdPromilePovPojisteni' items=items />:</td><td><@tool.place object=object name='mzdPromilePovPojisteni' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdTdZavZaloha'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdTdZavZaloha' items=items />:</td><td><@tool.place object=object name='mzdTdZavZaloha' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdTdZavSrazka'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdTdZavSrazka' items=items />:</td><td><@tool.place object=object name='mzdTdZavSrazka' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdTdZavDobirka'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdTdZavDobirka' items=items />:</td><td><@tool.place object=object name='mzdTdZavDobirka' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdTdIntHruba'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdTdIntHruba' items=items />:</td><td><@tool.place object=object name='mzdTdIntHruba' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdTdIntNahrad'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdTdIntNahrad' items=items />:</td><td><@tool.place object=object name='mzdTdIntNahrad' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdIspvOkres'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdIspvOkres' items=items />:</td><td><@tool.place object=object name='mzdIspvOkres' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdRefundNahrNemoc'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdRefundNahrNemoc' items=items />:</td><td><@tool.place object=object name='mzdRefundNahrNemoc' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov'], it)>
                                    <div id="view-fragment-33" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['mzdSmenyHodinDenne', 'mzdSmenyDnuTydne', 'mzdSmenyPocPracDoby', 'mzdSmenySvatkyPrum', 'mzdNemocProplatAuto', 'mzdNemocProplatVarov'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="50%" /><col width="50%" />
                                            <#if canShowAtLeastOne(items, object, ['mzdSmenyHodinDenne'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSmenyHodinDenne' items=items />:</td><td><@tool.place object=object name='mzdSmenyHodinDenne' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdSmenyDnuTydne'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSmenyDnuTydne' items=items />:</td><td><@tool.place object=object name='mzdSmenyDnuTydne' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdSmenyPocPracDoby'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSmenyPocPracDoby' items=items />:</td><td><@tool.place object=object name='mzdSmenyPocPracDoby' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdSmenySvatkyPrum'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSmenySvatkyPrum' items=items />:</td><td><@tool.place object=object name='mzdSmenySvatkyPrum' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdNemocProplatAuto'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdNemocProplatAuto' items=items />:</td><td><@tool.place object=object name='mzdNemocProplatAuto' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdNemocProplatVarov'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdNemocProplatVarov' items=items />:</td><td><@tool.place object=object name='mzdNemocProplatVarov' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
                                    <div id="view-fragment-34" class="tab-pane">
                                        <div class="flexibee-table-border">
                                        <#if canShowAtLeastOne(items, object, ['mzdSplatSocPoj', 'mzdSplatZdravPoj', 'mzdSplatDanZal', 'mzdSplatDanSra', 'mzdSplatDanRez', 'mzdSplatDobirka'], it)>
                                        <table class="flexibee-tbl-1">
                                            <col width="20%" /><col width="80%" />
                                            <#if canShowAtLeastOne(items, object, ['mzdSplatSocPoj'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSplatSocPoj' items=items />:</td><td><@tool.place object=object name='mzdSplatSocPoj' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdSplatZdravPoj'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSplatZdravPoj' items=items />:</td><td><@tool.place object=object name='mzdSplatZdravPoj' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdSplatDanZal'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSplatDanZal' items=items />:</td><td><@tool.place object=object name='mzdSplatDanZal' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdSplatDanSra'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSplatDanSra' items=items />:</td><td><@tool.place object=object name='mzdSplatDanSra' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdSplatDanRez'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSplatDanRez' items=items />:</td><td><@tool.place object=object name='mzdSplatDanRez' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                            <#if canShowAtLeastOne(items, object, ['mzdSplatDobirka'], it)>
                                            <tr>
                                                <td><@tool.label name='mzdSplatDobirka' items=items />:</td><td><@tool.place object=object name='mzdSplatDobirka' items=items marker=marker /></td>
                                            </tr>
                                            </#if>
                                        </table>
                                        </#if>
                                        </div>

                                    </div>
                                    </#if>
                                </div>
                            </#if>
                        </div>
                        </#if>
                    </div>
                </#if>
            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad', 'eetProvozovna', 'eetLimit', 'eetModK'], it)>
            <div id="view-fragment-45" class="tab-pane">
                <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad', 'eetProvozovna', 'eetLimit', 'eetModK'], it)>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad'], it)>
                        <li class="active"><a href="#view-fragment-43" data-toggle="tab">Typy dokladů a účty</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['eetProvozovna', 'eetLimit', 'eetModK'], it)>
                        <li><a href="#view-fragment-44" data-toggle="tab">${lang('labels', 'dokladEBEetPN')}</a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad', 'tdZbytekVynos', 'tdZbytekNaklad', 'tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl', 'ucetZaokrVynos', 'ucetZaokrNaklad', 'tdOsUpravaPrijem', 'tdOsUpravaVydej', 'ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad'], it)>
                        <div id="view-fragment-43" class="tab-pane active">
                            <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad'], it)>
                            <h4>Kurzové rozdíly</h4>
                            <div id="view-fragment-37" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilVynos', 'tdKurzovyRozdilNaklad'], it)>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilVynos'], it)>
                                    <tr>
                                        <td><@tool.label name='tdKurzovyRozdilVynos' items=items />:</td><td><@tool.place object=object name='tdKurzovyRozdilVynos' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdKurzovyRozdilNaklad'], it)>
                                    <tr>
                                        <td><@tool.label name='tdKurzovyRozdilNaklad' items=items />:</td><td><@tool.place object=object name='tdKurzovyRozdilNaklad' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canShowAtLeastOne(items, object, ['tdZbytekVynos', 'tdZbytekNaklad'], it)>
                            <h4>Zbytkové rozdíly</h4>
                            <div id="view-fragment-38" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canShowAtLeastOne(items, object, ['tdZbytekVynos', 'tdZbytekNaklad'], it)>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canShowAtLeastOne(items, object, ['tdZbytekVynos'], it)>
                                    <tr>
                                        <td><@tool.label name='tdZbytekVynos' items=items />:</td><td><@tool.place object=object name='tdZbytekVynos' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdZbytekNaklad'], it)>
                                    <tr>
                                        <td><@tool.label name='tdZbytekNaklad' items=items />:</td><td><@tool.place object=object name='tdZbytekNaklad' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canShowAtLeastOne(items, object, ['tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl'], it)>
                            <h4>Zálohový daňový doklad</h4>
                            <div id="view-fragment-39" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canShowAtLeastOne(items, object, ['tdFavZalohovyDanDokl', 'tdFapZalohovyDanDokl'], it)>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canShowAtLeastOne(items, object, ['tdFavZalohovyDanDokl'], it)>
                                    <tr>
                                        <td><@tool.label name='tdFavZalohovyDanDokl' items=items />:</td><td><@tool.place object=object name='tdFavZalohovyDanDokl' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdFapZalohovyDanDokl'], it)>
                                    <tr>
                                        <td><@tool.label name='tdFapZalohovyDanDokl' items=items />:</td><td><@tool.place object=object name='tdFapZalohovyDanDokl' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canShowAtLeastOne(items, object, ['ucetZaokrVynos', 'ucetZaokrNaklad'], it)>
                            <h4>Účty pro zaokrouhlovací rozdíl</h4>
                            <div id="view-fragment-40" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canShowAtLeastOne(items, object, ['ucetZaokrVynos', 'ucetZaokrNaklad'], it)>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canShowAtLeastOne(items, object, ['ucetZaokrVynos'], it)>
                                    <tr>
                                        <td><@tool.label name='ucetZaokrVynos' items=items />:</td><td><@tool.place object=object name='ucetZaokrVynos' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['ucetZaokrNaklad'], it)>
                                    <tr>
                                        <td><@tool.label name='ucetZaokrNaklad' items=items />:</td><td><@tool.place object=object name='ucetZaokrNaklad' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canShowAtLeastOne(items, object, ['tdOsUpravaPrijem', 'tdOsUpravaVydej'], it)>
                            <h4>${lang('labels', 'nastaveniPomocDoklPN')}</h4>
                            <div id="view-fragment-41" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canShowAtLeastOne(items, object, ['tdOsUpravaPrijem', 'tdOsUpravaVydej'], it)>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canShowAtLeastOne(items, object, ['tdOsUpravaPrijem'], it)>
                                    <tr>
                                        <td><@tool.label name='tdOsUpravaPrijem' items=items />:</td><td><@tool.place object=object name='tdOsUpravaPrijem' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['tdOsUpravaVydej'], it)>
                                    <tr>
                                        <td><@tool.label name='tdOsUpravaVydej' items=items />:</td><td><@tool.place object=object name='tdOsUpravaVydej' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                            <#if canShowAtLeastOne(items, object, ['ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad'], it)>
                            <h4>${lang('labels', 'nastaveniUctyPomocDoklPN')}</h4>
                            <div id="view-fragment-42" class="tab-pane">
                                <div class="flexibee-table-border">
                                <#if canShowAtLeastOne(items, object, ['ucetOsUpravaDphSniz', 'ucetOsUpravaDphZakl', 'ucetOsUpravaZaklad'], it)>
                                <table class="flexibee-tbl-1">
                                    <col width="20%" /><col width="80%" />
                                    <#if canShowAtLeastOne(items, object, ['ucetOsUpravaDphSniz'], it)>
                                    <tr>
                                        <td><@tool.label name='ucetOsUpravaDphSniz' items=items />:</td><td><@tool.place object=object name='ucetOsUpravaDphSniz' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['ucetOsUpravaDphZakl'], it)>
                                    <tr>
                                        <td><@tool.label name='ucetOsUpravaDphZakl' items=items />:</td><td><@tool.place object=object name='ucetOsUpravaDphZakl' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                    <#if canShowAtLeastOne(items, object, ['ucetOsUpravaZaklad'], it)>
                                    <tr>
                                        <td><@tool.label name='ucetOsUpravaZaklad' items=items />:</td><td><@tool.place object=object name='ucetOsUpravaZaklad' items=items marker=marker /></td>
                                    </tr>
                                    </#if>
                                </table>
                                </#if>
                                </div>

                            </div>
                            </#if>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['eetProvozovna', 'eetLimit', 'eetModK'], it)>
                        <div id="view-fragment-44" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['eetProvozovna', 'eetLimit', 'eetModK'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canShowAtLeastOne(items, object, ['eetProvozovna'], it)>
                                <tr>
                                    <td><@tool.label name='eetProvozovna' items=items />:</td><td><@tool.place object=object name='eetProvozovna' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['eetLimit'], it)>
                                <tr>
                                    <td><@tool.label name='eetLimit' items=items />:</td><td><@tool.place object=object name='eetLimit' items=items marker=marker /></td>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['eetModK'], it)>
                                <tr>
                                    <td><@tool.label name='eetModK' items=items />:</td><td><@tool.place object=object name='eetModK' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>
            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['denniKurzK', 'rocniKurzK', 'manualCisDokl'], it)>
            <div id="view-fragment-47" class="tab-pane">
                <#if canShowAtLeastOne(items, object, ['denniKurzK', 'rocniKurzK'], it)>
                <h4>Způsob aktualizace kurzů</h4>
                <div id="view-fragment-46" class="tab-pane">
                    <div class="flexibee-table-border">
                    <#if canShowAtLeastOne(items, object, ['denniKurzK', 'rocniKurzK'], it)>
                    <table class="flexibee-tbl-1">
                        <col width="20%" /><col width="80%" />
                        <#if canShowAtLeastOne(items, object, ['denniKurzK'], it)>
                        <tr>
                            <td><@tool.label name='denniKurzK' items=items />:</td><td><@tool.place object=object name='denniKurzK' items=items marker=marker /></td>
                        </tr>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['rocniKurzK'], it)>
                        <tr>
                            <td><@tool.label name='rocniKurzK' items=items />:</td><td><@tool.place object=object name='rocniKurzK' items=items marker=marker /></td>
                        </tr>
                        </#if>
                    </table>
                    </#if>
                    </div>

                </div>
                </#if>

                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['manualCisDokl'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['manualCisDokl'], it)>
                    <tr>
                        <td><@tool.label name='manualCisDokl' items=items />:</td><td><@tool.place object=object name='manualCisDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<br><br>
<div class="flexibee-table-border">
    <table class="flexibee-tbl-1">
        <col width="50%"/><col width="50%"/>

        <tr>
            <td><strong>Logo</strong></td>
            <#if isMobile></tr>
            <tr></#if>
            <td><strong>Podpis a razítko</strong></td>
        </tr>

        <tr>
            <td>
                <div id="flexibee-nastaveni-logo">
                    <#if it.evidenceResource.logoId(it.object.id)??>
                        <img src="/c/${it.companyId}/priloha/${it.evidenceResource.logoId(it.object.id)}/thumbnail"/>
                    <#else>Žádné</#if>
                </div>
            </td>
            <#if isMobile></tr><tr></#if>
            <td>
                <div id="flexibee-nastaveni-podraz">
                    <#if it.evidenceResource.podrazId(it.object.id)??>
                        <img src="/c/${it.companyId}/priloha/${it.evidenceResource.podrazId(it.object.id)}/thumbnail"/>
                    <#else>Žádné</#if>
                </div>
            </td>
        </tr>
    </table>
</div>

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>
