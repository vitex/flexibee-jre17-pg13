<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>
<#assign title="Dokumentace detailu "+it.name />
<#assign titleId="root"/>
<#include "/header.ftl" />

<p>Dokumentace detailu</p>
<ul>
    <li>Načtení záznamu s ID: <code>${it.id}</code></li>
</ul>

<ul>
    <li><a href="properties">Přehled jednotlivých položek</a></li>
    <li><a href="relations">Přehled vazeb</a></li>
    <li><a href="reports">Přehled reportů</a></li>
</ul>

<#include "/i-devdoc-footer.ftl" />

<#include "/footer.ftl" />
</#escape>