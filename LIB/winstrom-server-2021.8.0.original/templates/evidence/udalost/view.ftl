<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('visible')}
<#if  canShowAtLeastOne(items, object, ['typAkt'])>
    ${marker.mark('druhUdalK')}
</#if>

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['druhUdalK', 'typAkt', 'zodpPrac', 'firma', 'zakazka', 'kontakt', 'zahajeni', 'volno', 'dokonceni', 'celodenni', 'stavUdalK', 'termin', 'predmet', 'umisteni', 'prioritaK'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canShowAtLeastOne(items, object, ['druhUdalK'], it)>
        <tr>
            <td><@tool.label name='druhUdalK' items=items />:</td><td colspan="3"><@tool.place object=object name='druhUdalK' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['typAkt', 'zodpPrac'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['typAkt'], it)>
            <td><@tool.label name='typAkt' items=items />:</td><td><@tool.place object=object name='typAkt' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['zodpPrac'], it)>
            <td><@tool.label name='zodpPrac' items=items />:</td><td><@tool.place object=object name='zodpPrac' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['firma', 'zakazka'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['firma'], it)>
            <td><@tool.label name='firma' items=items />:</td><td><@tool.place object=object name='firma' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['zakazka'], it)>
            <td><@tool.label name='zakazka' items=items />:</td><td><@tool.place object=object name='zakazka' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kontakt'], it)>
        <tr>
            <td><@tool.label name='kontakt' items=items />:</td><td><@tool.place object=object name='kontakt' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['zahajeni', 'volno'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['zahajeni'], it)>
            <td><@tool.label name='zahajeni' items=items />:</td><td><@tool.place object=object name='zahajeni' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['volno'], it)>
            <td><@tool.label name='volno' items=items />:</td><td><@tool.place object=object name='volno' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['dokonceni'], it)>
        <tr>
            <td><@tool.label name='dokonceni' items=items />:</td><td><@tool.place object=object name='dokonceni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['celodenni'], it)>
        <tr>
            <td><@tool.label name='celodenni' items=items />:</td><td><@tool.place object=object name='celodenni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['stavUdalK', 'termin'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['stavUdalK'], it)>
            <td><@tool.label name='stavUdalK' items=items />:</td><td><@tool.place object=object name='stavUdalK' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['termin'], it)>
            <td><@tool.label name='termin' items=items />:</td><td><@tool.place object=object name='termin' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['predmet'], it)>
        <tr>
            <td><@tool.label name='predmet' items=items level='0'/>:</td><td colspan="3"><@tool.place object=object level='0' name='predmet' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['umisteni'], it)>
        <tr>
            <td><@tool.label name='umisteni' items=items />:</td><td colspan="3"><@tool.place object=object name='umisteni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['prioritaK'], it)>
        <tr>
            <td><@tool.label name='prioritaK' items=items />:</td><td colspan="3"><@tool.place object=object name='prioritaK' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['popis', 'cenik', 'doklSklad', 'majetek', 'doklFak', 'doklObch', 'doklInt', 'poznam'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['popis'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">Popis</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['cenik', 'doklSklad', 'majetek', 'doklFak', 'doklObch', 'doklInt'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">Relace</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['poznam'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['popis'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['cenik', 'doklSklad', 'majetek', 'doklFak', 'doklObch', 'doklInt'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['cenik', 'doklSklad', 'majetek', 'doklFak', 'doklObch', 'doklInt'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['cenik'], it)>
                    <tr>
                        <td><@tool.label name='cenik' items=items />:</td><td><@tool.place object=object name='cenik' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['doklSklad'], it)>
                    <tr>
                        <td><@tool.label name='doklSklad' items=items />:</td><td><@tool.place object=object name='doklSklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['majetek'], it)>
                    <tr>
                        <td><@tool.label name='majetek' items=items />:</td><td><@tool.place object=object name='majetek' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['doklFak'], it)>
                    <tr>
                        <td><@tool.label name='doklFak' items=items />:</td><td><@tool.place object=object name='doklFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['doklObch'], it)>
                    <tr>
                        <td><@tool.label name='doklObch' items=items />:</td><td><@tool.place object=object name='doklObch' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['doklInt'], it)>
                    <tr>
                        <td><@tool.label name='doklInt' items=items />:</td><td><@tool.place object=object name='doklInt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['poznam'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>
