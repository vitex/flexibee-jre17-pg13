<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />


<#-- nezobrazované položky -->
${marker.mark('modul')}
${marker.mark('kod')}
${marker.mark('zamekK')}
${marker.mark('typPohybuK')}
${marker.mark('generovatSkl')}
${marker.mark('bezPolozek')}
${marker.mark('prodejka')}
${marker.mark('zdrojProSkl')}
${marker.mark('hromFakt')}

<div id="creditcheck-container">
</div>

<#if isMobile == false>
<table class="flexibee-doklad-layout">
    <col width="50%"/>
    <col width="50%"/>
<tr><td>

<#include "/parts/i-stav.ftl" />
<#include "/parts/i-zauctovani.ftl" />
<#include "/parts/i-fakturace.ftl" />

</td><td>

<#include "/parts/i-prijemce.ftl" />
<#include "/parts/i-castky.ftl" />

</td></tr></table>
<#else>

<#include "/parts/i-stav.ftl" />
<#include "/parts/i-prijemce.ftl" />
<#include "/parts/i-castky.ftl" />

<#include "/parts/i-zauctovani.ftl" />
<#include "/parts/i-fakturace.ftl" />
</#if>

<#include "/parts/i-dalsi-informace.ftl" />
<#include "/parts/i-polozky.ftl" />

<#if isSlowMobile == false && isMobile == false>

<#import "/l-prijemce-tools.ftl" as prijemce />
<@prijemce.placeCreditCheckJavaScript />

<script type="text/javascript">
    $(function() {
        <#if object.ic != '' && object.dic != ''>
            var ic = '${object.ic}';
            var dic = '${object.dic}';
            var stat = '<#if object.stat??>${object.stat.kod}<#else>CZ</#if>';

            handleCreditCheck(stat, ic, dic);
        </#if>
    });
</script>
</#if>


<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

</#escape>