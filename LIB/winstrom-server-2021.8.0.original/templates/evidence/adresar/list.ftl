<#ftl encoding="utf-8"  strip_whitespace=true />
<#escape x as x?default("")?html>

<#include "/i-header-list.ftl" />
<#include "/evidence/i-list-begin.ftl" />

<#if it.rowCount &gt; 0>
<table class="table flexibee-tbl-list table-striped table-hover table-condensed">
    <thead>
    <tr>
        <th><@tool.listHeader name="nazev" items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
        <th><@tool.listHeader name="kod"   items=it.devDocItemsForHtmlMap url=url urlSuffix=filterQueryParam orderProperty=orderProperty orderDirection=orderDirection/></th>
    </tr>
    </thead>
    <tbody class="flexibee-tbl-list-body">
    <#list it.list as object>
        <#assign dataUrl = "/c/" + it.companyResource.companyId + "/" + it.evidenceType.path + "/" + object.id?string('0') />
    <tr class="<#if object_index % 2 = 0>flexibee-even</#if> flexibee-clickable flexibee-keynav">
        <td colspan="2">
        <a href="${dataUrl}">
            <big><strong><@tool.place marker=marker object=object name="nazev" items=it.devDocItemsForHtmlMap /></strong></big><br/>
            <#if isMobile == false && object['skupFir']??><small><div style="float:right;color:gray;text-align:right">${object['skupFir'].nazev}</small></div></#if>
            <small><small>IČO: <@tool.placeList marker=marker object=object name=["ic", "dic", "ic-dph"] items=it.devDocItemsForHtmlMap /></small></small><br/>
            <@tool.placeList marker=marker object=object name=["ulice", "mesto", "psc", "stat"] items=it.devDocItemsForHtmlMap />
</a></td>
    </tr>
        </#list>
    </tbody>
</table>
</#if>

<#include "/evidence/i-list-end.ftl" />


<#include "/footer.ftl" />
</#escape>