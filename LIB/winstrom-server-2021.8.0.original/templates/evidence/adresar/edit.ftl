<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('visible')}
${marker.mark('vatId')}
${marker.mark('kodPojistovny')}
${marker.mark('nazevPojistovny')}
${marker.mark('stitky')}
${marker.mark('revize')}
${marker.mark('nespolehlivyPlatce')}
${marker.mark('insolvence')}


<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <#if isSlowMobile == false && isMobile == false>
    <table class="table">
        <col width="100%" />
        <col />
    <tr>
    <td style="vertical-align:top">
    </#if>

    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['nazev', 'nazev2', 'skupFir', 'kod', 'typVztahuK', 'eanKod'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items level='0' /></td><td><@edit.place object=object name='nazev' items=items marker=marker  level='0'/></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev2'])>
        <tr>
            <td><@edit.label name='nazev2' items=items level='0' /></td><td><@edit.place object=object name='nazev2' items=items marker=marker  level='0'/></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['skupFir'])>
        <tr>
            <td><@edit.label name='skupFir' items=items /></td><td><@edit.place object=object name='skupFir' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items level='3' /></td><td><@edit.place object=object name='kod' items=items marker=marker  level='3'/></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['typVztahuK'])>
        <tr>
            <td><@edit.label name='typVztahuK' items=items level='3' /></td><td><@edit.place object=object name='typVztahuK' items=items marker=marker  level='3'/></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['eanKod'])>
        <tr>
            <td><@edit.label name='eanKod' items=items /></td><td><@edit.place object=object name='eanKod' items=items marker=marker  type='ean'/></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['ic', 'dic', 'vatId', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'zodpOsoba', 'platceDph', 'postovniShodna', 'faJmenoFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'faEanKod', 'skupCen', 'slevaDokl', 'stredisko', 'splatDny', 'limitFak', 'formExportK', 'formaUhradyCis', 'limitPoSplatDny', 'limitPoSplatZakaz', 'popis', 'poznam', 'osloveni', 'katastrUzemi', 'parcela', 'rodCis', 'datNaroz', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['ic', 'dic', 'vatId', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'zodpOsoba', 'platceDph'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>${lang('labels', 'zaklInformacePN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['postovniShodna', 'faJmenoFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'faEanKod'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'postovniPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['skupCen', 'slevaDokl', 'stredisko', 'splatDny', 'limitFak', 'formExportK', 'formaUhradyCis', 'limitPoSplatDny', 'limitPoSplatZakaz'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'upresneniPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'osloveni'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['katastrUzemi', 'parcela', 'rodCis', 'datNaroz'])>
            <li><a href="#edit-fragment-5" data-toggle="tab"><span>${lang('labels', 'ostatniPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-6" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['ic', 'dic', 'vatId', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'zodpOsoba', 'platceDph'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['ic', 'dic', 'vatId', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'zodpOsoba', 'platceDph'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['ic', 'dic'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ic'])>
                        <td><@edit.label name='ic' items=items /></td><td><@edit.place object=object name='ic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['dic'])>
                        <td><@edit.label name='dic' items=items /></td><td><@edit.place object=object name='dic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['vatId'])>
                    <tr>
                        <td><@edit.label name='vatId' items=items /></td><td><@edit.place object=object name='vatId' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ulice'])>
                    <tr>
                        <td><@edit.label name='ulice' items=items /></td><td colspan="3"><@edit.place object=object name='ulice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mesto', 'psc'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mesto'])>
                        <td><@edit.label name='mesto' items=items /></td><td><@edit.place object=object name='mesto' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['psc'])>
                        <td><@edit.label name='psc' items=items /></td><td><@edit.place object=object name='psc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stat'])>
                    <tr>
                        <td><@edit.label name='stat' items=items /></td><td colspan="3"><@edit.place object=object name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['tel'])>
                    <tr>
                        <td><@edit.label name='tel' items=items /></td><td><@edit.place object=object name='tel' items=items marker=marker  type='tel'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mobil', 'fax'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mobil'])>
                        <td><@edit.label name='mobil' items=items /></td><td><@edit.place object=object name='mobil' items=items marker=marker  type='tel'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['fax'])>
                        <td><@edit.label name='fax' items=items /></td><td><@edit.place object=object name='fax' items=items marker=marker  type='fax'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['email', 'www'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['email'])>
                        <td><@edit.label name='email' items=items /></td><td><@edit.place object=object name='email' items=items marker=marker  type='email'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['www'])>
                        <td><@edit.label name='www' items=items /></td><td><@edit.place object=object name='www' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zodpOsoba', 'platceDph'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['zodpOsoba'])>
                        <td><@edit.label name='zodpOsoba' items=items /></td><td><@edit.place object=object name='zodpOsoba' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['platceDph'])>
                        <td><@edit.label name='platceDph' items=items /></td><td><@edit.place object=object name='platceDph' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['postovniShodna', 'faJmenoFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'faEanKod'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['postovniShodna', 'faJmenoFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'faEanKod'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['postovniShodna'])>
                    <tr>
                        <td><@edit.label name='postovniShodna' items=items /></td><td><@edit.place object=object name='postovniShodna' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faJmenoFirmy'])>
                    <tr>
                        <td><@edit.label name='faJmenoFirmy' items=items /></td><td><@edit.place object=object name='faJmenoFirmy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faUlice'])>
                    <tr>
                        <td><@edit.label name='faUlice' items=items /></td><td><@edit.place object=object name='faUlice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faMesto'])>
                    <tr>
                        <td><@edit.label name='faMesto' items=items /></td><td><@edit.place object=object name='faMesto' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faPsc'])>
                    <tr>
                        <td><@edit.label name='faPsc' items=items /></td><td><@edit.place object=object name='faPsc' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faStat'])>
                    <tr>
                        <td><@edit.label name='faStat' items=items /></td><td><@edit.place object=object name='faStat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['faEanKod'])>
                    <tr>
                        <td><@edit.label name='faEanKod' items=items /></td><td><@edit.place object=object name='faEanKod' items=items marker=marker  type='ean'/></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['skupCen', 'slevaDokl', 'stredisko', 'splatDny', 'limitFak', 'formExportK', 'formaUhradyCis', 'limitPoSplatDny', 'limitPoSplatZakaz'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['skupCen', 'slevaDokl', 'stredisko', 'splatDny', 'limitFak', 'formExportK', 'formaUhradyCis', 'limitPoSplatDny', 'limitPoSplatZakaz'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['skupCen'])>
                    <tr>
                        <td><@edit.label name='skupCen' items=items /></td><td><@edit.place object=object name='skupCen' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['slevaDokl'])>
                    <tr>
                        <td><@edit.label name='slevaDokl' items=items /></td><td><@edit.place object=object name='slevaDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stredisko'])>
                    <tr>
                        <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['splatDny'])>
                    <tr>
                        <td><@edit.label name='splatDny' items=items /></td><td><@edit.place object=object name='splatDny' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['limitFak'])>
                    <tr>
                        <td><@edit.label name='limitFak' items=items /></td><td><@edit.place object=object name='limitFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['formExportK'])>
                    <tr>
                        <td><@edit.label name='formExportK' items=items /></td><td><@edit.place object=object name='formExportK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['formaUhradyCis'])>
                    <tr>
                        <td><@edit.label name='formaUhradyCis' items=items /></td><td><@edit.place object=object name='formaUhradyCis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['limitPoSplatDny'])>
                    <tr>
                        <td><@edit.label name='limitPoSplatDny' items=items /></td><td><@edit.place object=object name='limitPoSplatDny' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['limitPoSplatZakaz'])>
                    <tr>
                        <td><@edit.label name='limitPoSplatZakaz' items=items /></td><td><@edit.place object=object name='limitPoSplatZakaz' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'osloveni'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'osloveni'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td colspan="4"><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=10 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td colspan="4"><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['osloveni'])>
                    <tr>
                        <td><@edit.label name='osloveni' items=items /></td><td><@edit.place object=object name='osloveni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['katastrUzemi', 'parcela', 'rodCis', 'datNaroz'])>
            <div id="edit-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['katastrUzemi', 'parcela', 'rodCis', 'datNaroz'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['katastrUzemi'])>
                    <tr>
                        <td><@edit.label name='katastrUzemi' items=items /></td><td><@edit.place object=object name='katastrUzemi' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['parcela'])>
                    <tr>
                        <td><@edit.label name='parcela' items=items /></td><td><@edit.place object=object name='parcela' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['rodCis'])>
                    <tr>
                        <td><@edit.label name='rodCis' items=items /></td><td><@edit.place object=object name='rodCis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['datNaroz'])>
                    <tr>
                        <td><@edit.label name='datNaroz' items=items /></td><td><@edit.place object=object name='datNaroz' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-6" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />


<#if isSlowMobile == false>
<script type="text/javascript">
    var oldIc;
    var oldDic;
    var oldStat;

    var statCode; // kód právě vybraného státu

    function statChanged() {
        var q = $("#flexibee-inp-stat-flexbox_input").val() || '<#if object.stat??>${object.stat.kod}<#else>CZ</#if>';
        if (/^[A-Z]{2}:/.test(q)) {
            q = q.substring(0, 2);
        }
        $.getJSON('${formListUrl(it.companyResource, 'STAT')}.json?mode=suggest&q=' + q, function(data) {
            if (data && data.results && data.results[0]) {
                statCode = data.results[0].name.substring(0, 2);
            }
        });
    }

    function onCompanyReload() {
        var ic = $("#flexibee-inp-ic").val();
        var dic = $("#flexibee-inp-dic").val();
        var stat = '<#if object.stat??>${object.stat.kod}<#else>CZ</#if>';

        if (ic != oldIc || dic != oldDic || stat != oldStat) {
            oldIc = ic;
            oldDic = dic;
            oldStat = stat;

            $('#logobox-container-td').css('display', 'none');

            if (ic != '' && dic != '') {
                $('#logobox-container').html('<center><img id="logobox-img" src="https://support.flexibee.eu/remote/logo/?stat=' + stat + '&amp;ic=' + ic + '&amp;vatid=' + dic + '" alt="Logo ${object.nazev}"/><br/><small><a href="http://www.logobox.cz/" style="text-decoration:none;color:gray" rel="nofollow">logobox.cz<'+'/a><'+'/small><'+'/center>');
                $("#logobox-img").load(function() {
                    $('#logobox-container-td').css('display', 'block');
                });
            }
        }
    }

    function humanifyValue(val) {
        if (val === undefined || val === null) {
            return "";
        } else if (val === "true" || val === true) {
            return "ano";
        } else if (val === "false" || val === false) {
            return "ne";
        } else {
            return $('<div/>').text(val).html();
        }
    }

    <#if !isMobile>
    <#--
      - TODO pokud jsou hodnoty získané ze support.flexibee.eu/remote/... ve stejném formátu jako z REST API,
      - pak by šlo s výhodou použít flexibee-formandjson.js
      -->
    function updateFromInternet() {
        var titleByPropName = {
            <#list it.devDocItems as prop>
                ${prop.propertyName}: "${prop.title}"<#if prop_has_next>,</#if>
            </#list>
        };

        var ic = $("#flexibee-inp-ic").val();
        var dic = $("#flexibee-inp-dic").val();
        var stat = '<#if object.stat??>${object.stat.kod}<#else>CZ</#if>';

        if (ic != '' || dic != '') {
            $.getJSON('https://support.flexibee.eu/remote/company-info/' + stat + '/?ic=' + ic + '&dic=' + dic + '&format=json', function(data) {
                if (data && data.winstrom && data.winstrom.adresar) {
                    var html = '<div class="flexibee-dialog-2"><p>Na internetu byly nalezeny tyto změny:<'+'/p>'
                            + '<table><col width="33%"/><col width="3%"/><col width="30%"/><col width="3%"/><col width="30%"/>'
                            + '<tr><td/><td/><td>původní<'+'/td><td/><td>nová<'+'/td><'+'/tr>';

                    var foundChange = false;
                    for (var f in data.winstrom.adresar) {
                        <#-- titleByPropName[f] -- podmínka, zda to políčko u nás existuje -->
                        if (titleByPropName[f] && typeof(data.winstrom.adresar[f]) == 'string') {
                            var origValue = $(document).fieldTitle(f);

                            var newValue = data.winstrom.adresar[f].replace(/^code:/, "");
                            if (newValue == "true" || newValue == "false") { newValue = newValue == "true"; }

                            if (f == "stat" && newValue == statCode) {
                                continue;
                            }

                            if (origValue != newValue) {
                                var title = titleByPropName[f] || f;
                                html += '<tr><td><span>' + title + '<'+'/span><'+'/td>'
                                        + '<td><input id="orig-' + f + '" type="radio" name="ares-' + f + '" value="0"/><'+'/td><td><label for="orig-' + f + '">' + humanifyValue(origValue) + '<'+'/label><'+'/td>'
                                        + '<td><input id="new-' + f + '" type="radio" name="ares-' + f + '" value="1" checked="checked"/><'+'/td><td><label for="new-' + f + '">' + humanifyValue(newValue) + '<'+'/label><'+'/td>'
                                        + '<'+'/tr>';

                                foundChange = true;
                            }
                        }
                    }

                    html += "<"+"/table><"+"/div>";

                    if (foundChange) {
                        $(html).dialog({
                            width: 400,
                            minWidth: 400,
                            title: 'Aktualizace z internetu',
                            buttons: [ // ani z pohledu do zdrojáků mi není jasné proč, ale tlačítka musí být v opačném pořadí
                                {
                                    text: "Zrušit",
                                    click: function() {
                                        $(this).dialog("close");
                                    }
                                },
                                {
                                    text: "OK",
                                    click: function() {
                                        for (var f in data.winstrom.adresar) {
                                            if (titleByPropName[f] && typeof(data.winstrom.adresar[f]) == 'string') {
                                                var origValue = $(document).fieldTitle(f);
                                                var newValue = data.winstrom.adresar[f].replace(/^code:/, "");
                                                if (newValue == "true" || newValue == "false") { newValue = newValue == "true"; }
                                                var newValueSelected = $("input[name='ares-" + f + "']:checked").val() == "1";

                                                //console.log("origValue of ", f, " is ", origValue, ", newValue is ", newValue, " and change is ", newValueSelected ? "yes" : "no");
                                                if (newValueSelected && origValue != newValue) {
                                                    //console.log("new value for ", f, " will be ", newValue);

                                                    var field = $("#flexibee-inp-" + f);
                                                    if (!field.length) {
                                                        field = $("#flexibee-inp-" + f + "-flexbox");
                                                    }

                                                    if (f == "stat" && newValue != statCode) {
                                                        field.fillFlexBoxData('${formListUrl(it.companyResource, 'STAT')}', newValue);
                                                    } else if (field.is(":checkbox")) {
                                                        if (newValue) {
                                                            field.attr("checked", "checked");
                                                        } else {
                                                            field.removeAttr("checked");
                                                        }
                                                    } else {
                                                        field.valWithAlert(newValue);
                                                    }
                                                }
                                            }
                                        }

                                        $(this).dialog("close");

                                        if (window.mainForm) { window.mainForm.forceAmend(); }
                                    }
                                }
                            ]
                        });
                    } else {
                        alert("Nebyla nalezena žádná změna");
                    }
                } else {
                    alert("${lang('messages', 'adresarAktInternetNotFound', 'Na internetu se nepodařilo nalézt informace o vybrané firmě.')}");
                }
            });
        } else {
            alert("${lang('messages', 'internetUpdate.ic_dic_empty', 'Není vyplněné IČO ani DIČ.')}");
        }
    }
    </#if>

    $(function() {
        statChanged();
        onCompanyReload();
        <#if !isMobile>
            $(".flexibee-toolbar-item-aktualizace-z-internetu").click(function() {
                updateFromInternet();
                return false;
            });
        </#if>
        $("#flexibee-inp-stat-flexbox").bind("flexbox-change", statChanged);

        <#if !object.id?? || object.id == -1>
            var extraParams = { country: "<#if object.stat?? && false>${object.stat.kod}<#else>${settings().statLegislativa.kod}</#if>", format: "json" };

            $("#flexibee-inp-stat-flexbox_input").change(function() {
                var stat = $(this).val();
                if (stat.length > 2) {
                    stat = stat.substring(0, 2);
                }
                if (stat != '') {
                    extraParams.country = stat;
                    onCompanyReload();
                }
            });

            $("#flexibee-inp-ic").change(function() {
               onCompanyReload();
            });

            $("#flexibee-inp-dic").change(function() {
               onCompanyReload();
            });

            $("#flexibee-inp-nazev").autocomplete("https://support.flexibee.eu/remote/suggest/", {
                width: 280,
                extraParams: extraParams,
                dataType: "json",
                selectFirst: false,
                minChars: 3,

                parse: function(data) {
                    return $.map(data.winstrom.adresar, function(row) {
                        return {
                            data: row,
                            value: row.nazev,
                            result: row.nazev
                        }
                    });
                },
                formatItem: function(item) {
                    var nazev = item.nazev;
                    if (nazev.length > 50) {
                        nazev = nazev.substring(0, 50) + "...";
                    }
                    return nazev + "<br/><small style=\"color:gray\">" + item.ulice + ", " + item.mesto + (item.ic ? " | IČO: " + item.ic : "") + "</small>";
                }
            }).result(function(e, item) {
                $("#flexibee-inp-nazev").valWithAlert(item.nazev).change();
                $("#flexibee-inp-ic").valWithAlert(item.ic).change();
                $("#flexibee-inp-ulice").valWithAlert(item.ulice).change();

                $("#flexibee-inp-stat-flexbox").fillFlexBoxData('${formListUrl(it.companyResource, 'STAT')}', item.stat);

                $("#flexibee-inp-psc").valWithAlert(item.psc).change();
                $("#flexibee-inp-dic").valWithAlert(item.dic).change();
                $("#flexibee-inp-mesto").valWithAlert(item.mesto).change();

                $("#flexibee-inp-kod").valWithAlert("").change();
            });

        </#if>

        <#assign pscUrl = formListUrl(it.companyResource, 'PSC')!"" />
        $("#flexibee-inp-psc, #flexibee-inp-mesto").suggestPsc('${pscUrl}', '#flexibee-inp-stat', function(data) {
                $("#flexibee-inp-mesto").valWithAlert(data.nazev).change();
                $("#flexibee-inp-psc").valWithAlert(data.kod).change();
        });
        $("#flexibee-inp-faPsc, #flexibee-inp-faMesto").suggestPsc('${pscUrl}', '#flexibee-inp-faStat', function(data) {
                $("#flexibee-inp-faMesto").valWithAlert(data.nazev).change();
                $("#flexibee-inp-faPsc").valWithAlert(data.kod).change();
        });
    });

</script>
</#if>

<@edit.generateNaiveJavascript object=object it=it />

<#include "/i-footer-edit.ftl" />
</#escape>
