<#ftl encoding="utf-8" strip_whitespace=true/>
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('visible')}
${marker.mark('kodPojistovny')}
${marker.mark('nazevPojistovny')}
${marker.mark('postovniShodna')}
${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}
${marker.mark('nazev')}

<#if isSlowMobile == false && isMobile == false>

<#import "/l-prijemce-tools.ftl" as prijemce />
<@prijemce.placeCreditCheckJavaScript />

<script type="text/javascript">
    $(function() {
        <#-- TODO nevím proč se tu používalo https, ale server momentálně nemá rozumný certifikát a prohlížeče dělají haryky -->
        $('#logobox-container').html('<img id="logobox-img" src="https://support.flexibee.eu/remote/logo/?stat=<#if object.stat??>${object.stat.kod}<#else>CZ</#if>&amp;ic=${object.ic}&amp;vatid=${object.dic}" alt="Logo ${object.nazev}"/><div class="logobox-container-label"><center><small><a href="http://www.logobox.cz/" style="text-decoration:none;color:gray" rel="nofollow">logobox.cz<'+'/a><'+'/small><'+'/center></div>');
        $("#logobox-img").load(function() {
            $('#logobox-container-td').css('display', 'block');
        });

        <#if object.ic?? && object.ic != '' || object.dic?? && object.dic != ''>
            var ic = '${object.ic}';
            var dic = '${object.dic}';
            var stat = '<#if object.stat??>${object.stat.kod}<#else>CZ</#if>';

            handleCreditCheck(stat, ic, dic);
        </#if>
    });
</script>
</#if>

<div id="creditcheck-container">
</div>

<#--AUTOGEN BEGIN-->
    <#if isSlowMobile == false && isMobile == false>
    <table class="table">
        <col width="100%" />
        <col />
    <tr>
    <td style="vertical-align:top">
    </#if>

    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['nazev2', 'skupFir', 'kod', 'typVztahuK', 'eanKod'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['nazev2'], it)>
        <tr>
            <td><@tool.label name='nazev2' items=items level='0'/>:</td><td><@tool.place object=object level='0' name='nazev2' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['skupFir'], it)>
        <tr>
            <td><@tool.label name='skupFir' items=items />:</td><td><@tool.place object=object name='skupFir' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['typVztahuK'], it)>
        <tr>
            <td><@tool.label name='typVztahuK' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='typVztahuK' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['eanKod'], it)>
        <tr>
            <td><@tool.label name='eanKod' items=items />:</td><td><@tool.place object=object name='eanKod' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if isSlowMobile == false && isMobile == false>
    </td>
    <td id="logobox-container-td" style="display:none">
    <div id="logobox-container"></div>
    </td>
    </tr></table>
    </#if>

    <#include "i-info.ftl"/>

    <#if canShowAtLeastOne(items, object, ['ic', 'dic', 'vatId', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'zodpOsoba', 'platceDph', 'faJmenoFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'faEanKod', 'skupCen', 'slevaDokl', 'stredisko', 'splatDny', 'limitFak', 'formExportK', 'formaUhradyCis', 'limitPoSplatDny', 'limitPoSplatZakaz', 'popis', 'poznam', 'osloveni', 'katastrUzemi', 'parcela', 'rodCis', 'datNaroz', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['ic', 'dic', 'vatId', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'zodpOsoba', 'platceDph'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'zaklInformacePN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['faJmenoFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'faEanKod'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'postovniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['skupCen', 'slevaDokl', 'stredisko', 'splatDny', 'limitFak', 'formExportK', 'formaUhradyCis', 'limitPoSplatDny', 'limitPoSplatZakaz'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'upresneniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam', 'osloveni'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['katastrUzemi', 'parcela', 'rodCis', 'datNaroz'], it)>
            <li><a href="#view-fragment-5" data-toggle="tab">${lang('labels', 'ostatniPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-6" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['ic', 'dic', 'vatId', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'zodpOsoba', 'platceDph'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['ic', 'dic', 'vatId', 'ulice', 'mesto', 'psc', 'stat', 'tel', 'mobil', 'fax', 'email', 'www', 'zodpOsoba', 'platceDph'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['ic', 'dic'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ic'], it)>
                        <td><@tool.label name='ic' items=items />:</td><td><@tool.place object=object name='ic' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['dic'], it)>
                        <td><@tool.label name='dic' items=items />:</td><td><@tool.place object=object name='dic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['vatId'], it)>
                    <tr>
                        <td><@tool.label name='vatId' items=items />:</td><td><@tool.place object=object name='vatId' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ulice'], it)>
                    <tr>
                        <td><@tool.label name='ulice' items=items />:</td><td colspan="3"><@tool.place object=object name='ulice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['mesto', 'psc'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['mesto'], it)>
                        <td><@tool.label name='mesto' items=items />:</td><td><@tool.place object=object name='mesto' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['psc'], it)>
                        <td><@tool.label name='psc' items=items />:</td><td><@tool.place object=object name='psc' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stat'], it)>
                    <tr>
                        <td><@tool.label name='stat' items=items />:</td><td colspan="3"><@tool.place object=object name='stat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['tel'], it)>
                    <tr>
                        <td><@tool.label name='tel' items=items />:</td><td><@tool.placeTel object=object name='tel' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['mobil', 'fax'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['mobil'], it)>
                        <td><@tool.label name='mobil' items=items />:</td><td><@tool.placeTel object=object name='mobil' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['fax'], it)>
                        <td><@tool.label name='fax' items=items />:</td><td><@tool.place object=object name='fax' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['email', 'www'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['email'], it)>
                        <td><@tool.label name='email' items=items />:</td><td><@tool.placeEmail object=object name='email' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['www'], it)>
                        <td><@tool.label name='www' items=items />:</td><td><@tool.place object=object name='www' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zodpOsoba', 'platceDph'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['zodpOsoba'], it)>
                        <td><@tool.label name='zodpOsoba' items=items />:</td><td><@tool.place object=object name='zodpOsoba' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['platceDph'], it)>
                        <td><@tool.label name='platceDph' items=items />:</td><td><@tool.place object=object name='platceDph' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['faJmenoFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'faEanKod'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['faJmenoFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat', 'faEanKod'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['faJmenoFirmy'], it)>
                    <tr>
                        <td><@tool.label name='faJmenoFirmy' items=items />:</td><td><@tool.place object=object name='faJmenoFirmy' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['faUlice'], it)>
                    <tr>
                        <td><@tool.label name='faUlice' items=items />:</td><td><@tool.place object=object name='faUlice' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['faMesto'], it)>
                    <tr>
                        <td><@tool.label name='faMesto' items=items />:</td><td><@tool.place object=object name='faMesto' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['faPsc'], it)>
                    <tr>
                        <td><@tool.label name='faPsc' items=items />:</td><td><@tool.place object=object name='faPsc' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['faStat'], it)>
                    <tr>
                        <td><@tool.label name='faStat' items=items />:</td><td><@tool.place object=object name='faStat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['faEanKod'], it)>
                    <tr>
                        <td><@tool.label name='faEanKod' items=items />:</td><td><@tool.place object=object name='faEanKod' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['skupCen', 'slevaDokl', 'stredisko', 'splatDny', 'limitFak', 'formExportK', 'formaUhradyCis', 'limitPoSplatDny', 'limitPoSplatZakaz'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['skupCen', 'slevaDokl', 'stredisko', 'splatDny', 'limitFak', 'formExportK', 'formaUhradyCis', 'limitPoSplatDny', 'limitPoSplatZakaz'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['skupCen'], it)>
                    <tr>
                        <td><@tool.label name='skupCen' items=items />:</td><td><@tool.place object=object name='skupCen' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['slevaDokl'], it)>
                    <tr>
                        <td><@tool.label name='slevaDokl' items=items />:</td><td><@tool.place object=object name='slevaDokl' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['stredisko'], it)>
                    <tr>
                        <td><@tool.label name='stredisko' items=items />:</td><td><@tool.place object=object name='stredisko' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['splatDny'], it)>
                    <tr>
                        <td><@tool.label name='splatDny' items=items />:</td><td><@tool.place object=object name='splatDny' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['limitFak'], it)>
                    <tr>
                        <td><@tool.label name='limitFak' items=items />:</td><td><@tool.place object=object name='limitFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['formExportK'], it)>
                    <tr>
                        <td><@tool.label name='formExportK' items=items />:</td><td><@tool.place object=object name='formExportK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['formaUhradyCis'], it)>
                    <tr>
                        <td><@tool.label name='formaUhradyCis' items=items />:</td><td><@tool.place object=object name='formaUhradyCis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['limitPoSplatDny'], it)>
                    <tr>
                        <td><@tool.label name='limitPoSplatDny' items=items />:</td><td><@tool.place object=object name='limitPoSplatDny' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['limitPoSplatZakaz'], it)>
                    <tr>
                        <td><@tool.label name='limitPoSplatZakaz' items=items />:</td><td><@tool.place object=object name='limitPoSplatZakaz' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam', 'osloveni'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam', 'osloveni'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td colspan="4"><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td colspan="4"><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['osloveni'], it)>
                    <tr>
                        <td><@tool.label name='osloveni' items=items />:</td><td><@tool.place object=object name='osloveni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['katastrUzemi', 'parcela', 'rodCis', 'datNaroz'], it)>
            <div id="view-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['katastrUzemi', 'parcela', 'rodCis', 'datNaroz'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['katastrUzemi'], it)>
                    <tr>
                        <td><@tool.label name='katastrUzemi' items=items />:</td><td><@tool.place object=object name='katastrUzemi' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['parcela'], it)>
                    <tr>
                        <td><@tool.label name='parcela' items=items />:</td><td><@tool.place object=object name='parcela' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['rodCis'], it)>
                    <tr>
                        <td><@tool.label name='rodCis' items=items />:</td><td><@tool.place object=object name='rodCis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['datNaroz'], it)>
                    <tr>
                        <td><@tool.label name='datNaroz' items=items />:</td><td><@tool.place object=object name='datNaroz' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-6" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

    <div class="btn btn-default"><a class="flexibee-clickable" href="/c/${companyId}/faktura-vydana/${urlEncode("(firma=${it.object.id?c})")}"><span>${lang('dials', 'modulUcetni.FAV')}</span></a></div>
    <div class="btn btn-default"><a class="flexibee-clickable" href="/c/${companyId}/faktura-prijata/${urlEncode("(firma=${it.object.id?c})")}"><span>${lang('dials', 'modulUcetni.FAP')}</span></a></div>
<#--    <li><a class="flexibee-clickable" href="/c/${companyId}/objednavka-prijata/(firma=${it.object.id?c})"><span>Objednávky přijaté</span></a></li> -->
</ul>

<#include "/parts/i-dalsi-informace.ftl" />


<#include "/i-footer-view.ftl" />
</#escape>