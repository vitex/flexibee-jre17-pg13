<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

// TODO move to a single location
Ext.define('FlexiBee.data.ComboSuggestStore', {
    extend: 'Ext.data.Store',

    targetEvidence: '',

    autoLoad: false,
    fields: ['id', 'name'],
    remoteFilter: true,
    proxy: {
        type: 'rest',
        extraParams: {
            'mode': 'suggest'
        },
        // startParam, limitParam, sortParam a directionParam mají defaultní hodnoty kompatibilní s našimi názvy
        pageParam: undefined,
        groupParam: undefined,
        format: 'json',
        reader: {
            type: 'json',
            root: 'results',
            idProperty: 'id',
            totalProperty: 'total'
        }
    },

    constructor: function(cfg) {
        this.callParent(arguments);

        this.proxy.url = '/c/${it.companyId}/' + this.targetEvidence;
    },

    load: function(cfg) {
        if (cfg && cfg.params && cfg.params.id) { // the combobox is loading its initial value
            var originalUrl = this.proxy.url;
            // TODO what if the originalUrl already contains a filter?
            this.proxy.url = originalUrl + '/(id in (' + cfg.params.id + '))';
            delete cfg.params.id; // don't want that in URL (even if it does no harm)
            try {
                this.callParent(arguments);
            } finally {
                this.proxy.url = originalUrl;
            }
        } else {
            this.callParent(arguments);
        }
    }
});

<#-- TODO singleton! -->
Ext.define('FlexiBee.${it.senchaName}.Properties', {
    items: [
    <#list it.properties.devDocItems as item>
        {
            id: '${item.propertyName}',
            name: '${item.name}',
            title: '${item.title}',
            type: '${item.type}',
            fkName: '${item.fkName}',
            fkEvidenceType: '${item.fkEvidenceType}',
            fkFormId: ${item.fkFormId?c},
            fkTag: <#if item.fkEvidenceType??>'${item.fkEvidenceType.path}'<#else>null</#if>,
            showToUser: ${item.showToUser?string},
            isVisible: ${item.isVisible?string},
            isSortable: ${item.isSortable?string},
            isWritable: ${item.isWritable?string},
            isOverwritable: ${item.isOverWritable?string},
            isUpperCase: ${item.isUpperCase?string},
            width: <#if item.width??>${item.width?c}<#else>null</#if>,
            <#-- TODO isHighlight? -->
            inId: ${item.inId?string},
            inSummary: ${item.inSummary?string},
            inDetail: ${item.inDetail?string},
            mandatory: <#if item.mandatory??>${item.mandatory?string}<#else>null</#if>,
            minLength: <#if item.minLength??>${item.minLength?c}<#else>null</#if>,
            maxLength: <#if item.maxLength??>${item.maxLength?c}<#else>null</#if>,
            minValue:  <#if item.minValue??>${item.minValue?c}<#else>null</#if>,
            maxValue:  <#if item.maxValue??>${item.maxValue?c}<#else>null</#if>,
            digits:    <#if item.digits??>${item.digits?c}<#else>null</#if>,
            decimal:   <#if item.decimal??>${item.decimal?c}<#else>null</#if>,
            <#if item.type == 'select'>
            possibleValues: [
                <#if item.possibleValues??><#list item.possibleValues as value>
                { key: '${value.key}', value: '${value.name}' }<#if value_has_next>,</#if>
                </#list></#if>
            ],
            possibleValuesMap: {
                <#if item.possibleValues??><#list item.possibleValues as value>
                '${value.key}': '${value.name}'<#if value_has_next>,</#if>
                </#list></#if>
            }
            </#if>
            <#-- links nemá smysl, to jsou stejně FreeMarkerové šablony, ty v JavaScriptu budu těžko interpretovat -->
        }<#if item_has_next>,</#if>
    </#list>
    ],

    itemsMap: {},

    constructor: function() {
        var me = this;
        // TODO the "items" array might change later, e.g. by invoking a main.Menu plugin!!
        // TODO this also applies to other places where this idiom is used
        Ext.iterate(me.items, function(item) {
            me.itemsMap[item.id] = item;
        });
    },

    _asGridColumn: function(prop) {
        if (!prop.showToUser) {
            return null;
        }

        var column = {
            text: prop.name,
            dataIndex: prop.id,
            draggable: true,
            sortable: prop.isSortable,
            groupable: prop.isSortable,
            filterable: prop.isSortable,
            hidden: !prop.isVisible
        };

        var rendering = {};
        switch (prop.type) {
            case 'logic':    rendering = { xtype: 'booleancolumn', trueText: 'Ano', falseText: 'Ne' };  break;
            case 'integer':  rendering = { xtype: 'numbercolumn', format: '0,000', align: 'right' };    break;
            case 'numeric':  rendering = { xtype: 'numbercolumn', format: '0,000.00', align: 'right' }; break;
            case 'date':     rendering = { xtype: 'datecolumn', format: 'j.n.Y' };                      break;
            case 'datetime': rendering = { xtype: 'datecolumn', format: 'j.n.Y G:i' };                  break;
            case 'relation':
                rendering = {
                    renderer: function(value, metaData, record) {
                        return record.data[prop.id + '@showAs'];
                    }
                };
                break;
            case 'select':
                rendering = {
                    renderer: function(value) {
                        return prop.possibleValuesMap[value] || value;
                    }
                };
                break;
        }
        Ext.apply(column, rendering);

        var editor = {};
        switch (prop.type) {
            case 'logic':    editor.xtype = 'checkbox'; break;
            case 'integer':  editor.xtype = 'numberfield'; break;
            case 'numeric':  editor.xtype = 'numberfield'; break;
            case 'date':     editor.xtype = 'datefield'; break;
            case 'datetime': editor.xtype = 'datefield'; break;
            case 'select':   editor.xtype = 'combobox'; break;
            default:         editor.xtype = 'textfield'; break;
        }
        if (prop.mandatory) editor.allowBlank = false;
        if (prop.maxLength) editor.maxLength = prop.maxLength;
        if (prop.minLength) editor.minLength = prop.minLength;
        if (prop.minValue)  editor.minValue = prop.minValue;
        if (prop.maxValue)  editor.maxValue = prop.maxValue;
        if (prop.decimal)   editor.decimalPrecision = prop.decimal;
        if (prop.width)     editor.width = prop.width;
        Ext.apply(column, {
            editor: editor
        });

        return column;
    },

    _allAsGridColumns: function() {
        var me = this;

        if (this.convertedToColumns) {
            return this.convertedToColumns;
        }

        var columns = [];
        Ext.iterate(this.items, function(prop) {
            var column = me._asGridColumn(prop);
            if (column) {
                columns.push(column);
            }
        });

        this.convertedToColumns = columns;
        return columns;
    },

    gridColumns: function(callback) {
        var me = this;

        if (callback) {
            var propertyGridColumnFactory = function(id, cfg) {
                var prop = me.itemsMap[id];
                if (!prop) {
                    Ext.log("Unknown property '" + id + "' in '${it.senchaName}'");
                    return null;
                }

                var gridColumn = me._asGridColumn(prop);
                if (cfg) {
                    Ext.apply(gridColumn, cfg);
                }
                return gridColumn;
            };

            var gridColumns = callback(propertyGridColumnFactory);
            if (gridColumns) {
                return gridColumns;
            }
        }

        return me._allAsGridColumns();
    },

    asModelFields: function() {
        if (this.convertedToModelFields) {
            return this.convertedToModelFields;
        }

        var modelFields = [];
        Ext.iterate(this.items, function(prop) {
            var modelField = {
                name: prop.id
            };

            if (prop.id === 'id') {
                modelField.useNull = true;
            }

            switch (prop.type) {
                case 'logic':    modelField.type = 'bool';   break;
                case 'integer':  modelField.type = 'int';    break;
                case 'numeric':  modelField.type = 'float';  break;
                case 'date':     modelField.type = 'date';   break;
                case 'datetime': modelField.type = 'date';   break;
                case 'relation': modelField.type = 'auto';   break;
                case 'select':   modelField.type = 'auto';   break;
                default:         modelField.type = 'string'; break;
            }

            Ext.JSON.encodeDate = function(o)
            {
                if (o) {
                    return '"' + o.toISOString() + '"';
                }

                return o;
            };

            if (prop.type === 'date') {
                modelField.dateFormat = 'c';
                modelField.convert = function(value, record) {
                        var dt;
                        if (typeof(value)=='string' && isNaN(value)) {
                            // JavaScript can't parse YYYY-MM-DD+01:00
                            value = value.replace(/(\+.*)/g, '');
                            dt = Ext.Date.parseDate(value, "c");
                        }
                        return dt;
                    };
            } else if (prop.type === 'datetime') {
                modelField.dateFormat = 'c';
                modelField.convert = function(value, record) {
                        var dt;
                        if (typeof(value)=='string' && isNaN(value)) {
                            dt = Ext.Date.parse(value, "c");
                        }
                        return dt;
                    };
            } else if (prop.type === 'relation') {
                modelField.mapping = prop.id + '@internalId';
            }

            modelFields.push(modelField);

            // only for showing the text value in grid
            if (prop.type === 'relation') {
                modelFields.push({
                    name: prop.id + '@showAs',
                    type: 'string',
                    persist: false
                });
            }
        });

        this.convertedToModelFields = modelFields;
        return modelFields;
    },

    asModelValidations: function() {
        if (this.convertedToModelValidations) {
            return this.convertedToModelValidations;
        }

        var modelValidations = [];
        Ext.iterate(this.items, function(prop) {
            if (prop.mandatory) {
                modelValidations.push({
                    type: 'presence',
                    field: prop.id
                });
            }

            if (prop.minLength || prop.maxLength) {
                var lengthValidation = {
                    type: 'length',
                    field: prop.id
                };
                if (prop.minLength) {
                    lengthValidation.min = prop.minLength;
                }
                if (prop.maxLength) {
                    lengthValidation.max = prop.maxLength;
                }
                modelValidations.push(lengthValidation);
            }

            if (prop.type === 'select') {
                var possibleValueValidation = {
                    type: 'inclusion',
                    field: prop.id,
                    list: []
                };
                Ext.iterate(prop.possibleValues, function(possibleValue) {
                    possibleValueValidation.list.push(possibleValue.key);
                });
                modelValidations.push(possibleValueValidation);
            }
        });

        this.convertedToModelValidations = modelValidations;
        return modelValidations;
    },

    _asFormItem: function(prop) {
        if (prop.id === "id" || prop.id === "lastUpdate") { // should never appear in the form
            return null;
        }

        var formItem = {
            name: prop.id,
            fieldLabel: prop.name
        };

        switch (prop.type) {
            case 'logic':    formItem.xtype = 'checkbox';                                    break;
            case 'integer':  formItem.xtype = 'numberfield'; formItem.allowDecimals = false; break;
            case 'numeric':  formItem.xtype = 'numberfield';                                 break;
            case 'date':     formItem.xtype = 'datefield';   formItem.format = 'j.n.Y';      break;
            case 'datetime': formItem.xtype = 'datefield';   formItem.format = 'j.n.Y G:i';  break;
            case 'select':   formItem.xtype = 'combobox';                                    break;
            case 'relation': formItem.xtype = 'combobox';                                    break;
            default:         formItem.xtype = 'textfield';                                   break;
        }

        if (prop.mandatory) { formItem.allowBlank = false; }
        if (prop.type !== 'relation') {
            if (prop.maxLength) { formItem.maxLength = prop.maxLength; formItem.enforceMaxLength = true; }
            if (prop.minLength) { formItem.minLength = prop.minLength; }
        }
        if (prop.decimal)   { formItem.decimalPrecision = prop.decimal; }
        if (prop.maxValue)  { formItem.maxValue = prop.maxValue; }
        if (prop.minValue)  { formItem.minValue = prop.minValue; }

        if (prop.id === 'stitky') {
            Ext.apply(formItem, {
                xtype: 'boxselect',
                delimiter: ',',
                typeAhead: true,
                minChars: 2,
                pageSize: 15,
                maxWidth: 500,
                matchFieldWidth: true,
                forceSelection: true,
                multiSelect: true,
                queryParam: 'q',
                store: Ext.create('FlexiBee.data.ComboSuggestStore', {
                    targetEvidence: 'stitek'
                }),
                displayField: 'name',
                valueField: 'id'
            });
        }

        if (prop.type === 'select') {
            Ext.apply(formItem, {
                forceSelection: true,
                editable: false,
                multiSelect: false,
                queryMode: 'local',
                store: Ext.create('Ext.data.Store', {
                    fields: ['key', 'value'],
                    data: prop.possibleValues
                }),
                displayField: 'value',
                valueField: 'key'
            });
        }

        if (prop.type === 'relation') {
            Ext.apply(formItem, {
                typeAhead: true,
                minChars: 2,
                pageSize: 15,
                matchFieldWidth: false,
                forceSelection: true,
                multiSelect: false,
                queryParam: 'q',
                store: Ext.create('FlexiBee.data.ComboSuggestStore', {
                    targetEvidence: prop.fkTag
                }),
                displayField: 'name',
                valueField: 'id',
                listeners: {
                    render: function(combobox) { // load initial value
                        var me = this;

                        var form = combobox.findParentByType('form').getForm();
                        var model = form.getRecord();
                        var value = model.data[combobox.getName()];

                        var params = {};
                        params[me.valueField] = value;
                        combobox.store.load({
                            params: params,
                            callback: function() {
                                me.setValue(value);
                            }
                        });
                    },

                    select: function(combobox) {
                        var form = combobox.findParentByType('form').getForm();
                        var model = form.getRecord();

                        // sync @showAs model field
                        model.beginEdit();
                        var showAsKey = combobox.getName() + '@showAs';
                        if (model.fields.getByKey(showAsKey)) {
                            var value = combobox.getRawValue();
                            var colonIndex = value.indexOf(':');
                            if (colonIndex > 0) {
                                colonIndex += 2;
                            }
                            model.set(showAsKey, value.substring(colonIndex));
                        }
                        model.endEdit();
                    }
                }
            });
        }

        return formItem;
    },

    _allAsFormItems: function() {
        var me = this;

        if (this.convertedToFormItems) {
            return this.convertedToFormItems;
        }

        var formItems = [];
        Ext.iterate(this.items, function(prop) {
            var formItem = me._asFormItem(prop);
            if (formItem) {
                formItems.push(formItem);
            }
        });

        this.convertedToFormItems = formItems;
        return formItems;
    },

    formItems: function(callback) {
        var me = this;

        if (callback) {
            var propertyFormItemFactory = function(id, cfg) {
                var prop = me.itemsMap[id];
                if (!prop) {
                    Ext.log("Unknown property '" + id + "' in '${it.senchaName}'");
                    return null;
                }

                var formItem = me._asFormItem(prop);
                if (cfg) {
                    Ext.apply(formItem, cfg);
                }
                return formItem;
            };

            var formItems = callback(propertyFormItemFactory);
            if (formItems) {
                return formItems;
            }
        }

        return me._allAsFormItems();
    }
});

</#escape>
