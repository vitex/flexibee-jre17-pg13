<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>


${marker.mark('uzivatel')}
${marker.mark('polObch')}
${marker.mark('skladMj')}


<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['firma', 'cenik', 'uzivatel', 'sklad', 'mnozstvi', 'datumOd', 'datumDo', 'poznamka'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['firma'])>
        <tr>
            <td><@edit.label name='firma' items=items /></td><td><@edit.place object=object name='firma' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['cenik'])>
        <tr>
            <td><@edit.label name='cenik' items=items /></td><td><@edit.place object=object name='cenik' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['uzivatel'])>
        <tr>
            <td><@edit.label name='uzivatel' items=items /></td><td><@edit.place object=object name='uzivatel' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['sklad'])>
        <tr>
            <td><@edit.label name='sklad' items=items /></td><td><@edit.place object=object name='sklad' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['mnozstvi'])>
        <tr>
            <td><@edit.label name='mnozstvi' items=items /></td><td><@edit.place object=object name='mnozstvi' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['datumOd'])>
        <tr>
            <td><@edit.label name='datumOd' items=items /></td><td><@edit.place object=object name='datumOd' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['datumDo'])>
        <tr>
            <td><@edit.label name='datumDo' items=items /></td><td><@edit.place object=object name='datumDo' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['poznamka'])>
        <tr>
            <td><@edit.label name='poznamka' items=items /><br><@edit.textarea object=object name='poznamka' items=items marker=marker rows=4 /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
