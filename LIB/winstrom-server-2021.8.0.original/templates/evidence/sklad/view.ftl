<#ftl encoding="utf-8" strip_whitespace=true/>
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('platiOd')}
${marker.mark('platiDo')}
${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}
${marker.mark('modul')}
${marker.mark('ucetni')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['nazev', 'kod', 'radaPrijem', 'radaVydej'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td colspan="5"><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kod', 'radaPrijem', 'radaVydej'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['kod'], it)>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['radaPrijem'], it)>
            <td><@tool.label name='radaPrijem' items=items />:</td><td><@tool.place object=object name='radaPrijem' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['radaVydej'], it)>
            <td><@tool.label name='radaVydej' items=items />:</td><td><@tool.place object=object name='radaVydej' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['buc', 'iban', 'smerKod', 'bic', 'specSym', 'typDoklK', 'bucPrimarni', 'nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www', 'elBanFormat', 'ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'typUcOpVydej', 'typUcOpPrijem', 'zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK', 'tiskAutomat', 'popisDoklad', 'uvodTxt', 'zavTxt', 'popis', 'poznam', 'ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'ekokomK', 'primarni', 'automatickySklad', 'workFlow'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['buc', 'iban', 'smerKod', 'bic', 'specSym', 'typDoklK', 'bucPrimarni', 'nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www', 'elBanFormat'], it)>
            <li class="active"><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'bankaPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'typUcOpVydej', 'typUcOpPrijem'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'uctoPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'], it)>
            <li><a href="#view-fragment-5" data-toggle="tab">${lang('labels', 'typDoklEBZaokrouhleni')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['tiskAutomat'], it)>
            <li><a href="#view-fragment-6" data-toggle="tab">${lang('labels', 'autoTiskPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt'], it)>
            <li><a href="#view-fragment-7" data-toggle="tab">${lang('labels', 'textyDokladPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-8" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'ekokomK', 'primarni', 'automatickySklad', 'workFlow'], it)>
            <li><a href="#view-fragment-9" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['buc', 'iban', 'smerKod', 'bic', 'specSym', 'typDoklK', 'bucPrimarni', 'nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www', 'elBanFormat'], it)>
            <div id="view-fragment-3" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['buc', 'iban', 'smerKod', 'bic', 'specSym', 'typDoklK', 'bucPrimarni'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['buc', 'iban'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['buc'], it)>
                        <td><@tool.label name='buc' items=items />:</td><td><@tool.place object=object name='buc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['iban'], it)>
                        <td><@tool.label name='iban' items=items />:</td><td><@tool.place object=object name='iban' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['smerKod', 'bic'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['smerKod'], it)>
                        <td><@tool.label name='smerKod' items=items />:</td><td><@tool.place object=object name='smerKod' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['bic'], it)>
                        <td><@tool.label name='bic' items=items />:</td><td><@tool.place object=object name='bic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['specSym', 'typDoklK'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['specSym'], it)>
                        <td><@tool.label name='specSym' items=items />:</td><td><@tool.place object=object name='specSym' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['typDoklK'], it)>
                        <td><@tool.label name='typDoklK' items=items />:</td><td><@tool.place object=object name='typDoklK' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['bucPrimarni'], it)>
                    <tr>
                        <td><@tool.label name='bucPrimarni' items=items />:</td><td colspan="3"><@tool.place object=object name='bucPrimarni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canShowAtLeastOne(items, object, ['nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www', 'elBanFormat'], it)>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canShowAtLeastOne(items, object, ['nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www'], it)>
                        <li class="active"><a href="#view-fragment-1" data-toggle="tab">Informace o bance</a></li>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['elBanFormat'], it)>
                        <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'elektronickaBankaPN')}</a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canShowAtLeastOne(items, object, ['nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www'], it)>
                        <div id="view-fragment-1" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
                                <#if canShowAtLeastOne(items, object, ['nazBanky', 'tel'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['nazBanky'], it)>
                                    <td><@tool.label name='nazBanky' items=items />:</td><td colspan="3"><@tool.place object=object name='nazBanky' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['tel'], it)>
                                    <td><@tool.label name='tel' items=items />:</td><td><@tool.placeTel object=object name='tel' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['ulice', 'mobil'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['ulice'], it)>
                                    <td><@tool.label name='ulice' items=items />:</td><td colspan="3"><@tool.place object=object name='ulice' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['mobil'], it)>
                                    <td><@tool.label name='mobil' items=items />:</td><td><@tool.placeTel object=object name='mobil' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['psc', 'mesto', 'fax'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['psc'], it)>
                                    <td><@tool.label name='psc' items=items />:</td><td><@tool.place object=object name='psc' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['mesto'], it)>
                                    <td><@tool.label name='mesto' items=items />:</td><td><@tool.place object=object name='mesto' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['fax'], it)>
                                    <td><@tool.label name='fax' items=items />:</td><td><@tool.place object=object name='fax' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['stat', 'email'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['stat'], it)>
                                    <td><@tool.label name='stat' items=items />:</td><td colspan="3"><@tool.place object=object name='stat' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['email'], it)>
                                    <td><@tool.label name='email' items=items />:</td><td><@tool.placeEmail object=object name='email' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canShowAtLeastOne(items, object, ['menaBanky', 'www'], it)>
                                <tr>
                                    <#if canShowAtLeastOne(items, object, ['menaBanky'], it)>
                                    <td><@tool.label name='menaBanky' items=items />:</td><td colspan="3"><@tool.place object=object name='menaBanky' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canShowAtLeastOne(items, object, ['www'], it)>
                                    <td><@tool.label name='www' items=items />:</td><td><@tool.placeWww object=object name='www' items=items marker=marker /></td>
                                    </#if>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canShowAtLeastOne(items, object, ['elBanFormat'], it)>
                        <div id="view-fragment-2" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canShowAtLeastOne(items, object, ['elBanFormat'], it)>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canShowAtLeastOne(items, object, ['elBanFormat'], it)>
                                <tr>
                                    <td><@tool.label name='elBanFormat' items=items />:</td><td><@tool.place object=object name='elBanFormat' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'typUcOpVydej', 'typUcOpPrijem'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'typUcOpVydej', 'typUcOpPrijem'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['ucetni', 'stredisko'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ucetni'], it)>
                        <td><@tool.label name='ucetni' items=items />:</td><td><@tool.place object=object name='ucetni' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['stredisko'], it)>
                        <td><@tool.label name='stredisko' items=items />:</td><td><@tool.place object=object name='stredisko' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['cinnost'], it)>
                    <tr>
                        <td><@tool.label name='cinnost' items=items />:</td><td><@tool.place object=object name='cinnost' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['primUcet', 'mena'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['primUcet'], it)>
                        <td><@tool.label name='primUcet' items=items />:</td><td><@tool.place object=object name='primUcet' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['mena'], it)>
                        <td><@tool.label name='mena' items=items />:</td><td><@tool.place object=object name='mena' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['statDph'], it)>
                    <tr>
                        <td><@tool.label name='statDph' items=items />:</td><td><@tool.place object=object name='statDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typUcOpVydej'], it)>
                    <tr>
                        <td><@tool.label name='typUcOpVydej' items=items />:</td><td><@tool.place object=object name='typUcOpVydej' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['typUcOpPrijem'], it)>
                    <tr>
                        <td><@tool.label name='typUcOpPrijem' items=items />:</td><td><@tool.place object=object name='typUcOpPrijem' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'], it)>
            <div id="view-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['zaokrJakDphK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrJakDphK' items=items />:</td><td><@tool.place object=object name='zaokrJakDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaokrNaDphK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrNaDphK' items=items />:</td><td><@tool.place object=object name='zaokrNaDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaokrJakSumK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrJakSumK' items=items />:</td><td><@tool.place object=object name='zaokrJakSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zaokrNaSumK'], it)>
                    <tr>
                        <td><@tool.label name='zaokrNaSumK' items=items />:</td><td><@tool.place object=object name='zaokrNaSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['tiskAutomat'], it)>
            <div id="view-fragment-6" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['tiskAutomat'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['tiskAutomat'], it)>
                    <tr>
                        <td><@tool.label name='tiskAutomat' items=items />:</td><td><@tool.place object=object name='tiskAutomat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt'], it)>
            <div id="view-fragment-7" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['popisDoklad'], it)>
                    <tr>
                        <td><@tool.label name='popisDoklad' items=items />:</td><td><@tool.place object=object name='popisDoklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['uvodTxt'], it)>
                    <tr>
                        <td><@tool.label name='uvodTxt' items=items />:<br><@tool.placeArea object=object name='uvodTxt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['zavTxt'], it)>
                    <tr>
                        <td><@tool.label name='zavTxt' items=items />:<br><@tool.placeArea object=object name='zavTxt' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-8" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'ekokomK', 'primarni', 'automatickySklad', 'workFlow'], it)>
            <div id="view-fragment-9" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'ekokomK', 'primarni', 'automatickySklad', 'workFlow'], it)>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canShowAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['ucetObdobiOd'], it)>
                        <td><@tool.label name='ucetObdobiOd' items=items />:</td><td><@tool.place object=object name='ucetObdobiOd' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['ucetObdobiDo'], it)>
                        <td><@tool.label name='ucetObdobiDo' items=items />:</td><td><@tool.place object=object name='ucetObdobiDo' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['razeniProTiskK'], it)>
                    <tr>
                        <td><@tool.label name='razeniProTiskK' items=items />:</td><td colspan="3"><@tool.place object=object name='razeniProTiskK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ekokomK'], it)>
                    <tr>
                        <td><@tool.label name='ekokomK' items=items />:</td><td colspan="3"><@tool.place object=object name='ekokomK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['primarni'], it)>
                    <tr>
                        <td><@tool.label name='primarni' items=items />:</td><td colspan="3"><@tool.place object=object name='primarni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['automatickySklad'], it)>
                    <tr>
                        <td><@tool.label name='automatickySklad' items=items />:</td><td colspan="3"><@tool.place object=object name='automatickySklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['workFlow'], it)>
                    <tr>
                        <td><@tool.label name='workFlow' items=items />:</td><td colspan="3"><@tool.place object=object name='workFlow' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/parts/i-dalsi-informace.ftl" />


<#include "/i-footer-view.ftl" />
</#escape>