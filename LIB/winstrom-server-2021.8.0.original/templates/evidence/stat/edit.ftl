<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['nazev', 'clenEu', 'mena', 'telPredvolba'])>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['clenEu'])>
        <tr>
            <td><@edit.label name='clenEu' items=items /></td><td><@edit.place object=object name='clenEu' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['mena'])>
        <tr>
            <td><@edit.label name='mena' items=items /></td><td><@edit.place object=object name='mena' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['telPredvolba'])>
        <tr>
            <td><@edit.label name='telPredvolba' items=items /></td><td><@edit.place object=object name='telPredvolba' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['kod', 'kodAlpha3', 'kodNum'])>
    <h4>${lang('labels', 'statKodyDleIsoLB')}</h4>
    <div id="edit-fragment-1" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['kod', 'kodAlpha3', 'kodNum'])>
        <table class="flexibee-tbl-1">
            <col width="10%" /><col width="20%" /><col width="15%" /><col width="20%" /><col width="15%" /><col width="20%" />
            <#if canEditAtLeastOne(items, object, ['kod', 'kodAlpha3', 'kodNum'])>
            <tr>
                <#if canEditAtLeastOne(items, object, ['kod'])>
                <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['kodAlpha3'])>
                <td><@edit.label name='kodAlpha3' items=items /></td><td><@edit.place object=object name='kodAlpha3' items=items marker=marker /></td>
                </#if>
                <#if isMobile></tr><tr></#if>
                <#if canEditAtLeastOne(items, object, ['kodNum'])>
                <td><@edit.label name='kodNum' items=items /></td><td><@edit.place object=object name='kodNum' items=items marker=marker /></td>
                </#if>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canEditAtLeastOne(items, object, ['nazZemeC25', 'kodDph'])>
    <h4>${lang('labels', 'statNastFinSpravaPN')}</h4>
    <div id="edit-fragment-2" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['nazZemeC25', 'kodDph'])>
        <table class="flexibee-tbl-1">
            <col width="20%" /><col width="80%" />
            <#if canEditAtLeastOne(items, object, ['nazZemeC25'])>
            <tr>
                <td><@edit.label name='nazZemeC25' items=items /></td><td><@edit.place object=object name='nazZemeC25' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['kodDph'])>
            <tr>
                <td><@edit.label name='kodDph' items=items /></td><td><@edit.place object=object name='kodDph' items=items marker=marker /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canEditAtLeastOne(items, object, ['vatId', 'vatIdMoss', 'fuKraj', 'fuUzPrac'])>
    <h4>${lang('labels', 'statNastProStatPN')}</h4>
    <div id="edit-fragment-3" class="tab-pane">
        <div class="flexibee-table-border">
        <#if canEditAtLeastOne(items, object, ['vatId', 'vatIdMoss', 'fuKraj', 'fuUzPrac'])>
        <table class="flexibee-tbl-1">
            <col width="20%" /><col width="80%" />
            <#if canEditAtLeastOne(items, object, ['vatId'])>
            <tr>
                <td><@edit.label name='vatId' items=items /></td><td><@edit.place object=object name='vatId' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['vatIdMoss'])>
            <tr>
                <td><@edit.label name='vatIdMoss' items=items /></td><td><@edit.place object=object name='vatIdMoss' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['fuKraj'])>
            <tr>
                <td><@edit.label name='fuKraj' items=items /></td><td><@edit.place object=object name='fuKraj' items=items marker=marker /></td>
            </tr>
            </#if>
            <#if canEditAtLeastOne(items, object, ['fuUzPrac'])>
            <tr>
                <td><@edit.label name='fuUzPrac' items=items /></td><td><@edit.place object=object name='fuUzPrac' items=items marker=marker /></td>
            </tr>
            </#if>
        </table>
        </#if>
        </div>

    </div>
    </#if>

    <#if canEditAtLeastOne(items, object, ['popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li class="active"><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-5" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-4" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
