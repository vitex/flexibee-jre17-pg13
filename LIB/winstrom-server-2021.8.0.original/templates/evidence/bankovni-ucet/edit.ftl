<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('stitky')}
${marker.mark('modul')}
${marker.mark('platiOd')}
${marker.mark('platiDo')}
${marker.mark('zapocet')}
${marker.mark('elBanFormat')}
${marker.mark('sloVypis')}
${marker.mark('sloPrikaz')}
${marker.mark('priVypis')}
${marker.mark('priPrikaz')}
${marker.mark('zkrKlienta')}
${marker.mark('bucKonverze')}
${marker.mark('formaUhrK')}
${marker.mark('splatDny')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['nazev', 'kod', 'radaPrijem', 'radaVydej'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td colspan="5"><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kod', 'radaPrijem', 'radaVydej'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['kod'])>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['radaPrijem'])>
            <td><@edit.label name='radaPrijem' items=items /></td><td><@edit.place object=object name='radaPrijem' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['radaVydej'])>
            <td><@edit.label name='radaVydej' items=items /></td><td><@edit.place object=object name='radaVydej' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['buc', 'iban', 'smerKod', 'bic', 'specSym', 'typDoklK', 'bucPrimarni', 'nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www', 'elBanFormat', 'ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'typUcOpVydej', 'typUcOpPrijem', 'zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK', 'tiskAutomat', 'popisDoklad', 'uvodTxt', 'zavTxt', 'popis', 'poznam', 'ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'ekokomK', 'primarni', 'automatickySklad', 'workFlow'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['buc', 'iban', 'smerKod', 'bic', 'specSym', 'typDoklK', 'bucPrimarni', 'nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www', 'elBanFormat'])>
            <li class="active"><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'bankaPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'typUcOpVydej', 'typUcOpPrijem'])>
            <li><a href="#edit-fragment-4" data-toggle="tab"><span>${lang('labels', 'uctoPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
            <li><a href="#edit-fragment-5" data-toggle="tab"><span>${lang('labels', 'typDoklEBZaokrouhleni')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['tiskAutomat'])>
            <li><a href="#edit-fragment-6" data-toggle="tab"><span>${lang('labels', 'autoTiskPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt'])>
            <li><a href="#edit-fragment-7" data-toggle="tab"><span>${lang('labels', 'textyDokladPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-8" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'ekokomK', 'primarni', 'automatickySklad', 'workFlow'])>
            <li><a href="#edit-fragment-9" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['buc', 'iban', 'smerKod', 'bic', 'specSym', 'typDoklK', 'bucPrimarni', 'nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www', 'elBanFormat'])>
            <div id="edit-fragment-3" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['buc', 'iban', 'smerKod', 'bic', 'specSym', 'typDoklK', 'bucPrimarni'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['buc', 'iban'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['buc'])>
                        <td><@edit.label name='buc' items=items /></td><td><@edit.place object=object name='buc' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['iban'])>
                        <td><@edit.label name='iban' items=items /></td><td><@edit.place object=object name='iban' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['smerKod', 'bic'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['smerKod'])>
                        <td><@edit.label name='smerKod' items=items /></td><td><@edit.place object=object name='smerKod' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['bic'])>
                        <td><@edit.label name='bic' items=items /></td><td><@edit.place object=object name='bic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['specSym', 'typDoklK'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['specSym'])>
                        <td><@edit.label name='specSym' items=items /></td><td><@edit.place object=object name='specSym' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['typDoklK'])>
                        <td><@edit.label name='typDoklK' items=items /></td><td><@edit.place object=object name='typDoklK' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['bucPrimarni'])>
                    <tr>
                        <td><@edit.label name='bucPrimarni' items=items /></td><td colspan="3"><@edit.place object=object name='bucPrimarni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <#if canEditAtLeastOne(items, object, ['nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www', 'elBanFormat'])>
                    <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
                        <#if canEditAtLeastOne(items, object, ['nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www'])>
                        <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>Informace o bance</span></a></li>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['elBanFormat'])>
                        <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'elektronickaBankaPN')}</span></a></li>
                        </#if>
                    </ul>
                    <div class="tab-content">
                        <#if canEditAtLeastOne(items, object, ['nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www'])>
                        <div id="edit-fragment-1" class="tab-pane active">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['nazBanky', 'tel', 'ulice', 'mobil', 'psc', 'mesto', 'fax', 'stat', 'email', 'menaBanky', 'www'])>
                            <table class="flexibee-tbl-1">
                                <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
                                <#if canEditAtLeastOne(items, object, ['nazBanky', 'tel'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['nazBanky'])>
                                    <td><@edit.label name='nazBanky' items=items /></td><td colspan="3"><@edit.place object=object name='nazBanky' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['tel'])>
                                    <td><@edit.label name='tel' items=items /></td><td><@edit.place object=object name='tel' items=items marker=marker  type='tel'/></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['ulice', 'mobil'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['ulice'])>
                                    <td><@edit.label name='ulice' items=items /></td><td colspan="3"><@edit.place object=object name='ulice' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['mobil'])>
                                    <td><@edit.label name='mobil' items=items /></td><td><@edit.place object=object name='mobil' items=items marker=marker  type='tel'/></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['psc', 'mesto', 'fax'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['psc'])>
                                    <td><@edit.label name='psc' items=items /></td><td><@edit.place object=object name='psc' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['mesto'])>
                                    <td><@edit.label name='mesto' items=items /></td><td><@edit.place object=object name='mesto' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['fax'])>
                                    <td><@edit.label name='fax' items=items /></td><td><@edit.place object=object name='fax' items=items marker=marker  type='fax'/></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['stat', 'email'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['stat'])>
                                    <td><@edit.label name='stat' items=items /></td><td colspan="3"><@edit.place object=object name='stat' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['email'])>
                                    <td><@edit.label name='email' items=items /></td><td><@edit.place object=object name='email' items=items marker=marker  type='email'/></td>
                                    </#if>
                                </tr>
                                </#if>
                                <#if canEditAtLeastOne(items, object, ['menaBanky', 'www'])>
                                <tr>
                                    <#if canEditAtLeastOne(items, object, ['menaBanky'])>
                                    <td><@edit.label name='menaBanky' items=items /></td><td colspan="3"><@edit.place object=object name='menaBanky' items=items marker=marker /></td>
                                    </#if>
                                    <#if isMobile></tr><tr></#if>
                                    <#if canEditAtLeastOne(items, object, ['www'])>
                                    <td><@edit.label name='www' items=items /></td><td><@edit.place object=object name='www' items=items marker=marker  type='url'/></td>
                                    </#if>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                        <#if canEditAtLeastOne(items, object, ['elBanFormat'])>
                        <div id="edit-fragment-2" class="tab-pane">
                            <div class="flexibee-table-border">
                            <#if canEditAtLeastOne(items, object, ['elBanFormat'])>
                            <table class="flexibee-tbl-1">
                                <col width="20%" /><col width="80%" />
                                <#if canEditAtLeastOne(items, object, ['elBanFormat'])>
                                <tr>
                                    <td><@edit.label name='elBanFormat' items=items /></td><td><@edit.place object=object name='elBanFormat' items=items marker=marker /></td>
                                </tr>
                                </#if>
                            </table>
                            </#if>
                            </div>

                        </div>
                        </#if>
                    </div>
                </#if>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'typUcOpVydej', 'typUcOpPrijem'])>
            <div id="edit-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko', 'cinnost', 'primUcet', 'mena', 'statDph', 'typUcOpVydej', 'typUcOpPrijem'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['ucetni', 'stredisko'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ucetni'])>
                        <td><@edit.label name='ucetni' items=items /></td><td><@edit.place object=object name='ucetni' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['stredisko'])>
                        <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cinnost'])>
                    <tr>
                        <td><@edit.label name='cinnost' items=items /></td><td><@edit.place object=object name='cinnost' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['primUcet', 'mena'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['primUcet'])>
                        <td><@edit.label name='primUcet' items=items /></td><td><@edit.place object=object name='primUcet' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mena'])>
                        <td><@edit.label name='mena' items=items /></td><td><@edit.place object=object name='mena' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['statDph'])>
                    <tr>
                        <td><@edit.label name='statDph' items=items /></td><td><@edit.place object=object name='statDph' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typUcOpVydej'])>
                    <tr>
                        <td><@edit.label name='typUcOpVydej' items=items /></td><td><@edit.place object=object name='typUcOpVydej' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typUcOpPrijem'])>
                    <tr>
                        <td><@edit.label name='typUcOpPrijem' items=items /></td><td><@edit.place object=object name='typUcOpPrijem' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
            <div id="edit-fragment-5" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['zaokrJakDphK', 'zaokrNaDphK', 'zaokrJakSumK', 'zaokrNaSumK'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['zaokrJakDphK'])>
                    <tr>
                        <td><@edit.label name='zaokrJakDphK' items=items /></td><td><@edit.place object=object name='zaokrJakDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrNaDphK'])>
                    <tr>
                        <td><@edit.label name='zaokrNaDphK' items=items /></td><td><@edit.place object=object name='zaokrNaDphK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrJakSumK'])>
                    <tr>
                        <td><@edit.label name='zaokrJakSumK' items=items /></td><td><@edit.place object=object name='zaokrJakSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zaokrNaSumK'])>
                    <tr>
                        <td><@edit.label name='zaokrNaSumK' items=items /></td><td><@edit.place object=object name='zaokrNaSumK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['tiskAutomat'])>
            <div id="edit-fragment-6" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['tiskAutomat'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['tiskAutomat'])>
                    <tr>
                        <td><@edit.label name='tiskAutomat' items=items /></td><td><@edit.place object=object name='tiskAutomat' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt'])>
            <div id="edit-fragment-7" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popisDoklad', 'uvodTxt', 'zavTxt'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['popisDoklad'])>
                    <tr>
                        <td><@edit.label name='popisDoklad' items=items /></td><td><@edit.place object=object name='popisDoklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['uvodTxt'])>
                    <tr>
                        <td><@edit.label name='uvodTxt' items=items /><br><@edit.textarea object=object name='uvodTxt' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zavTxt'])>
                    <tr>
                        <td><@edit.label name='zavTxt' items=items /><br><@edit.textarea object=object name='zavTxt' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-8" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=10 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'ekokomK', 'primarni', 'automatickySklad', 'workFlow'])>
            <div id="edit-fragment-9" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo', 'razeniProTiskK', 'ekokomK', 'primarni', 'automatickySklad', 'workFlow'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['ucetObdobiOd', 'ucetObdobiDo'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['ucetObdobiOd'])>
                        <td><@edit.label name='ucetObdobiOd' items=items /></td><td><@edit.place object=object name='ucetObdobiOd' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['ucetObdobiDo'])>
                        <td><@edit.label name='ucetObdobiDo' items=items /></td><td><@edit.place object=object name='ucetObdobiDo' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['razeniProTiskK'])>
                    <tr>
                        <td><@edit.label name='razeniProTiskK' items=items /></td><td colspan="3"><@edit.place object=object name='razeniProTiskK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ekokomK'])>
                    <tr>
                        <td><@edit.label name='ekokomK' items=items /></td><td colspan="3"><@edit.place object=object name='ekokomK' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['primarni'])>
                    <tr>
                        <td><@edit.label name='primarni' items=items /></td><td colspan="3"><@edit.place object=object name='primarni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['automatickySklad'])>
                    <tr>
                        <td><@edit.label name='automatickySklad' items=items /></td><td colspan="3"><@edit.place object=object name='automatickySklad' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['workFlow'])>
                    <tr>
                        <td><@edit.label name='workFlow' items=items /></td><td colspan="3"><@edit.place object=object name='workFlow' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
