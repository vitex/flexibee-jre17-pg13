<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('typOrganizace')}

<#-- odpovídá základnímu ceníku -->
${marker.mark('typCenyDphK')}
${marker.mark('zaokrJakK')}
${marker.mark('zaokrNaK')}
${marker.mark('typCenyVychoziK')}
${marker.mark('typVypCenyK')}
${marker.mark('procZakl')}
${marker.mark('typCenyVychozi25K')}
${marker.mark('typVypCeny25K')}
${marker.mark('limMnoz2')}
${marker.mark('procento2')}
${marker.mark('limMnoz3')}
${marker.mark('procento3')}
${marker.mark('limMnoz4')}
${marker.mark('procento4')}
${marker.mark('limMnoz5')}
${marker.mark('procento5')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['nazev', 'kod'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items level='3'/>:</td><td><@tool.place object=object level='3' name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['popis', 'poznam', 'hlidatMinMarzi', 'minMarze', 'ucetProtiVfa', 'ucetProtiPfa', 'ucetProtiSklp', 'ucetProtiSklv', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['hlidatMinMarzi', 'minMarze'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">Základní cenová úroveň</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetProtiVfa', 'ucetProtiPfa', 'ucetProtiSklp', 'ucetProtiSklv'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">Účtování skupiny zboží</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-4" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['hlidatMinMarzi', 'minMarze'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['hlidatMinMarzi', 'minMarze'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['hlidatMinMarzi', 'minMarze'], it)>
                    <tr>
                        <#if canShowAtLeastOne(items, object, ['hlidatMinMarzi'], it)>
                        <td><@tool.label name='hlidatMinMarzi' items=items />:</td><td><@tool.place object=object name='hlidatMinMarzi' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canShowAtLeastOne(items, object, ['minMarze'], it)>
                        <td><@tool.label name='minMarze' items=items />:</td><td><@tool.place object=object name='minMarze' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['ucetProtiVfa', 'ucetProtiPfa', 'ucetProtiSklp', 'ucetProtiSklv'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['ucetProtiVfa', 'ucetProtiPfa', 'ucetProtiSklp', 'ucetProtiSklv'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['ucetProtiVfa'], it)>
                    <tr>
                        <td>Pro vydané faktury / pokladna - příjem:</td><td><@tool.place object=object name='ucetProtiVfa' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ucetProtiPfa'], it)>
                    <tr>
                        <td>Pro přijaté faktury / pokladna - výdej:</td><td><@tool.place object=object name='ucetProtiPfa' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ucetProtiSklp'], it)>
                    <tr>
                        <td>Pro příjem na sklad:</td><td><@tool.place object=object name='ucetProtiSklp' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ucetProtiSklv'], it)>
                    <tr>
                        <td>Pro výdej ze skladu:</td><td><@tool.place object=object name='ucetProtiSklv' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-4" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>
