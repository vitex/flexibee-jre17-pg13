<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('visible')}

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['kod', 'nazev', 'popis', 'stat'], it)>
    <table class="flexibee-tbl-1">
        <col width="20%" /><col width="80%" />
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['nazev'], it)>
        <tr>
            <td><@tool.label name='nazev' items=items />:</td><td><@tool.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['popis'], it)>
        <tr>
            <td><@tool.label name='popis' items=items />:</td><td><@tool.place object=object name='popis' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['stat'], it)>
        <tr>
            <td><@tool.label name='stat' items=items />:</td><td><@tool.place object=object name='stat' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>