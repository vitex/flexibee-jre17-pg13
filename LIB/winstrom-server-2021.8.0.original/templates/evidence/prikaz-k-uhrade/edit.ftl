<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['typDokl', 'celCastka', 'zahranicni', 'stavPrikazK', 'jmenoSoub', 'mena', 'datVytvor', 'konSym', 'poradiPrikaz', 'datSplat', 'nazFirmy', 'faUlice', 'faMesto', 'faPsc', 'faStat'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['typDokl', 'celCastka'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['typDokl'])>
            <td><@edit.label name='typDokl' items=items /></td><td><@edit.place object=object name='typDokl' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['celCastka'])>
            <td><@edit.label name='celCastka' items=items /></td><td><@edit.place object=object name='celCastka' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['zahranicni'])>
        <tr>
            <td><@edit.label name='zahranicni' items=items /></td><td><@edit.place object=object name='zahranicni' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['stavPrikazK', 'jmenoSoub'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['stavPrikazK'])>
            <td><@edit.label name='stavPrikazK' items=items /></td><td><@edit.place object=object name='stavPrikazK' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['jmenoSoub'])>
            <td><@edit.label name='jmenoSoub' items=items /></td><td><@edit.place object=object name='jmenoSoub' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['mena', 'datVytvor'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['mena'])>
            <td><@edit.label name='mena' items=items /></td><td><@edit.place object=object name='mena' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['datVytvor'])>
            <td><@edit.label name='datVytvor' items=items /></td><td><@edit.place object=object name='datVytvor' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['konSym', 'poradiPrikaz'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['konSym'])>
            <td><@edit.label name='konSym' items=items /></td><td><@edit.place object=object name='konSym' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['poradiPrikaz'])>
            <td><@edit.label name='poradiPrikaz' items=items /></td><td><@edit.place object=object name='poradiPrikaz' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['datSplat'])>
        <tr>
            <td><@edit.label name='datSplat' items=items /></td><td><@edit.place object=object name='datSplat' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazFirmy', 'faUlice'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['nazFirmy'])>
            <td><@edit.label name='nazFirmy' items=items /></td><td><@edit.place object=object name='nazFirmy' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['faUlice'])>
            <td><@edit.label name='faUlice' items=items /></td><td><@edit.place object=object name='faUlice' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['faMesto', 'faPsc'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['faMesto'])>
            <td><@edit.label name='faMesto' items=items /></td><td><@edit.place object=object name='faMesto' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['faPsc'])>
            <td><@edit.label name='faPsc' items=items /></td><td><@edit.place object=object name='faPsc' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['faStat'])>
        <tr>
            <td><@edit.label name='faStat' items=items /></td><td><@edit.place object=object name='faStat' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

<#--AUTOGEN END-->

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
