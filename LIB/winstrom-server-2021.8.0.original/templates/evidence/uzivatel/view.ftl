<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-view.ftl" />

<#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canShowAtLeastOne(items, object, ['kod', 'titul', 'jmeno', 'prijmeni', 'titulZa', 'ulice', 'mesto', 'psc', 'stat', 'funkce', 'tel', 'mobil', 'email', 'www', 'fax'], it)>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
        <#if canShowAtLeastOne(items, object, ['kod'], it)>
        <tr>
            <td><@tool.label name='kod' items=items />:</td><td colspan="5"><@tool.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['titul', 'jmeno', 'prijmeni'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['titul'], it)>
            <td><@tool.label name='titul' items=items />:</td><td><@tool.place object=object name='titul' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['jmeno'], it)>
            <td><@tool.label name='jmeno' items=items />:</td><td><@tool.place object=object name='jmeno' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['prijmeni'], it)>
            <td><@tool.label name='prijmeni' items=items />:</td><td><@tool.place object=object name='prijmeni' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['titulZa'], it)>
        <tr>
            <td><@tool.label name='titulZa' items=items />:</td><td><@tool.place object=object name='titulZa' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['ulice', 'mesto', 'psc'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['ulice'], it)>
            <td><@tool.label name='ulice' items=items />:</td><td><@tool.place object=object name='ulice' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['mesto'], it)>
            <td><@tool.label name='mesto' items=items />:</td><td><@tool.place object=object name='mesto' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['psc'], it)>
            <td><@tool.label name='psc' items=items />:</td><td><@tool.place object=object name='psc' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['stat'], it)>
        <tr>
            <td><@tool.label name='stat' items=items />:</td><td colspan="5"><@tool.place object=object name='stat' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['funkce', 'tel', 'mobil'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['funkce'], it)>
            <td><@tool.label name='funkce' items=items />:</td><td><@tool.place object=object name='funkce' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['tel'], it)>
            <td><@tool.label name='tel' items=items />:</td><td><@tool.placeTel object=object name='tel' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['mobil'], it)>
            <td><@tool.label name='mobil' items=items />:</td><td><@tool.placeTel object=object name='mobil' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canShowAtLeastOne(items, object, ['email', 'www', 'fax'], it)>
        <tr>
            <#if canShowAtLeastOne(items, object, ['email'], it)>
            <td><@tool.label name='email' items=items />:</td><td><@tool.placeEmail object=object name='email' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['www'], it)>
            <td><@tool.label name='www' items=items />:</td><td><@tool.placeWww object=object name='www' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canShowAtLeastOne(items, object, ['fax'], it)>
            <td><@tool.label name='fax' items=items />:</td><td><@tool.place object=object name='fax' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canShowAtLeastOne(items, object, ['role', 'ucetni', 'addUser', 'popis', 'poznam', 'platiOd', 'platiDo'], it)>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canShowAtLeastOne(items, object, ['role', 'ucetni', 'addUser'], it)>
            <li class="active"><a href="#view-fragment-1" data-toggle="tab">Přístupová práva</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <li><a href="#view-fragment-2" data-toggle="tab">${lang('labels', 'textyPN')}</a></li>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <li><a href="#view-fragment-3" data-toggle="tab">${lang('labels', 'spravaPN')}</a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canShowAtLeastOne(items, object, ['role', 'ucetni', 'addUser'], it)>
            <div id="view-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['role', 'ucetni', 'addUser'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canShowAtLeastOne(items, object, ['role'], it)>
                    <tr>
                        <td><@tool.label name='role' items=items />:</td><td><@tool.place object=object name='role' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['ucetni'], it)>
                    <tr>
                        <td>Právo zamykat doklady a období:</td><td><@tool.place object=object name='ucetni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['addUser'], it)>
                    <tr>
                        <td><@tool.label name='addUser' items=items />:</td><td><@tool.place object=object name='addUser' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
            <div id="view-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['popis', 'poznam'], it)>
                <table class="flexibee-tbl-1">
                    <#if canShowAtLeastOne(items, object, ['popis'], it)>
                    <tr>
                        <td><@tool.label name='popis' items=items />:<br><@tool.placeArea object=object name='popis' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['poznam'], it)>
                    <tr>
                        <td><@tool.label name='poznam' items=items />:<br><@tool.placeArea object=object name='poznam' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
            <div id="view-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canShowAtLeastOne(items, object, ['platiOd', 'platiDo'], it)>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canShowAtLeastOne(items, object, ['platiOd'], it)>
                    <tr>
                        <td><@tool.label name='platiOd' items=items />:</td><td><@tool.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canShowAtLeastOne(items, object, ['platiDo'], it)>
                    <tr>
                        <td><@tool.label name='platiDo' items=items />:</td><td><@tool.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

<@tool.listItems items=items object=object marker=marker />

<@tool.showSubEvidences subEvidences=it.evidenceResource.getSubEvidences(object) marker=marker />

<#include "/i-footer-view.ftl" />
</#escape>