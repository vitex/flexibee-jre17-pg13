<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('jazykK')}

<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['kod', 'titul', 'jmeno', 'prijmeni', 'titulZa', 'ulice', 'mesto', 'psc', 'stat', 'funkce', 'tel', 'mobil', 'email', 'www', 'fax'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" /><col width="15%" /><col width="18%" />
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td colspan="5"><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['titul', 'jmeno', 'prijmeni'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['titul'])>
            <td><@edit.label name='titul' items=items /></td><td><@edit.place object=object name='titul' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['jmeno'])>
            <td><@edit.label name='jmeno' items=items /></td><td><@edit.place object=object name='jmeno' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['prijmeni'])>
            <td><@edit.label name='prijmeni' items=items /></td><td><@edit.place object=object name='prijmeni' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['titulZa'])>
        <tr>
            <td><@edit.label name='titulZa' items=items /></td><td><@edit.place object=object name='titulZa' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['ulice', 'mesto', 'psc'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['ulice'])>
            <td><@edit.label name='ulice' items=items /></td><td><@edit.place object=object name='ulice' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['mesto'])>
            <td><@edit.label name='mesto' items=items /></td><td><@edit.place object=object name='mesto' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['psc'])>
            <td><@edit.label name='psc' items=items /></td><td><@edit.place object=object name='psc' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['stat'])>
        <tr>
            <td><@edit.label name='stat' items=items /></td><td colspan="5"><@edit.place object=object name='stat' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['funkce', 'tel', 'mobil'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['funkce'])>
            <td><@edit.label name='funkce' items=items /></td><td><@edit.place object=object name='funkce' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['tel'])>
            <td><@edit.label name='tel' items=items /></td><td><@edit.place object=object name='tel' items=items marker=marker  type='tel'/></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['mobil'])>
            <td><@edit.label name='mobil' items=items /></td><td><@edit.place object=object name='mobil' items=items marker=marker  type='tel'/></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['email', 'www', 'fax'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['email'])>
            <td><@edit.label name='email' items=items /></td><td><@edit.place object=object name='email' items=items marker=marker  type='email'/></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['www'])>
            <td><@edit.label name='www' items=items /></td><td><@edit.place object=object name='www' items=items marker=marker  type='url'/></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['fax'])>
            <td><@edit.label name='fax' items=items /></td><td><@edit.place object=object name='fax' items=items marker=marker  type='fax'/></td>
            </#if>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['role', 'ucetni', 'addUser', 'popis', 'poznam', 'platiOd', 'platiDo'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['role', 'ucetni', 'addUser'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>Přístupová práva</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'spravaPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['role', 'ucetni', 'addUser'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['role', 'ucetni', 'addUser'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <#if canEditAtLeastOne(items, object, ['role'])>
                    <tr>
                        <td><@edit.label name='role' items=items /></td><td><@edit.place object=object name='role' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['ucetni'])>
                    <tr>
                        <td><label for="flexibee-inp-ucetni">Právo zamykat doklady a období:</label></td><td><@edit.place object=object name='ucetni' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['addUser'])>
                    <tr>
                        <td><@edit.label name='addUser' items=items /></td><td><@edit.place object=object name='addUser' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['platiOd', 'platiDo'])>
                <table class="flexibee-tbl-1">
                    <col width="20%" /><col width="80%" />
                    <tr>
                        <td colspan="2">Platnost se udává v kalendářních rocích, pro neomezenou platnost zadejte 0-9999</td>
                    </tr>

                    <#if canEditAtLeastOne(items, object, ['platiOd'])>
                    <tr>
                        <td><@edit.label name='platiOd' items=items /></td><td><@edit.place object=object name='platiOd' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiDo'])>
                    <tr>
                        <td><@edit.label name='platiDo' items=items /></td><td><@edit.place object=object name='platiDo' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

<#--AUTOGEN END-->

    <#if it.evidenceResource.getCanChangePassword(it.userContext)>
    <h4 style="margin-top: 1em; margin-bottom: 0.3em;">Přihlašovací údaje</h4>
    <table class="flexibee-tbl-1">
        <#if !object.persistent?? || !object.persistent> <#-- nově vytvářený objekt je třídy Object (viz ActionList.getEditableObject) -->
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if object.persistent?? && object.persistent>
        ${marker.mark('kod')}
        <tr>
            <td colspan="2">Následující pole vyplňte, pokud chcete změnit heslo, jinak je ponechte prázdné</td>
        </tr>
        <#if it.evidenceResource.getNeedPreviousPasswordBeforeChange(object)>
        <tr>
            <td><label for="flexibee-inp-oldPassword">Původní heslo:</label></td>
            <td><input type="password" class="flexibee-inp flexibee-focus" name="oldPassword" id="flexibee-inp-oldPassword" /></td>
        </tr>
        </#if>
        </#if>
        <tr>
            <td><label for="flexibee-inp-password"><#if object.persistent?? && object.persistent>Nové heslo:<#else>Heslo:</#if></label></td>
            <td><input type="password" class="flexibee-inp flexibee-focus" name="password" id="flexibee-inp-password" /></td>
        </tr>
        <tr>
            <td><label for="flexibee-inp-passwordAgain"><#if object.persistent?? && object.persistent>Nové heslo znovu:<#else>Heslo znovu:</#if></label></td>
            <td><input type="password" class="flexibee-inp flexibee-focus" name="passwordAgain" id="flexibee-inp-passwordAgain" /></td>
        </tr>
    </table>
    </#if>

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<@edit.generateNaiveJavascript object=object it=it />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
