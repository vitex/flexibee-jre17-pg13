<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign myTitle = "Generování faktur" />
<#include "/i-header-edit.ftl"/>

<#if vygenerovanoFaktur??>
    <div class="alert alert-success">
        <strong>Generování faktur úspěšně dokončeno.</strong>
        <#if vygenerovanoFaktur?number &gt; 0>
            Počet vygenerovaných faktur: ${vygenerovanoFaktur}
        <#else>
            Nebyla vygenerována žádná nová faktura.
        </#if>
    </div>

    <@edit.buttonsBegin/>
       <a class="flexibee-focus btn btn-primary flexibee-clickable" data-dismiss="modal"  href="${it.goBackURI}"><span>${lang('buttons', 'goBack', 'Zpět XXX')}</span></a>
    <@edit.buttonsEnd/>
<#else>

<#if generovaniFakturResult??>
    <#if generovaniFakturResult.ok??>
        <div class="alert alert-success">${generovaniFakturResult.ok}</div>
    </#if>

    <div class="alert alert-danger">
        <ul class="flexibee-errors">
            <li>U následujících smluv se vyskytly chyby:</li>
            <#list generovaniFakturResult.errors as error>
                <li>${error}</li>
            </#list>
        </ul>
    </div>
</#if>


<@edit.beginForm />
    <p>Vygenerovat faktury?</p>
    <@edit.okCancelButton />
<@edit.endForm />

</#if>


<#include "/i-footer-edit.ftl" />
</#escape>