<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-edit.ftl"/>

${marker.mark('nazevA')}
${marker.mark('nazevB')}
${marker.mark('nazevC')}
${marker.mark('typOrganizace')}


<@edit.beginForm />
    <#--AUTOGEN BEGIN-->
    <div id="flexibee-smlouva-hlavicka">

    <div class="flexibee-table-border">
    <#if canEditAtLeastOne(items, object, ['typSml', 'firma', 'kod', 'nazev'])>
    <table class="flexibee-tbl-1">
        <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
        <#if canEditAtLeastOne(items, object, ['typSml', 'firma'])>
        <tr>
            <#if canEditAtLeastOne(items, object, ['typSml'])>
            <td><@edit.label name='typSml' items=items /></td><td class="neverHide"><@edit.place object=object name='typSml' items=items marker=marker /></td>
            </#if>
            <#if isMobile></tr><tr></#if>
            <#if canEditAtLeastOne(items, object, ['firma'])>
            <td><@edit.label name='firma' items=items /></td><td><@edit.place object=object name='firma' items=items marker=marker /></td>
            </#if>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['kod'])>
        <tr>
            <td><@edit.label name='kod' items=items /></td><td><@edit.place object=object name='kod' items=items marker=marker /></td>
        </tr>
        </#if>
        <#if canEditAtLeastOne(items, object, ['nazev'])>
        <tr>
            <td><@edit.label name='nazev' items=items /></td><td colspan="3"><@edit.place object=object name='nazev' items=items marker=marker /></td>
        </tr>
        </#if>
    </table>
    </#if>
    </div>

    <#if canEditAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo', 'zakazka', 'stredisko', 'stavSml', 'typDoklFak', 'frekFakt', 'den', 'mesic', 'varSym', 'konSym', 'popis', 'poznam'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <#if canEditAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo', 'zakazka', 'stredisko', 'stavSml'])>
            <li class="active"><a href="#edit-fragment-1" data-toggle="tab"><span>Základní</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['typDoklFak', 'frekFakt', 'den', 'mesic', 'varSym', 'konSym'])>
            <li><a href="#edit-fragment-2" data-toggle="tab"><span>Nastavení generace</span></a></li>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <li><a href="#edit-fragment-3" data-toggle="tab"><span>${lang('labels', 'textyPN')}</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <#if canEditAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo', 'zakazka', 'stredisko', 'stavSml'])>
            <div id="edit-fragment-1" class="tab-pane active">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo', 'zakazka', 'stredisko', 'stavSml'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['smlouvaOd', 'smlouvaDo'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['smlouvaOd'])>
                        <td><@edit.label name='smlouvaOd' items=items /></td><td><@edit.place object=object name='smlouvaOd' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['smlouvaDo'])>
                        <td><@edit.label name='smlouvaDo' items=items /></td><td><@edit.place object=object name='smlouvaDo' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['zakazka'])>
                    <tr>
                        <td><@edit.label name='zakazka' items=items /></td><td><@edit.place object=object name='zakazka' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stredisko'])>
                    <tr>
                        <td><@edit.label name='stredisko' items=items /></td><td><@edit.place object=object name='stredisko' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['stavSml'])>
                    <tr>
                        <td><@edit.label name='stavSml' items=items /></td><td><@edit.place object=object name='stavSml' items=items marker=marker /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['typDoklFak', 'frekFakt', 'den', 'mesic', 'varSym', 'konSym'])>
            <div id="edit-fragment-2" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['typDoklFak', 'frekFakt', 'den', 'mesic', 'varSym', 'konSym'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['typDoklFak'])>
                    <tr>
                        <td><@edit.label name='typDoklFak' items=items /></td><td><@edit.place object=object name='typDoklFak' items=items marker=marker /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['frekFakt'])>
                    <tr>
                        <td><@edit.label name='frekFakt' items=items level='2' /></td><td><@edit.place object=object name='frekFakt' items=items marker=marker  level='2'/></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['den', 'mesic'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['den'])>
                        <td><@edit.label name='den' items=items /></td><td><@edit.place object=object name='den' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['mesic'])>
                        <td><@edit.label name='mesic' items=items /></td><td><@edit.place object=object name='mesic' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['varSym', 'konSym'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['varSym'])>
                        <td><@edit.label name='varSym' items=items /></td><td><@edit.place object=object name='varSym' items=items marker=marker /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['konSym'])>
                        <td><@edit.label name='konSym' items=items /></td><td><@edit.place object=object name='konSym' items=items marker=marker /></td>
                        </#if>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
            <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
            <div id="edit-fragment-3" class="tab-pane">
                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['popis', 'poznam'])>
                <table class="flexibee-tbl-1">
                    <#if canEditAtLeastOne(items, object, ['popis'])>
                    <tr>
                        <td><@edit.label name='popis' items=items /><br><@edit.textarea object=object name='popis' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['poznam'])>
                    <tr>
                        <td><@edit.label name='poznam' items=items /><br><@edit.textarea object=object name='poznam' items=items marker=marker rows=3 /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

            </div>
            </#if>
        </div>
    </#if>

    </div>

    <#assign oldIt=it, oldItems=items, oldObject=object, oldMarker=marker />
    <#global it=it.getSubEvidence("smlouva-polozka") />
    <#assign items=it.editHandler.propertiesMap, object=it.editHandler.object, marker=newMarker() />

    <div id="flexibee-smlouva-polozky" style="display:none">

    <br><br>

    <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'cenaMj', 'cenaCelkem', 'typCenyDphK', 'typSzbDphK', 'platiOdData', 'platiDoData', 'frekFakt', 'datPoslFaktObd', 'cisRad'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#edit-fragment-4" data-toggle="tab"><span>Standardní položky smlouvy</span></a></li>

            <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'cenaMj', 'cenaCelkem', 'typCenyDphK', 'typSzbDphK', 'platiOdData', 'platiDoData', 'frekFakt', 'datPoslFaktObd', 'cisRad'])>
            <li><a href="#edit-fragment-5" data-toggle="tab"><span>+</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <div id="edit-fragment-4" class="tab-pane active">
                <div id="flexibee-smlouva-polozky-list" class="flexibee-table-border">
                      <table class="table">
                        <col width="16.6%"/><col width="16.6%"/>
                        <col width="16.6%"/><col width="16.6%"/>
                        <col width="16.6%"/><col width="16.6%"/>
                      </table>
                    </div>

            </div>

            <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'cenaMj', 'cenaCelkem', 'typCenyDphK', 'typSzbDphK', 'platiOdData', 'platiDoData', 'frekFakt', 'datPoslFaktObd', 'cisRad'])>
            <div id="edit-fragment-5" class="tab-pane">
                <div id="flexibee-smlouva-polozky-edit">

                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'cenaMj', 'cenaCelkem', 'typCenyDphK', 'typSzbDphK', 'platiOdData', 'platiDoData', 'frekFakt', 'datPoslFaktObd', 'cisRad'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['cenik'])>
                    <tr>
                        <td><@edit.label name='cenik' items=items prefix="polozkySmlouvy__" /></td><td colspan="3"><@edit.place object=object name='cenik' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kod', 'sklad'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['kod'])>
                        <td><@edit.label name='kod' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='kod' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['sklad'])>
                        <td><@edit.label name='sklad' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='sklad' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['nazev'])>
                    <tr>
                        <td><@edit.label name='nazev' items=items prefix="polozkySmlouvy__" /></td><td colspan="3"><@edit.place object=object name='nazev' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mnozMj', 'cenaMj'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mnozMj'])>
                        <td><@edit.label name='mnozMj' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='mnozMj' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['cenaMj'])>
                        <td><@edit.label name='cenaMj' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='cenaMj' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cenaCelkem'])>
                    <tr>
                        <td><@edit.label name='cenaCelkem' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='cenaCelkem' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typCenyDphK', 'typSzbDphK'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['typCenyDphK'])>
                        <td><@edit.label name='typCenyDphK' items=items prefix="polozkySmlouvy__" level='3' /></td><td><@edit.place object=object name='typCenyDphK' items=items marker=marker prefix="polozkySmlouvy__"  level='3'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['typSzbDphK'])>
                        <td><@edit.label name='typSzbDphK' items=items prefix="polozkySmlouvy__" level='3' /></td><td><@edit.place object=object name='typSzbDphK' items=items marker=marker prefix="polozkySmlouvy__"  level='3'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiOdData', 'platiDoData'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['platiOdData'])>
                        <td><@edit.label name='platiOdData' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='platiOdData' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['platiDoData'])>
                        <td><@edit.label name='platiDoData' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='platiDoData' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['frekFakt', 'datPoslFaktObd'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['frekFakt'])>
                        <td><@edit.label name='frekFakt' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='frekFakt' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['datPoslFaktObd'])>
                        <td><@edit.label name='datPoslFaktObd' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='datPoslFaktObd' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cisRad'])>
                    <tr>
                        <td><@edit.label name='cisRad' items=items prefix="polozkySmlouvy__" /></td><td><@edit.place object=object name='cisRad' items=items marker=marker prefix="polozkySmlouvy__" /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <div class="flexibee-buttons" <#if isMobile>data-role="controlgroup"  data-type="horizontal"</#if>>
                        <input type="button" name="ok" value="OK" class="flexibee-button" id="flexibee-smlouva-polozky-edit-ok" />
                        <input type="button" name="cancel" value="Zrušit" class="flexibee-button negative" id="flexibee-smlouva-polozky-edit-cancel" />
                      </div>
                    </div>

            </div>
            </#if>
        </div>
    </#if>

    </div>

    <#global it=oldIt />
    <#assign items=oldItems, object=oldObject, marker=oldMarker />

    <#assign oldIt=it, oldItems=items, oldObject=object, oldMarker=marker />
    <#global it=it.getSubEvidence("smlouva-polozka") />
    <#assign items=it.editHandler.propertiesMap, object=it.editHandler.object, marker=newMarker() />

    <div id="flexibee-smlouva-polozky-ext" style="display:none">

    <br><br>

    <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'cenaMj', 'cenaCelkem', 'typCenyDphK', 'typSzbDphK', 'platiOdData', 'platiDoData', 'frekFakt', 'datPoslFaktObd', 'cisRad'])>
        <ul id="tabs" class="flexibee-tab nav nav-tabs" data-tabs="tabs">
            <li class="active"><a href="#edit-fragment-6" data-toggle="tab"><span>Externí položky smlouvy</span></a></li>

            <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'cenaMj', 'cenaCelkem', 'typCenyDphK', 'typSzbDphK', 'platiOdData', 'platiDoData', 'frekFakt', 'datPoslFaktObd', 'cisRad'])>
            <li><a href="#edit-fragment-7" data-toggle="tab"><span>+</span></a></li>
            </#if>
        </ul>
        <div class="tab-content">
            <div id="edit-fragment-6" class="tab-pane active">
                <div id="flexibee-smlouva-polozky-ext-list" class="flexibee-table-border">
                      <table class="table">
                        <col width="16.6%"/><col width="16.6%"/>
                        <col width="16.6%"/><col width="16.6%"/>
                        <col width="16.6%"/><col width="16.6%"/>
                      </table>
                    </div>

            </div>

            <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'cenaMj', 'cenaCelkem', 'typCenyDphK', 'typSzbDphK', 'platiOdData', 'platiDoData', 'frekFakt', 'datPoslFaktObd', 'cisRad'])>
            <div id="edit-fragment-7" class="tab-pane">
                <div id="flexibee-smlouva-polozky-ext-edit">

                <div class="flexibee-table-border">
                <#if canEditAtLeastOne(items, object, ['cenik', 'kod', 'sklad', 'nazev', 'mnozMj', 'cenaMj', 'cenaCelkem', 'typCenyDphK', 'typSzbDphK', 'platiOdData', 'platiDoData', 'frekFakt', 'datPoslFaktObd', 'cisRad'])>
                <table class="flexibee-tbl-1">
                    <col width="15%" /><col width="35%" /><col width="15%" /><col width="35%" />
                    <#if canEditAtLeastOne(items, object, ['cenik'])>
                    <tr>
                        <td><@edit.label name='cenik' items=items prefix="polozkySmlouvyExt__" /></td><td colspan="3"><@edit.place object=object name='cenik' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['kod', 'sklad'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['kod'])>
                        <td><@edit.label name='kod' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='kod' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['sklad'])>
                        <td><@edit.label name='sklad' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='sklad' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['nazev'])>
                    <tr>
                        <td><@edit.label name='nazev' items=items prefix="polozkySmlouvyExt__" /></td><td colspan="3"><@edit.place object=object name='nazev' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['mnozMj', 'cenaMj'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['mnozMj'])>
                        <td><@edit.label name='mnozMj' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='mnozMj' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['cenaMj'])>
                        <td><@edit.label name='cenaMj' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='cenaMj' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cenaCelkem'])>
                    <tr>
                        <td><@edit.label name='cenaCelkem' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='cenaCelkem' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['typCenyDphK', 'typSzbDphK'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['typCenyDphK'])>
                        <td><@edit.label name='typCenyDphK' items=items prefix="polozkySmlouvyExt__" level='3' /></td><td><@edit.place object=object name='typCenyDphK' items=items marker=marker prefix="polozkySmlouvyExt__"  level='3'/></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['typSzbDphK'])>
                        <td><@edit.label name='typSzbDphK' items=items prefix="polozkySmlouvyExt__" level='3' /></td><td><@edit.place object=object name='typSzbDphK' items=items marker=marker prefix="polozkySmlouvyExt__"  level='3'/></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['platiOdData', 'platiDoData'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['platiOdData'])>
                        <td><@edit.label name='platiOdData' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='platiOdData' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['platiDoData'])>
                        <td><@edit.label name='platiDoData' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='platiDoData' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['frekFakt', 'datPoslFaktObd'])>
                    <tr>
                        <#if canEditAtLeastOne(items, object, ['frekFakt'])>
                        <td><@edit.label name='frekFakt' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='frekFakt' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                        </#if>
                        <#if isMobile></tr><tr></#if>
                        <#if canEditAtLeastOne(items, object, ['datPoslFaktObd'])>
                        <td><@edit.label name='datPoslFaktObd' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='datPoslFaktObd' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                        </#if>
                    </tr>
                    </#if>
                    <#if canEditAtLeastOne(items, object, ['cisRad'])>
                    <tr>
                        <td><@edit.label name='cisRad' items=items prefix="polozkySmlouvyExt__" /></td><td><@edit.place object=object name='cisRad' items=items marker=marker prefix="polozkySmlouvyExt__" /></td>
                    </tr>
                    </#if>
                </table>
                </#if>
                </div>

                <div class="flexibee-buttons" <#if isMobile>data-role="controlgroup"  data-type="horizontal"</#if>>
                        <input type="button" name="ok" value="OK" class="flexibee-button" id="flexibee-smlouva-polozky-ext-edit-ok" />
                        <input type="button" name="cancel" value="Zrušit" class="flexibee-button negative" id="flexibee-smlouva-polozky-ext-edit-cancel" />
                      </div>
                    </div>

            </div>
            </#if>
        </div>
    </#if>

    </div>

    <#global it=oldIt />
    <#assign items=oldItems, object=oldObject, marker=oldMarker />

<#--AUTOGEN END-->

<#-- toto nechceme zobrazovat v patičce, když to nemám editovatelné -->
${marker.mark('sablona')}

    <br/>
    <@edit.listItems items=items object=object marker=marker />

    <@edit.saveButton />
<@edit.endForm />

<#import "/evidence/l-smlouva-edit.ftl" as smlouvaEdit />
<@smlouvaEdit.generateJavaScript it=it object=object />

<#include "/parts/i-dalsi-informace.ftl" />

<#include "/i-footer-edit.ftl" />
</#escape>
