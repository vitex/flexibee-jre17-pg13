<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#include "/i-header-list.ftl" />

<#if true>
<#include "/evidence/i-list.ftl" />
<#else>

<div class="btn-group" role="toolbar">
  <button type="button" class="btn btn-default active"><i class="fa fa-th-large"></i></button>
  <button type="button" class="btn btn-default"><i class="fa fa-th-list"></i></button>
</div>

<#include "/evidence/i-list-begin.ftl" />


<#if it.rowCount &gt; 0>
    <div class="row">
        <#list it.list as object>
        <div class="<#if object_index % 2 = 0>flexibee-even</#if> column flexibee-keynav col-xs-6 col-sm-3" style="width:200px;height:200">
                <a href="/c/${it.companyResource.companyId}/${it.evidenceType.path}/${object.id?string('0')}" class="thumbnail">
                <img src="/c/${it.companyResource.companyId}/${it.evidenceType.path}/${object.id?string('0')}/thumbnail.png?width=200&amp;height=200"/>
                <div class="caption">${object.nazev}</div>
                </a>
        </div>
        </#list>
    </div>
</#if>

<#include "/evidence/i-list-end.ftl" />
</#if>

<#include "/footer.ftl" />
</#escape>