<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>



Ext.define('FlexiBee.main.dashboard.BuyDashboard', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.fbBuyDashboard',

    requires: [
        'Ext.tip.QuickTipManager',
        'Ext.panel.Panel',
        'Ext.JSON',
        'FlexiBee.main.dashboard.panel.BasePanel',
        'FlexiBee.main.dashboard.BaseDashboard'
    ],

    title: '${lblText('nakupPN')}',

    items: [
        {
            xtype: 'container',
            region: 'center',
            items: [
                {
                    xtype: 'fbDashboardDokladPanel',
                    data: [
                        {
                            title: '${actText('openDoklady$$FAP')}',
                            evidenceName: 'faktura-prijata'
                        }
                    ]
                }
            ]
        }
    ]
});

</#escape>
