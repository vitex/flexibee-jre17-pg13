<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.All', {
});

<#list [
"root/sencha-MessageBus",
"root/sencha-Plugins",
"dashboard/sencha-DashboardPanel.ftl",
"dashboard/sencha-BaseDashboard.ftl",
"dashboard/sencha-SalesDashboard.ftl",
"dashboard/sencha-MoneyDashboard.ftl",
] as i>
<#include resolveTemplate("/" + i) />
</#list>


</#escape>
