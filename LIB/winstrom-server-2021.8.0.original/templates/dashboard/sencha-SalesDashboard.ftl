<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>



Ext.define('FlexiBee.main.dashboard.SalesDashboard', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.fbSalesDashboard',

    requires: [
        'Ext.tip.QuickTipManager',
        'Ext.panel.Panel',
        'Ext.JSON',
        'FlexiBee.main.dashboard.panel.BasePanel',
        'FlexiBee.main.dashboard.panel.DokladPanel',
        'FlexiBee.main.dashboard.BaseDashboard'
    ],

    title: '${lblText('prodejPN')}',

    items: [
        {
            xtype: 'container',
            region: 'center',
            items: [
                {
                    xtype: 'fbDashboardDokladPanel',
                    data: [
                        {
                            title: '${actText('openDoklady$$FAV')}',
                            evidenceName: 'faktura-vydana'
                        }
                    ]
                }
            ]
        }
    ]
});

</#escape>
