<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>



Ext.define('FlexiBee.main.dashboard.DashboardPanel', {
    extend: 'Ext.container.Container',
    alias: 'widget.fbDashboardPanel',

    requires: [
        'Ext.tip.QuickTipManager',
        'FlexiBee.root.Plugins',
        'Ext.form.Panel',
        'Ext.container.Container',
        'FlexiBee.main.dashboard.panel.BasePanel',
        'FlexiBee.main.dashboard.panel.BSPPanel',
        'FlexiBee.main.dashboard.panel.DokladPanel',
        'FlexiBee.main.dashboard.BaseDashboard',
        'FlexiBee.main.dashboard.MoneyDashboard',
        'FlexiBee.main.dashboard.AnalyzaProdejeDashboard',
        'FlexiBee.main.dashboard.SalesDashboard',
        'FlexiBee.main.dashboard.BuyDashboard',
        'Ext.panel.Panel'
    ],

    layout: {
        type: 'table',
        columns: 2
    },


    items: [
        {
            xtype: 'fbMoneyDashboard'
        },
        {
            xtype: 'fbAnalyzaProdejeDashboard'
        },
        {
            xtype: 'fbSalesDashboard'
        },
        {
            xtype: 'fbBuyDashboard'
        }
    ]

});

</#escape>
