<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>



Ext.define('FlexiBee.main.dashboard.panel.BasePanel', {
    extend: 'Ext.container.Container',

    requires: [
        'Ext.tip.QuickTipManager',
        'Ext.container.Container',
        'Ext.JSON',
        'FlexiBee.main.dashboard.panel.BSPModel'
    ],

    padding: 15,
    evidenceName: '',
    title: 'Titulek',

    groupBy: 'monthly',
    gridComponent: undefined,

    items: [],

    collectParams: function() {
        var self = this;

        var params = {};
        if (self.groupBy == 'yearly') {
            var year = new Date().getFullYear();
            params['current'] = 'filter%5BfilterRok.datVyst%5D='+year;
            params['prev'] = 'filter%5BfilterRok.datVyst%5D='+(year-1);
        } else if (self.groupBy == 'quarterly') {
            var year = new Date().getFullYear();
            params['current'] = 'filter%5BfilterRok.datVyst%5D='+year+'&';
            params['prev'] = 'filter%5BfilterRok.datVyst%5D='+(year-1);

            //filterObdobi.datVyst%5D=Q1%2F2012
        } else if (self.groupBy == 'monthly') {
            var year = new Date().getFullYear();
            params['current'] = 'filter%5BfilterRok.datVyst%5D='+year;
            params['prev'] = 'filter%5BfilterRok.datVyst%5D='+(year-1);
        }

        return params;
    },

    processValues: function(content, prevContent, data) {
        var self = this;

        return data;
    },

    addData: function(dataDefinition) {
        var title = dataDefinition.title;
        var evidenceName = dataDefinition.evidenceName;

        var self = this;
        var params = self.collectParams();

        Ext.Ajax.request({
            url: '/c/${it.companyId}/' + evidenceName + '/$sum.json?' + params['prev'],
            success: function(response) {
                var text = response.responseText;
                var js = Ext.JSON.decode(text, false);

                var prevContent = js['winstrom']['sum'];

                Ext.Ajax.request({
                    url: '/c/${it.companyId}/' + evidenceName + '/$sum.json?' + params['current'],
                    success: function(response){
                        var text = response.responseText;
                        var js = Ext.JSON.decode(text, false);

                        var content = js['winstrom']['sum'];

                        var data = [];
                        data = self.processValues(content, prevContent, data, dataDefinition);

                        self.gridComponent.store.loadRawData(data, true);
                    }
                });
            }
        });
    },

    initComponent: function() {
        var self = this;

        self.gridComponent = Ext.create(self.grid, {
        });

        self.items = [ self.gridComponent ];

        for (var i = 0; i < self.data.length;i++) {
            var d = self.data[i];
            self.addData(d);
        }

        self.callParent(arguments);
    }
});

</#escape>
