<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.dashboard.panel.BSPModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Model'
    ],

    fields: [
        {
            id: 'group',
            name: 'group',
            title: 'Skupina'
        },
        {
            id: 'date',
            name: 'date',
            title: 'Datum'
        },
        {
            id: 'pocatek',
            name: 'pocatek',
            title: 'Počátek'
        },
        {
            id: 'prijmy',
            name: 'prijmy',
            title: 'Příjmy'
        },
        {
            id: 'vydaje',
            name: 'vydaje',
            title: 'Výdaje'
        },
        {
            id: 'zustatek',
            name: 'zustatek',
            title: 'Zůstatek'
        },
        {
            id: 'currency',
            name: 'currency',
            title: 'Měna'
        }
    ]
});


</#escape>
