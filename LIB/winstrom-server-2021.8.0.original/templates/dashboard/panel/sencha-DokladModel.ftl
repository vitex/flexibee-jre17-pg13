<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.dashboard.panel.DokladModel', {
    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.Model'
    ],

    fields: [
        {
            id: 'group',
            name: 'group',
            title: 'Datum'
        },
        {
            id: 'date',
            name: 'date',
            title: 'Datum'
        },
        {
            id: 'obrat',
            name: 'obrat',
            title: 'Zisk'
        },
        {
            id: 'naklady',
            name: 'naklady',
            title: 'Náklady'
        },
        {
            id: 'naklady',
            name: 'naklady',
            title: 'Náklady'
        },
        {
            id: 'currency',
            name: 'currency',
            title: 'Měna'
        }
    ]
});


</#escape>
