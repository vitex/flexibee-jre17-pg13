<#ftl encoding="utf-8" />
<#escape x as x?default("")?js_string>

Ext.define('FlexiBee.main.dashboard.panel.DokladGrid', {
    extend: 'Ext.grid.Panel',

    requries: [
        'FlexiBee.main.dashboard.panel.DokladModel',
        'Ext.data.Store'
    ],

    store: undefined,
    border: 0,
    columns: [
        {
            text: '',
            dataIndex: 'date',
            flex: 1
        },
        {
            text: 'Obrat',
            dataIndex: 'obrat',
            format: '0,000.00',
            align: 'right',
            xtype: 'numbercolumn',
        },
        {
            text: 'Náklady',
            dataIndex: 'naklady',
            align: 'right',
            xtype: 'numbercolumn',
            format: '0,000.00',
        },
        {
            text: 'K úhradě',
            dataIndex: 'zustatek',
            align: 'right',
            xtype: 'numbercolumn',
            format: '0,000.00',
        }
    ],
    features: [{
             id: 'group',
             ftype: 'groupingsummary',
             groupHeaderTpl: '{name}',
             hideGroupedHeader: false,
             enableGroupingMenu: false
    }],
    initComponent: function() {
        var self = this;

        self.store = Ext.create('Ext.data.Store', {
            model: 'FlexiBee.main.dashboard.panel.DokladModel',
            groupField: 'group',
            data: { 'items': [] },
            proxy: {
                type: 'memory',
                reader: {
                    type: 'json',
                    root: 'items'
                }
            }
        });

        self.callParent(arguments);
    }
});


</#escape>
