<#ftl encoding="utf-8" />
<#escape x as x?default("")?html>

<#assign m = menuTool().root/>
<!--FLEXIBEE:MENU:START-->
    <div class="flexibee-aplication-menu column sidebar-offcanvas" id="sidebar">
        <div id="lg-menu">
        <ul class="nav">
            <li><a href="/admin/disk-usage"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.disk_usage.title.short')}</a></li>
            <li><a href="/u"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.users.title')}</a></li>
            <li><a href="/a"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.accesses.title')}</a></li>
            <li><a href="/g"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.license_groups.title')}</a></li>
            <li><a href="/default-license"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.default_licence.title')}</a></li>
            <#if loggedUser.permissions.canManageAuthSettings()>
            <li><a href="/admin/auth-settings"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.authorization.title')}</a></li>
            </#if>
<#--            <li><a href="/admin/server-settings"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.server_params.title.short')}</a></li> -->

<#--             <li><a href="/sencha/ssl"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.ssl_certificate.title')}</a></li> -->
             <#if loggedUser.permissions.canAccessThreadPool()>
             <li><a href="/status/thread-pool"><i class="glyphicon glyphicon-cog"></i> Seznam vláken</a></li>
             </#if>
             <#if loggedUser.permissions.canAccessConnPool()>
             <li><a href="/status/conn-pool"><i class="glyphicon glyphicon-cog"></i> Databázová spojení</a></li>
            </#if>
            <li><a href="/status/session"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.users_logged_in.title')}</a></li>
            <#if loggedUser.permissions.canAccessClusterStatus()>
            <li><a href="/status/cluster"><i class="glyphicon glyphicon-cog"></i> ${dlgText('admin.run_in_cluster.title')}</a></li>
            </#if>
            <#if loggedUser.isInstanceAdmin()>
            <li><a href="/status/web-hooks"><i class="glyphicon glyphicon-cog"></i> ${dlgText('webHooks')}</a></li>
            </#if>
        </ul>
        </div>

    </div>

</#escape>
