Ukázka, jak změnit šablonu e-mailu pro odeslání dokladu. Musí být přítomny oba soubory:

  - odeslani-dokladu.html (šablona pro HTML podobu e-mailu)
  - odeslani-dokladu.txt (šablona pro textovou podobu e-mailu)

Šablony jsou velmi jednoduché, lze jen používat proměnné: ${promenna}. Dostupné proměnné jsou:

  - content: text e-mailu
  - application: identifikátor FlexiBee
