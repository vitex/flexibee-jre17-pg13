// ukázka, jak nastavit načítání vlastních tříd a vykonání nějakého kódu ihned po spuštění aplikace
Ext.define('com.example.ApplicationInitPluginSample', {
    createPathsConfig: function(basePath) {
        return {
            'com.example': basePath + '/sencha/assets/com.example'
        };
    },

    applicationLaunched: function() {
        console.log('Application launched');
    }
});
