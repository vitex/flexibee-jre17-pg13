// ukázka, jak přidat položky do hlavního toolbaru
//
// metoda defineAdditionalItems může vrátit jednu položku nebo pole
Ext.define('com.example.MainToolbarPluginSample', {
    defineAdditionalItems: function() {
        return [
            '-',
            {
                text: 'Podpora FlexiBee',
                xtype: 'splitbutton',
                menu: [
                    {
                        text: 'Můj FlexiBee'
                    },
                    {
                        text: 'Moje požadavky'
                    },
                    {
                        text: 'Moje telefonní hovory'
                    }
                ]
            }
        ];
    }
});
