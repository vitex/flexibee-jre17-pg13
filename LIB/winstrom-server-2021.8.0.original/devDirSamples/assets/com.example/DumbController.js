// ukázka vlastního controlleru, který ale nic nedělá, jen zobrazí adresář (ani na nic nereaguje)
Ext.define('com.example.DumbController', {
    extend: 'FlexiBee.main.AbstractController',

    title: '',

    createView: function() {
        var tabTitle = this.title;
        return Ext.create('FlexiBee.adresar.List', {
            closable: true,
            title: tabTitle
        });
    },

    setupControl: function() {
        this.controlOver({
        });
    }
});
