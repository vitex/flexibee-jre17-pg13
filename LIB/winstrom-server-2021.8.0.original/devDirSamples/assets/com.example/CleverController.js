// ukázka vlastního controlleru, který zobrazí vlastní panel a reaguje i na události
Ext.define('com.example.CleverController', {
    extend: 'FlexiBee.main.AbstractController',

    requires: [
        'FlexiBee.adresar.Model'
    ],

    title: '',

    createView: function() {
        var tabTitle = this.title;
        return Ext.create('com.example.AdresarForm', {
            closable: true,
            title: tabTitle,
            padding: 10
        });
    },

    setupControl: function() {
        this.controlOver({
            'button': {
                click: this.onButtonClicked
            }
        });

        var form = this.view.getForm();
        FlexiBee.adresar.Model.load(199, { // 199 vždy existuje
            success: function(model) {
                form.loadRecord(model);
            },
            failure: function(model) {
                window.alert("Nepodařilo se načíst záznam");
            }
        });
    },

    onButtonClicked: function() {
        var me = this;

        var form = me.view.getForm();
        var model = form.updateRecord().getRecord();
        model.save({
            callback: function(model, operation) {
                if (!operation.wasSuccessful()) {
                    window.alert("Nepodařilo se uložit záznam");
                } else {
                    me.closeTab();
                }
            }
        });
    }
});
