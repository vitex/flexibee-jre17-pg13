Jak začít s modifikací webové aplikace FlexiBee.

Je potřeba mít verzi, která to podporuje (tj. 10.17.x a novější). 

1) Je potřeba upravit flexibee-server.xml a přidat tam

    <entry key="developerDirectory">/devel/</entry> 

Místo /devel/ dejte adresář, kde budou data pro modifikaci (např. C:\Projekty\FlexiBee\). 

2) Restartovat FlexiBee

3) V daném adresáři je potřeba vytvořit adresář "default" (tzv. výchozí instance - jiná hodnota má
smysl pouze v cloudovém provozu) a v něm adresář s identifikátorem firmy (ten je stejný jako přes
webové rozhraní). Případně je možné použít speciální identifikátor "!all" (bez uvozovek).

4) Nakopírovat ukázkové skripty do $developerDirectory/$instance/$company.

5) Do prohlížeče zadat https://localhost:5434/c/$company/sencha/web
