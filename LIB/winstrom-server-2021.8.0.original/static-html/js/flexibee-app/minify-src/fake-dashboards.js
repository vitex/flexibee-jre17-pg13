webpackJsonp([8],{

/***/ 1026:
/***/ function(module, exports) {

	module.exports = {
		"dashboard-panel": [
			{
				"id": 1,
				"kod": "FAKE-DASH",
				"nazev": "Můj Dashboard",
				"nazevA": "My Dashboard",
				"nazevB": "Mein Dashboard",
				"nazevC": "",
				"popis": "Základní statistiky zaměstnance",
				"popisA": "Employee basic statistics",
				"popisB": "Mitarbeiter grundlegende Statistiken",
				"popisC": "",
				"definition": [
					{
						"header": "",
						"description": "",
						"className": "col-xs-12 dashboard-selector",
						"component": "CxDashboardSelector",
						"componentProps": {
							"dateRanges": [
								"w",
								"m",
								"y",
								"d",
								"+1d",
								"-5m..-1m",
								"-5w..-1w",
								"-5y..-1y"
							],
							"selectedDateRange": "m",
							"maxVisibleButtons": 2,
							"settingsKey": "CxDashboardSelector1-1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-12",
						"component": "CxDashboardLanguageSwitch",
						"componentProps": {
							"settingsKey": "CxDashboardLanguageSwitch1-1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-6 col-sm-3",
						"component": "CxDashboardBankKPITable",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardBankKPITable1-1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-6 col-sm-3",
						"component": "CxDashboardCashDeskKPITable",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardCashDeskKPITable1-1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-6 col-sm-3",
						"component": "CxDashboardStockLevelKPITable",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardStockLevelKPITable1-1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-12 col-sm-6",
						"component": "CxDashboardBankAccountTurnoverChart",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardBankAccountTurnoverChart1-1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-12 col-sm-6",
						"component": "CxDashboardCashDeskTurnoverChart",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardCashDeskTurnoverChart1-1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-12 col-sm-6",
						"component": "CxDashboardRevenueCostChart",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardRevenueCostChart1-1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-12 col-sm-6",
						"component": "CxDashboardCashBankBalanceTable",
						"componentProps": {
							"settingsKey": "CxDashboardCashBankBalanceTable1"
						}
					},
					{
						"header": "",
						"description": "",
						"className": "col-xs-12 col-sm-6",
						"component": "CxDashboardReceivablesTable",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardReceivablesTable1-1",
							"maxRows": 20,
							"dueDateBeforeDays": 30,
							"excludeNonAccounting": false
						}
					}
				],
				"author": null,
				"visibilityK": "dashboardPanelVisibility.shared-all",
				"priority": 20
			},
			{
				"id": 2,
				"kod": "FAKE-DASH2",
				"nazev": "Můj Dashboard",
				"nazevA": "My Dashboard",
				"nazevB": "Mein Dashboard",
				"nazevC": "",
				"popis": "Základní statistiky zaměstnance",
				"popisA": "Employee basic statistics",
				"popisB": "Mitarbeiter grundlegende Statistiken",
				"popisC": "",
				"definition": [
					{
						"header": "Customer satisfaction",
						"className": "col-xs-12 col-sm-6",
						"component": "CxChartWithSwitch",
						"componentProps": {
							"data": {
								"labels": [
									1,
									2,
									3,
									4,
									5,
									6,
									7,
									8
								],
								"series": [
									[
										5,
										9,
										7,
										8,
										5,
										3,
										5,
										4
									]
								]
							},
							"options": {
								"low": 0,
								"showArea": true
							},
							"type": "Line",
							"settingsKey": "CxChartWithSwitch2-1"
						}
					},
					{
						"header": "Sample CxTurnoverChart",
						"className": "col-xs-12 col-sm-5",
						"component": "CxDashboardTurnoverComponent",
						"componentProps": {
							"settingsKey": "CxChartWithSwitch2-2"
						}
					},
					{
						"header": "Banka",
						"description": "nějaký popis",
						"className": "col-xs-12 col-sm-6",
						"component": "CxDashboardSimpleTable",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardSimpleTable1-summary",
							"summary": {
								"entity": "banka-polozka",
								"group": "month(datUcto)",
								"fields": [
									"sumZkl"
								]
							},
							"columns": [
								{
									"name": "Měsíc",
									"field": "arg"
								},
								{
									"name": "Kč",
									"field": "sumZkl"
								}
							]
						}
					},
					{
						"header": "CxDashboardSimpleTable data",
						"description": "nějaký popis",
						"className": "col-xs-12 col-sm-6",
						"component": "CxDashboardSimpleTable",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardSimpleTable1-data",
							"data": [
								{
									"a": "1/2015",
									"b": "300 000 Kč",
									"c": "16. 5. 2015",
									"d": "-"
								},
								{
									"a": "2/2015",
									"b": "250 000 Kč",
									"c": "18. 5. 2015",
									"d": "+"
								},
								{
									"a": "3/2015",
									"b": "10 000 Kč",
									"c": "20. 5. 2015",
									"d": "-"
								}
							],
							"columns": [
								{
									"name": "Číslo faktury",
									"field": "a"
								},
								{
									"name": "Vystavení",
									"field": "c"
								},
								{
									"name": "Částka",
									"field": "b"
								}
							]
						}
					},
					{
						"header": "Table loaded after you choose division",
						"description": "nějaký popis",
						"className": "col-xs-12 col-sm-6",
						"component": "CxDashboardSimpleTable",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxDashboardSimpleTable1-query",
							"query": {
								"id": "20",
								"params": {
									"param_3": "obrmd-obrdal",
									"param_x": "${selectedDivision}"
								}
							},
							"columns": [
								{
									"name": "Měsíc",
									"field": "month"
								},
								{
									"name": "Kolik",
									"field": "value"
								}
							]
						}
					},
					{
						"header": "Pohledávky",
						"description": "Největší faktury",
						"className": "col-xs-12 col-sm-6",
						"component": "CxSimpleTable",
						"componentProps": {
							"height": "400px",
							"settingsKey": "CxSimpleTable1-1",
							"data": [
								{
									"a": "1/2015",
									"b": "300 000 Kč",
									"c": "16. 5. 2015",
									"d": "-"
								},
								{
									"a": "2/2015",
									"b": "250 000 Kč",
									"c": "18. 5. 2015",
									"d": "+"
								},
								{
									"a": "3/2015",
									"b": "10 000 Kč",
									"c": "20. 5. 2015",
									"d": "-"
								}
							],
							"columns": [
								{
									"name": "Číslo faktury",
									"field": "a"
								},
								{
									"name": "Vystavení",
									"field": "c"
								},
								{
									"name": "Částka",
									"field": "b"
								}
							]
						}
					},
					{
						"comments": [
							"TODO should be: /uzivatelsky-dotaz/code:DESKA-PRO GRAFY2/call.json?param_3=obrmd-obrdal"
						],
						"header": "/uzivatelsky-dotaz/20/call.json?param_3=obrmd-obrdal",
						"className": "col-xs-12",
						"component": "CxChartWithSwitch",
						"componentProps": {
							"height": "300px",
							"chartistData": {
								"labels": [
									"s1",
									"s2",
									"s3",
									"s4",
									"s5",
									"s6",
									"s7",
									"s8",
									"s9",
									"s10",
									"s11",
									"s12",
									"s13",
									"s14",
									"s15",
									"s16",
									"s17",
									"s18",
									"s19",
									"s20",
									"s21",
									"s22",
									"s23",
									"s24"
								],
								"series": [
									[
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0,
										0
									]
								]
							},
							"query": {
								"comments": [
									"TODO should be: \"code:DESKA-PRO GRAFY2\", but that causes an error:",
									"The request was redirected to http..., which is disallowed for cross-origin requests that require preflight."
								],
								"id": "20",
								"params": {
									"param_3": "obrmd-obrdal"
								},
								"displayColumns": [
									"value"
								]
							},
							"chartistOptions": {
								"high": 10,
								"low": -10
							},
							"chartistType": "Bar",
							"settingsKey": "CxChartWithSwitch1-1"
						}
					},
					{
						"header": "Customer satisfaction",
						"className": "col-xs-12 col-sm-6",
						"component": "CxChartWithSwitch",
						"componentProps": {
							"height": "300px",
							"chartistData": {
								"labels": [
									1,
									2,
									3,
									4,
									5,
									6,
									7,
									8
								],
								"series": [
									[
										5,
										9,
										7,
										8,
										5,
										3,
										5,
										4
									]
								]
							},
							"chartistOptions": {
								"low": 0,
								"showArea": true
							},
							"chartistType": "Line",
							"settingsKey": "CxChartWithSwitch1-2"
						}
					}
				],
				"author": null,
				"visibilityK": "dashboardPanelVisibility.shared-all",
				"priority": 20
			}
		]
	}

/***/ }

});