(function($, window, undefined) {

var ItemsHelper = function(options) {
    this.id = options.evidence.prefix;
    this.evidenceName = options.evidence.name; // název subevidence, tedy evidence pro položky (např. faktura-vydana-polozka)
    this.filledProps = {};
    this.converter = new flexibee.FormAndJson(options.evidence.prefix, options.evidence.props, this.filledProps, options.dependencies);
    this.defaults = options.evidence.defaults || {}; // defaultní hodnoty, které se použijí při vytváření JSONu z aktuálního formuláře
                                                     // hodnota _nesmí_ být funkce (zatím?)
    this.hideOnStart = options.evidence.hideOnStart;

    this.listTable = $(options.page.list); // část stránky se seznamem položek
    this.editForm = $(options.page.edit); // část stránky s editačním formulářem jedné položky
    this.activateList = options.page.activateList; // funkce, která přepne zobrazení na seznam položek
    this.activateEdit = options.page.activateEdit; // funkce, která přepne zobrazení na editaci jedné položky
    this.changeEditTitle = options.page.changeEditTitle; // funkce, která změní nadpis editace položky
    this.okButton = $("input[type='button'][name='ok']", this.editForm);
    this.cancelButton = $("input[type='button'][name='cancel']", this.editForm);

    this.renderItem = options.data.renderItem; // funkce, která vyrenderuje jednu položku seznamu
    this.postConvertForm2Json = options.data.postConvertForm2Json; // volá se při přechodu edit -> list
    this.postConvertJson2Form = options.data.postConvertJson2Form; // volá se při přechodu list -> edit
    this.postItemDeleted = options.data.postItemDeleted; // volá se po smazání položky (ještě před doplněním formuláře)
    this.itemTitle = options.data.itemTitle; // funkce, která vrátí "název" položky (nějaký rozumný krátký popisný text)
    this.isItemEmpty = options.data.isItemEmpty; // funkce, která zjistí, jestli je položka prázdná (nepovinné)
    this.items = options.data.initialData; // položky (pole javascriptových objektů)

    this.haveSignificantItemsFunction = options.data.haveSignificantItems;

    this.accessibility = new flexibee.Accessibility(this.editForm, options.evidence.prefix);
};

ItemsHelper.prototype.getId = function() {
    return this.id;
};

ItemsHelper.prototype.haveSignificantItems = function() {
    if (this.haveSignificantItemsFunction) {
        return this.haveSignificantItemsFunction(this);
    } else {
        return this.getAllItems().length > 0;
    }
};

ItemsHelper.prototype.getAllItems = function() {
    var _this = this;

    if (_this.currentEditedObjIndex < 0) {
        var edited = _this.converter.convertForm2Json(_this.editForm);
        if (!_this.isEditedObjectEmpty(edited)) {
            return _this.items.concat(edited);
        }
    }

    return _this.items;
};

// --- friend FormHelper ---

ItemsHelper.prototype.start = function(formHelper) {
    var _this = this;

    _this.accessibility.setFormHelper(formHelper);
    
    _this.invokeAmendForm = function() {
        formHelper.amendForm(null, _this);
    };
    
    _this.clearEditFormAndGoToList();
    _this.bindButtons();
};

ItemsHelper.prototype.getEditForm = function() {
    return this.editForm;
};

ItemsHelper.prototype.getItemsJson = function(field, previousValues) {
    var _this = this;

    console.log("items for amend could be ", _this.items); /*<STRIP-LINE>*/

    var resultArray = $.map(_this.items, function(item) {
        return _this.copyAndRepairItem(item);
    });

    var edited = _this.copyAndRepairItem(_this.converter.convertForm2Json(_this.editForm, field, previousValues));
    if (_this.currentEditedObjIndex < 0) {
        if (!_this.isEditedObjectEmpty(edited)) {
            $.each(_this.defaults, function(key, value) {
                if (edited[key] == undefined) {
                    edited[key] = value;
                }
            });

            resultArray.push(edited);
        }
    } else {
        if (_this.items[_this.currentEditedObjIndex].id && _this.items[_this.currentEditedObjIndex].id > 0) {
            edited.id = _this.items[_this.currentEditedObjIndex].id;
        }
        resultArray[_this.currentEditedObjIndex] = edited;
    }

    console.log("items for amend are ", resultArray); /*<STRIP-LINE>*/

    // 2násobné zanoření pole položek v JSONu je nedokumentované a omezeně podporované
    //var result = { removeAll : true };
    //result[_this.evidenceName] = resultArray;
    //return result;
    
    return resultArray;
};

ItemsHelper.prototype.amendForm = function(items) {
    var _this = this;

    if (!$.isArray(items) || items.length == 0) {
        _this.items = [];
        if (_this.afterAmend) {
            _this.afterAmend(false);
            _this.afterAmend = null;
        } else {
            _this.clearEditFormAndGoToList();
        }
        return;
    }

    var obj = null;    
    if (_this.currentEditedObjIndex < 0 && _this.items.length < items.length) {
        // nově vytvářený objekt (který ještě není v _this.items) se vždy posílá na konci, takže by taky měl přijít na konci
        var lastItem = items[items.length - 1];
        if (lastItem.id < 0) { // tahle kontrola je nutná -- žádný nově vytvářený objekt být vůbec nemusí
            obj = items[items.length - 1];
            console.log("new item object for amend found"); /*<STRIP-LINE>*/
        }
    } else {
        obj = items[_this.currentEditedObjIndex]; // počítám s tím, že pořadí se vždy shoduje
        console.log("existing item object for amend found"); /*<STRIP-LINE>*/
    }

    if (obj) {
        console.log("amending items form with ", obj); /*<STRIP-LINE>*/
        _this.converter.convertJson2Form(obj, _this.editForm);
        _this.accessibility.update(obj);
    }

    if (_this.afterAmend) {
        _this.afterAmend(true, obj);
        _this.afterAmend = null;
    }
};

ItemsHelper.prototype.amendFormBad = function() {
    this.afterAmend = null;
    $.hideWait();
};

// --- private static ---

ItemsHelper.ADD_NEW_TITLE = "+";

// --- private ---

ItemsHelper.prototype.isEditedObjectEmpty = function(editedObj) {
    if (this.isItemEmpty) {
        return this.isItemEmpty(editedObj);
    }

    return $.isEmptyObject(editedObj);
};

ItemsHelper.prototype.copyAndRepairItem = function(item) {
    var copy = $.extend({}, item);
    $.each(copy, function(key, value) {
        if (/\$/.test(key)) {
            delete copy[key];
        }
    });
    if (copy.id && copy.id <= 0) {
        delete copy.id;
    }
    return this.converter.repairJson(copy);
};

ItemsHelper.prototype.clearEditFormAndGoToList = function(options) {
    options = options || { doRedraw: true };

    this.changeEditTitle(this.constructor.ADD_NEW_TITLE);
    this.currentEditedObjIndex = -1; // index aktuálně editovaného objektu v poli this.items, -1 pokud tam ještě není

    if (options.doRedraw) {
        this.redraw();
    }
    this.activateList();

    // TODO nemusí být zcela přesné, může se stát, že některé políčko položek má být podle hlavního objektu zakázáno trvale
    this.accessibility.enableAll();
    
    var _this = this;

    $(this.editForm).allInputs().each(function() {
        // vynulování inputů
        if($(this).hasClass("flexibee-flexbox")) {
            $(this).select2("val", ""); // select2 nejak nereaguje na val("")
        } else if ($(this).is(':checkbox')) {
            var thisCheckBox = this;
            $.each(_this.defaults, function (key, value) {
                var propName = $(thisCheckBox).attr('name');
                propName = propName.replace(_this.id, '');
                
                if (propName === key) {
                    $(thisCheckBox).prop('checked', value === "true"); // checkboxům nastavujeme hodnotu z defaults, vymazat vlastně nejdou (true/false)
                }
            });
        } else {
            $(this).val("");
        }
    });
    
    this.clearFilledProps();
    
    this.hideOrShowInputs();

    $(".ffb-input", this.editForm).each(function() {
        $(this).val($(this).attr("placeholder")).addClass("watermark");
    });
    $(".flexibee-computed-value", this.editForm).text("");
    $(".flexibee-error-head", this.editForm).remove();
};

ItemsHelper.prototype.hideOrShowInputs = function (data) {
    
    var _this = this;
    
    $.each(_this.hideOnStart, function(index, propname) {
            
        var filled = false;
            
        if (data && data[propname] && data[propname].length) {
            filled = true;
        }
            
        if (!filled) {
            _this.accessibility.changeState(propname, "hidden");
        } else {
            _this.accessibility.changeState(propname, "enabled");
        }
    });  
};

ItemsHelper.prototype.showInputsHiddenOnStart = function () {
    var _this = this;
    
    $.each(_this.hideOnStart, function(index, propname) {
        _this.accessibility.changeState(propname, "enabled");
    });
};

ItemsHelper.prototype.clearFilledProps = function() {
    for (var member in this.filledProps) delete this.filledProps[member];
};

ItemsHelper.prototype.fillFilledProps = function(item) {
    var _this = this;
    
    this.clearFilledProps();
        
    if (item) {
        $.each(item, function(prop, val) {
            if (prop && val && val !== "") {
                
                if (/@|\$/.test(prop)) { // @ -- firma@ref, firma@showAs; $ -- $readonly, mj$nazev
                    return;
                }
                
                _this.filledProps[prop] = true;
            }
        });
    }
};

ItemsHelper.prototype.redraw = function() {
    var _this = this;

    console.log("redrawing items in ", _this.listTable); /*<STRIP-LINE>*/
    
    $("tbody", _this.listTable).remove();

    if (_this.items.length == 0) {
        $("table", _this.listTable).append('<tbody><tr><td>Žádné</td></tr></tbody>');
        return;
    }

    $.each(_this.items, function(i, item) {
        $("table", _this.listTable).append(''
                + '<tbody class="flexibee-row">\n'
                + _this.renderItem(item, "flexibee-edit-item-" + i, "flexibee-delete-item-" + i)
                + '</tbody>\n'
                );

        $("#flexibee-edit-item-" + i, _this.listTable).clickData(i, function(event) {
            var i = event.data;
            console.log("will edit ", i, item); /*<STRIP-LINE>*/
            _this.currentEditedObjIndex = i;

            _this.converter.convertJson2Form(item, _this.editForm);
            if (_this.postConvertJson2Form) {
                _this.postConvertJson2Form(item, _this.editForm);
            }
            _this.hideOrShowInputs(item);
            
            _this.fillFilledProps(item);
            
            _this.changeEditTitle(_this.itemTitle(item) + " [" + (i+1) + "]");
            _this.activateEdit();
        });

        $("#flexibee-delete-item-" + i, _this.listTable).clickData(i, function(event) {
            var i = event.data;
            if (confirm("Opravdu chcete smazat položku " + _this.itemTitle(_this.items[i]) + "?")) {
                console.log("will delete ", i); /*<STRIP-LINE>*/
                _this.items.splice(i, 1);
                if (_this.postItemDeleted) {
                    _this.postItemDeleted(_this);
                }
                _this.invokeAmendForm(); // kvůli tomu, že se může změnit viditelnost/editovatelnost políček
                $("tbody:eq(" + i + ")", _this.listTable).fadeOut("fast", function() {
                    _this.redraw();
                });
            }
        });
    });

    $("tbody:first", _this.listTable).css("border-top-style", "none");
    $("tbody:last", _this.listTable).css("border-bottom-style", "none");
};

ItemsHelper.prototype.bindButtons = function() {
    var _this = this;

    $(_this.okButton).delayedClick(function() {
        $.showWait();

        _this.afterAmend = function(haveObject, jsonFromServer) {
            // i když je haveObject == true, pořád klidně může být jsonFromServer == null|undefined

            if (haveObject) {
                console.log("ok pressed, creating new obj"); /*<STRIP-LINE>*/
                var newObj = _this.converter.convertForm2Json(_this.editForm);
                $.each(_this.defaults, function(key, value) {
                    if (newObj[key] == undefined) {
                        newObj[key] = value;
                    }
                });
                if (_this.postConvertForm2Json) {
                    _this.postConvertForm2Json(_this.editForm, newObj, jsonFromServer);
                }
    
                if (_this.currentEditedObjIndex < 0) {
                    console.log("adding ", newObj, " to items"); /*<STRIP-LINE>*/
                    _this.items.push(newObj);
                } else {
                    if (_this.items[_this.currentEditedObjIndex].id) {
                        newObj.id = _this.items[_this.currentEditedObjIndex].id;
                    }
                    console.log("changing obj ", _this.currentEditedObjIndex, " in items, will be ", newObj); /*<STRIP-LINE>*/
                    _this.items[_this.currentEditedObjIndex] = newObj;
                }
            }

            _this.clearEditFormAndGoToList();
            $.hideWait();
        };

        // je určitě nutné zkontrolovat, zda jsou data v pořádku, tzn. spustit doplnění formuláře
        // není třeba testovat, zda už doplnění běží, FormHelper.amendForm() se s tím umí vyrovnat
        _this.invokeAmendForm();
    });

    $(_this.cancelButton).delayedClick(function() {
        console.log("cancel pressed"); /*<STRIP-LINE>*/
        _this.postItemDeleted();
        _this.clearEditFormAndGoToList({doRedraw: false});
        _this.invokeAmendForm(); // kvůli tomu, že se může změnit viditelnost/editovatelnost políček
    });
};

// ---

// expose to global object
window.flexibee = window.flexibee || {};
window.flexibee.ItemsHelper = ItemsHelper;

})(jQuery, window);
