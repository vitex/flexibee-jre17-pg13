(function($, window, undefined) {

$.ajaxTransport(function(options, originalOptions, xhr) {
    var xdrMakesSense = options.crossDomain && !$.support.cors && $.browser.msie && window.XDomainRequest;
    var canHandle = options.type == "GET" && options.dataType == "json" && options.async;

    if (xdrMakesSense && canHandle) {
        var xdr;
        return {
            send: function(headers, callback) {
                xdr = new XDomainRequest();
                xdr.open(options.type, options.url);
                xdr.onload = function() {
                    callback(200, "success", {
                        text: xdr.responseText,
                        json: JSON.parse(xdr.responseText)
                    });
                };
                xdr.onerror = function() {
                    callback(404, "error");
                };
                xdr.send();
            },
            abort: function() {
                if (xdr) {
                    xdr.abort();
                }
            }
        };
    }
});

})(jQuery, window);
