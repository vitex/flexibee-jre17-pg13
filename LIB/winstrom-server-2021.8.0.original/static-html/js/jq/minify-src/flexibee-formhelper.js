(function($, window, undefined) {

var FormHelper = function(options) {
    this.evidenceName = options.evidence.name; // název evidence (např. faktura-vydana)
    this.evidenceUrl = options.evidence.url; // URL evidence (např. /c/firma/faktura-vydana)
    this.csrfToken = options.csrfToken;
    this.converter = new flexibee.FormAndJson("", options.evidence.props, options.evidence.filledProps, options.dependencies);
    this.itemsDependencies = options.itemsDependencies || {};
    this.defaults = options.evidence.defaults || {}; // defaultní hodnoty, které se použijí při odesílání JSONu na server (ale do formuláře se necpou)
                                                     // hodnota smí být funkce, pak bude zavolána a this bude FormHelper
    this.stripDependenciesOfFieldsOnFirstAmend = options.evidence.stripDependenciesOfFieldsOnFirstAmend;
    this.onAfterAmend = options.onAfterAmend;
    this.onFieldChangedBeforeAmend = options.onFieldChangedBeforeAmend;
    this.mainForm = $(options.page.form);

    this.accessibility = new flexibee.Accessibility(this.mainForm);
    this.accessibility.setFormHelper(this);

    this.items = options.items || {};

    this.itemsHelperByInputName = {};

    this.previousValues = {};
    this.previousValuesByItemsHelper = {};

    this.errorFields = {}; // jména všech položek (mapa field->boolean), o kterých je známo, že mají chybnou hodnotu

    this.originalByDuplicate = {};
    this.allDuplicatesByOriginal = {};

    this.ajaxManager = $.manageAjax.create("ajaxManager-" + this.evidenceName, {
        // tohle je spíš hack
        // občas si totiž AjaxManager myslí, že stále provádí request, i když jsem abortnul celou frontu,
        // a tenhle request by tak považoval za duplicitní a vůbec by ho neprovedl; tímhle tu kontrolu vypnu
        preventDoubleRequests: false,
        abortOld: true
    });
};

FormHelper.prototype.start = function() {
    var _this = this;
    setTimeout(function() {
        _this.doStart();
    }, 200); // FlexBoxy se inicializují až po 100 ms

    if (flexibee.isMobile) {
        $("form").jqmData("ajax", "false"); // TODO selektor
    }

    window.mainForm = _this;
};

FormHelper.prototype.forceAmend = function() {
    this.amendForm();
};

FormHelper.prototype.doStart = function() {
    var _this = this;

    console.log("FormHelper starting on ", _this.mainForm); /*<STRIP-LINE>*/

    // inicializace ItemsHelperů a mapování formulářových políček na ItemsHelpery
    _this.allForms = _this.mainForm;
    $.each(_this.items, function(key, itemsHelper) {
        _this.allForms = $(_this.allForms).add(itemsHelper.getEditForm());
        itemsHelper.start(_this);

        $(itemsHelper.getEditForm()).allInputs().each(function() {
            _this.itemsHelperByInputName[$(this).flexibeeName()] = itemsHelper;
        });
    });

    // mapování duplikátů na originál a originálu na všechny duplikáty
    $(".flexibee-dup", _this.allForms).each(function() {
        var name = $(this).flexibeeName();
        var origName = name.replace(/-dup\d+$/, "");
        console.log("Item ", name, " is duplicate of ", origName); /*<STRIP-LINE>*/

        _this.originalByDuplicate[name] = origName;

        if (!_this.allDuplicatesByOriginal[origName]) {
            _this.allDuplicatesByOriginal[origName] = new Array();
        }
        _this.allDuplicatesByOriginal[origName].push(name);
    });

    function handleChange(field, oldVal, newVal) {
        console.log("Changed ", field, ", old value is ", oldVal, ", new value is ", newVal, ", change is ", newVal != oldVal); /*<STRIP-LINE>*/
        if (newVal != oldVal) {
            window.wasChanged = true;

            // všechna duplicitní políčka musí mít stejnou hodnotu
            _this.unifyOriginalAndDuplicates(field, newVal);
            field = _this.originalByDuplicate[field] || field;

            var itemsHelper = _this.itemsHelperByInputName[field];

            var previousValues;
            if (itemsHelper) {
                previousValues = _this.previousValuesByItemsHelper[itemsHelper.getId()] || {};
                _this.previousValuesByItemsHelper[itemsHelper.getId()] = previousValues;
            } else {
                previousValues = _this.previousValues;
            }
            previousValues[field] = oldVal;
            console.log("saving previous value for ", field, " = ", oldVal, " in itemsHelper ", itemsHelper); /*<STRIP-LINE>*/

            if (_this.errorFields[field]) {
                delete _this.errorFields[field];
                delete _this.previousValues[field]; // předchozí hodnota byla chybná, nemá smysl ji posílat

                var input = $("#flexibee-inp-" + field);

                if (input.parent(".flexibee-error-box").length) {
                    input.parent(".flexibee-error-box").children(".flexibee-errors").remove();
                    input.unwrap();
                }
            }

                if (itemsHelper && itemsHelper.isEditedObjectEmpty(itemsHelper.copyAndRepairItem(itemsHelper.converter.convertForm2Json(itemsHelper.editForm)))) {
                    return; // jedná se o právě editovanou položku a je prázdná. Nechceme tedy vyvolat dry-run
                }

            if (_this.onFieldChangedBeforeAmend) {
                _this.onFieldChangedBeforeAmend(field, oldVal, newVal);
            }

            _this.amendForm(field, itemsHelper);
        }
    }

    // TODO tohle není úplně ideální, chtělo by to nějaký jednotný způsob detekce změn ve formulářových polích, který
    // TODO by byl schopen v callbacku předat nejen novou hodnotu, ale i starou (a nejlíp i název políčka)
    // TODO 
    // TODO prozatím to tady dělám tak, že u FlexBoxů a Autocompleterů používám vlastní dohackovaný event, který takhle
    // TODO funguje, u checkboxů mi stačí reagovat na událost click, protože původní hodnota je vždy negací nové hodnoty,
    // TODO a u ostatních normálních inputů si nabinduju focus, v jeho handleru si zapamatuju starou hodnotu a nabinduju blur

    $(_this.allForms).allFlexBoxes().bind("change", function(e) {
		var name = $(this).flexibeeName();
		var oldVal = e.removed === null ? null : e.removed.id; // select2 vrací removed == null, pokud nebyla vybrána žádná položka (např. firma u vydané faktury)
		var newVal = e.added === undefined ? null : e.added.id; // select2 při vymazání hodnoty vraci e.added === undefined
        handleChange(name, oldVal, newVal);
    });

    $(_this.allForms).allAutocompleters().not(".flexibee-suggest-psc").bind("autocompleter-change", function(ev, name, oldVal, newVal) {
        handleChange(name, oldVal, newVal);
    });

    $(_this.allForms).allAutocompleters().filter(".flexibee-suggest-psc").bind("suggest-psc", function(ev, name, oldVal, newVal) {
        handleChange(name, oldVal, newVal);
    });

    $(":checkbox", _this.allForms).click(function() {
        var propName = $(this).flexibeeName();
        var newVal = $(this).is(":checked");
        // WTF? v tuhle chvíli platí (přinejmenším ve Firefoxu a Chromiu)
        // this.is(:checked) == true && this.val() == false
        $(this).val(newVal);
        handleChange(propName, !newVal, newVal);
    });

    $(_this.allForms).allInputs().not(":checkbox").focus(function() {
        var propName = $(this).flexibeeName();
        var oldVal = $(this).val();
        console.log("Focused ", $(this), " value is ", oldVal); /*<STRIP-LINE>*/

        // pokud zrovna běží doplňování formuláře, původní hodnota (oldVal), kterou jsem si zapamatoval,
        // se může lišit od té, která bude doplněna
        // proto při úspěšném doplnění zapamatovanou hodnotu změním na tu doplněnou, aby se zase ihned nespouštělo
        // doplňování, pokud se doplněná hodnota skutečně od té zapamatované liší (navíc by to druhé doplnění
        // nemuselo být noop, pokud existují mezi políčky nějaké závislosti)
        //
        // úplně stačí, když tento handler bude jen jeden a změní zapamatovanou hodnotu jen posledního políčka,
        // které dostalo focus, protože všechna případná dřívější políčka už nové doplnění nespustí 
        _this.onAmendComplete = function(wsData) {
            var data = wsData.winstrom;
            if (data.results && data.results.length > 0 && data.results[0].content) {
                var obj = data.results[0].content[_this.evidenceName];
                if (obj && obj[propName]) {
                    var amendedVal = _this.converter.valueJson2Form(propName, obj[propName], obj);
                    console.log("amend complete, changing remembered ", oldVal, " to ", amendedVal); /*<STRIP-LINE>*/
                    oldVal = amendedVal;
                }
            }
        };

        function handleBlur() {
            var newVal = $(this).val();
            handleChange(propName, oldVal, newVal);
        }

        if ($(this).isDatepicker()) {
            // datepicker má jen jeden event handler pro každý typ události, ne seznam, do kterého by se pořád přidávalo
            $(this).datepickerOnClose(handleBlur);
        }

        $(this).one("blur", handleBlur);
    });

    $(".flexibee-submit-and-new-button").show();

    var submitButton; // tlačítko, které vyvolalo submit
    $(":submit").focus(function() {
        submitButton = this;
    });

    // kvůli položkám nestačí normální odeslání formuláře, takže hezky ručně AJAX
    $("form").submit(function(ev) { // TODO selektor (co když je formulářů ve stránce víc?)
        _this.onAmendFullyComplete = function() {
            var submitAndNew = $(submitButton).attr("name") === "submit-and-new";

            window.wasChanged = false; // při opravdovém uložení nemá smysl se ptát (a to se může stát)
            _this.previousValues = {}; // při opravdovém uložení žádné postvalidy!
            _this.previousValuesByItemsHelper = {};
            _this.errorFields = {};
            var wsData = _this.prepareJsonForServer();
            $.jsonAjax({
                manager: _this.ajaxManager,
                type: "PUT",
                url: _this.evidenceUrl + "?no-http-errors=true&token=" + _this.csrfToken,
                data: wsData,
                success: function(wsData) {
                    $.hideWait();

                    if (JSON.parse(wsData.winstrom.success)) {
                        window.wasChanged = false;
                        var id = wsData.winstrom.results[0].id;
                        window.location = _this.evidenceUrl + (submitAndNew ? ";new" : "/" + id);
                    } else {
                        _this.doAmendForm(wsData, null, true); // zobrazí chybové hlášky
                        _this.onAmendFullyComplete = null;
                        $('html, body').animate({ scrollTop: 0 }, 'slow');
                    }
                },
                failure: function(wsData) {
                    $.hideWait();
                    window.alert("Došlo k fatální chybě");
                    console.log("fatal error ", wsData.winstrom.message); /*<STRIP-LINE>*/
                }
            });
        };
        if (!_this.amendRunning) {
            _this.onAmendFullyComplete();
        }
        return false;
    });

    _this.amendRightAfterStart = true; // aby se při počátečním doplnění formuláře nezobrazily chybové hlášky
    _this.amendForm();
};

// --- friend ItemsHelper ---

// parametry field i itemsHelper jsou nepovinné
FormHelper.prototype.amendForm = function(field, itemsHelper) {
    var _this = this;

    console.log("Amending form"); /*<STRIP-LINE>*/

    _this.amendRunning = true;

    var wsData = _this.prepareJsonForServer(field);
    $.jsonAjax({
        manager: _this.ajaxManager,
        type: "PUT",
        url: _this.evidenceUrl + "?dry-run=true&use-internal-id=true&no-http-errors=true&as-gui=true&token=" + _this.csrfToken,
        data: wsData,
        success: function(wsData) {
            if (_this.onAmendComplete) {
                _this.onAmendComplete(wsData);
                _this.onAmendComplete = null;
            }
            
            if (itemsHelper) {
                itemsHelper.showInputsHiddenOnStart(); // v odpovědi od serveru bude jen info o schování, ne zobrazení
            }
            
            _this.previousValues = {};
            _this.previousValuesByItemsHelper = {};
            _this.doAmendForm(wsData, itemsHelper);
        },
        failure: function(wsData) {
            console.log("fatal error ", wsData.winstrom.message); /*<STRIP-LINE>*/
        },
        complete: function() {
            // stačilo by jen poprvé, když je amendRightAfterStart true, ale přenastavit to pokaždé ničemu nevadí
            _this.amendRightAfterStart = false;

            if (_this.onAmendFullyComplete) {
                _this.onAmendFullyComplete();
                _this.onAmendFullyComplete = null;
            }

            _this.amendRunning = false;
        }
    });
};

// --- friend Accessibility ---

// vraci seznam vsech fieldu pro danou property - jak duplikaty, tak i originaly
FormHelper.prototype.allFieldsForProperty = function(field) {
    var _this = this;

    var result = [];

    field = _this.originalByDuplicate[field] || field;

    var isFlexBox = false;
    if ($("#flexibee-inp-" + field + "-flexbox").length) {
        result.push($("#flexibee-inp-" + field + "-flexbox"));
        isFlexBox = true;
    } else if ($("#flexibee-inp-" + field).length) {
        result.push($("#flexibee-inp-" + field));
    }

    if (_this.allDuplicatesByOriginal[field]) {
        $.each(_this.allDuplicatesByOriginal[field], function(i, dupName) {
            var field = isFlexBox ? $("#flexibee-inp-" + dupName + "-flexbox") : $("#flexibee-inp-" + dupName);
            if (field.length) {
                result.push(field);
            }
        });
    }

    return result;
};

// --- private ---

FormHelper.prototype.unifyOriginalAndDuplicates = function(field, newVal) {
    var _this = this;

    $(_this.allFieldsForProperty(field)).each(function(idx, data) {
        if ($(data).hasClass("flexibee-flexbox")) {
            var id = $(data).attr("id");
            $("#" + id + "_hidden").val(newVal);
            var flexLabel = $("#flexibee-inp-" + field + "-flexbox_input").val();
            $("#" + id + "_input").val(flexLabel);
        } else {
            $(data).val(newVal);
        }
    });
};

// parametr field je nepovinný
FormHelper.prototype.prepareJsonForServer = function(field) {
    var _this = this;

    var defaults = $.extend({}, _this.defaults);
    $.each(defaults, function(key, value) {
        if ($.isFunction(value)) {
            defaults[key] = value.call(_this);
        }
    });

    var data = $.extend({}, defaults, _this.converter.convertForm2Json(_this.mainForm, field, _this.previousValues, _this.errorFields));

    if (_this.amendRightAfterStart && _this.stripDependenciesOfFieldsOnFirstAmend && !data.id) {
        console.log("stripping dependencies of ", _this.stripDependenciesOfFieldsOnFirstAmend, " on first amend of new object"); /*<STRIP-LINE>*/
        var dependencies = _this.converter.dependencies;
        $.each(_this.stripDependenciesOfFieldsOnFirstAmend, function(index, field) {
            if (dependencies[field]) {
                console.log("will strip out ", dependencies[field], " because of dependencies on ", field); /*<STRIP-LINE>*/
                $.each(dependencies[field], function(index, value) {
                    delete data[value];
                });
            }
        });
    }

    $.each(_this.items, function(key, itemsHelper) {
        var itemsJson = itemsHelper.getItemsJson(field, _this.previousValuesByItemsHelper[itemsHelper.getId()]);
        data[key] = itemsJson;
        data[key + '@removeAll'] = "true";

        var haveItems = itemsJson && itemsJson.length > 0 && itemsHelper.haveSignificantItems();
        if (haveItems && _this.itemsDependencies[key]) {
            console.log("items ", key, " dependencies forcing to delete ", _this.itemsDependencies[key]); /*<STRIP-LINE>*/
            $.each(_this.itemsDependencies[key], function(index, depKey) {
                delete data[depKey];
            });
        }
    });

    var wsData = { "winstrom" : { } };
    wsData.winstrom[_this.evidenceName] = data;
    return wsData;
};

// parametry itemsHelper a wasSubmit jsou nepovinné
FormHelper.prototype.doAmendForm = function(wsData, itemsHelper, wasSubmit) {
    var _this = this;

    var data = wsData.winstrom;

    var errorBoxParent = _this.mainForm;
    if (itemsHelper) {
        var itemsEditForm = itemsHelper.getEditForm();
        if (itemsEditForm.children(".flexibe-table-border")) {
            errorBoxParent = itemsEditForm.children(".flexibee-table-border").first();
        } else {
            errorBoxParent = itemsEditForm;
        }
    }

    $(".flexibee-error-head", errorBoxParent).remove();
    $(".flexibee-error-box .flexibee-inp").each(function() {
        var input = $(this);
        input.parent(".flexibee-error-box").children(".flexibee-errors").remove();
        input.unwrap();
    });
    $(".flexibee-ro-input-keep").each(function() {
        var input = $(this);
        input.removeClass("flexibee-ro-input-keep").blur();
    });

    var success = JSON.parse(data.success); // success je sice logická hodnota, ale WinStrom Server ji posílá jako string

    if (!success) {
        var errorBoxHtml = '<div class="flexibee-error-head"><ul class="flexibee-errors"></ul></div>';
        if ($(errorBoxParent).is("table")) {
            var cols = $(errorBoxParent).find("col").length;
            if (!cols) {
                cols = 1;
            }
            $(errorBoxParent).prepend('<tr><td colspan="' + cols +'">' + errorBoxHtml + '</td></tr>');
        } else {
            $(errorBoxParent).prepend(errorBoxHtml);
        }

        if (data.results.length > 0 && data.results[0].content) {
            _this.accessibility.update(data.results[0].content[_this.evidenceName]);
        }

        var couldAddErrorToField = true;

        $.each(data.results[0].errors, function(index, value) {
            var message = $.trim(value.message.replace(/\[[^\]]*\]$/, "")); // IE neumí String.trim()

            $(".flexibee-errors", errorBoxParent).append('<li></li>').findLast("li").text(message);

            if (value["for"]) {
                var field = value["for"];
                _this.errorFields[field] = true;

                var input = $("#flexibee-inp-" + field);
                if (!input.length) {
                    input = $("#flexibee-inp-" + field + "-flexbox");
                }

                if (!input.length) {
                    couldAddErrorToField = false;
                    return;
                }
                
                if (!input.parent(".flexibee-error-box").length) {
                    if (input.hasClass("flexibee-ro-input")) {
                        input.addClass("flexibee-ro-input-keep");
                        input.css("display", "block");
                    }
                    var refocus = input.is(":focus");
                    input.wrap("<div class=\"flexibee-error-box\">");
                    input.parent(".flexibee-error-box").append("<ul class=\"flexibee-errors\"></ul>");
                    if (refocus) {
                        input.focus();
                    }
                }
                input.parent(".flexibee-error-box").children(".flexibee-errors").append("<li></li>").findLast("li").text(message);
                input.parent(".flexibee-error-box").addClass('has-error');
            } else {
                couldAddErrorToField = false;
            }
        });

        if (!wasSubmit && !itemsHelper && couldAddErrorToField) {
            $(".flexibee-error-head", errorBoxParent).remove();
        }

        $.each(_this.items, function(key, itemsHelper) {
            itemsHelper.amendFormBad();
        });
    }

    if (success && data.results.length > 0 && data.results[0].content) {
        var obj = data.results[0].content[_this.evidenceName];
        _this.converter.convertJson2Form(obj, _this.mainForm, _this.errorFields);
        _this.accessibility.update(obj);

        $.each(_this.items, function(key, itemsHelper) {
            itemsHelper.amendForm(obj[key]);
        });

        if (_this.onAfterAmend) {
            _this.onAfterAmend(obj);
        }
    }

};

// ---

// expose to global object
window.flexibee = window.flexibee || {};
window.flexibee.FormHelper = FormHelper;

})(jQuery, window);
