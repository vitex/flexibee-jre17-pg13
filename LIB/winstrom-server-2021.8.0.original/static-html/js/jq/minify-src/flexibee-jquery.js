// read http://docs.jquery.com/Plugins/Authoring first!

// zde už lze používat API poskytované dvojicí jquery-web/jquery-mobile

(function($) {

$.extend($.expr[':'], {
    focus: function(e) { // http://stackoverflow.com/questions/2683742/is-there-a-has-focus-in-javascript-or-jquery/3478393
        try {
            return e == document.activeElement;
        } catch(e) {
            return false;
        }
    },

    accessibilityHidden: function(e) {
        return $(e).data("accessibilityHidden");
    },

    accessibilityVisible: function(e) {
        return !$(e).data("accessibilityHidden");
    }
});

$.extend({
    // pokud options obsahují vlastnost manager, bere se, že jde o instanci AjaxManageru a ta se použije pro
    // odeslání requestu (a ještě před tím se vyčistí a abortne celá fronta -- to je jako kdyby AjaxManager byl vytvořen
    // s volbou "queue: clear" + se všechny requesty abortnou)
    // pokud ne, použije se normální jQuery.ajax
    jsonAjax: function(options) {
        var manager = options.manager;
        delete options.manager;

        var realOptions = {
            contentType: "application/json",
            // TODO AjaxManager zatím nepodporuje pořádně jQuery 1.5, zakomentování dataType to řeší a snad nic nerozbíjí
            //dataType: "json",
            headers: {
                // jQuery tam strčí i */*, což si WinStrom Server vyloží jako nutnost poslat text/html, a to nechci
                // není to úplně nutné, JSON přijatý jako text/html bude jQuery i tak parsovat jako JSON, ale stejně
                "Accept": "application/json"
            }
        };
        $.extend(realOptions, options);

        realOptions.url += /\?/.test(realOptions.url) ? "&" : "?"; // prohlížeč na Androidu cachuje i PUT requesty!
        realOptions.url += "ts=" + new Date().getTime(); // proto každému AJAXovému requestu přidám do query timestamp

        if (typeof(realOptions.data) != "string") {
            realOptions.data = JSON.stringify(realOptions.data);
        }

        if (realOptions.failure) {
            realOptions.error = function(xhr, status) {
                if (status != "abort") {
                    var data = xhr.responseText ? JSON.parse(xhr.responseText) : null;
                    realOptions.failure(data, xhr, status);
                }
            }
        }
        
        if (manager) {
            console.log("clearing and aborting ajax queue ", manager.name); /*<STRIP-LINE>*/
            manager.clear(true);
            manager.add(realOptions);
        } else {
            $.ajax(realOptions);
        }
    },

    /**
     * This function shows wait dialog after 200ms of working. This timeout omits blinking of wait icon.
     */
    showWait: function() {
        if (window.flexibeeShowWaitTimeout) {
            clearTimeout(window.flexibeeShowWaitTimeout);
        }
        window.flexibeeShowWaitTimeout = setTimeout(function() {
            window.flexibeeShowWaitTimeout = null;
            $("body").before('<div class="flexibee-dialog-wait"><div><i class="fa fa-refresh fa-spin fa-5x"></i></div></div>');            
        }, 200);
    },

    hideWait: function() {
        if (window.flexibeeShowWaitTimeout) {
            clearTimeout(window.flexibeeShowWaitTimeout);
            window.flexibeeShowWaitTimeout = null;
        }

        $(".flexibee-dialog-wait").remove();
    },

    formatNumber: function(number) {
        // if (number == Math.round(number)) { return number.toFixed(0); }
        return number.toFixed(2).replace(".", ",");
    }
});

$.extend($.fn, {
    flexibeeName: function() {
        if (this.hasClass("flexibee-flexbox")) {
            return this.attr("id").replace(/^flexibee-inp-/, "").replace(/-flexbox$/, "");
        }
        return this.attr("id").replace(/^flexibee-inp-/, "").replace(/-flexbox_hidden$/, "");
    },

    textNumber: function(number) {
        this.text($.formatNumber(number));
    },

    // jako find(selector).last()
    findLast: function(selector) {
        return this.find(selector).last();
    },

    allInputs: function() {
        // pozn.: nelze odstranit všechny inputy, které vyhovují selektoru :hidden, to odstraní i ty, které jen nejsou vidět!
        return $(":input", this).not("input[type='hidden'], :button, :submit, :reset, :image, .ffb-input").add("input[id$='flexbox_hidden']", this);
    },

    allFlexBoxes: function() {
        return $(".flexibee-flexbox", this);
    },

    allAutocompleters: function() {
        return $(".flexibee-ac_input", this);
    },

    delayedClick: function(handler) {
        return this.each(function() {
            $(this).onClick(function() {
                setTimeout(handler, 0);
            });
        });
    },

    fillFlexBoxData: function(url, data) {
        return this.each(function() {
            var $this = $(this);
            var id = '#' + $this.attr('id');
            $.getJSON(url + '.json?mode=suggest&q=' + data, function(data) {
                if (data && data.results && data.results[0]) {
                    console.log("Filling FlexBox ", id, " with ", data.results[0]); /*<STRIP-LINE>*/
                    $(id + "_input").val(data.results[0].name).change();
                    $(id + "_hidden").val(data.results[0].id);
                }
            });
        });
    },

    fieldValue: function(name) {
        var field = $("#flexibee-inp-" + name, this);
        if (!field.length) {
            field = $("#flexibee-inp-" + name + "-flexbox", this);
        }

        if (field.hasClass("flexibee-flexbox")) {
            return $("#flexibee-inp-" + name + "-flexbox_hidden").val();
        } else if (field.is(":checkbox")) {
            return field.is(":checked");
        } else {
            return $(field).val();
        }
    },

    // totéž jako fieldValue, liší se jen pro FlexBoxy
    fieldTitle: function(name) {
        var field = $("#flexibee-inp-" + name, this);
        if (!field.length) {
            // je to FlexBox
            return $("#flexibee-inp-" + name + "-flexbox_input").val();
        }

        return this.fieldValue(name);
    },

    isDatepicker: function() {
        return this.hasClass("hasDatepicker");
    },

    datepickerOnClose: function(handler) {
        // změna měsíce způsobí ztrátu a opětovné získání focusu na inputu, což vede k novému volání této funkce
        //
        // kdyby se zde v tu chvíli ale volalo $(this).datepicker("option", "onClose", handler), způsobilo by to
        // zavření a znovuotevření okýnka (tak je volání datepicker("option") prostě implementované), následovalo by
        // znovunačtení datumu z inputu a ve výsledku by nebylo možné změnit měsíc
        //
        // proto tento hack: handler narvu natvrdo přímo do datové struktury datepickeru
        return this.each(function() {
            var $this = $(this);
            $this.data("datepicker").settings.onClose = handler;
        });
    },

    inlineEdit: function(options) {
        // define some options with sensible default values
        // - hoverClass: the css classname for the hover style
        options = $.extend({
            hoverClass: 'hover'
        }, options);

        return this.each(function() {
            // define self container
            var $this = $(this);

            var tagName = $this[0].tagName.toLowerCase();

            if (tagName != "input" && tagName != "select" && tagName != "textarea") {
                return;
            }

            if (flexibee.isMobile && tagName == "select") {
                // s jQueryMobile by select zůstal zobrazený a vybraná hodnota by tak byla ve stránce dvakrát 
                return;
            }

            var changeValue = function() {
                var t;
                if ($this.children("option").length) {
                    t = $this.children("option:selected").text();
                } else {
                    t = $this.val();
                }

                if (t == "") {
                    t = "&nbsp;";
                }
                label.html(t);
            };

            var id = $(this).attr("id");
            var labelId = id + "-ro-label";

            var $other = $();
            if (/-flexbox_input$/.test(id)) {
                $other = $("#" + id.replace(/-flexbox_input$/, "-flexbox_arrow"));
            }

            $this.addClass("flexibee-ro-input");

            $this.parent().append("<div id=\"" + labelId + "\"></div>");

            var label = $("#" + labelId);

            label.attr("class", "flexibee-level-3 flexibee-ro-label");

            changeValue();
            $this.css("display", "none");
            $other.css("display", "none");

            $this.change(function() {
                changeValue();
            });

            $this.bind("blur", function(event) {
                if ($this.hasClass("flexibee-ro-input-keep")) {
                    return;
                }
                changeValue();
                $this.css("display", "none");
                $other.css("display", "none");
                label.css("display", "block");
            });

            // bind the click event to the current element, in this example it's span.editable
            label.onClick(function() {
                $this.css("display", "block");
                $other.css("display", "block");
                label.css("display", "none");
                $this.onClick();
                $this.focus();
            });
        });
    },

    flexibeeRefocusInput: function() {
        return this.each(function() {
            var $this = $(this);
            if ($this.is(":focus")) {
                // před skrytím inputu, který má focus, dám focus nejbližšímu následujícímu viditelnému inputu
                // (nebudu se moc snažit, pokud v tabulce už žádný není, tak holt smůla)
                var myTd = $this.closest("td"); // nemusí to být přímý předek (např. u roztahovacích textareí)
                var inputs = myTd.nextAll("td").find(":input:not(:hidden)");
                if (!inputs.length) {
                    inputs = myTd.parent("tr").next("tr:not(:hidden)").children("td:not(:hidden)").find(":input:not(:hidden)");
                }
                if (inputs.length) {
                    inputs.first().focus();
                }
            }
        });
    },
    
    accessibilityShow: function() {
        return this.each(function() {
            var $this = $(this);
            $this.show();
            $this.data("accessibilityHidden", false);
        });
    },

    accessibilityHide: function() {
        return this.each(function() {
            var $this = $(this);
            $this.hide();
            $this.data("accessibilityHidden", true);
        });
    },

    flexibeeEnableInput: function() {
        return this.each(function() {
            var $this = $(this);
            if (!$this.length) return;

            if ($this.hasClass("flexibee-flexbox")) {
                console.log("enabling flexbox ", this); /*<STRIP-LINE>*/
                var id = $this.attr("id");
                $("#" + id + "_input", this).removeAttr("disabled");
                $("#" + id + "_arrow", this).show();
            } else {
                $this.removeAttr("disabled");
            }
        });
    },

    flexibeeDisableInput: function() {
        return this.each(function() {
            var $this = $(this);
            if (!$this.length) return;

            $this.flexibeeRefocusInput();

            if ($this.hasClass("flexibee-flexbox")) {
                console.log("disabling flexbox ", this); /*<STRIP-LINE>*/
                var id = $this.attr("id");
                $("#" + id + "_input", this).attr("disabled", "disabled");
                $("#" + id + "_arrow", this).hide();
            } else {
                $this.attr("disabled", "disabled");
            }
        });
    },

    flexibeeHideInput: function() {
        return this.each(function() {
            var $this = $(this);
            if (!$this.length) return;

            $this.flexibeeRefocusInput();

            var myTd = $this.closest("td"); // nemusí to být přímý předek (např. u roztahovacích textareí)
            
            if(myTd[0].className === "neverHide") {
                return;
            }
            
            var name = $this.flexibeeName();

            // skryju svůj label
            if (myTd.prev("td").children("label[for='flexibee-inp-" + name + "']").length) {
                myTd.prev("td").accessibilityHide();
            }

            // skryju sebe
            myTd.accessibilityHide();

            // skryju nadřazené TR, pokud jsem byl poslední viditelný
            if (!myTd.parent("tr").children("td:accessibilityVisible").length) {
                myTd.parent("tr").accessibilityHide();
            } else {
                return;
            }

            // skryju nadřazenou záložku, pokud jsem byl v jejím obsahu poslední viditelný
            var tabPanel = myTd.closest(".ui-tabs-panel");
            if (!tabPanel.find("tr:accessibilityVisible").length) {
                var tabsNav = tabPanel.prevAll("ul.ui-tabs-nav");
                var tabToHide = tabsNav.find("a[href='#" + tabPanel.attr("id") + "']").parent("li");
                tabToHide.accessibilityHide();

                if (tabToHide.is(".ui-tabs-selected")) {
                    // pokud jsem skryl právě vybranou záložku (což nemusí být vždy pravda!), vyberu jinou (nejbližší
                    // předchozí viditelnou, případně nejbližší následující viditelnou)
                    if (tabToHide.prevAll("li:accessibilityVisible").length) {
                        tabToHide.prevAll("li:accessibilityVisible").first().children("a").onClick();
                    } else if (tabToHide.nextAll("li:accessibilityVisible").length) {
                        tabToHide.nextAll("li:accessibilityVisible").first().children("a").onClick();
                    }
                }
            } else {
                return;
            }

            // skryju celý div se záložkami, pokud jsem byl v poslední viditelné
            if (!tabsNav.children("li:accessibilityVisible").length) {
                tabsNav.parent(".ui-tabs").accessibilityHide();
            }
        });
    },

    flexibeeShowInput: function() {
        return this.each(function() {
            var $this = $(this);
            if (!$this.length) return;

            var myTd = $this.closest("td");
            var name = $this.flexibeeName();

            // zobrazím svůj label
            if (myTd.prev("td").children("label[for='flexibee-inp-" + name + "']").length) {
                myTd.prev("td").accessibilityShow();
            }

            // zobrazím sebe
            myTd.accessibilityShow();

            // zobrazím nadřazené TR
            myTd.parent("tr").accessibilityShow();

            // zobrazím nadřazenou záložku
            var tabPanel = myTd.closest(".ui-tabs-panel");
            var tabsNav = tabPanel.prevAll("ul.ui-tabs-nav");
            tabsNav.find("a[href='#" + tabPanel.attr("id") + "']").parent("li").show();

            // zobrazím celý div se záložkami
            tabsNav.parent(".ui-tabs").show();

            // pokud jsem zobrazil první záložku, vyberu ji
            if (tabsNav.find("li:accessibilityVisible").length == 1) {
                tabsNav.find("li:accessibilityVisible").children("a").onClick();
            }
        });
    },

    capitalize: function() {
        return this.keypress(function(e) {
            if (e.target.createTextRange) {
                var r = document.selection.createRange().duplicate();
                r.moveEnd('character', e.target.value.length);
                if (r.text == '') mstart = e.target.value.length;
                else mstart = e.target.value.lastIndexOf(r.text);
                r.moveStart('character', -e.target.value.length);
                mend = r.text.length;
            } else {
                mstart = e.target.selectionStart;
                mend = e.target.selectionEnd;
            }

            if (e.which > 96 && e.which < 123 || e.which > 220) {
                e.preventDefault();
                e.stopPropagation();
                z = $(e.target).val();
                front = z.substring(0, mstart);
                back = z.substring(mend);
                $(e.target).val(front + String.fromCharCode(e.which).toUpperCase() + back);

                if (e.target.createTextRange) {
                    var range = e.target.createTextRange();
                    range.move("character", mend + 1);
                    range.select();
                } else if (e.target.selectionStart) {
                    e.target.focus();
                    e.target.setSelectionRange(mend + 1, mend + 1);
                }
            }
        });
    },

    suggestPsc: function(pscUrl, statId, onSelect) {
        return this.each(function() {
            var $input = $(this);
            $input.addClass("flexibee-suggest-psc");

            $input.autocomplete(pscUrl + '.json?mode=suggest', {
                width: 280,
                extraParams: {
                    "stat": function() {
                        var stat = $(statId + "-flexbox_hidden").val();
                        return stat;
                    }
                },
                dataType: "json",
                selectFirst: false,
                copyData: false,
                minChars: 1,
                parse: function(data) {
                    return $.map(data.results, function(row) {
                        return {
                            data: row,
                            value: row.name,
                            result: row.id
                        }
                    });
                },
                formatItem: function(item) {
                    var nazev = item.name;
                    if (nazev.length > 50) {
                        nazev = nazev.substring(0, 50) + "...";
                    }
                    return nazev;
                }
            }).result(function(e, item) {
                $.getJSON(pscUrl + '/' + item.id + '.json', function(data) {
                    var oldVal = $input.val();
                    onSelect(data.winstrom.psc[0]);
                    $input.triggerHandler("suggest-psc", [$input.attr("name"), oldVal, $input.val()]);
                });
            });
        });
    },

    valWithAlert: function(newValue) {
        return this.each(function() {
            var $this = $(this);
            var origValue = $this.val();
            $this.val(newValue);

            if ($.mobile) {
                // TODO na mobilu zvýrazňování neumíme (elemToHighlight.animate nefunguje a zničí provádění JavaScriptu)
                return;
            }

            if (newValue != origValue) {
                var elemToHighlight = $this;

                var id = $this.attr("id");
                if (/flexbox_hidden$/.test(id)) {
                    elemToHighlight = $(id.replace(/flexbox_hidden$/, "flexbox_input"));
                }

                var origColor = elemToHighlight.css('background-color');
                elemToHighlight.animate({'background-color': '#ffffaa'}, 1000).delay(500).animate({'background-color': origColor}, 1500);
            }
        });
    }
});

})(jQuery);
