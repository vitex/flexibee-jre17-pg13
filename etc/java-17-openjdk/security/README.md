Upravený soubor "java.security" kvůli potřebě již nebezpečných sha1/rsa-sha1/.. algoritmů

"java.security.original" je z verze 17~19-1 (viz. níže)

```
openjdk-17-jre-headless:
  Installed: 17~19-1
  Candidate: 17~19-1
  Version table:
 *** 17~19-1 500
        500 http://ftp.cz.debian.org/debian bullseye/main amd64 Packages
        100 /var/lib/dpkg/status
```

"java.security.diff" je patch (pro informaci o rozdílu)

"java.security" je pak upravená verze, se kterou je provozováno/testováno
